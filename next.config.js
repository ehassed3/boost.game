module.exports = {
  i18n: {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
  },
  async redirects() {
    return [
      {
        source: '/de/',
        destination: '/',
        permanent: true,
      },
    ];
  },
  trailingSlash: true,
};
