import React from 'react';

import SERVER from '@constants/server';

import UserStatus from '@interfaces/userStatus';

interface UserStatusProps {
  userStatus?: UserStatus;
}

const UserStatusStateContext = React.createContext<UserStatusProps | null>(
  null
);

export const UserStatusProvider: React.FC = ({ children }) => {
  const [userStatus, setUserStatus] = React.useState<UserStatus>();

  React.useEffect(() => {
    fetch(`${SERVER}/api/update_user/`);

    fetch(`${SERVER}/api/get_user_status/`)
      .then((res) => res.json())
      .then((result) => {
        setUserStatus(result);
      });
  }, []);

  return (
    <UserStatusStateContext.Provider value={{ userStatus }}>
      {children}
    </UserStatusStateContext.Provider>
  );
};

export const useUserStatus = (): UserStatusProps => {
  const userStatus = React.useContext(UserStatusStateContext);

  if (!userStatus) {
    throw new Error(
      '[Context UserStatus] Could not find required `userStatus` object. <UserStatusProvider> needs to exist in the component ancestry.'
    );
  }

  return userStatus;
};
