import React from 'react';
import { useRouter } from 'next/router';

import { Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import GAME from '@constants/game';
import POSITION from '@constants/position';

interface Support {
  open: boolean;
  message: string;
  game?: GAME;
  position?: POSITION;
}

interface SupportProps {
  support: Support;
  setSupport: (support: Support) => void;
}

const SupportStateContext = React.createContext<SupportProps | null>(null);

export const SupportProvider: React.FC = ({ children }) => {
  const isMobile = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down('xs')
  );
  const router = useRouter();

  const [support, setSupport] = React.useState<Support>({
    open: router.asPath.endsWith('#chat'),
    message: '',
  });

  React.useEffect(() => {
    const hasChat = router.asPath.endsWith('#chat');

    if (!hasChat && !isMobile) {
      return;
    }

    if (support.open && !hasChat) {
      router.push(`${router.asPath}#chat`);
      return;
    }

    if (!support.open && hasChat) {
      router.push(router.asPath.split('#')[0]);
    }
  }, [support.open]);

  React.useEffect(() => {
    const hasChat = router.asPath.endsWith('#chat');

    if (support.open && !hasChat) {
      setSupport({ ...support, open: false });
      return;
    }
    if (!support.open && hasChat) {
      setSupport({ ...support, open: true });
    }
  }, [router.asPath]);

  return (
    <SupportStateContext.Provider value={{ support, setSupport }}>
      {children}
    </SupportStateContext.Provider>
  );
};

export const useSupport = (): SupportProps => {
  const support = React.useContext(SupportStateContext);

  if (!support) {
    throw new Error(
      '[Context Support] Could not find required `support` object. <SupportProvider> needs to exist in the component ancestry.'
    );
  }

  return support;
};
