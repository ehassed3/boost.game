import React from 'react';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';

interface Game {
  [GAME.POE]: {
    [POSITION.POWER_LEVELING]: {
      rating?: Rating;
    };
  };
  [GAME.WOW]: {
    [CATEGORY.CHARACTER_BOOST]: {
      [POSITION.POWER_LEVELING]: {
        rating?: Rating;
      };
      [POSITION.COVENANT]: {
        rating?: Rating;
      };
    };
    [CATEGORY.PVP]: {
      [POSITION.ARENA]: {
        rating?: Rating;
      };
      [POSITION.HONOR]: {
        rating?: Rating;
      };
      [POSITION.CONQUEST_CAP]: {
        rating?: Rating;
      };
    };
    [CATEGORY.DUNGEONS_AND_RAIDS]: {
      [POSITION.RAIDS]: {
        rating?: Rating;
      };
      [POSITION.MYTHIC_DUNGEONS]: {
        rating?: Rating;
      };
      [POSITION.MYTHIC_8_DUNGEONS]: {
        rating?: Rating;
      };
    };
    [CATEGORY.TORGHAST_AND_SOUL_ASH]: {
      [POSITION.TORGHAST_TOWER_LAYER]: {
        rating?: Rating;
      };
      [POSITION.SOUL_ASH]: {
        rating?: Rating;
      };
    };
    [CATEGORY.ACHIEVEMENTS]: {
      [POSITION.HERO]: {
        rating?: Rating;
      };
    };
  };
  [GAME.WOW_CLASSIC]: {
    [CATEGORY.POWER_LEVELING]: {
      [POSITION.POWER_LEVELING]: {
        rating?: Rating;
      };
    };
    [CATEGORY.PVP]: {
      [POSITION.HONOR]: {
        rating?: Rating;
      };
      [POSITION.RANK]: {
        rating?: Rating;
      };
    };
  };
  [GAME.DOTA_2]: {
    [POSITION.BOOST]: {
      rating?: Rating;
    };
    [POSITION.CALIBRATION]: {
      rating?: Rating;
    };
    [POSITION.LOW_PRIORITY]: {
      rating?: Rating;
    };
  };
}

interface GameProps {
  game: Game;
  setGame: (game: Game) => void;
}

const GameStateContext = React.createContext<GameProps | null>(null);

export const GameProvider: React.FC = ({ children }) => {
  const [game, setGame] = React.useState({
    [GAME.POE]: {
      [POSITION.POWER_LEVELING]: {},
    },
    [GAME.WOW]: {
      [CATEGORY.CHARACTER_BOOST]: {
        [POSITION.POWER_LEVELING]: {},
        [POSITION.COVENANT]: {},
      },
      [CATEGORY.PVP]: {
        [POSITION.ARENA]: {},
        [POSITION.HONOR]: {},
        [POSITION.CONQUEST_CAP]: {},
      },
      [CATEGORY.DUNGEONS_AND_RAIDS]: {
        [POSITION.RAIDS]: {},
        [POSITION.MYTHIC_DUNGEONS]: {},
        [POSITION.MYTHIC_8_DUNGEONS]: {},
      },
      [CATEGORY.TORGHAST_AND_SOUL_ASH]: {
        [POSITION.TORGHAST_TOWER_LAYER]: {},
        [POSITION.SOUL_ASH]: {},
      },
      [CATEGORY.ACHIEVEMENTS]: {
        [POSITION.HERO]: {},
      },
    },
    [GAME.WOW_CLASSIC]: {
      [CATEGORY.POWER_LEVELING]: {
        [POSITION.POWER_LEVELING]: {},
      },
      [CATEGORY.PVP]: {
        [POSITION.HONOR]: {},
        [POSITION.RANK]: {},
      },
    },
    [GAME.DOTA_2]: {
      [POSITION.BOOST]: {},
      [POSITION.CALIBRATION]: {},
      [POSITION.LOW_PRIORITY]: {},
    },
  });

  return (
    <GameStateContext.Provider value={{ game, setGame }}>
      {children}
    </GameStateContext.Provider>
  );
};

export const useGame = (): GameProps => {
  const game = React.useContext(GameStateContext);

  if (!game) {
    throw new Error(
      '[Context Game] Could not find required `game` object. <GameProvider> needs to exist in the component ancestry.'
    );
  }

  return game;
};
