import React from 'react';

import getCurrencyByLocale from '@helpers/getCurrencyByLocale';

import Currency from '@interfaces/currency';

interface CurrencyProps {
  currency: Currency;
  setCurrency: (currency: Currency) => void;
}

const CurrencyStateContext = React.createContext<CurrencyProps | null>(null);

export const CurrencyProvider: React.FC<{ locale?: string }> = ({
  children,
  locale,
}) => {
  const [currency, setCurrency] = React.useState(getCurrencyByLocale(locale));

  return (
    <CurrencyStateContext.Provider value={{ currency, setCurrency }}>
      {children}
    </CurrencyStateContext.Provider>
  );
};

export const useCurrency = (): CurrencyProps => {
  const currency = React.useContext(CurrencyStateContext);

  if (!currency) {
    throw new Error(
      '[Context Currency] Could not find required `currency` object. <CurrencyProvider> needs to exist in the component ancestry.'
    );
  }

  return currency;
};
