import * as React from 'react';
import { useSession } from 'next-auth/client';

import UserSession from '@components/Session/User';
import WithoutSession from '@components/Session/Without';

const AccountPage: React.FC = () => {
  const [session, loading] = useSession();

  return session || loading ? (
    <UserSession user={session?.user} />
  ) : (
    <WithoutSession />
  );
};

export default AccountPage;
