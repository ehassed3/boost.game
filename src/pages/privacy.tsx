import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h3,

    fontWeight: 400,
    marginBottom: '60px',
    textAlign: 'center',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h4,

      marginBottom: '40px',
      wordBreak: 'break-word',
    },
  },
  description: {
    marginBottom: '8px',
  },
  subtitle: {
    ...theme.typography.h6,

    marginTop: '32px',
    marginBottom: '16px',
  },
  date: {
    marginTop: '36px',
  },
}));

const PrivacyPage: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <>
      <MuiContainer>
        <MuiTypography className={classes.title} component="h1">
          {intl.formatMessage({ id: 'privacy.title' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.description1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.description2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph1|3' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph2|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph2|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph2|3' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph3|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph3|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph4|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph4|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph4|3' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph5|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph5|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph5|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph5|3description1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph5|3description2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'privacy.paragraph6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph6|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'privacy.paragraph6|2' })}
        </MuiTypography>
        <div className={classes.date}>
          <MuiTypography className={classes.description} component="h2">
            {intl.formatMessage({ id: 'privacy.version' })}
          </MuiTypography>
          <MuiTypography className={classes.description} component="h2">
            {intl.formatMessage({ id: 'privacy.date' })}
          </MuiTypography>
        </div>
      </MuiContainer>
    </>
  );
};

export default PrivacyPage;
