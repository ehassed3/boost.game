import * as React from 'react';
import { AppProps } from 'next/app';
import { IntlProvider } from 'react-intl';
import { Provider as SessionProvider } from 'next-auth/client';
import { SWRConfig } from 'swr';
import { motion, AnimatePresence } from 'framer-motion';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { CurrencyProvider } from '@context/currency';
import { SupportProvider } from '@context/support';
import { UserStatusProvider } from '@context/userStatus';
import { GameProvider } from '@context/game';

import Layout from '@components/Layout';
import Footer from '@components/Footer';

import THEME from '@constants/theme';

import isGamePage from '@helpers/isGamePage';
import getMessages from '@helpers/getMessages';

const BoostGameApp: React.FC<AppProps> = ({ Component, pageProps, router }) => {
  const [shouldComponentUpdate, setShouldComponentUpdate] = React.useState(
    true
  );
  const [isVisibleComponent, setIsVisibleComponent] = React.useState(true);
  const [timedOut, setTimedOut] = React.useState(true);

  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');

    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }

    window.history.scrollRestoration = 'manual';

    router.events.on('routeChangeStart', (path, { shallow }) => {
      if (
        shallow ||
        !isVisibleComponent ||
        window.history.state.options.locale !== router.locale
      ) {
        return;
      }

      if (
        isGamePage(router.pathname, router.locale) &&
        isGamePage(path, router.locale)
      ) {
        setShouldComponentUpdate(false);
        return;
      }

      setIsVisibleComponent(false);
      setTimedOut(false);
    });

    router.events.on('routeChangeComplete', () => {
      setTimedOut(true);
    });
  }, []);

  const ComponentRef = React.useRef(<Component {...pageProps} />);

  React.useEffect(() => {
    if (!shouldComponentUpdate) {
      setShouldComponentUpdate(true);
      return;
    }

    ComponentRef.current = <Component {...pageProps} />;
  }, [Component]);

  return (
    <SWRConfig
      value={{
        fetcher: (input, init) => fetch(input, init).then((res) => res.json()),
      }}
    >
      <IntlProvider
        locale={router.locale || 'en'}
        defaultLocale={router.defaultLocale || 'en'}
        messages={getMessages(router.locale)}
      >
        <ThemeProvider theme={THEME}>
          <CurrencyProvider locale={router.locale}>
            <SupportProvider>
              <SessionProvider session={pageProps.session}>
                <UserStatusProvider>
                  <GameProvider>
                    <CssBaseline />
                    <Layout router={router}>
                      <AnimatePresence
                        initial={false}
                        onExitComplete={() => {
                          setIsVisibleComponent(true);
                          window.scrollTo({ top: 0 });
                        }}
                      >
                        {isVisibleComponent && timedOut && (
                          <motion.main
                            initial={{ opacity: 0 }}
                            animate={{
                              opacity: 1,
                              transition: { duration: 0.5 },
                            }}
                            exit={{ opacity: 0, transition: { duration: 0.4 } }}
                          >
                            {ComponentRef.current}
                            <Footer />
                          </motion.main>
                        )}
                      </AnimatePresence>
                    </Layout>
                  </GameProvider>
                </UserStatusProvider>
              </SessionProvider>
            </SupportProvider>
          </CurrencyProvider>
        </ThemeProvider>
      </IntlProvider>
    </SWRConfig>
  );
};

export default BoostGameApp;
