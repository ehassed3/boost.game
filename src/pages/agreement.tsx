import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h3,

    fontWeight: 400,
    marginBottom: '60px',
    textAlign: 'center',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h4,

      marginBottom: '40px',
      wordBreak: 'break-word',
    },
  },
  description: {
    marginBottom: '8px',
  },
  info: {
    marginTop: '16px',
  },
  subtitle: {
    ...theme.typography.h6,

    marginTop: '32px',
    marginBottom: '16px',
  },
  date: {
    marginTop: '36px',
  },
}));

const AgreementPage: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <>
      <MuiContainer>
        <MuiTypography className={classes.title} component="h1">
          {intl.formatMessage({ id: 'agreement.title' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.description1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.description2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.description3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.description4' })}
        </MuiTypography>
        <div className={classes.info}>
          <MuiTypography className={classes.description} component="p">
            {intl.formatMessage({ id: 'agreement.name' })}
          </MuiTypography>
          <MuiTypography className={classes.description} component="p">
            {intl.formatMessage({ id: 'agreement.tin' })}
          </MuiTypography>
          <MuiTypography className={classes.description} component="p">
            {intl.formatMessage({ id: 'agreement.psrn' })}
          </MuiTypography>
          <MuiTypography className={classes.description} component="p">
            {intl.formatMessage({ id: 'agreement.email' })}
          </MuiTypography>
        </div>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|1|7' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description1',
          })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description2',
          })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description3',
          })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description4',
          })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description5',
          })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph2description6',
          })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph3|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph3|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|7' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph4|2|8' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|1|6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph5|3' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|5' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|6' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph6|7' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph7' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|1|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|1|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph7|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph8' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({
            id: 'agreement.paragraph8description',
          })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph9' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph9|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph9|1|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph9|1|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph9|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph10' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph10|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph10|2' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph11' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph11|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph11|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph11|3' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph11|4' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph11.5' })}
        </MuiTypography>
        <MuiTypography className={classes.subtitle} component="h2">
          {intl.formatMessage({ id: 'agreement.paragraph12' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph12|1' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph12|2' })}
        </MuiTypography>
        <MuiTypography className={classes.description} component="p">
          {intl.formatMessage({ id: 'agreement.paragraph12|3' })}
        </MuiTypography>
        <div className={classes.date}>
          <MuiTypography className={classes.description} component="h2">
            {intl.formatMessage({ id: 'agreement.version' })}
          </MuiTypography>
          <MuiTypography className={classes.description} component="h2">
            {intl.formatMessage({ id: 'agreement.date' })}
          </MuiTypography>
        </div>
      </MuiContainer>
    </>
  );
};

export default AgreementPage;
