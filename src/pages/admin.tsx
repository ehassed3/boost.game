import * as React from 'react';
import { GetServerSideProps } from 'next';
import ErrorPage from 'next/error';
import { getSession } from 'next-auth/client';

import AdminSession from '@components/Session/Admin';

import SERVER from '@constants/server';

import { PublicUser } from '@interfaces/user';

interface Props {
  admin: PublicUser | null;
}

const AdminPage: React.FC<Props> = ({ admin }) =>
  admin ? <AdminSession /> : <ErrorPage statusCode={404} />;

export const getServerSideProps: GetServerSideProps<Props> = async ({
  req,
}) => {
  const session = await getSession({ req });
  const resAdmin = await fetch(
    `${SERVER}/api/get_admin/?email=${session?.user.email}`
  );
  const admin = resAdmin.ok ? await resAdmin.json() : null;

  return { props: { admin } };
};

export default AdminPage;
