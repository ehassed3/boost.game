import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';

const Dota2Page: React.FC = () => <Game initial={GAME.DOTA_2} />;

export default Dota2Page;
