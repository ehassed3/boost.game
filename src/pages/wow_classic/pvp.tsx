import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const WowClassicPvpPage: React.FC = () => (
  <Game initial={GAME.WOW_CLASSIC} initialCategory={CATEGORY.PVP} />
);

export default WowClassicPvpPage;
