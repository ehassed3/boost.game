import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';

const WowClassicPage: React.FC = () => <Game initial={GAME.WOW_CLASSIC} />;

export default WowClassicPage;
