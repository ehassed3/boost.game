import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const WowClassicPowerLevelingPage: React.FC = () => (
  <Game initial={GAME.WOW_CLASSIC} initialCategory={CATEGORY.POWER_LEVELING} />
);

export default WowClassicPowerLevelingPage;
