import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';

const PoePage: React.FC = () => <Game initial={GAME.POE} />;

export default PoePage;
