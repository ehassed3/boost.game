import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';

const WowPage: React.FC = () => <Game initial={GAME.WOW} />;

export default WowPage;
