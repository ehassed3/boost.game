import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const WowTorghastAndSoulAshPage: React.FC = () => (
  <Game initial={GAME.WOW} initialCategory={CATEGORY.TORGHAST_AND_SOUL_ASH} />
);

export default WowTorghastAndSoulAshPage;
