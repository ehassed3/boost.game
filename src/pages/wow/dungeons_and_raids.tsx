import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const WowDungeonsAndRaidsPage: React.FC = () => (
  <Game initial={GAME.WOW} initialCategory={CATEGORY.DUNGEONS_AND_RAIDS} />
);

export default WowDungeonsAndRaidsPage;
