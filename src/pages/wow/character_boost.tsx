import * as React from 'react';

import Game from '@components/Game';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const WowCharacterBoostPage: React.FC = () => (
  <Game initial={GAME.WOW} initialCategory={CATEGORY.CHARACTER_BOOST} />
);

export default WowCharacterBoostPage;
