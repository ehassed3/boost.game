import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';

import { PERMISSION_ID } from '@constants/discord';

import { Client, Manager } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);
handler.use(validate([check('shortId').isString()]));

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const { shortId } = req.body;

  const session = await getSession({ req });

  if (!session) {
    return res.status(400).send('No access');
  }

  const [manager]: Manager[] = await req.db
    .collection('managers')
    .find({ email: session.user.email })
    .toArray();

  if (!manager) {
    return res.status(400).send('No access');
  }

  await req.db
    .collection('orders')
    .updateOne(
      { shortId, manager: { $exists: false } },
      { $set: { manager: manager.email } }
    );

  const [order]: Order[] = await req.db
    .collection('orders')
    .find({ shortId })
    .toArray();

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: order.user })
    .toArray();

  await fetch(`https://discordapp.com/api/channels/${order.channelId}`, {
    method: 'PATCH',
    headers: {
      Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      permission_overwrites: [
        {
          id: user.id,
          type: 1,
          allow: PERMISSION_ID.CLIENT,
        },
        {
          id: manager.id,
          type: 1,
          allow: PERMISSION_ID.MANAGER,
        },
      ],
    }),
  });

  const intl = createServerIntl(order.locale || 'en');

  await fetch(
    `https://discordapp.com/api/channels/${order.channelId}/messages`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        content: intl.formatMessage(
          { id: 'updatedOrder.processing' },
          { manager: `<@${manager.id}>` }
        ),
      }),
    }
  );

  return res.status(200).send('Success');
});

export default handler;
