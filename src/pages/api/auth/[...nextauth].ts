import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';

import { GUILD_ID } from '@constants/discord';
import SERVER from '@constants/server';

const Auth: typeof NextAuth = (req, res) =>
  NextAuth(req, res, {
    callbacks: {
      signIn: async (user, account, profile) => {
        if (!profile.avatar) {
          user.image = '';
        }

        await fetch(
          `https://discordapp.com/api/guilds/${GUILD_ID}/members/${account.id}`,
          {
            method: 'PUT',
            headers: {
              Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ access_token: account.accessToken }),
          }
        );

        return Promise.resolve(true);
      },
      jwt: async (token, _, __, profile) => {
        if (profile) {
          token.discriminator = profile.discriminator;
          token.id = profile.id;
        }

        return Promise.resolve(token);
      },
      session: async (session, user) =>
        Promise.resolve({
          ...session,
          user: {
            ...session.user,
            discriminator: user.discriminator,
            id: user.id,
          },
        }),
      redirect: async (url) => Promise.resolve(url),
    },
    pages: {
      error: SERVER,
    },
    providers: [
      {
        ...Providers.Discord({
          clientId: process.env.DISCORD_CLIENT_ID || '',
          clientSecret: process.env.DISCORD_CLIENT_SECRET || '',
        }),
        ...{ scope: 'identify email guilds.join' },
      },
    ],
    secret: process.env.SECRET,
  });

export default Auth;
