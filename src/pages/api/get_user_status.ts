import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

import getUserStatus from '@helpers/getUserStatus';

import { Client } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(200).json(getUserStatus(0));
  }

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: session.user.email })
    .toArray();

  if (!user) {
    return res.status(200).json(getUserStatus(0));
  }

  return res.status(200).json(getUserStatus(user.amount || 0));
});

export default handler;
