import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import GAMES from '@constants/games';
import CATEGORIES from '@constants/categories';
import POSITIONS from '@constants/positions';

import { Order } from '@interfaces/order';
import Rating from '@interfaces/rating';
import { User, PublicBooster } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('game').isIn(GAMES),
    check('category').optional().isIn(CATEGORIES),
    check('position').isIn(POSITIONS),
  ])
);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const { game, category, position } = req.body;

  const orders: Order[] = await req.db
    .collection('orders')
    .find({
      game: { $in: [game] },
      ...(category ? { category: { $in: [category] } } : {}),
      position: { $in: [position] },
      review: { $exists: true, $ne: null },
      user: { $exists: true, $ne: null },
    })
    .sort({ _id: -1 })
    .toArray();

  const boosters: PublicBooster[] = await req.db
    .collection('boosters')
    .find(
      {
        games: { $in: [game] },
        ...(category ? { categories: { $in: [category] } } : {}),
        positions: { $in: [position] },
      },
      {
        projection: { _id: 0, email: 0, games: 0, categories: 0, positions: 0 },
      }
    )
    .toArray();

  const rating: Rating = {
    boosters,
    reviews: await Promise.all(
      orders.map(async (order) => {
        const [user]: User[] = await req.db
          .collection('users')
          .find({
            email: order.user,
          })
          .toArray();

        return {
          image: user?.image || '',
          name: user?.name || 'Anonymous',
          rating: order.review?.rating || 0,
          screenshots: order.screenshots,
          showScreenshots: !!order.review?.showScreenshots,
          text: order.review?.text || '',
          title: order.title,
        };
      })
    ),
    totalReviews: orders.length,
    value:
      +(
        orders.reduce((acc, order) => acc + (order.review?.rating || 0), 0) /
        orders.length
      ).toFixed(1) || 0,
  };

  return res.status(200).json(rating);
});

export default handler;
