import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import { Manager, PublicManager } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('name').optional().isString(),
    check('email').optional().isEmail(),
  ])
);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, email } = req.body;

  const session = await getSession({ req });

  if (!session || !session.user) {
    const [manager]: PublicManager[] = await req.db
      .collection('managers')
      .find(
        {
          ...(name ? { name } : {}),
          ...(email ? { email } : {}),
        },
        { projection: { _id: 0, email: 0, id: 0 } }
      )
      .toArray();
    return manager
      ? res.status(200).json(manager)
      : res.status(404).send('Not found');
  }

  if (!name && !email) {
    const [sessionManager]: Manager[] = await req.db
      .collection('managers')
      .find({ email: session.user.email })
      .toArray();
    return sessionManager
      ? res.status(200).json(sessionManager)
      : res.status(404).send('Not found');
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();

  const [manager]: PublicManager[] = name
    ? await req.db
        .collection('managers')
        .find(
          {
            ...(name ? { name } : {}),
            ...(email ? { email } : {}),
          },
          {
            ...(!admin
              ? {
                  projection: {
                    _id: 0,
                    email: 0,
                    id: 0,
                  },
                }
              : {}),
          }
        )
        .toArray()
    : [];
  return manager
    ? res.status(200).json(manager)
    : res.status(404).send('Not found');
});

export default handler;
