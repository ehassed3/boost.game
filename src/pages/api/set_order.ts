import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { ObjectID } from 'mongodb';

import database from '@middlewares/database';

import createServerIntl from '@helpers/createServerIntl';
import getShortMongoId from '@helpers/getShortMongoId';
import { getMarketEmbed, getOrderEmbed } from '@helpers/getEmbed';
import getUserRole from '@helpers/getUserRole';
import getUSDAmount from '@helpers/getUSDAmount';

import STATUS from '@constants/status';
import {
  GUILD_ID,
  CHANNEL_ID,
  PERMISSION_ID,
  ROLE_ID,
} from '@constants/discord';

import { InsertedRawOrder } from '@interfaces/order';
import { User, Client, Manager } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);

// const IPS = [
//   '31.186.100.49',
//   '178.132.203.105',
//   '52.29.152.23',
//   '52.19.56.234',
// ];

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  // if (
  //   !req.headers['x-forwarded-for'] ||
  //   IPS.indexOf(String(req.headers['x-forwarded-for'])) === -1
  // ) {
  //   return res.status(400).json({ error: { message: 'No access' } });
  // }

  const method = String(req.query.method);
  const _id = String(req.query['params[account]']);

  if (!ObjectID.isValid(_id)) {
    return res.status(400).json({ error: { message: 'Order not found' } });
  }

  const [order]: InsertedRawOrder[] = await req.db
    .collection('orders')
    .find({ _id: new ObjectID(_id) })
    .toArray();

  if (!order) {
    return res.status(400).json({ error: { message: 'Order not found' } });
  }

  if (method !== 'pay') {
    return res
      .status(200)
      .json({ result: { message: 'Request processed successfully' } });
  }

  if (order.promocode) {
    await req.db
      .collection('promocodes')
      .deleteOne({ name: order.promocode.name, unlimited: false });
  }

  const shortId = getShortMongoId(_id);
  const status = STATUS.PAUSED;

  const resChannel = await fetch(
    `https://discordapp.com/api/guilds/${GUILD_ID}/channels`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: shortId,
        type: 0,
      }),
    }
  );
  const channel = resChannel.ok ? await resChannel.json() : null;
  const channelId = channel?.id || '';

  await req.db
    .collection('orders')
    .updateOne(
      { _id: new ObjectID(_id) },
      { $set: { channelId, shortId, status } },
      (error) => {
        if (error) {
          res.status(400).json({ error: { message: error } });
        }
      }
    );

  let [user]: User[] & Client[] = await req.db
    .collection('users')
    .find({ email: order.user })
    .toArray();

  if (!user) {
    [user] = await req.db
      .collection('users')
      .find({ email: 'ehassed3@gmail.com' })
      .toArray();
  }

  const amount = (user.amount || 0) + getUSDAmount(order.result.price);

  await req.db
    .collection('users')
    .updateOne({ id: user.id }, { $set: { amount } }, (error) => {
      if (error) {
        res.status(400).json({ error: { message: error } });
      }
    });

  const [admin]: User[] = await req.db
    .collection('admins')
    .find({ email: order.user })
    .toArray();
  const [manager]: Manager[] = await req.db
    .collection('managers')
    .find({ email: order.user })
    .toArray();

  await fetch(
    `https://discordapp.com/api/guilds/${GUILD_ID}/members/${user.id}`,
    {
      method: 'PATCH',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        roles: [getUserRole(amount, admin, manager)],
      }),
    }
  );

  await fetch(
    `https://discordapp.com/api/channels/${CHANNEL_ID.ORDERS}/messages`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        embed: getOrderEmbed({ ...order, channelId, shortId, status }),
      }),
    }
  );

  await fetch(
    `https://discordapp.com/api/channels/${CHANNEL_ID.MARKET}/messages`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        content: `<@&${ROLE_ID.MANAGER}>`,
        embed: getMarketEmbed({ ...order, channelId, shortId, status }, user),
      }),
    }
  );

  await fetch(`https://discordapp.com/api/channels/${channelId}`, {
    method: 'PATCH',
    headers: {
      Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      permission_overwrites: [
        {
          id: user.id,
          type: 1,
          allow: PERMISSION_ID.CLIENT,
        },
      ],
    }),
  });

  const intl = createServerIntl(order.locale || 'en');

  await fetch(`https://discordapp.com/api/channels/${channelId}/messages`, {
    method: 'POST',
    headers: {
      Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      content: intl.formatMessage(
        { id: 'confirmedOrder.title' },
        { user: `<@${user.id}>`, manager: `<@&${ROLE_ID.MANAGER}>` }
      ),
    }),
  });

  return res
    .status(200)
    .json({ result: { message: 'Request processed successfully' } });
});

export default handler;
