import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('name').isString().isLength({ min: 1, max: 20 }),
    check('percent').isInt({ min: 0, max: 30 }),
    check('unlimited').isBoolean(),
  ])
);

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, percent, unlimited, locale = 'en' } = req.body;

  const intl = createServerIntl(locale);

  const session = await getSession({ req });

  if (!session || !session.user) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'serverError.access' }));
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();
  const [manager] = await req.db
    .collection('managers')
    .find({ email: session.user.email })
    .toArray();
  if (!admin && !manager) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'serverError.access' }));
  }

  const result = await req.db
    .collection('promocodes')
    .updateOne(
      { name },
      { $setOnInsert: { name, percent, unlimited } },
      { upsert: true }
    );

  if (!result.upsertedCount) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'createPromocode.errorExists' }));
  }

  return res
    .status(200)
    .send(intl.formatMessage({ id: 'createPromocode.success' }, { name }));
});

export default handler;
