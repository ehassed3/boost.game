import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import { PublicUser } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('name').optional().isString(),
    check('email').optional().isEmail(),
  ])
);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, email } = req.body;

  const [admin]: PublicUser[] = await req.db
    .collection('admins')
    .find(
      {
        ...(name ? { name } : {}),
        ...(email ? { email } : {}),
      },
      { projection: { _id: 0, email: 0 } }
    )
    .toArray();

  if (!admin) {
    return res.status(404).send('Not found');
  }

  return res.status(200).json(admin);
});

export default handler;
