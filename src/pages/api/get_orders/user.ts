import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

import getPublicOrder from '@helpers/getOrderData/getPublicOrder';

import { Client, Manager, Booster } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(404).send('No access');
  }

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: session.user.email })
    .toArray();

  if (!user) {
    return res.status(404).send('No access');
  }

  const orders: Order[] = await req.db
    .collection('orders')
    .find({
      user: session.user.email,
      shortId: { $exists: true, $ne: null },
    })
    .sort({ _id: -1 })
    .toArray();

  const boosters: Booster[] = await req.db
    .collection('boosters')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.booster || acc.indexOf(order.booster) !== -1) {
            return acc;
          }

          return [...acc, order.booster];
        }, []),
      },
    })
    .toArray();
  const managers: Manager[] = await req.db
    .collection('managers')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.manager || acc.indexOf(order.manager) !== -1) {
            return acc;
          }

          return [...acc, order.manager];
        }, []),
      },
    })
    .toArray();

  const publicOrders = orders.map((order) =>
    getPublicOrder(order, boosters, managers, user)
  );

  return res.status(200).json(publicOrders);
});

export default handler;
