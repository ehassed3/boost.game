import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import getPublicOrder from '@helpers/getOrderData/getPublicOrder';

import { Client, Manager, Booster } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);
handler.use(validate([check('name').isString()]));

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name } = req.body;

  const [manager]: Manager[] = await req.db
    .collection('managers')
    .find({ name })
    .toArray();

  const orders: Order[] = await req.db
    .collection('orders')
    .find({
      manager: manager?.email,
      shortId: { $exists: true, $ne: null },
    })
    .sort({ _id: -1 })
    .toArray();

  const boosters: Booster[] = await req.db
    .collection('boosters')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.booster || acc.indexOf(order.booster) !== -1) {
            return acc;
          }

          return [...acc, order.booster];
        }, []),
      },
    })
    .toArray();
  const users: Client[] = await req.db
    .collection('users')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.user || acc.indexOf(order.user) !== -1) {
            return acc;
          }

          return [...acc, order.user];
        }, []),
      },
    })
    .toArray();

  const publicOrders = orders.map((order) =>
    getPublicOrder(order, boosters, manager, users)
  );

  return res.status(200).json(publicOrders);
});

export default handler;
