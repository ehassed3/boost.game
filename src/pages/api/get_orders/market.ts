import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

import getPublicOrder from '@helpers/getOrderData/getPublicOrder';

import { Client, Booster } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(404).send('No access');
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();
  const [manager] = await req.db
    .collection('managers')
    .find({ email: session.user.email })
    .toArray();

  if (!admin && !manager) {
    return res.status(404).send('No access');
  }

  const orders: Order[] = await req.db
    .collection('orders')
    .find({
      manager: { $exists: false },
      shortId: { $exists: true, $ne: null },
    })
    .sort({ _id: -1 })
    .toArray();

  const boosters: Booster[] = await req.db
    .collection('boosters')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.booster || acc.indexOf(order.booster) !== -1) {
            return acc;
          }

          return [...acc, order.booster];
        }, []),
      },
    })
    .toArray();
  const users: Client[] = await req.db
    .collection('users')
    .find({
      email: {
        $in: orders.reduce((acc: string[], order) => {
          if (!order.user || acc.indexOf(order.user) !== -1) {
            return acc;
          }

          return [...acc, order.user];
        }, []),
      },
    })
    .toArray();

  const publicOrders = orders.map((order) =>
    getPublicOrder(order, boosters, [], users)
  );

  return res.status(200).json(publicOrders);
});

export default handler;
