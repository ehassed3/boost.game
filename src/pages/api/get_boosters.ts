import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

import { Booster, PublicBooster } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    const boosters: PublicBooster[] = await req.db
      .collection('boosters')
      .find(
        {},
        {
          projection: {
            _id: 0,
            email: 0,
            games: 0,
            categories: 0,
            positions: 0,
          },
        }
      )
      .toArray();
    return res.status(200).json(boosters);
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();
  if (admin) {
    const boosters: Booster[] = await req.db
      .collection('boosters')
      .find()
      .toArray();
    return res.status(200).json(boosters);
  }

  const [manager] = await req.db
    .collection('managers')
    .find({ email: session.user.email })
    .toArray();
  if (manager) {
    const boosters: Booster[] = await req.db
      .collection('boosters')
      .find()
      .toArray();
    return res.status(200).json(boosters);
  }

  const boosters: PublicBooster[] = await req.db
    .collection('boosters')
    .find(
      {},
      {
        projection: { _id: 0, email: 0, games: 0, categories: 0, positions: 0 },
      }
    )
    .toArray();
  return res.status(200).json(boosters);
});

export default handler;
