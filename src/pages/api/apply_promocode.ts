import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';

const handler = nextConnect();

handler.use(database);
handler.use(validate([check('name').isString().isLength({ min: 0, max: 20 })]));

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, locale = 'en' } = req.body;

  const [promocode] = await req.db
    .collection('promocodes')
    .find({ name })
    .toArray();

  if (!promocode) {
    const intl = createServerIntl(locale);
    return res
      .status(404)
      .send(
        intl.formatMessage({ id: 'applyPromocode.errorNotFound' }, { name })
      );
  }

  return res.status(200).json(promocode);
});

export default handler;
