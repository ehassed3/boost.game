import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

import { Manager, PublicManager } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    const managers: PublicManager[] = await req.db
      .collection('managers')
      .find({}, { projection: { _id: 0, email: 0, id: 0 } })
      .toArray();
    return res.status(200).json(managers);
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();
  if (admin) {
    const managers: Manager[] = await req.db
      .collection('managers')
      .find()
      .toArray();
    return res.status(200).json(managers);
  }

  const managers: PublicManager[] = await req.db
    .collection('managers')
    .find({}, { projection: { _id: 0, email: 0, id: 0 } })
    .toArray();
  return res.status(200).json(managers);
});

export default handler;
