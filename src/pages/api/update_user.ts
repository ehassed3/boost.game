import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { ObjectID } from 'mongodb';

import database from '@middlewares/database';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(400).send('No access');
  }

  const [user] = await req.db
    .collection('users')
    .find({ email: session.user.email })
    .toArray();

  if (user) {
    await req.db
      .collection('users')
      .updateOne(
        { _id: new ObjectID(user._id) },
        { $set: { ...session.user } }
      );
  } else {
    await req.db.collection('users').insertOne(session.user);
  }

  return res.status(200).send('Success');
});

export default handler;
