import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';
import getUserStatus from '@helpers/getUserStatus';
import getRawOrder from '@helpers/getOrderData/getRawOrder';
import getPayUrl from '@helpers/getPayUrl';

import GAMES from '@constants/games';
import CATEGORIES from '@constants/categories';
import POSITIONS from '@constants/positions';
import { USD, EUR, RUB } from '@constants/currencies';

import { Client } from '@interfaces/user';
import { DirtyOrder, InsertedRawOrder } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('category').optional().isIn(CATEGORIES),
    check('currency.code').isIn([USD.code, EUR.code, RUB.code]),
    check('currency.multiplier').notEmpty().isNumeric(),
    check('formValue').notEmpty(),
    check('game').isIn(GAMES),
    check('locale').isIn(['en', 'de', 'ru']),
    check('position').isIn(POSITIONS),
    check('promocode.name').optional().notEmpty().isString(),
    check('promocode.percent').optional().isInt({ min: 0, max: 30 }),
    check('promocode.unlimited').optional().isBoolean(),
    check('user').optional().isEmail(),
  ])
);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });
  const dirtyOrder: DirtyOrder = req.body;

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: session?.user.email })
    .toArray();
  const userStatus = getUserStatus(user?.amount || 0);

  let promocodePercent = 0;
  if (dirtyOrder.promocode) {
    const [foundPromocode] = await req.db
      .collection('promocodes')
      .find({ name: dirtyOrder.promocode.name })
      .toArray();
    promocodePercent = foundPromocode?.percent || 0;
  }

  const intl = createServerIntl(dirtyOrder.locale || 'en');

  const rawOrder = getRawOrder(
    intl,
    dirtyOrder,
    session?.user.email || req.body.email || '',
    userStatus.percent,
    promocodePercent
  );

  const result = await req.db.collection('orders').insertOne(rawOrder);

  const insertedRawOrder: InsertedRawOrder = {
    ...rawOrder,
    _id: result.insertedId,
  };

  return res.status(200).redirect(getPayUrl(insertedRawOrder));
});

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  const dirtyOrder: DirtyOrder = req.body;

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: session?.user.email })
    .toArray();
  const userStatus = getUserStatus(user?.amount || 0);

  let promocodePercent = 0;
  if (dirtyOrder.promocode) {
    const [foundPromocode] = await req.db
      .collection('promocodes')
      .find({ name: dirtyOrder.promocode.name })
      .toArray();
    promocodePercent = foundPromocode?.percent || 0;
  }

  const intl = createServerIntl(dirtyOrder.locale || 'en');

  const rawOrder = getRawOrder(
    intl,
    dirtyOrder,
    session?.user.email || req.body.email || '',
    userStatus.percent,
    promocodePercent
  );

  const result = await req.db.collection('orders').insertOne(rawOrder);

  const insertedRawOrder: InsertedRawOrder = {
    ...rawOrder,
    _id: result.insertedId,
  };

  return res.status(200).send(getPayUrl(insertedRawOrder));
});

export default handler;
