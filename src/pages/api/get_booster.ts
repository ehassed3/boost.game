import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import { Booster, PublicBooster } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('name').optional().isString(),
    check('email').optional().isEmail(),
  ])
);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, email } = req.body;

  const session = await getSession({ req });

  if (!session || !session.user) {
    const [booster]: PublicBooster[] = await req.db
      .collection('boosters')
      .find(
        {
          ...(name ? { name } : {}),
          ...(email ? { email } : {}),
        },
        {
          projection: {
            _id: 0,
            email: 0,
            games: 0,
            categories: 0,
            positions: 0,
          },
        }
      )
      .toArray();
    return booster
      ? res.status(200).json(booster)
      : res.status(404).send('Not found');
  }

  if (!name && !email) {
    const [sessionBooster]: Booster[] = await req.db
      .collection('boosters')
      .find({ email: session.user.email })
      .toArray();
    return sessionBooster
      ? res.status(200).json(sessionBooster)
      : res.status(404).send('Not found');
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();
  const [manager] = await req.db
    .collection('managers')
    .find({ email: session.user.email })
    .toArray();

  const [booster]: PublicBooster[] = name
    ? await req.db
        .collection('boosters')
        .find(
          {
            ...(name ? { name } : {}),
            ...(email ? { email } : {}),
          },
          {
            ...(!admin && !manager
              ? {
                  projection: {
                    _id: 0,
                    email: 0,
                    games: 0,
                    categories: 0,
                    positions: 0,
                  },
                }
              : {}),
          }
        )
        .toArray()
    : [];
  return booster
    ? res.status(200).json(booster)
    : res.status(404).send('Not found');
});

export default handler;
