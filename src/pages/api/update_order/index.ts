import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';
import { getStatusUpdatedEmbed } from '@helpers/getEmbed';

import SERVER from '@constants/server';
import STATUS from '@constants/status';

import { Client, Admin, Booster } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('shortId').isString(),
    check('booster').optional().isEmail(),
    check('manager').optional().isEmail(),
    check('screenshots').optional().isArray(),
    check('screenshots.*').optional().isString(),
    check('status')
      .optional()
      .isIn([STATUS.PAUSED, STATUS.LIVE, STATUS.CLOSED]),
  ])
);

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    shortId,
    booster: boosterEmail,
    manager,
    screenshots,
    status,
  } = req.body;

  const session = await getSession({ req });

  if (!session) {
    return res.status(400).send('Error');
  }

  const [admin]: Admin[] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();

  if (!admin) {
    return res.status(400).send('No access');
  }

  await req.db.collection('orders').updateOne(
    { shortId },
    {
      $set: {
        ...(boosterEmail ? { booster: boosterEmail } : {}),
        ...(manager ? { manager } : {}),
        ...(screenshots ? { screenshots } : {}),
        ...(status ? { status } : {}),
      },
    }
  );

  const [order]: Order[] = await req.db
    .collection('orders')
    .find({ shortId, manager: session.user.email })
    .toArray();

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: order.user })
    .toArray();

  const [booster]: Booster[] = await req.db
    .collection('boosters')
    .find({ email: order.booster })
    .toArray();

  const intl = createServerIntl(order.locale || 'en');

  if (status) {
    await fetch(
      `https://discordapp.com/api/channels/${order.channelId}/messages`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          content: `<@${user.id}>, <@${manager.id}>`,
          embed: getStatusUpdatedEmbed(intl, status),
        }),
      }
    );

    if (status === STATUS.LIVE && order.stream && booster?.streamChannel) {
      await fetch(
        `https://discordapp.com/api/channels/${order.channelId}/messages`,
        {
          method: 'POST',
          headers: {
            Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            content: `https://twitch.tv/${booster.streamChannel}`,
          }),
        }
      );
    }
  }

  if (boosterEmail && booster) {
    await fetch(
      `https://discordapp.com/api/channels/${order.channelId}/messages`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          ...(!status ? { content: `<@${user.id}>, <@${manager.id}>` } : {}),
          embed: {
            title: intl.formatMessage(
              { id: 'updatedOrder.boosterTitle' },
              { name: booster.name }
            ),
            description: intl.formatMessage(
              { id: 'updatedOrder.boosterDescription' },
              { name: booster.name }
            ),
            url: `${SERVER}/boosters/${booster.name}`,
            image: booster.image ? { url: booster.image } : {},
          },
        }),
      }
    );
  }

  if (screenshots && order.screenshots) {
    await fetch(
      `https://discordapp.com/api/channels/${order.channelId}/messages`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          ...(!status && !booster
            ? { content: `<@${user.id}>, <@${manager.id}>` }
            : {}),
          embed: {
            title: intl.formatMessage({ id: 'updatedOrder.screenshotsTitle' }),
            description: intl.formatMessage({
              id: 'updatedOrder.screenshotsDescription',
            }),
            url: `${SERVER}/account`,
            image: { url: order.screenshots[order.screenshots.length - 1] },
          },
        }),
      }
    );
  }

  return res.status(200).send('Success');
});

export default handler;
