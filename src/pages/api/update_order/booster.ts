import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';
import { getStatusUpdatedEmbed } from '@helpers/getEmbed';

import STATUS from '@constants/status';

import { Client, Manager, Booster } from '@interfaces/user';
import { Order } from '@interfaces/order';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('shortId').isString(),
    check('status').isIn([STATUS.PAUSED, STATUS.LIVE]),
  ])
);

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const { shortId, status } = req.body;

  const session = await getSession({ req });

  if (!session) {
    return res.status(400).send('No access');
  }

  const [booster]: Booster[] = await req.db
    .collection('boosters')
    .find({ email: session.user.email })
    .toArray();

  if (!booster) {
    return res.status(400).send('No access');
  }

  await req.db
    .collection('orders')
    .updateOne({ shortId, booster: session.user.email }, { $set: { status } });

  const [order]: Order[] = await req.db
    .collection('orders')
    .find({ shortId, booster: session.user.email })
    .toArray();

  const [user]: Client[] = await req.db
    .collection('users')
    .find({ email: order.user })
    .toArray();

  const [manager]: Manager[] = await req.db
    .collection('managers')
    .find({ email: order.manager })
    .toArray();

  const intl = createServerIntl(order.locale || 'en');

  await fetch(
    `https://discordapp.com/api/channels/${order.channelId}/messages`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        content: `<@${user.id}>, <@${manager.id}>`,
        embed: getStatusUpdatedEmbed(intl, status),
      }),
    }
  );

  if (status === STATUS.LIVE && order.stream && booster?.streamChannel) {
    await fetch(
      `https://discordapp.com/api/channels/${order.channelId}/messages`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          content: `https://twitch.tv/${booster.streamChannel}`,
        }),
      }
    );
  }

  return res.status(200).send('Success');
});

export default handler;
