import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';
import { check } from 'express-validator';

import database from '@middlewares/database';
import validate from '@middlewares/validate';

import createServerIntl from '@helpers/createServerIntl';

import { Order } from '@interfaces/order';
import { Booster, Manager } from '@interfaces/user';

const handler = nextConnect();

handler.use(database);
handler.use(
  validate([
    check('shortId').isString(),
    check('rating').isInt({ min: 1, max: 5 }),
    check('showScreenshots').isBoolean(),
    check('text').isString().isLength({ min: 1, max: 280 }),
  ])
);

handler.post(async (req: NextApiRequest, res: NextApiResponse) => {
  const { shortId, rating, showScreenshots, text, locale = 'en' } = req.body;

  const intl = createServerIntl(locale);

  const session = await getSession({ req });

  if (!session || !session.user) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'serverError.access' }));
  }

  const [order]: Order[] = await req.db
    .collection('orders')
    .find({
      shortId,
      review: { $exists: false },
    })
    .toArray();

  if (!order) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'addReview.errorExists' }));
  }

  if (order.user !== session.user.email) {
    return res
      .status(400)
      .send(intl.formatMessage({ id: 'serverError.access' }));
  }

  await req.db
    .collection('orders')
    .updateOne(
      { shortId, user: session.user.email },
      { $set: { review: { rating, showScreenshots, text } } }
    );

  const [booster]: Booster[] = await req.db
    .collection('boosters')
    .find({ email: order.booster })
    .toArray();
  if (booster) {
    await req.db.collection('boosters').updateOne(
      { email: order.booster },
      {
        $set: {
          rating:
            (booster.rating * booster.reviews + rating) / (booster.reviews + 1),
          reviews: booster.reviews + 1,
        },
      }
    );
  }

  const [manager]: Manager[] = await req.db
    .collection('managers')
    .find({ email: order.manager })
    .toArray();
  if (manager) {
    await req.db.collection('managers').updateOne(
      { email: order.booster },
      {
        $set: {
          rating:
            (manager.rating * manager.reviews + rating) / (manager.reviews + 1),
          reviews: manager.reviews + 1,
        },
      }
    );
  }

  return res.status(200).send('Success');
});

export default handler;
