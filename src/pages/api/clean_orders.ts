import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import { getSession } from 'next-auth/client';

import database from '@middlewares/database';

const handler = nextConnect();

handler.use(database);

handler.get(async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req });

  if (!session || !session.user) {
    return res.status(400).send('No access');
  }

  const [admin] = await req.db
    .collection('admins')
    .find({ email: session.user.email })
    .toArray();

  if (!admin) {
    return res.status(400).send('No access');
  }

  await req.db.collection('orders').deleteMany({ shortId: { $exists: false } });

  return res.status(200).send('Success');
});

export default handler;
