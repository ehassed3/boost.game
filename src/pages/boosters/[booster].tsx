import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import ErrorPage from 'next/error';

import BoosterSession from '@components/Session/Booster';

import ROLE from '@constants/role';
import SERVER from '@constants/server';

import { Booster, PublicBooster } from '@interfaces/user';

interface Props {
  booster: Booster | PublicBooster | null;
}

const BoosterPage: React.FC<Props> = ({ booster }) =>
  booster ? (
    <BoosterSession booster={booster} role={ROLE.USER} />
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getStaticPaths: GetStaticPaths = async ({ locales = [] }) => {
  const resBoosters = await fetch(`${SERVER}/api/get_boosters/`);
  const boosters: PublicBooster[] = resBoosters.ok
    ? await resBoosters.json()
    : [];

  const paths = locales
    .map((locale) =>
      boosters.map((booster) => ({
        params: { booster: booster.name },
        locale,
      }))
    )
    .flat();

  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const resBooster = await fetch(
    `${SERVER}/api/get_booster/?name=${params?.booster}`
  );
  const booster: Booster | PublicBooster | null = resBooster.ok
    ? await resBooster.json()
    : null;

  return { props: { booster } };
};

export default BoosterPage;
