import * as React from 'react';
import { GetServerSideProps } from 'next';
import ErrorPage from 'next/error';
import { getSession } from 'next-auth/client';

import BoosterSession from '@components/Session/Booster';

import SERVER from '@constants/server';
import ROLE from '@constants/role';

import { Booster } from '@interfaces/user';

interface Props {
  booster: Booster | null;
}

const BoosterPage: React.FC<Props> = ({ booster }) =>
  booster ? (
    <BoosterSession booster={booster} role={ROLE.BOOSTER} />
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getServerSideProps: GetServerSideProps<Props> = async ({
  req,
}) => {
  const session = await getSession({ req });

  const resBooster = await fetch(
    `${SERVER}/api/get_booster/?email=${session?.user.email}`
  );
  const booster: Booster | null = resBooster.ok
    ? await resBooster.json()
    : null;

  return { props: { booster } };
};

export default BoosterPage;
