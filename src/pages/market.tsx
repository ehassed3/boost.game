import * as React from 'react';
import { GetServerSideProps } from 'next';
import ErrorPage from 'next/error';
import { getSession } from 'next-auth/client';

import Market from '@components/Market';

import SERVER from '@constants/server';

import { Manager } from '@interfaces/user';

interface Props {
  manager: Manager | null;
}

const MarketPage: React.FC<Props> = ({ manager }) =>
  manager ? <Market /> : <ErrorPage statusCode={404} />;

export const getServerSideProps: GetServerSideProps<Props> = async ({
  req,
}) => {
  const session = await getSession({ req });
  const resManager = await fetch(
    `${SERVER}/api/get_manager/?email=${session?.user.email}`
  );
  const manager = resManager.ok ? await resManager.json() : null;

  return { props: { manager } };
};

export default MarketPage;
