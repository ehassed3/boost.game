import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import ErrorPage from 'next/error';

import ManagerSession from '@components/Session/Manager';

import ROLE from '@constants/role';
import SERVER from '@constants/server';

import { Manager, PublicManager } from '@interfaces/user';

interface Props {
  manager: Manager | PublicManager | null;
}

const ManagerPage: React.FC<Props> = ({ manager }) =>
  manager ? (
    <ManagerSession manager={manager} role={ROLE.USER} />
  ) : (
    <ErrorPage statusCode={404} />
  );

export const getStaticPaths: GetStaticPaths = async ({ locales = [] }) => {
  const resManagers = await fetch(`${SERVER}/api/get_managers/`);
  const managers: PublicManager[] = resManagers.ok
    ? await resManagers.json()
    : [];

  const paths = locales
    .map((locale) =>
      managers.map((manager) => ({
        params: { manager: manager.name },
        locale,
      }))
    )
    .flat();

  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const resManager = await fetch(
    `${SERVER}/api/get_manager/?name=${params?.manager}`
  );
  const manager: Manager | PublicManager | null = resManager.ok
    ? await resManager.json()
    : null;

  return { props: { manager } };
};

export default ManagerPage;
