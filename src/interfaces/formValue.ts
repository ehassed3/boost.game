export interface PoePowerLevelingFormValue {
  difficulty: {
    label: string;
    name: 'softcore' | 'hardcore';
  };
  levels: number[];
  stream: boolean;
}

export interface WowPowerLevelingFormValue {
  fast: boolean;
  gear: number;
  isGear: boolean;
  levels: number[];
  stream: boolean;
  torghast: boolean;
}

export interface WowCovenantFormValue {
  covenant: {
    label: string;
    name: string;
  };
  isSwitched: boolean;
  levels: number[];
  stream: boolean;
}

export interface WowArenaFormValue {
  player: {
    label: string;
    name: '2vs2' | '3vs3';
  };
  rating: number;
  selfplay: boolean;
  stream?: boolean;
}

export interface WowHonorFormValue {
  honor: number;
  stream: boolean;
}

export interface WowConquestCapFormValue {
  amount: number;
  stream: boolean;
}

export interface WowRaidsFormValue {
  difficulty: {
    label: string;
    name: 'normal' | 'heroic' | 'mythic';
  };
  fraction: {
    label: string;
    name: string;
  };
  raids: number;
  selfplay: boolean;
  stream?: boolean;
  traders: {
    value: string;
    amount: number;
    label: string;
  }[];
}

export interface WowMythicDungeonsFormValue {
  fraction: {
    name: string;
    label: string;
  };
  mythic: {
    level: number;
    amount: number;
  };
  runs: number;
  selfplay: boolean;
  specific: boolean;
  stream?: boolean;
  timer: boolean;
  traders: {
    value: string;
    amount: number;
    label: string;
  }[];
}

export interface WowMythic8DungeonsFormValue {
  fraction: {
    name: string;
    label: string;
  };
  selfplay: boolean;
  stream?: boolean;
  traders: {
    value: string;
    amount: number;
    label: string;
  }[];
}

export interface WowTorghastTowerLayerFormValue {
  layer: number;
  selfplay: boolean;
  stream?: boolean;
  torghast: boolean;
  unlock: boolean;
  wings: {
    label: string;
    multiplier: number;
  };
}

export interface WowSoulAshFormValue {
  selfplay: boolean;
  stream?: boolean;
}

export interface WowHeroFormValue {
  amount: number;
  stream?: boolean;
}

export interface WowClassicPowerLevelingFormValue {
  fast: boolean;
  levels: number[];
  mount: boolean;
  professions: {
    amount?: number;
    name: string;
    title: string;
  }[];
  stream: boolean;
}

export interface WowClassicHonorFormValue {
  fast: boolean;
  honor: number;
  hours: number;
  isSwitched: boolean;
  stream: boolean;
}

export interface WowClassicRankFormValue {
  ranks: number[];
  stream: boolean;
}

export interface Dota2BoostFormValue {
  booster: boolean;
  heroes: boolean;
  ranks: number[];
  stream: boolean;
}

export interface Dota2CalibrationFormValue {
  booster: boolean;
  games: number;
  heroes: boolean;
  rank: number;
  ranks: {
    image: string;
    imageAvif: string;
    imageWebp: string;
    name: string;
    value: number;
  }[];
  stream: boolean;
}

export interface Dota2LowPriorityFormValue {
  booster: boolean;
  games: number;
  heroes: boolean;
  stream: boolean;
}

type FormValue =
  | PoePowerLevelingFormValue
  | WowPowerLevelingFormValue
  | WowCovenantFormValue
  | WowArenaFormValue
  | WowHonorFormValue
  | WowConquestCapFormValue
  | WowRaidsFormValue
  | WowMythicDungeonsFormValue
  | WowMythic8DungeonsFormValue
  | WowTorghastTowerLayerFormValue
  | WowSoulAshFormValue
  | WowHeroFormValue
  | WowClassicPowerLevelingFormValue
  | WowClassicHonorFormValue
  | WowClassicRankFormValue
  | Dota2BoostFormValue
  | Dota2CalibrationFormValue
  | Dota2LowPriorityFormValue;

export default FormValue;
