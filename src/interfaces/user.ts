import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

export interface PublicUser {
  image: string;
  name: string;
}

export interface User extends PublicUser {
  discriminator: string;
  email: string;
  id: string;
}

export interface Client extends User {
  amount: number;
}

export interface Admin extends PublicUser {
  email: string;
}

export interface PublicManager extends PublicUser {
  description: Record<string, string>;
  rating: number;
  reviews: number;
}

export interface Manager extends PublicManager {
  email: string;
  id: string;
}

export interface PublicBooster extends PublicUser {
  description: Record<string, string>;
  streamChannel?: string;
  rating: number;
  reviews: number;
}

export interface Booster extends PublicBooster {
  categories: CATEGORY[];
  email: string;
  games: GAME[];
  positions: POSITION[];
}
