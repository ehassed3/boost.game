import { USER_STATUS } from '@constants/userStatus';

interface UserStatus {
  amount: number;
  percent: number;
  status: USER_STATUS;
}

export default UserStatus;
