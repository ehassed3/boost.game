interface Price {
  amount: number;
  currency: string;
}

export default Price;
