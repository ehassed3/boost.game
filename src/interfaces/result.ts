import Price from '@interfaces/price';

export interface Result {
  amount: number;
  discountPercent?: number;
  hours: number;
}

export interface FormattedResult {
  discountPercent: number;
  fullPrice: Price;
  price: Price;
  time: string;
}
