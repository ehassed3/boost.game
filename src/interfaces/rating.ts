import { PublicBooster } from '@interfaces/user';
import { FullReview } from '@interfaces/review';

interface Rating {
  boosters: PublicBooster[];
  reviews: FullReview[];
  totalReviews: number;
  value: number;
}

export default Rating;
