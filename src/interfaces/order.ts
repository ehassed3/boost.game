import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';
import STATUS from '@constants/status';

import Currency from '@interfaces/currency';
import FormValue from '@interfaces/formValue';
import Promocode from '@interfaces/promocode';
import { Review } from '@interfaces/review';
import { FormattedResult } from '@interfaces/result';
import { PublicBooster, PublicManager, PublicUser } from '@interfaces/user';

export interface DirtyOrder {
  category?: CATEGORY;
  currency: Currency;
  formValue: FormValue;
  game: GAME;
  locale: string;
  position: POSITION;
  promocode?: Promocode | null;
  user?: string;
}

export interface RawOrder {
  category?: CATEGORY;
  details?: string[];
  game: GAME;
  locale: string;
  position: POSITION;
  promocode?: Promocode | null;
  result: FormattedResult;
  stream?: boolean;
  title: string;
  user: string;
}

export interface InsertedRawOrder extends RawOrder {
  _id: string;
}

export interface Order extends InsertedRawOrder {
  booster?: string;
  channelId?: string;
  manager?: string;
  review?: Review;
  screenshots?: string[];
  shortId: string;
  status: STATUS;
}

export interface PublicOrder {
  booster?: PublicBooster;
  category?: CATEGORY;
  channelId?: string;
  details?: string[];
  game: GAME;
  manager?: PublicManager;
  position: POSITION;
  result: FormattedResult;
  review?: Review;
  screenshots?: string[];
  shortId: string;
  status: STATUS;
  stream?: boolean;
  title: string;
  user: PublicUser;
}
