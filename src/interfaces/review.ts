export interface Review {
  rating: number;
  showScreenshots: boolean;
  text: string;
}

export interface FullReview extends Review {
  image: string;
  name: string;
  screenshots?: string[];
  title: string;
}
