interface Currency {
  code: string;
  multiplier: number;
}

export default Currency;
