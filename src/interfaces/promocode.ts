interface Promocode {
  name: string;
  percent: number;
  unlimited: boolean;
}

export default Promocode;
