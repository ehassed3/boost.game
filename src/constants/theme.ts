import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const THEME = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1440,
      xl: 1920,
    },
  },
  palette: {
    type: 'dark',
    background: {
      default: '#000000',
      paper: '#0e0e10',
    },
    primary: { main: '#432ca2' },
    secondary: { main: '#b71a1a' },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        '@global': {
          '*': {
            msOverflowStyle: 'none',
            scrollbarWidth: 'none',

            '&::-webkit-scrollbar': {
              display: 'none',
            },
          },
          main: {
            display: 'flex',
            flexDirection: 'column',
            minHeight: '120vh',
          },
          input: {
            MozAppearance: 'textfield',

            '&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
              WebkitAppearance: 'none',
              margin: 0,
            },
          },
        },
      },
    },
  },
});

export default THEME;
