import GAME from '@constants/game';

const GAMES = [GAME.POE, GAME.WOW, GAME.WOW_CLASSIC, GAME.DOTA_2];

export default GAMES;
