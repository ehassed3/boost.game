export enum GAME {
  CS_GO = 'cs_go',
  DESTINY_2 = 'destiny_2',
  DOTA_2 = 'dota_2',
  HEARTHSTONE = 'hearthstone',
  LOL = 'lol',
  OVERWATCH = 'overwatch',
  POE = 'poe',
  VALORANT = 'valorant',
  WOW = 'wow',
  WOW_CLASSIC = 'wow_classic',
}

export default GAME;
