export enum POSITION {
  ARENA = 'arena',
  ATTUNEMENTS = 'attunements',
  BOOST = 'boost',
  CALIBRATION = 'calibration',
  CONQUEST_CAP = 'conquest_cap',
  COVENANT = 'covenant',
  HERO = 'hero',
  HONOR = 'honor',
  LOW_PRIORITY = 'low_priority',
  MYTHIC_DUNGEONS = 'mythic_dungeons',
  MYTHIC_8_DUNGEONS = 'mythic_8_dungeons',
  POWER_LEVELING = 'power_leveling',
  RAIDS = 'raids',
  RANK = 'rank',
  SOUL_ASH = 'soul_ash',
  TORGHAST_TOWER_LAYER = 'torghast_tower_layer',
}

export default POSITION;
