enum ROLE {
  ADMIN = 'admin',
  BOOSTER = 'booster',
  CLIENT = 'client',
  MANAGER = 'manager',
  USER = 'user',
}

export default ROLE;
