import CATEGORY from '@constants/category';

const CATEGORIES = [
  CATEGORY.ACHIEVEMENTS,
  CATEGORY.CHARACTER_BOOST,
  CATEGORY.DUNGEONS_AND_RAIDS,
  CATEGORY.OTHER,
  CATEGORY.POWER_LEVELING,
  CATEGORY.PVE,
  CATEGORY.PVP,
  CATEGORY.TORGHAST_AND_SOUL_ASH,
];

export default CATEGORIES;
