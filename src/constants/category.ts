export enum CATEGORY {
  ACHIEVEMENTS = 'achievements',
  DUNGEONS_AND_RAIDS = 'dungeons_and_raids',
  OTHER = 'other',
  POWER_LEVELING = 'power_leveling',
  CHARACTER_BOOST = 'character_boost',
  TORGHAST_AND_SOUL_ASH = 'torghast_and_soul_ash',
  PVE = 'pve',
  PVP = 'pvp',
}

export default CATEGORY;
