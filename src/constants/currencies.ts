import Currency from '@interfaces/currency';

export const USD: Currency = {
  code: 'USD',
  multiplier: 1,
};

export const EUR: Currency = {
  code: 'EUR',
  multiplier: 0.8,
};

export const RUB: Currency = {
  code: 'RUB',
  multiplier: 75,
};

const CURRENCIES = [USD, EUR, RUB];

export default CURRENCIES;
