enum STATUS {
  CLOSED = 'closed',
  FAIL = 'fail',
  LIVE = 'live',
  PAUSED = 'paused',
  SUCCESS = 'success',
}

export default STATUS;
