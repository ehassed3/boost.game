export enum USER_STATUS {
  MASTER = 'master',
  SHADOW = 'shadow',
  WARRIOR = 'warrior',
}

export enum USER_STATUS_AMOUNT {
  MASTER = 100,
  SHADOW = 500,
  WARRIOR = 0,
}

export enum USER_STATUS_PERCENT {
  MASTER = 5,
  SHADOW = 10,
  WARRIOR = 0,
}
