import * as React from 'react';

import MuiSvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  className?: string;
  fontSize?: 'small' | 'inherit' | 'default' | 'large';
}

const LogoSymbol: React.FC<Props> = ({ className, fontSize }) => (
  <MuiSvgIcon className={className} viewBox="152 40 80 182" fontSize={fontSize}>
    <path
      fill="#432ca2"
      d="M244.2 87.7l-35.7 38.8c-.9 1-1.6 2.1-2.1 3.4-2.5 6 .5 13 6.7 15.2 1.2.4 2.4.6 3.6.6 4.7 0 9.1-3 11-7.6l20-48.1c.7-1.6-.6-2.9-2-2.9-.5-.1-1 .1-1.5.6z"
    />
    <circle
      transform="rotate(-22.141 179.023 200.195)"
      fill="#dd37ef"
      cx="179"
      cy="200.2"
      r="8.8"
    />
    <linearGradient
      id="icon_1"
      gradientUnits="userSpaceOnUse"
      x1="136.17"
      y1="121.564"
      x2="241.327"
      y2="121.564"
    >
      <stop offset="0" stopColor="#dd37ef" />
      <stop offset=".094" stopColor="#c635e4" />
      <stop offset=".307" stopColor="#9832cc" />
      <stop offset=".51" stopColor="#732fba" />
      <stop offset=".7" stopColor="#592ead" />
      <stop offset=".868" stopColor="#492ca5" />
      <stop offset="1" stopColor="#432ca2" />
    </linearGradient>
    <path
      d="M192.8 75.6c11.4-4.6 23.8-5.7 35.7-3.5l12.8-14c-17.9-5.7-37.4-5.1-54.9 2-41.2 16.8-61.1 63.9-44.3 105.2 3 7.4 7.1 14.3 12.1 20.5 1.6 2 4.1 3.1 6.5 3.1 1.9 0 3.7-.6 5.3-1.9 3.6-2.9 4.1-8.2 1.2-11.8-4-4.9-7.2-10.4-9.6-16.3-13.3-32.6 2.5-70 35.2-83.3z"
      fill="url(#icon_1)"
    />
    <linearGradient
      id="icon_2"
      gradientUnits="userSpaceOnUse"
      x1="167.635"
      y1="134.323"
      x2="249.058"
      y2="134.323"
    >
      <stop offset="0" stopColor="#dd37ef" />
      <stop offset=".094" stopColor="#c635e4" />
      <stop offset=".307" stopColor="#9832cc" />
      <stop offset=".51" stopColor="#732fba" />
      <stop offset=".7" stopColor="#592ead" />
      <stop offset=".868" stopColor="#492ca5" />
      <stop offset="1" stopColor="#432ca2" />
    </linearGradient>
    <path
      d="M235.4 160.2c-14.3 10.3-34.3 6.9-44.6-7.4-8.9-12.5-7.6-29.3 2.5-40.2v-.1l25-27.2c-10.7-.3-21.3 2.6-30.1 8.9-22.1 15.8-27.2 46.6-11.4 68.6 9.6 13.4 24.7 20.5 40.1 20.5 9.9 0 19.9-3 28.6-9.2 3.9-2.8 4.8-8.2 2-12-2.8-3.8-8.2-4.7-12.1-1.9z"
      fill="url(#icon_2)"
    />
  </MuiSvgIcon>
);

export default LogoSymbol;
