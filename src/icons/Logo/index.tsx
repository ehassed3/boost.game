import * as React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiButton from '@material-ui/core/Button';

import Link from '@components/Link';

import LogoSymbol from '@icons/Logo/Symbol';

const useStyles = makeStyles((theme: Theme) => ({
  logo: {
    height: '68px',
    overflow: 'hidden',

    [theme.breakpoints.down('xs')]: {
      height: '60px',
    },
  },
  symbol: {
    height: '68px',
    width: '162px',

    [theme.breakpoints.down('xs')]: {
      height: '60px',
      width: '120px',
    },
  },
}));

interface Props {
  className?: string;
}

const Logo: React.FC<Props> = ({ className }) => {
  const classes = useStyles();

  return (
    <MuiButton
      className={className ? `${className} ${classes.logo}` : classes.logo}
      color="inherit"
      component={Link}
      href="/"
    >
      <LogoSymbol className={classes.symbol} />
    </MuiButton>
  );
};

export default Logo;
