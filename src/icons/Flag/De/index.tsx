import * as React from 'react';

import MuiSvgIcon from '@material-ui/core/SvgIcon';

interface Props {
  className?: string;
  fontSize?: 'small' | 'inherit' | 'default' | 'large';
}

const FlagDe: React.FC<Props> = ({ className, fontSize }) => (
  <MuiSvgIcon
    className={className}
    viewBox="0 0 16 16"
    style={{ borderRadius: '4px' }}
    fontSize={fontSize}
  >
    <path
      fill="#0e0e0e"
      d="M22.58 0H1.828C.818 0 0 .818 0 1.828v3.503h24.408V1.828C24.408.818 23.59 0 22.58 0z"
    />
    <path
      fill="#ffe15a"
      d="M0 14.164c0 1.01.818 1.828 1.828 1.828H22.58c1.01 0 1.828-.819 1.828-1.828V10.66H0v3.503z"
    />
    <path fill="#e6000d" d="M0 5.33L24.408 5.33 24.408 10.661 0 10.661z" />
  </MuiSvgIcon>
);

export default FlagDe;
