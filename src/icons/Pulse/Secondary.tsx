import * as React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

import THEME from '@constants/theme';

const useStyles = makeStyles((theme: Theme) => ({
  '@keyframes pulse': {
    '0%': {
      transform: 'scale(0.95)',
      boxShadow: `0 0 0 0 ${fade(theme.palette.secondary.main, 0.7)}`,
    },
    '70%': {
      transform: 'scale(1)',
      boxShadow: `0 0 0 12px ${fade(theme.palette.secondary.main, 0)}`,
    },
    '100%': {
      transform: 'scale(0.95)',
      boxShadow: `0 0 0 0 ${fade(theme.palette.secondary.main, 0)}`,
    },
  },
  pulse: {
    animation: '$pulse 1.6s infinite',
    backgroundColor: THEME.palette.secondary.main,
    boxShadow: `0 0 0 0 ${THEME.palette.secondary.main}`,
    borderRadius: '50%',
    content: '""',
    height: '8px',
    transform: 'scale(1)',
    width: '8px',
  },
}));

interface Props {
  className?: string;
}

const PulseSecondary: React.FC<Props> = ({ className }) => {
  const classes = useStyles();

  return (
    <div
      className={className ? `${className} ${classes.pulse}` : classes.pulse}
    />
  );
};

export default PulseSecondary;
