import * as React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiSvgIcon from '@material-ui/core/SvgIcon';

const useStyles = makeStyles(() => ({
  discount: {
    position: 'relative',
    pointerEvents: 'none',
  },
  text: {
    fontWeight: 500,
    position: 'relative',
    textAlign: 'center',
    width: '34px',
    zIndex: 1,
  },
  icon: {
    height: '28px',
    left: '-5px',
    position: 'absolute',
    top: '-3px',
    width: '44px',
  },
}));

interface Props {
  className?: string;
  percent: number;
}

const Discount: React.FC<Props> = ({ className, percent }) => {
  const classes = useStyles();

  return (
    <div
      className={
        className ? `${className} ${classes.discount}` : classes.discount
      }
    >
      <MuiTypography className={classes.text}>{`-${percent}%`}</MuiTypography>
      <MuiSvgIcon className={classes.icon} viewBox="0 0 37 21">
        <path
          xmlns="http://www.w3.org/2000/svg"
          fill="#f73d33"
          d="M1.024 2.353c5.67.27 11.345.252 17.025-.055A169.369 169.369 0 0035.057.512c.534-.084 1.033.295 1.114.847.005.033.008.066.01.098l.818 16.096c.027.533-.353.996-.867 1.056a281.594 281.594 0 01-17.634 1.532c-5.642.306-11.214.416-16.716.332a.994.994 0 01-.963-.958l-.818-16.1c-.028-.558.387-1.033.927-1.062a.95.95 0 01.096 0z"
        />
      </MuiSvgIcon>
    </div>
  );
};

export default Discount;
