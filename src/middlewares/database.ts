import { NextApiRequest, NextApiResponse } from 'next';
import nextConnect, { Middleware } from 'next-connect';
import { MongoClient } from 'mongodb';

const client = new MongoClient(process.env.MONGO_URL || '', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const database: Middleware<NextApiRequest, NextApiResponse> = async (
  req,
  _,
  next
) => {
  if (!client.isConnected()) {
    await client.connect();
  }

  req.dbClient = client;
  req.db = client.db('DirtyDB');

  return next();
};

const middleware = nextConnect();

middleware.use(database);

export default middleware;
