import { NextApiRequest, NextApiResponse } from 'next';
import { Middleware } from 'next-connect';
import { validationResult, ValidationChain } from 'express-validator';
import qs from 'qs';
import isEmpty from 'lodash/isEmpty';

const validate = (
  validations: ValidationChain[]
): Middleware<NextApiRequest, NextApiResponse> => async (req, res, next) => {
  const urlBody = req.url
    ? qs.parse(req.url.split('?')[1], {
        decoder: (str, _, charset) => {
          const strWithoutPlus = str.replace(/\+/g, ' ');
          if (charset === 'iso-8859-1') {
            // unescape never throws, no try...catch needed:
            return strWithoutPlus.replace(/%[0-9a-f]{2}/gi, unescape);
          }

          if (/^(\d+|\d*\.\d+)$/.test(str)) {
            return parseFloat(str);
          }

          const keywords: Record<string, boolean | null | undefined> = {
            true: true,
            false: false,
            null: null,
            undefined,
          };
          if (str in keywords) {
            return keywords[str];
          }

          // utf-8
          try {
            return decodeURIComponent(strWithoutPlus);
          } catch (e) {
            return strWithoutPlus;
          }
        },
      })
    : {};
  if (!isEmpty(urlBody)) {
    req.body = urlBody;
  }

  if (!req.body) {
    return next();
  }

  if (typeof req.body === 'string') {
    req.body = JSON.parse(req.body);
  }

  await Promise.all(
    validations.map((validation) => {
      return validation.run(req);
    })
  );

  const errors = validationResult(req);

  if (errors.isEmpty()) {
    return next();
  }

  return res.status(422).json({ errors: errors.array() });
};

export default validate;
