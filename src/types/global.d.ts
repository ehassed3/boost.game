export declare global {
  interface Window {
    Chatra: any;
  }

  namespace NodeJS {
    interface Global {
      Chatra: any;
    }

    interface Process {
      browser: boolean;
    }
  }
}
