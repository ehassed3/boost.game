import DirtyUser from '@interfaces/user';

declare module 'next-auth/client' {
  interface Session {
    user: DirtyUser;
    accessToken?: string;
    expires: string;
  }
}

declare module 'next-auth' {
  interface User extends DirtyUser {
    discriminator: string;
    id: string;
  }
}
