import { MongoClient, Db } from 'mongodb';

declare module 'http' {
  interface IncomingMessage {
    db: Db;
    dbClient: MongoClient;
  }
}
