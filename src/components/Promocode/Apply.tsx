import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTextField from '@material-ui/core/TextField';
import MuiButton from '@material-ui/core/Button';
import MuiSnackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import MuiLinearProgress from '@material-ui/core/LinearProgress';
import MuiDialog from '@material-ui/core/Dialog';

import MuiLocalOfferIcon from '@material-ui/icons/LocalOffer';

import SERVER from '@constants/server';
import themeComponent from '@constants/theme';

import Promocode from '@interfaces/promocode';

const useStyles = makeStyles((theme: Theme) => ({
  dialog: {
    padding: '28px 16px 20px',
    maxWidth: '320px',
  },
  icon: {
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: '50%',
    height: '100px',
    margin: '0 auto 24px',
    padding: '12px',
    width: '100px',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  name: {
    marginBottom: '24px',
  },
  loader: {
    bottom: '0',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
  promocode?: Promocode | null;
  setPromocode: (promocode: Promocode | null) => void;
}

const ApplyPromocode: React.FC<Props> = ({
  className,
  promocode,
  setPromocode,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [name, setName] = React.useState(promocode ? promocode.name : '');

  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const onApply = () => {
    setLoading(true);

    fetch(`${SERVER}/api/apply_promocode/`, {
      method: 'POST',
      body: JSON.stringify({ name, locale: intl.locale }),
    })
      .then((res) => {
        setLoading(false);

        if (!res.ok) {
          setName('');
          return res.text();
        }

        return res.json();
      })
      .then((result) => {
        if (typeof result === 'string') {
          setError(result);
          return;
        }

        setPromocode(result);
        setOpen(false);
      })
      .catch(() => {
        setLoading(false);

        setName('');
        setError(intl.formatMessage({ id: 'serverError' }));
      });
  };

  return (
    <>
      <MuiButton
        className={className}
        size="small"
        variant="outlined"
        color="inherit"
        style={{
          color: promocode ? themeComponent.palette.success.dark : undefined,
        }}
        onClick={() => {
          setOpen(true);
        }}
      >
        {promocode
          ? `${promocode.name}, -${promocode.percent}%`
          : intl.formatMessage({ id: 'applyPromocode.enterText' })}
      </MuiButton>
      {open && (
        <MuiDialog
          classes={{ paper: classes.dialog }}
          maxWidth="xs"
          fullWidth
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        >
          <MuiLocalOfferIcon className={classes.icon} />
          <form
            className={classes.form}
            onSubmit={(evt) => {
              evt.preventDefault();

              if (!promocode) {
                onApply();
              }
            }}
          >
            <MuiTextField
              className={classes.name}
              label={intl.formatMessage({ id: 'applyPromocode.fieldLabel' })}
              variant="filled"
              size="small"
              disabled={!!promocode}
              value={name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setName(event.target.value.toUpperCase());
              }}
              onKeyPress={(event) => {
                const element = event.target as HTMLInputElement;

                if (!element || event.key !== 'Enter') {
                  return;
                }

                element.blur();
              }}
              InputProps={{ inputProps: { maxLength: 20 } }}
            />
            <MuiButton
              size="large"
              variant="contained"
              color={promocode ? 'secondary' : 'primary'}
              disabled={!promocode && !name}
              onClick={
                promocode
                  ? () => {
                      setName('');
                      setPromocode(null);
                    }
                  : onApply
              }
            >
              {promocode
                ? intl.formatMessage({ id: 'applyPromocode.removeButton' })
                : intl.formatMessage({ id: 'applyPromocode.applyButton' })}
            </MuiButton>
          </form>
          {loading && <MuiLinearProgress className={classes.loader} />}
        </MuiDialog>
      )}
      {!!error && (
        <MuiSnackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          autoHideDuration={6000}
          open={!!error}
          onClose={() => {
            setError('');
          }}
        >
          <MuiAlert
            onClose={() => {
              setError('');
            }}
            severity="error"
          >
            {error}
          </MuiAlert>
        </MuiSnackbar>
      )}
    </>
  );
};

export default ApplyPromocode;
