import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiTextField from '@material-ui/core/TextField';
import MuiButton from '@material-ui/core/Button';
import MuiSnackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import Checkbox from '@components/Checkbox';

import SERVER from '@constants/server';
import STATUS from '@constants/status';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h5,

    marginBottom: '20px',
  },
  fields: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: '24px',
  },
  name: {
    marginRight: '20px',
    width: '200px',

    [theme.breakpoints.down('xs')]: {
      flexGrow: 1,
      width: 'auto',
    },
  },
  percent: {
    width: '100px',

    [theme.breakpoints.down('xs')]: {
      flexGrow: 1,
      width: 'auto',
    },
  },
  controls: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    position: 'relative',
    width: '160px',
  },
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
}

const CreatePromocode: React.FC<Props> = ({ className }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [name, setName] = React.useState('');
  const [percent, setPercent] = React.useState('');
  const [unlimited, setUnlimited] = React.useState(false);

  const [loading, setLoading] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [status, setStatus] = React.useState('');

  const onCreateClick = () => {
    setLoading(true);

    fetch(`${SERVER}/api/set_promocode/`, {
      method: 'POST',
      body: JSON.stringify({
        name,
        percent: +percent,
        unlimited,
        locale: intl.locale,
      }),
    })
      .then((res) => {
        setLoading(false);

        setName('');
        setPercent('');

        setStatus(res.ok ? STATUS.SUCCESS : STATUS.FAIL);
        return res.text();
      })
      .then((message) => {
        setMessage(message);
      })
      .catch(() => {
        setLoading(false);

        setName('');
        setPercent('');

        setStatus(STATUS.FAIL);
        setMessage(intl.formatMessage({ id: 'serverError' }));
      });
  };

  return (
    <>
      <div className={className}>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'createPromocode.title' })}
        </MuiTypography>
        <div className={classes.fields}>
          <MuiTextField
            className={classes.name}
            label={intl.formatMessage({ id: 'createPromocode.nameFieldLabel' })}
            variant="filled"
            size="small"
            value={name}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setName(event.target.value.toUpperCase());
            }}
            onKeyPress={(event) => {
              const element = event.target as HTMLInputElement;

              if (!element || event.key !== 'Enter') {
                return;
              }

              element.blur();
            }}
            InputProps={{ inputProps: { maxLength: 20 } }}
          />
          <MuiTextField
            className={classes.percent}
            label={intl.formatMessage({
              id: 'createPromocode.percentFieldLabel',
            })}
            variant="filled"
            size="small"
            value={percent}
            type="number"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              let newPercent = event.target.value;

              if (newPercent.length > event.target.maxLength) {
                return;
              }

              if (+newPercent > +event.target.max) {
                newPercent = event.target.max;
              } else if (+newPercent < +event.target.min) {
                newPercent = event.target.min;
              }

              setPercent(newPercent);
            }}
            onKeyPress={(event) => {
              const element = event.target as HTMLInputElement;

              if (!element || event.key !== 'Enter') {
                return;
              }

              element.blur();
            }}
            InputProps={{
              inputProps: {
                maxLength: 2,
                min: 0,
                max: 30,
              },
            }}
          />
        </div>
        <div className={classes.controls}>
          <Checkbox
            label={intl.formatMessage({
              id: 'createPromocode.unlimitedCheckbox',
            })}
            name="promocodeUnlimited"
            color="primary"
            size="small"
            onChange={(
              _: React.ChangeEvent<HTMLInputElement>,
              checked: boolean
            ) => {
              setUnlimited(checked);
            }}
          />
          <MuiButton
            className={classes.button}
            variant="contained"
            disabled={loading || !name || !percent}
            onClick={onCreateClick}
          >
            {intl.formatMessage({ id: 'createPromocode.button' })}
            {loading && <MuiLinearProgress className={classes.loader} />}
          </MuiButton>
        </div>
      </div>
      {!!message && (
        <MuiSnackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          autoHideDuration={6000}
          open={!!message}
          onClose={() => {
            setMessage('');
            setStatus('');
          }}
        >
          <MuiAlert
            onClose={() => {
              setMessage('');
              setStatus('');
            }}
            severity={status === STATUS.SUCCESS ? 'success' : 'error'}
          >
            {message}
          </MuiAlert>
        </MuiSnackbar>
      )}
    </>
  );
};

export default CreatePromocode;
