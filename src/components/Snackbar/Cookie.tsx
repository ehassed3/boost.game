import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSnackbar from '@material-ui/core/Snackbar';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import Link from '@components/Link';

const useStyles = makeStyles((theme: Theme) => ({
  snackbar: {
    [theme.breakpoints.down('xs')]: {
      bottom: '72px',
      left: 'auto',
      top: 'auto',
    },
  },
  content: {
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.primary,
  },
  link: {
    color: theme.palette.text.primary,
    textDecoration: 'underline',
  },
}));

const CookieSnackbar: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(true);

  const onCloseClick = () => {
    document.cookie = 'privacy=true; expires=Tue, 19 Jan 2038 03:14:07 UTC;';
    setOpen(false);
  };

  return (
    <MuiSnackbar
      className={classes.snackbar}
      ContentProps={{ className: classes.content }}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={open}
      message={
        <>
          {intl.formatMessage(
            { id: 'cookieSnackbar.title' },
            {
              link: (
                <Link
                  className={classes.link}
                  href="/privacy"
                  onClick={onCloseClick}
                >
                  {intl.formatMessage({ id: 'cookieSnackbar.link' })}
                </Link>
              ),
            }
          )}
        </>
      }
      action={
        <MuiIconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={onCloseClick}
        >
          <MuiCloseIcon fontSize="small" />
        </MuiIconButton>
      }
    />
  );
};

export default CookieSnackbar;
