import * as React from 'react';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiBottomNavigation from '@material-ui/core/BottomNavigation';
import MuiBottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import MuiBadge from '@material-ui/core/Badge';

import MuiHomeIcon from '@material-ui/icons/Home';
import MuiPersonIcon from '@material-ui/icons/Person';
import MuiChatIcon from '@material-ui/icons/Chat';

import { useSupport } from '@context/support';

import GAMES from '@constants/games';

const useStyles = makeStyles((theme: Theme) => ({
  bottomNavigation: {
    bottom: '0',
    display: 'none',
    left: '0',
    position: 'fixed',
    right: '0',
    zIndex: 4,

    [theme.breakpoints.down('xs')]: {
      display: 'flex',
    },
  },
  bottomNavigationAction: {
    padding: '8px 12px',

    '&:hover': {
      textDecoration: 'none',
    },
  },
}));

enum NAVIGATION {
  ACCOUNT = 'account',
  CHAT = 'chat',
  DEFAULT = 'default',
  HOME = 'home',
}

const getNavigation = (
  asPath: string,
  pathname: string,
  first?: boolean
): NAVIGATION => {
  if (!first && asPath.endsWith('#chat')) {
    return NAVIGATION.CHAT;
  }

  const item = pathname.split('/')[1];

  if (item === NAVIGATION.ACCOUNT) {
    return NAVIGATION.ACCOUNT;
  }

  if (!item || GAMES.some((game) => game === item)) {
    return NAVIGATION.HOME;
  }

  return NAVIGATION.DEFAULT;
};

interface Props {
  asPath: string;
  pathname: string;
}

const BottomNavigation: React.FC<Props> = ({ asPath, pathname }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [navigation, setNavigation] = React.useState(
    getNavigation(asPath, pathname, true)
  );
  const { support, setSupport } = useSupport();

  const router = useRouter();

  React.useEffect(() => {
    setNavigation(getNavigation(router.asPath, router.pathname));
  }, [router.asPath]);

  return (
    <MuiBottomNavigation
      className={classes.bottomNavigation}
      value={navigation}
      onChange={(_, value) => {
        setNavigation(value);
      }}
    >
      <MuiBottomNavigationAction
        className={classes.bottomNavigationAction}
        label={intl.formatMessage({ id: 'bottomNavigation.home' })}
        value={NAVIGATION.HOME}
        icon={<MuiHomeIcon />}
        onClick={() => {
          const item = router.pathname.split('/')[1];
          if (support.open && (!item || GAMES.some((game) => game === item))) {
            setSupport({ ...support, open: false });
            return;
          }

          router.push('/');
        }}
      />
      <MuiBottomNavigationAction
        className={classes.bottomNavigationAction}
        label={intl.formatMessage({ id: 'bottomNavigation.chat' })}
        value={NAVIGATION.CHAT}
        icon={
          <MuiBadge
            badgeContent={support.message ? '1' : undefined}
            color="error"
          >
            <MuiChatIcon />
          </MuiBadge>
        }
        onClick={() => {
          setSupport({ ...support, open: true, message: '' });
        }}
      />
      <MuiBottomNavigationAction
        className={classes.bottomNavigationAction}
        label={intl.formatMessage({ id: 'bottomNavigation.account' })}
        value={NAVIGATION.ACCOUNT}
        icon={<MuiPersonIcon />}
        onClick={() => {
          const item = router.pathname.split('/')[1];
          if (support.open && item === NAVIGATION.ACCOUNT) {
            setSupport({ ...support, open: false });
            return;
          }

          router.push('/account');
        }}
      />
    </MuiBottomNavigation>
  );
};

export default BottomNavigation;
