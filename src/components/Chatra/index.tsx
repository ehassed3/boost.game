import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MuiBadge from '@material-ui/core/Badge';
import MuiTypography from '@material-ui/core/Typography';
import MuiDialog from '@material-ui/core/Dialog';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import { useSupport } from '@context/support';

import ChatraDetails from '@components/Chatra/Details';

const useStyles = makeStyles((theme: Theme) => ({
  chatra: {
    bottom: '20px',
    cursor: 'pointer',
    position: 'fixed',
    right: '20px',
    zIndex: 2,
  },
  badge: {
    left: '40px',
  },
  button: {
    backgroundColor: theme.palette.background.default,
    borderRadius: '12px',
    boxShadow: '0 0 3px rgb(0 0 0 / 10%), 0 0 10px rgb(0 0 0 / 30%)',
    color: theme.palette.text.primary,
    fontWeight: 500,
    padding: '8px 12px 8px 26px',

    '&::before': {
      backgroundColor: '#51d161',
      borderRadius: '50%',
      content: '""',
      height: '8px',
      left: '12px',
      position: 'absolute',
      top: '16px',
      width: '8px',
    },

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  dialog: {
    [theme.breakpoints.down('xs')]: {
      bottom: '56px !important',
    },
  },
  paper: {
    alignItems: 'flex-start',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    display: 'flex',
    flexDirection: 'row',
    height: '84vh',
    maxWidth: '680px',
    pointerEvents: 'none',

    [theme.breakpoints.down('xs')]: {
      height: '100%',
      margin: '0',
      maxHeight: 'none',
      width: '100vw',
    },
  },
  backdrop: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  close: {
    color: '#000',
    pointerEvents: 'all',
    position: 'absolute',
    right: '2px',
    top: '2px',
  },
  icon: {
    flexShrink: 0,
    height: '20px',
    marginRight: '6px',
    width: '20px',
  },
  chat: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '12px',
    height: '100%',
    overflow: 'hidden',
    pointerEvents: 'all',
    width: '56%',
    flexShrink: 0,

    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
}));

const Chatra: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();
  const isMobile = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down('xs')
  );

  const { support, setSupport } = useSupport();

  React.useEffect(() => {
    const onMessage = (event: MessageEvent) => {
      if (support.open) {
        return;
      }

      const { data } = JSON.parse(event.data);

      if (data !== 'New message') {
        return;
      }

      setSupport({ ...support, message: 'New message' });
    };

    window.addEventListener('message', onMessage);

    return () => {
      window.removeEventListener('message', onMessage);
    };
  }, [support]);

  const wrapperRef = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (
      !support.open ||
      !window?.Chatra ||
      !wrapperRef?.current ||
      !!wrapperRef.current.childElementCount
    ) {
      return;
    }

    window.Chatra('restart');
  }, [support.open]);

  return (
    <>
      <MuiBadge
        classes={{ root: classes.chatra, badge: classes.badge }}
        badgeContent={support.message ? support.message : undefined}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        color="error"
        onClick={() => {
          gtag('event', 'click', {
            event_category: 'support',
            event_label: 'Show main chat',
          });
          setSupport({ ...support, open: true, message: '' });
        }}
      >
        <MuiTypography className={classes.button}>
          {intl.formatMessage({ id: 'supportChat.title' })}
        </MuiTypography>
      </MuiBadge>
      <MuiDialog
        classes={{ root: classes.dialog, paper: classes.paper }}
        BackdropProps={{ classes: { root: classes.backdrop } }}
        fullWidth
        open={support.open}
        keepMounted
        transitionDuration={isMobile ? 900 : undefined}
        onClose={() => {
          setSupport({ ...support, open: false });
        }}
      >
        <MuiIconButton
          className={classes.close}
          onClick={() => {
            setSupport({ ...support, open: false });
          }}
        >
          <MuiCloseIcon fontSize="small" />
        </MuiIconButton>
        <ChatraDetails />
        <div className={classes.chat} ref={wrapperRef} id="chatra-wrapper" />
      </MuiDialog>
    </>
  );
};

export default Chatra;
