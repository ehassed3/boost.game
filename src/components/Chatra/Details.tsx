import React from 'react';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiFormControl from '@material-ui/core/FormControl';
import MuiInputLabel from '@material-ui/core/InputLabel';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';
import MuiTextField from '@material-ui/core/TextField';
import MuiGrow from '@material-ui/core/Grow';

import MuiCheckIcon from '@material-ui/icons/Check';

import SelectList, { Value as SelectListValue } from '@components/SelectList';

import { useSupport } from '@context/support';

import Checkbox from '@components/Checkbox';
import ToggleButtonGroup, {
  Value as ToggleButtonGroupValue,
} from '@components/ToggleButtonGroup';

import getGameTitle from '@helpers/getGameTitle';
import getPositionText from '@helpers/getPositionText';

import GAME from '@constants/game';
import GAMES from '@constants/games';
import POSITION from '@constants/position';

const useStyles = makeStyles((theme: Theme) => ({
  details: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '12px 0 0 12px',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    margin: '16px 0',
    maxHeight: 'calc(100% - 32px)',
    overflowY: 'scroll',
    pointerEvents: 'all',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  detailsMain: {
    padding: '16px 12px 12px',
  },
  detailsItem: {
    position: 'relative',
  },
  checkIcon: {
    color: theme.palette.success.main,
    height: '16px',
    position: 'absolute',
    right: '4px',
    top: '0',
    width: '16px',
  },
  checkIconLabel: {
    top: '5px',
  },
  checkIconSelectList: {
    top: '8px',
  },
  checkIconCheckbox: {
    top: '16px',
  },
  checkIconField: {
    top: '20px',
  },
  title: {
    fontWeight: 500,
  },
  discount: {
    ...theme.typography.body2,

    color: theme.palette.text.secondary,
    marginBottom: '12px',
  },
  select: {
    marginBottom: '16px',
    marginTop: '8px',
    width: '100%',
  },
  detailsContainer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    padding: '16px 12px',
  },
  detailsLabel: {
    fontWeight: 500,
    marginBottom: '4px',
  },
  checkbox: {
    marginTop: '6px',
  },
  item: {
    alignItems: 'center',
    display: 'flex',
  },
  itemImage: {
    borderRadius: '4px',
    height: '20px',
    marginRight: '8px',
    width: '20px',
  },
  textField: {
    marginBottom: '12px',
    marginTop: '8px',
    width: '100%',
  },
  selectList: {
    marginBottom: '16px',
    marginTop: '12px',
    width: '100%',
  },
  toggleButtonGroupLabel: {
    marginTop: '8px',
    position: 'relative',
  },
  toggleButtonGroup: {
    margin: '8px 0px 12px',
    width: '100%',
  },
}));

enum FIELD {
  ARMORY = 'armory',
  CHARACTER = 'character',
  COVENANT = 'covenant',
  FRACTION = 'fraction',
  GAME = 'game',
  ILVL = 'ilvl',
  POSITION = 'position',
  RAIDS_DIFFICULTY = 'raidsDifficulty',
  RATING = 'rating',
  SELFPLAY = 'selfplay',
  STREAM = 'stream',
  TIME = 'time',
  TIMER = 'timer',
  TRADERS = 'traders',
  VPN = 'vpn',
}

enum CHARACTER {
  DEATH_KNIGHT = 'death_knight',
  DEMON_HUNTER = 'demon_hunter',
  DRUID = 'druid',
  HUNTER = 'hunter',
  MAGE = 'mage',
  MONK = 'monk',
  PALADIN = 'paladin',
  PRIEST = 'priest',
  ROGUE = 'rogue',
  SHAMAN = 'shaman',
  WARLOCK = 'warlock',
  WARRIOR = 'warrior',
}

enum COVENANT {
  KYRIAN = 'kyrian',
  NECROLORDS = 'necrolords',
  NIGHT_FAE = 'night_fae',
  VENTHYR = 'venthyr',
}

enum FRACTION {
  ALLIANCE = 'alliance',
  HORDE = 'horde',
}

enum TRADER {
  CLOTH = 'cloth',
  LEATHER = 'leather',
  MAIL = 'mail',
  PLATE = 'plate',
}

interface FormValue {
  [FIELD.ARMORY]?: string;
  [FIELD.TIME]?: {
    value: string;
    label: string;
  } | null;
  [FIELD.FRACTION]?: ToggleButtonGroupValue;
  [FIELD.CHARACTER]?: {
    value: string;
    label: string;
  } | null;
  [FIELD.RAIDS_DIFFICULTY]?: {
    value: string;
    label: string;
  } | null;
  [FIELD.COVENANT]?: ToggleButtonGroupValue;
  [FIELD.ILVL]?: string;
  [FIELD.RATING]?: string;
  [FIELD.TRADERS]?: SelectListValue[];
  [FIELD.SELFPLAY]?: boolean;
  [FIELD.TIMER]?: boolean;
  [FIELD.STREAM]?: boolean;
  [FIELD.VPN]?: boolean;
}

const ChatraDetails: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  const { support, setSupport } = useSupport();

  const positions = React.useMemo(() => {
    switch (support.game) {
      case GAME.WOW:
        return [
          {
            value: POSITION.POWER_LEVELING,
            label: getPositionText(support.game, POSITION.POWER_LEVELING, intl),
          },
          {
            value: POSITION.COVENANT,
            label: getPositionText(support.game, POSITION.COVENANT, intl),
          },
          {
            value: POSITION.ARENA,
            label: getPositionText(support.game, POSITION.ARENA, intl),
          },
          {
            value: POSITION.HONOR,
            label: getPositionText(support.game, POSITION.HONOR, intl),
          },
          {
            value: POSITION.CONQUEST_CAP,
            label: getPositionText(support.game, POSITION.CONQUEST_CAP, intl),
          },
          {
            value: POSITION.RAIDS,
            label: getPositionText(support.game, POSITION.RAIDS, intl),
          },
          {
            value: POSITION.MYTHIC_DUNGEONS,
            label: getPositionText(
              support.game,
              POSITION.MYTHIC_DUNGEONS,
              intl
            ),
          },
          {
            value: POSITION.MYTHIC_8_DUNGEONS,
            label: getPositionText(
              support.game,
              POSITION.MYTHIC_8_DUNGEONS,
              intl
            ),
          },
          {
            value: POSITION.TORGHAST_TOWER_LAYER,
            label: getPositionText(
              support.game,
              POSITION.TORGHAST_TOWER_LAYER,
              intl
            ),
          },
          {
            value: POSITION.SOUL_ASH,
            label: getPositionText(support.game, POSITION.SOUL_ASH, intl),
          },
          {
            value: POSITION.HERO,
            label: getPositionText(support.game, POSITION.HERO, intl),
          },
        ];
      case GAME.WOW_CLASSIC:
        return [
          {
            value: POSITION.POWER_LEVELING,
            label: getPositionText(support.game, POSITION.POWER_LEVELING, intl),
          },
          {
            value: POSITION.HONOR,
            label: getPositionText(support.game, POSITION.HONOR, intl),
          },
          {
            value: POSITION.RANK,
            label: getPositionText(support.game, POSITION.RANK, intl),
          },
        ];
      case GAME.DOTA_2:
        return [
          {
            value: POSITION.BOOST,
            label: getPositionText(support.game, POSITION.BOOST, intl),
          },
          {
            value: POSITION.CALIBRATION,
            label: getPositionText(support.game, POSITION.CALIBRATION, intl),
          },
          {
            value: POSITION.LOW_PRIORITY,
            label: getPositionText(support.game, POSITION.LOW_PRIORITY, intl),
          },
        ];
      default:
        return [];
    }
  }, [support.game, intl.locale]);

  const [formValue, setFormValue] = React.useState<FormValue | null>(null);

  React.useEffect(() => {
    if (!window?.Chatra) {
      return;
    }

    const body = {
      [FIELD.GAME]: support.game || null,
      [FIELD.POSITION]: support.position || null,
      [FIELD.TIME]: formValue && formValue.time ? formValue.time.value : null,
      [FIELD.FRACTION]:
        formValue && formValue.fraction ? formValue.fraction.name : null,
      [FIELD.CHARACTER]:
        formValue && formValue.character ? formValue.character.value : null,
      [FIELD.RAIDS_DIFFICULTY]:
        formValue && formValue.raidsDifficulty
          ? formValue.raidsDifficulty.value
          : null,
      [FIELD.COVENANT]:
        formValue && formValue.covenant ? formValue.covenant.name : null,
      [FIELD.TRADERS]:
        formValue && formValue.traders && formValue.traders.length
          ? formValue.traders.map((trader) => trader.value).join(', ')
          : null,
      [FIELD.ILVL]: formValue && formValue.ilvl ? formValue.ilvl : null,
      [FIELD.RATING]: formValue && formValue.rating ? formValue.rating : null,
      [FIELD.ARMORY]: formValue && formValue.armory ? formValue.armory : null,
      [FIELD.TIMER]:
        formValue && formValue.selfplay ? formValue.selfplay : null,
      [FIELD.SELFPLAY]: formValue && formValue.timer ? formValue.timer : null,
      [FIELD.STREAM]: formValue && formValue.stream ? formValue.stream : null,
      [FIELD.VPN]: formValue && formValue.vpn ? formValue.vpn : null,
    };

    window.Chatra('setIntegrationData', body);
  }, [support.game, support.position, formValue]);

  const fractions = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.fractionHorde' }),
        name: FRACTION.HORDE,
      },
      {
        label: intl.formatMessage({ id: 'wow.fractionAlliance' }),
        name: FRACTION.ALLIANCE,
      },
    ],
    [intl.locale]
  );

  const characters = React.useMemo(
    () => [
      {
        value: CHARACTER.DEATH_KNIGHT,
        label: intl.formatMessage({ id: 'wow.deathKnightCharacter' }),
      },
      {
        value: CHARACTER.DEMON_HUNTER,
        label: intl.formatMessage({ id: 'wow.demonHunterCharacter' }),
      },
      {
        value: CHARACTER.DRUID,
        label: intl.formatMessage({ id: 'wow.druidCharacter' }),
      },
      {
        value: CHARACTER.HUNTER,
        label: intl.formatMessage({ id: 'wow.hunterCharacter' }),
      },
      {
        value: CHARACTER.MAGE,
        label: intl.formatMessage({ id: 'wow.mageCharacter' }),
      },
      {
        value: CHARACTER.MONK,
        label: intl.formatMessage({ id: 'wow.monkCharacter' }),
      },
      {
        value: CHARACTER.PALADIN,
        label: intl.formatMessage({ id: 'wow.paladinCharacter' }),
      },
      {
        value: CHARACTER.PRIEST,
        label: intl.formatMessage({ id: 'wow.priestCharacter' }),
      },
      {
        value: CHARACTER.ROGUE,
        label: intl.formatMessage({ id: 'wow.rogueCharacter' }),
      },
      {
        value: CHARACTER.SHAMAN,
        label: intl.formatMessage({ id: 'wow.shamanCharacter' }),
      },
      {
        value: CHARACTER.WARLOCK,
        label: intl.formatMessage({ id: 'wow.warlockCharacter' }),
      },
      {
        value: CHARACTER.WARRIOR,
        label: intl.formatMessage({ id: 'wow.warriorCharacter' }),
      },
    ],
    [intl.locale]
  );

  const covenants = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.covenantKyrian' }),
        name: COVENANT.KYRIAN,
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantNecrolords' }),
        name: COVENANT.NECROLORDS,
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantNightFae' }),
        name: COVENANT.NIGHT_FAE,
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantVenthyr' }),
        name: COVENANT.VENTHYR,
      },
    ],
    [intl.locale]
  );

  const traders = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.traderCloth' }),
        value: TRADER.CLOTH,
      },
      {
        label: intl.formatMessage({ id: 'wow.traderLeather' }),
        value: TRADER.LEATHER,
      },
      {
        label: intl.formatMessage({ id: 'wow.traderMail' }),
        value: TRADER.MAIL,
      },
      {
        label: intl.formatMessage({ id: 'wow.traderPlate' }),
        value: TRADER.PLATE,
      },
    ],
    [intl.locale]
  );

  const raidsDifficults = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wowRaids.normalDifficulty' }),
        value: 'normal',
      },
      {
        label: intl.formatMessage({ id: 'wowRaids.heroicDifficulty' }),
        value: 'heroic',
      },
      {
        label: intl.formatMessage({ id: 'wowRaids.mythicDifficulty' }),
        value: 'mythic',
      },
    ],
    [intl.locale]
  );

  const times = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'supportChat.timeSoon' }),
        value: 'soon',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.time2Hour' }),
        value: '2hour',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.time3Hour' }),
        value: '3hour',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.time4Hour' }),
        value: '4hour',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.time5Hour' }),
        value: '5hour',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.time6Hour' }),
        value: '6hour',
      },
      {
        label: intl.formatMessage({ id: 'supportChat.timeNotSoon' }),
        value: 'notSoon',
      },
    ],
    [intl.locale]
  );

  React.useEffect(() => {
    switch (support.game) {
      case GAME.WOW:
        switch (support.position) {
          case POSITION.POWER_LEVELING:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.COVENANT]: covenants[2],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.COVENANT:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.COVENANT]: covenants[2],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.TORGHAST_TOWER_LAYER:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.ILVL]: '',
              [FIELD.SELFPLAY]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.SOUL_ASH:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.ILVL]: '',
              [FIELD.SELFPLAY]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.MYTHIC_DUNGEONS:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.TRADERS]: [],
              [FIELD.SELFPLAY]: false,
              [FIELD.TIMER]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.MYTHIC_8_DUNGEONS:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.TRADERS]: [],
              [FIELD.SELFPLAY]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.HERO:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.CONQUEST_CAP:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.HONOR:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.RAIDS:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.RAIDS_DIFFICULTY]: raidsDifficults[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.CHARACTER]: characters[3],
              [FIELD.TRADERS]: [],
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.ARENA:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.RATING]: '',
              [FIELD.ARMORY]: '',
              [FIELD.SELFPLAY]: false,
              [FIELD.VPN]: false,
            });
            break;
          default:
            setFormValue(null);
        }
        break;
      case GAME.WOW_CLASSIC:
        switch (support.position) {
          case POSITION.POWER_LEVELING:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.HONOR:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          case POSITION.RANK:
            setFormValue({
              [FIELD.TIME]: times[0],
              [FIELD.FRACTION]: fractions[0],
              [FIELD.STREAM]: false,
              [FIELD.VPN]: false,
            });
            break;
          default:
            setFormValue(null);
        }
        break;
      default:
        setFormValue(null);
    }
  }, [support.game, support.position]);

  const [checked, setChecked] = React.useState('');
  const [timer, setTimer] = React.useState<NodeJS.Timeout>();
  React.useEffect(() => {
    if (!checked) {
      return () => '';
    }

    if (timer) {
      clearTimeout(timer);
    }

    const newTimer = setTimeout(() => {
      setChecked('');
    }, 800);
    setTimer(newTimer);

    return () => {
      if (!timer) {
        return;
      }

      clearTimeout(timer);
    };
  }, [checked]);

  return (
    <div className={classes.details}>
      <div className={classes.detailsMain}>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'supportChat.containerTitle' })}
        </MuiTypography>
        <MuiTypography className={classes.discount}>
          {intl.formatMessage({ id: 'supportChat.containerDiscount' })}
        </MuiTypography>
        <MuiFormControl className={classes.select}>
          <MuiGrow in={checked === FIELD.GAME} timeout={400}>
            <MuiCheckIcon className={classes.checkIcon} fontSize="small" />
          </MuiGrow>
          <MuiInputLabel id="game-select">
            {intl.formatMessage({ id: 'supportChat.gameSelectLabel' })}
          </MuiInputLabel>
          <MuiSelect
            labelId="game-select"
            name={FIELD.GAME}
            value={support.game || ''}
            renderValue={() => getGameTitle(support.game)}
            onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
              setSupport({
                ...support,
                [FIELD.GAME]: event.target.value as GAME,
                [FIELD.POSITION]: undefined,
              });

              if (checked === FIELD.GAME && timer) {
                clearTimeout(timer);

                setTimer(
                  setTimeout(() => {
                    setChecked('');
                  }, 800)
                );
              } else {
                setChecked(FIELD.GAME);
              }
            }}
          >
            {map(GAMES, (gameItem) => (
              <MuiMenuItem
                key={gameItem}
                className={classes.item}
                value={gameItem}
              >
                {getGameTitle(gameItem)}
              </MuiMenuItem>
            ))}
          </MuiSelect>
        </MuiFormControl>
        {!!positions.length && (
          <MuiFormControl className={classes.select}>
            <MuiGrow in={checked === FIELD.POSITION} timeout={400}>
              <MuiCheckIcon className={classes.checkIcon} fontSize="small" />
            </MuiGrow>
            <MuiInputLabel id="position-select">
              {intl.formatMessage({ id: 'supportChat.positionSelectLabel' })}
            </MuiInputLabel>
            <MuiSelect
              labelId="position-select"
              name={FIELD.POSITION}
              value={support.position || ''}
              renderValue={() => {
                const foundPosition = positions.find(
                  (positionItem) => positionItem.value === support.position
                );

                if (!foundPosition) {
                  return '';
                }

                return foundPosition.label;
              }}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const newPosition = positions.find(
                  (positionItem) => positionItem.value === event.target.value
                );

                if (!newPosition) {
                  return;
                }

                setSupport({
                  ...support,
                  [FIELD.POSITION]: newPosition.value,
                });

                if (checked === FIELD.POSITION && timer) {
                  clearTimeout(timer);

                  setTimer(
                    setTimeout(() => {
                      setChecked('');
                    }, 800)
                  );
                } else {
                  setChecked(FIELD.POSITION);
                }
              }}
            >
              {map(positions, (positionItem) => (
                <MuiMenuItem
                  key={positionItem.value}
                  className={classes.item}
                  value={positionItem.value}
                >
                  {positionItem?.label}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          </MuiFormControl>
        )}
      </div>
      {!!formValue && (
        <div className={classes.detailsContainer}>
          <MuiTypography className={classes.detailsLabel}>
            {intl.formatMessage({ id: 'supportChat.detailsLabel' })}
          </MuiTypography>
          {map(formValue, (value, key) => (
            <div key={key} className={classes.detailsItem}>
              {key === FIELD.TIME && (
                <MuiFormControl className={classes.select}>
                  <MuiGrow in={checked === FIELD.TIME} timeout={400}>
                    <MuiCheckIcon
                      className={classes.checkIcon}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiInputLabel id="time-select">
                    {intl.formatMessage({ id: 'supportChat.timeSelectLabel' })}
                  </MuiInputLabel>
                  <MuiSelect
                    labelId="time-select"
                    name={FIELD.TIME}
                    value={formValue.time?.value || ''}
                    renderValue={() => {
                      const foundTime = times.find(
                        (timeItem) => timeItem.value === formValue.time?.value
                      );

                      return foundTime?.label || '';
                    }}
                    onChange={(
                      event: React.ChangeEvent<{ value: unknown }>
                    ) => {
                      const newTime = times.find(
                        (timeItem) => timeItem.value === event.target.value
                      );

                      if (!newTime) {
                        return;
                      }

                      setFormValue({
                        ...formValue,
                        [FIELD.TIME]: newTime,
                      });

                      if (checked === FIELD.TIME && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.TIME);
                      }
                    }}
                  >
                    {map(times, (timeItem) => (
                      <MuiMenuItem
                        key={timeItem.value}
                        className={classes.item}
                        value={timeItem.value}
                      >
                        <MuiTypography>{timeItem.label}</MuiTypography>
                      </MuiMenuItem>
                    ))}
                  </MuiSelect>
                </MuiFormControl>
              )}
              {key === FIELD.FRACTION && (
                <>
                  <MuiTypography className={classes.toggleButtonGroupLabel}>
                    {intl.formatMessage({ id: 'supportChat.fractionLabel' })}
                    <MuiGrow in={checked === FIELD.FRACTION} timeout={400}>
                      <MuiCheckIcon
                        className={`${classes.checkIcon} ${classes.checkIconLabel}`}
                        fontSize="small"
                      />
                    </MuiGrow>
                  </MuiTypography>
                  <ToggleButtonGroup
                    className={classes.toggleButtonGroup}
                    config={{
                      game: GAME.WOW,
                      name: 'fraction',
                      list: fractions,
                      orientation: 'vertical',
                      size: 'small',
                    }}
                    value={formValue.fraction}
                    setValue={(newValue: ToggleButtonGroupValue) => {
                      const newFraction = fractions.find(
                        (fractionItem) => fractionItem.name === newValue.name
                      );

                      if (!newFraction) {
                        return;
                      }

                      setFormValue({
                        ...formValue,
                        [FIELD.FRACTION]: newFraction,
                      });

                      if (checked === FIELD.FRACTION && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.FRACTION);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.CHARACTER && (
                <MuiFormControl className={classes.select}>
                  <MuiGrow in={checked === FIELD.CHARACTER} timeout={400}>
                    <MuiCheckIcon
                      className={classes.checkIcon}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiInputLabel id="character-select">
                    {intl.formatMessage({
                      id: 'supportChat.characterSelectLabel',
                    })}
                  </MuiInputLabel>
                  <MuiSelect
                    labelId="character-select"
                    name={FIELD.CHARACTER}
                    value={formValue.character?.value || ''}
                    renderValue={() => {
                      const foundCharacter = characters.find(
                        (characterItem) =>
                          characterItem.value === formValue.character?.value
                      );

                      return (
                        <div className={classes.item}>
                          <img
                            className={classes.itemImage}
                            src={`/static/wow/characters/${formValue.character?.value}.jpg`}
                            alt={foundCharacter?.label || ''}
                          />
                          <MuiTypography>
                            {foundCharacter?.label || ''}
                          </MuiTypography>
                        </div>
                      );
                    }}
                    onChange={(
                      event: React.ChangeEvent<{ value: unknown }>
                    ) => {
                      const newCharacter = characters.find(
                        (characterItem) =>
                          characterItem.value === event.target.value
                      );

                      if (!newCharacter) {
                        return;
                      }

                      setFormValue({
                        ...formValue,
                        [FIELD.CHARACTER]: newCharacter,
                      });

                      if (checked === FIELD.CHARACTER && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.CHARACTER);
                      }
                    }}
                  >
                    {map(characters, (characterItem) => (
                      <MuiMenuItem
                        key={characterItem.value}
                        className={classes.item}
                        value={characterItem.value}
                      >
                        <img
                          className={classes.itemImage}
                          srcSet={`/static/wow/characters/${characterItem.value}.jpg`}
                          alt={characterItem.label}
                        />
                        <MuiTypography>{characterItem.label}</MuiTypography>
                      </MuiMenuItem>
                    ))}
                  </MuiSelect>
                </MuiFormControl>
              )}
              {key === FIELD.COVENANT && (
                <>
                  <MuiTypography className={classes.toggleButtonGroupLabel}>
                    {intl.formatMessage({ id: 'supportChat.covenantLabel' })}
                    <MuiGrow in={checked === FIELD.COVENANT} timeout={400}>
                      <MuiCheckIcon
                        className={`${classes.checkIcon} ${classes.checkIconLabel}`}
                        fontSize="small"
                      />
                    </MuiGrow>
                  </MuiTypography>
                  <ToggleButtonGroup
                    className={classes.toggleButtonGroup}
                    config={{
                      game: GAME.WOW,
                      name: 'covenant',
                      list: covenants,
                      orientation: 'vertical',
                      size: 'small',
                    }}
                    value={formValue.covenant}
                    setValue={(newValue: ToggleButtonGroupValue) => {
                      const newCovenant = covenants.find(
                        (covenantItem) => covenantItem.name === newValue.name
                      );

                      if (!newCovenant) {
                        return;
                      }

                      setFormValue({
                        ...formValue,
                        [FIELD.COVENANT]: newCovenant,
                      });

                      if (checked === FIELD.COVENANT && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.COVENANT);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.RAIDS_DIFFICULTY && (
                <MuiFormControl className={classes.select}>
                  <MuiGrow
                    in={checked === FIELD.RAIDS_DIFFICULTY}
                    timeout={400}
                  >
                    <MuiCheckIcon
                      className={classes.checkIcon}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiInputLabel id="raids-difficulty-select">
                    {intl.formatMessage({
                      id: 'wowRaids.difficulty',
                    })}
                  </MuiInputLabel>
                  <MuiSelect
                    labelId="raids-difficulty-select"
                    name={FIELD.RAIDS_DIFFICULTY}
                    value={formValue.raidsDifficulty?.value || ''}
                    renderValue={() => {
                      const foundRaidsDifficulty = raidsDifficults.find(
                        (raidsDifficultyItem) =>
                          raidsDifficultyItem.value ===
                          formValue.raidsDifficulty?.value
                      );

                      return foundRaidsDifficulty?.label || '';
                    }}
                    onChange={(
                      event: React.ChangeEvent<{ value: unknown }>
                    ) => {
                      const newRaidsDifficulty = raidsDifficults.find(
                        (raidsDifficultyItem) =>
                          raidsDifficultyItem.value === event.target.value
                      );

                      if (!newRaidsDifficulty) {
                        return;
                      }

                      setFormValue({
                        ...formValue,
                        [FIELD.RAIDS_DIFFICULTY]: newRaidsDifficulty,
                      });

                      if (checked === FIELD.RAIDS_DIFFICULTY && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.RAIDS_DIFFICULTY);
                      }
                    }}
                  >
                    {map(raidsDifficults, (raidsDifficultyItem) => (
                      <MuiMenuItem
                        key={raidsDifficultyItem.value}
                        className={classes.item}
                        value={raidsDifficultyItem.value}
                      >
                        <MuiTypography>
                          {raidsDifficultyItem.label}
                        </MuiTypography>
                      </MuiMenuItem>
                    ))}
                  </MuiSelect>
                </MuiFormControl>
              )}
              {key === FIELD.ILVL && (
                <>
                  <MuiGrow in={checked === FIELD.ILVL} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconField}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiTextField
                    className={classes.textField}
                    label={intl.formatMessage({
                      id: 'supportChat.ilvlLabel',
                    })}
                    name={FIELD.ILVL}
                    variant="outlined"
                    type="number"
                    size="small"
                    value={formValue.ilvl}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.ILVL]: event.target.value,
                      });

                      if (checked === FIELD.ILVL && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.ILVL);
                      }
                    }}
                    onKeyPress={(event) => {
                      const element = event.target as HTMLInputElement;

                      if (!element || event.key !== 'Enter') {
                        return;
                      }

                      element.blur();
                    }}
                  />
                </>
              )}
              {key === FIELD.RATING && (
                <>
                  <MuiGrow in={checked === FIELD.RATING} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconField}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiTextField
                    className={classes.textField}
                    label={intl.formatMessage({
                      id: 'supportChat.ratingLabel',
                    })}
                    name={FIELD.RATING}
                    variant="outlined"
                    type="number"
                    size="small"
                    value={formValue.rating}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.RATING]: event.target.value,
                      });

                      if (checked === FIELD.RATING && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.RATING);
                      }
                    }}
                    onKeyPress={(event) => {
                      const element = event.target as HTMLInputElement;

                      if (!element || event.key !== 'Enter') {
                        return;
                      }

                      element.blur();
                    }}
                  />
                </>
              )}
              {key === FIELD.ARMORY && (
                <>
                  <MuiGrow in={checked === FIELD.ARMORY} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconField}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <MuiTextField
                    className={classes.textField}
                    label={intl.formatMessage({
                      id: 'supportChat.armoryLabel',
                    })}
                    name={FIELD.ARMORY}
                    variant="outlined"
                    size="small"
                    value={formValue.armory}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.ARMORY]: event.target.value,
                      });

                      if (checked === FIELD.ARMORY && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.ARMORY);
                      }
                    }}
                    onKeyPress={(event) => {
                      const element = event.target as HTMLInputElement;

                      if (!element || event.key !== 'Enter') {
                        return;
                      }

                      element.blur();
                    }}
                  />
                </>
              )}
              {key === FIELD.TRADERS && (
                <>
                  <MuiGrow in={checked === FIELD.TRADERS} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconSelectList}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <SelectList
                    className={classes.selectList}
                    config={{
                      list: traders,
                      game: GAME.WOW,
                      name: 'traders',
                      label: intl.formatMessage({ id: 'wow.traderLabel' }),
                      buttonLabel: intl.formatMessage({
                        id: 'wow.traderButton',
                      }),
                      max: 4,
                    }}
                    value={formValue.traders || []}
                    setValue={(newValue: SelectListValue[]) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.TRADERS]: newValue,
                      });

                      if (checked === FIELD.TRADERS && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.TRADERS);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.SELFPLAY && (
                <>
                  <MuiGrow in={checked === FIELD.SELFPLAY} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconCheckbox}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <Checkbox
                    className={classes.checkbox}
                    name={FIELD.SELFPLAY}
                    label={intl.formatMessage({
                      id: 'supportChat.selfplayLabel',
                    })}
                    helperText={intl.formatMessage({
                      id: 'supportChat.selfplayDescription',
                    })}
                    color="primary"
                    size="small"
                    checked={!!value}
                    onChange={(
                      _: React.ChangeEvent<HTMLInputElement>,
                      newChecked: boolean
                    ) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.SELFPLAY]: newChecked,
                      });

                      if (checked === FIELD.SELFPLAY && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.SELFPLAY);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.TIMER && (
                <>
                  <MuiGrow in={checked === FIELD.TIMER} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconCheckbox}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <Checkbox
                    className={classes.checkbox}
                    name={FIELD.TIMER}
                    label={intl.formatMessage({
                      id: 'wow.timerCheckboxLabel',
                    })}
                    helperText={intl.formatMessage({
                      id: 'wow.timerCheckboxDescription',
                    })}
                    color="primary"
                    size="small"
                    checked={!!value}
                    onChange={(
                      _: React.ChangeEvent<HTMLInputElement>,
                      newChecked: boolean
                    ) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.TIMER]: newChecked,
                      });

                      if (checked === FIELD.TIMER && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.TIMER);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.STREAM && (
                <>
                  <MuiGrow in={checked === FIELD.STREAM} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconCheckbox}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <Checkbox
                    className={classes.checkbox}
                    name={FIELD.STREAM}
                    label={intl.formatMessage({
                      id: 'supportChat.streamLabel',
                    })}
                    color="primary"
                    size="small"
                    checked={!!value}
                    onChange={(
                      _: React.ChangeEvent<HTMLInputElement>,
                      newChecked: boolean
                    ) => {
                      setFormValue({
                        ...formValue,
                        [FIELD.STREAM]: newChecked,
                      });

                      if (checked === FIELD.STREAM && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.STREAM);
                      }
                    }}
                  />
                </>
              )}
              {key === FIELD.VPN && (
                <>
                  <MuiGrow in={checked === FIELD.VPN} timeout={400}>
                    <MuiCheckIcon
                      className={`${classes.checkIcon} ${classes.checkIconCheckbox}`}
                      fontSize="small"
                    />
                  </MuiGrow>
                  <Checkbox
                    className={classes.checkbox}
                    name={FIELD.VPN}
                    label={intl.formatMessage({ id: 'supportChat.vpnLabel' })}
                    color="primary"
                    size="small"
                    checked={!!value}
                    onChange={(
                      _: React.ChangeEvent<HTMLInputElement>,
                      newChecked: boolean
                    ) => {
                      setFormValue({ ...formValue, [FIELD.VPN]: newChecked });

                      if (checked === FIELD.VPN && timer) {
                        clearTimeout(timer);

                        setTimer(
                          setTimeout(() => {
                            setChecked('');
                          }, 800)
                        );
                      } else {
                        setChecked(FIELD.VPN);
                      }
                    }}
                  />
                </>
              )}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default ChatraDetails;
