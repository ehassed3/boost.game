import * as React from 'react';
import { scroller } from 'react-scroll';

import MuiChip from '@material-ui/core/Chip';

interface Props {
  className?: string;
  name: string;
  label: string;
}

const ScrollChip: React.FC<Props> = ({ className, name, label }) => {
  const onChipClick = () => {
    scroller.scrollTo(name, { smooth: true });
  };

  return (
    <MuiChip
      className={className}
      label={label}
      color="primary"
      onClick={onChipClick}
    />
  );
};

export default ScrollChip;
