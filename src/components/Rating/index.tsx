import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiRating from '@material-ui/lab/Rating';
import MuiTypography from '@material-ui/core/Typography';
import MuiDialog from '@material-ui/core/Dialog';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import Review from '@components/Reviews/Review';

import RatingType from '@interfaces/rating';

const useStyles = makeStyles((theme: Theme) => ({
  '@keyframes pulse': {
    '0%': {
      opacity: 1,
    },
    '50%': {
      opacity: 0.4,
    },
    '100%': {
      opacity: 1,
    },
  },
  rating: {
    cursor: 'pointer',
    display: 'flex',
    outline: 0,
    position: 'relative',
  },
  ratingSkeleton: {
    alignItems: 'center',
    animation: '$pulse 1.5s ease-in-out 0.5s infinite',
    display: 'flex',
    height: '32px',
    position: 'relative',
  },
  stars: {
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.4rem',
    },
  },
  starsSkeleton: {
    color: '#282828',

    [theme.breakpoints.down('xs')]: {
      fontSize: '1.4rem',
    },
  },
  text: {
    ...theme.typography.h6,

    color: '#faaf00',
    fontWeight: 500,
    marginRight: '12px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.body1,

      marginRight: '8px',
    },
  },
  textSkeleton: {
    backgroundColor: '#282828',
    borderRadius: '4px',
    height: '16px',
    marginRight: '12px',
    width: '14px',

    [theme.breakpoints.down('xs')]: {
      height: '12px',
      marginRight: '8px',
    },
  },
  title: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    borderRadius: '8px',
    bottom: '-16px',
    color: '#faaf00',
    fontWeight: 600,
    padding: '0 4px',
    position: 'absolute',
    right: '0',
    whiteSpace: 'nowrap',
    zIndex: 1,

    [theme.breakpoints.down('xs')]: {
      bottom: '-22px',
    },
  },
  titleWrapperSkeleton: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    borderRadius: '8px',
    bottom: '-12px',
    padding: '7px 5px',
    position: 'absolute',
    right: '0',
    zIndex: 1,
  },
  titleSkeleton: {
    backgroundColor: '#282828',
    borderRadius: '4px',
    height: '10px',
    width: '80px',
  },
  dialog: {
    padding: '16px 12px 0',
  },
  dialogTitle: {
    ...theme.typography.h5,

    fontWeight: 600,
    marginBottom: '20px',
    paddingLeft: '8px',
  },
  reviews: {
    overflowY: 'scroll',
    paddingBottom: '16px',
  },
  review: {
    '&:not(:last-of-type)': {
      borderBottom: `1px solid ${theme.palette.divider}`,
      marginBottom: '20px',
      paddingBottom: '20px',
    },
  },
  close: {
    position: 'absolute',
    right: '12px',
    top: '10px',
  },
}));

interface Props {
  className?: string;
  rating?: RatingType;
}

const Rating: React.FC<Props> = ({ className, rating }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  return rating ? (
    <>
      {!!rating.value && (
        <>
          <div
            className={
              className ? `${className} ${classes.rating}` : classes.rating
            }
            onClick={() => {
              setOpen(true);
            }}
            onKeyPress={(event: React.KeyboardEvent<HTMLDivElement>) => {
              if (event.key !== 'Enter') {
                return;
              }

              setOpen(true);
            }}
            role="button"
            tabIndex={0}
          >
            <MuiTypography className={classes.text}>
              {rating.value}
            </MuiTypography>
            <MuiRating
              className={classes.stars}
              size="large"
              readOnly
              value={rating.value}
              precision={0.1}
            />
            <MuiTypography className={classes.title}>
              {intl.formatMessage(
                { id: 'rating.title' },
                { reviews: rating.totalReviews }
              )}
            </MuiTypography>
          </div>
          {open && (
            <MuiDialog
              classes={{ paper: classes.dialog }}
              maxWidth="xs"
              fullWidth
              open={open}
              onClose={() => {
                setOpen(false);
              }}
            >
              <MuiIconButton
                className={classes.close}
                onClick={() => setOpen(false)}
              >
                <MuiCloseIcon fontSize="small" />
              </MuiIconButton>
              <MuiTypography className={classes.dialogTitle}>
                {intl.formatMessage({ id: 'rating.dialogTitle' })}
              </MuiTypography>
              <div className={classes.reviews}>
                {rating.reviews.map((review, index) => (
                  <Review
                    key={index}
                    className={classes.review}
                    review={review}
                  />
                ))}
              </div>
            </MuiDialog>
          )}
        </>
      )}
    </>
  ) : (
    <div
      className={
        className
          ? `${className} ${classes.ratingSkeleton}`
          : classes.ratingSkeleton
      }
    >
      <div className={classes.textSkeleton} />
      <MuiRating
        className={classes.starsSkeleton}
        size="large"
        readOnly
        value={5}
      />
      <div className={classes.titleWrapperSkeleton}>
        <div className={classes.titleSkeleton} />
      </div>
    </div>
  );
};

export default Rating;
