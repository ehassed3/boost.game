import * as React from 'react';

import {
  makeStyles,
  withStyles,
  createStyles,
  Theme,
} from '@material-ui/core/styles';
import MuiBadge from '@material-ui/core/Badge';
import MuiAvatar, { AvatarProps } from '@material-ui/core/Avatar';

import STATUS from '@constants/status';

const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      backgroundColor: theme.palette.secondary.main,
      borderRadius: '50%',
      bottom: '-14%',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      color: theme.palette.secondary.main,
      height: '8px',
      right: '22%',
      top: 'auto',
      width: '8px',

      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.6s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2)',
        opacity: 0,
      },
    },
  })
)(MuiBadge);

const useStyles = makeStyles((theme: Theme) => ({
  badge: {
    borderRadius: '50%',
    height: '34px',
    width: '34px',
  },
  badgeAvatar: {
    backgroundColor: theme.palette.background.default,
    height: '100%',
    width: '100%',
  },
  avatar: {
    backgroundColor: theme.palette.background.default,
    height: '34px',
    width: '34px',
  },
}));

interface Props extends AvatarProps {
  status?: STATUS;
}

const Avatar: React.FC<Props> = (props) => {
  const { className, src, status } = props;

  const classes = useStyles();

  return status === STATUS.LIVE ? (
    <StyledBadge
      className={className ? `${className} ${classes.badge}` : classes.badge}
      overlap="circular"
      variant="dot"
    >
      <MuiAvatar
        {...props}
        className={classes.badgeAvatar}
        src={src || '/static/svg/account.svg'}
        alt="avatar"
      />
    </StyledBadge>
  ) : (
    <MuiAvatar
      {...props}
      className={className ? `${className} ${classes.avatar}` : classes.avatar}
      src={src || '/static/svg/account.svg'}
      alt="avatar"
    />
  );
};

export default Avatar;
