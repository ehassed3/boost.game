import * as React from 'react';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiToggleButton from '@material-ui/lab/ToggleButton';
import MuiToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import GAME from '@constants/game';

const useStyles = makeStyles((theme: Theme) => ({
  toggleButtonGroup: {
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      width: '100%',
    },
  },
  button: {
    [theme.breakpoints.down('xs')]: {
      '&:not(:first-child)': {
        borderLeft: '1px solid rgba(255, 255, 255, 0.12)',
        borderTop: '1px solid transparent',
        marginTop: '-1px',
        marginLeft: '0',
        borderBottomLeftRadius: '4px',
        borderTopLeftRadius: '0',
        borderTopRightRadius: '0',
      },

      '&:not(:last-child)': {
        borderBottomLeftRadius: '0',
        borderBottomRightRadius: '0',
      },

      '&:last-child': {
        borderTopRightRadius: '0',
      },
    },
  },
  picture: {
    marginRight: '6px',
    height: '24px',
    width: '16px',
  },
  image: {
    borderRadius: '6px',
    height: '100%',
    width: '100%',
  },
}));

export interface Value {
  label: string;
  name: string;
}

export interface Config {
  game: GAME;
  name: string;
  list: Value[];
  orientation?: 'horizontal' | 'vertical';
  size?: 'small' | 'medium' | 'large';
}

interface Props {
  className?: string;
  config: Config;
  value?: Value;
  setValue: (value: Value) => void;
}

const ToggleButtonGroup: React.FC<Props> = ({
  className,
  config,
  value,
  setValue,
}) => {
  const classes = useStyles();

  return (
    <MuiToggleButtonGroup
      className={
        className
          ? `${className} ${classes.toggleButtonGroup}`
          : classes.toggleButtonGroup
      }
      value={value?.name}
      orientation={config.orientation}
      size={config.size}
      exclusive
      onChange={(_: React.MouseEvent<HTMLElement>, newValue: string) => {
        const foundValue = config.list.find((item) => item.name === newValue);

        if (!foundValue) {
          return;
        }

        setValue(foundValue);
      }}
    >
      {map(config.list, (item) => (
        <MuiToggleButton
          key={item.name}
          className={classes.button}
          value={item.name}
        >
          <picture
            className={classes.picture}
            style={{
              opacity: value?.name !== item.name ? '0.5' : '1',
            }}
          >
            <source
              type="image/avif"
              srcSet={`/static/${config.game}/${config.name}/${item.name}.avif`}
            />
            <source
              type="image/webp"
              srcSet={`/static/${config.game}/${config.name}/${item.name}.webp`}
            />
            <img
              className={classes.image}
              alt={item.label}
              src={`/static/${config.game}/${config.name}/${item.name}.png`}
            />
          </picture>
          {item.label}
        </MuiToggleButton>
      ))}
    </MuiToggleButtonGroup>
  );
};

export default ToggleButtonGroup;
