import * as React from 'react';

import MuiLink from '@material-ui/core/Link';

const getColor = (mythic?: boolean, toy?: boolean) => {
  if (mythic) {
    return '#a335ee';
  }

  if (toy) {
    return '#0070dd';
  }

  return '#f1be44';
};

interface Props {
  className?: string;
  href: string;
  mythic?: boolean;
  toy?: boolean;
}

const WowHeadLink: React.FC<Props> = ({
  className,
  href,
  mythic,
  toy,
  children,
}) => (
  <MuiLink
    className={className}
    style={{ color: getColor(mythic, toy) }}
    href={href}
    data-wowhead={href}
    target="_blank"
    rel="noopener"
    underline="none"
  >
    {children}
  </MuiLink>
);

export default WowHeadLink;
