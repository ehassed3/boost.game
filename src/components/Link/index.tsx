import * as React from 'react';
import NextLink from 'next/link';

import { makeStyles } from '@material-ui/core/styles';
import MuiLink from '@material-ui/core/Link';

const useStyles = makeStyles(() => ({
  link: {
    position: 'relative',
  },
  line: {
    bottom: '-6px',
    height: 'auto',
    left: '8px',
    position: 'absolute',
    right: '8px',
    width: 'auto',
  },
}));

interface Props {
  className?: string;
  component?: React.ElementType;
  href: string;
  as?: string;
  target?: string;
  onClick?: (event: React.MouseEvent<HTMLAnchorElement>) => void;
  children?: React.ReactNode;
}

const Link = (
  {
    className,
    component,
    href,
    as = href,
    target,
    onClick,
    children,
    ...props
  }: Props,
  ref: React.Ref<HTMLAnchorElement>
) => {
  const classes = useStyles();

  return (
    <NextLink href={href} as={as} scroll={false}>
      <MuiLink
        {...props}
        className={className ? `${className} ${classes.link}` : classes.link}
        component={component || 'a'}
        href={as}
        target={target}
        ref={ref}
        onClick={onClick}
      >
        {children}
      </MuiLink>
    </NextLink>
  );
};

export default React.forwardRef(Link);
