import * as React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import GameCards from '@components/Game/Cards';
import Poe from '@components/Game/Poe';
import Dota2 from '@components/Game/Dota2';
import Wow from '@components/Game/Wow';
import WowClassic from '@components/Game/WowClassic';

import GAMES from '@constants/games';
import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const FORM: Record<
  string,
  React.ComponentType<{
    initial?: CATEGORY;
    isVisible: boolean;
  }>
> = {
  [GAME.POE]: Poe,
  [GAME.DOTA_2]: Dota2,
  [GAME.WOW]: Wow,
  [GAME.WOW_CLASSIC]: WowClassic,
};

interface Props {
  initial: GAME;
  initialCategory?: CATEGORY;
}

const Game: React.FC<Props> = ({ initial, initialCategory }) => {
  const [activeCard, setActiveCard] = React.useState(initial);
  const [isVisibleForm, setIsVisibleForm] = React.useState(true);
  const [formInitialCategory, setFormInitialCategory] = React.useState(
    initialCategory
  );

  const onCardsChange = (card: GAME) => {
    setActiveCard(card);
    setIsVisibleForm(false);
    setFormInitialCategory(undefined);
  };

  return (
    <>
      <GameCards initial={initial} onChange={onCardsChange} />
      <AnimatePresence
        initial={false}
        onExitComplete={() => {
          setIsVisibleForm(true);
        }}
      >
        {isVisibleForm && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.4 } }}
          >
            {map(GAMES, (game) =>
              React.createElement(FORM[game], {
                key: game,
                initial: formInitialCategory,
                isVisible: game === activeCard,
              })
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default Game;
