import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getDota2BoostResult from '@helpers/getOrderData/getResult/getDota2BoostResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { Dota2BoostFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '108% 74%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '108% 80%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Boost: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<Dota2BoostFormValue>({
    booster: false,
    heroes: false,
    ranks: [2250, 4500],
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.DOTA_2][POSITION.BOOST].rating
      ? ''
      : `/api/get_rating/?game=${GAME.DOTA_2}&position=${POSITION.BOOST}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.DOTA_2]: {
        ...game[GAME.DOTA_2],
        [POSITION.BOOST]: { rating },
      },
    });
  }, [rating]);

  return (
    <Position<Dota2BoostFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.DOTA_2}
      position={POSITION.BOOST}
      rating={game[GAME.DOTA_2][POSITION.BOOST].rating}
      description={{
        info: [intl.formatMessage({ id: 'dota2Boost.infoDescription' })],
        eta: [intl.formatMessage({ id: 'dota2Boost.etaDescription' })],
        safe: [intl.formatMessage({ id: 'dota2Boost.safeDescription' })],
        requirements: [
          intl.formatMessage({ id: 'dota2Boost.requirementsDescription' }),
        ],
      }}
      form={{
        duoSlider: {
          name: 'ranks',
          min: 0,
          max: 7000,
          startLabel: intl.formatMessage({ id: 'dota2Boost.sliderStartLabel' }),
          endLabel: intl.formatMessage({ id: 'dota2Boost.sliderEndLabel' }),
          adornments: [
            {
              image: '/static/dota_2/ranks/herald_1.png',
              imageAvif: '/static/dota_2/ranks/herald_1.avif',
              imageWebp: '/static/dota_2/ranks/herald_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [1]`,
              value: 0,
            },
            {
              image: '/static/dota_2/ranks/herald_2.png',
              imageAvif: '/static/dota_2/ranks/herald_2.avif',
              imageWebp: '/static/dota_2/ranks/herald_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [2]`,
              value: 175,
            },
            {
              image: '/static/dota_2/ranks/herald_3.png',
              imageAvif: '/static/dota_2/ranks/herald_3.avif',
              imageWebp: '/static/dota_2/ranks/herald_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [3]`,
              value: 325,
            },
            {
              image: '/static/dota_2/ranks/herald_4.png',
              imageAvif: '/static/dota_2/ranks/herald_4.avif',
              imageWebp: '/static/dota_2/ranks/herald_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [4]`,
              value: 475,
            },
            {
              image: '/static/dota_2/ranks/herald_5.png',
              imageAvif: '/static/dota_2/ranks/herald_5.avif',
              imageWebp: '/static/dota_2/ranks/herald_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [5]`,
              value: 625,
            },
            {
              image: '/static/dota_2/ranks/guardian_1.png',
              imageAvif: '/static/dota_2/ranks/guardian_1.avif',
              imageWebp: '/static/dota_2/ranks/guardian_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [1]`,
              value: 775,
            },
            {
              image: '/static/dota_2/ranks/guardian_2.png',
              imageAvif: '/static/dota_2/ranks/guardian_2.avif',
              imageWebp: '/static/dota_2/ranks/guardian_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [2]`,
              value: 925,
            },
            {
              image: '/static/dota_2/ranks/guardian_3.png',
              imageAvif: '/static/dota_2/ranks/guardian_3.avif',
              imageWebp: '/static/dota_2/ranks/guardian_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [3]`,
              value: 1100,
            },
            {
              image: '/static/dota_2/ranks/guardian_4.png',
              imageAvif: '/static/dota_2/ranks/guardian_4.avif',
              imageWebp: '/static/dota_2/ranks/guardian_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [4]`,
              value: 1250,
            },
            {
              image: '/static/dota_2/ranks/guardian_5.png',
              imageAvif: '/static/dota_2/ranks/guardian_5.avif',
              imageWebp: '/static/dota_2/ranks/guardian_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [5]`,
              value: 1400,
            },
            {
              image: '/static/dota_2/ranks/crusader_1.png',
              imageAvif: '/static/dota_2/ranks/crusader_1.avif',
              imageWebp: '/static/dota_2/ranks/crusader_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [1]`,
              value: 1550,
            },
            {
              image: '/static/dota_2/ranks/crusader_2.png',
              imageAvif: '/static/dota_2/ranks/crusader_2.avif',
              imageWebp: '/static/dota_2/ranks/crusader_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [2]`,
              value: 1700,
            },
            {
              image: '/static/dota_2/ranks/crusader_3.png',
              imageAvif: '/static/dota_2/ranks/crusader_3.avif',
              imageWebp: '/static/dota_2/ranks/crusader_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [3]`,
              value: 1850,
            },
            {
              image: '/static/dota_2/ranks/crusader_4.png',
              imageAvif: '/static/dota_2/ranks/crusader_4.avif',
              imageWebp: '/static/dota_2/ranks/crusader_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [4]`,
              value: 2025,
            },
            {
              image: '/static/dota_2/ranks/crusader_5.png',
              imageAvif: '/static/dota_2/ranks/crusader_5.avif',
              imageWebp: '/static/dota_2/ranks/crusader_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [5]`,
              value: 2175,
            },
            {
              image: '/static/dota_2/ranks/archon_1.png',
              imageAvif: '/static/dota_2/ranks/archon_1.avif',
              imageWebp: '/static/dota_2/ranks/archon_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [1]`,
              value: 2325,
            },
            {
              image: '/static/dota_2/ranks/archon_2.png',
              imageAvif: '/static/dota_2/ranks/archon_2.avif',
              imageWebp: '/static/dota_2/ranks/archon_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [2]`,
              value: 2475,
            },
            {
              image: '/static/dota_2/ranks/archon_3.png',
              imageAvif: '/static/dota_2/ranks/archon_3.avif',
              imageWebp: '/static/dota_2/ranks/archon_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [3]`,
              value: 2625,
            },
            {
              image: '/static/dota_2/ranks/archon_4.png',
              imageAvif: '/static/dota_2/ranks/archon_4.avif',
              imageWebp: '/static/dota_2/ranks/archon_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [4]`,
              value: 2775,
            },
            {
              image: '/static/dota_2/ranks/archon_5.png',
              imageAvif: '/static/dota_2/ranks/archon_5.avif',
              imageWebp: '/static/dota_2/ranks/archon_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [5]`,
              value: 2950,
            },
            {
              image: '/static/dota_2/ranks/legend_1.png',
              imageAvif: '/static/dota_2/ranks/legend_1.avif',
              imageWebp: '/static/dota_2/ranks/legend_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [1]`,
              value: 3100,
            },
            {
              image: '/static/dota_2/ranks/legend_2.png',
              imageAvif: '/static/dota_2/ranks/legend_2.avif',
              imageWebp: '/static/dota_2/ranks/legend_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [2]`,
              value: 3250,
            },
            {
              image: '/static/dota_2/ranks/legend_3.png',
              imageAvif: '/static/dota_2/ranks/legend_3.avif',
              imageWebp: '/static/dota_2/ranks/legend_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [3]`,
              value: 3400,
            },
            {
              image: '/static/dota_2/ranks/legend_4.png',
              imageAvif: '/static/dota_2/ranks/legend_4.avif',
              imageWebp: '/static/dota_2/ranks/legend_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [4]`,
              value: 3550,
            },
            {
              image: '/static/dota_2/ranks/legend_5.png',
              imageAvif: '/static/dota_2/ranks/legend_5.avif',
              imageWebp: '/static/dota_2/ranks/legend_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [5]`,
              value: 3700,
            },
            {
              image: '/static/dota_2/ranks/ancient_1.png',
              imageAvif: '/static/dota_2/ranks/ancient_1.avif',
              imageWebp: '/static/dota_2/ranks/ancient_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [1]`,
              value: 3850,
            },
            {
              image: '/static/dota_2/ranks/ancient_2.png',
              imageAvif: '/static/dota_2/ranks/ancient_2.avif',
              imageWebp: '/static/dota_2/ranks/ancient_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [2]`,
              value: 4025,
            },
            {
              image: '/static/dota_2/ranks/ancient_3.png',
              imageAvif: '/static/dota_2/ranks/ancient_3.avif',
              imageWebp: '/static/dota_2/ranks/ancient_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [3]`,
              value: 4175,
            },
            {
              image: '/static/dota_2/ranks/ancient_4.png',
              imageAvif: '/static/dota_2/ranks/ancient_4.avif',
              imageWebp: '/static/dota_2/ranks/ancient_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [4]`,
              value: 4325,
            },
            {
              image: '/static/dota_2/ranks/ancient_5.png',
              imageAvif: '/static/dota_2/ranks/ancient_5.avif',
              imageWebp: '/static/dota_2/ranks/ancient_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [5]`,
              value: 4475,
            },
            {
              image: '/static/dota_2/ranks/divine_1.png',
              imageAvif: '/static/dota_2/ranks/divine_1.avif',
              imageWebp: '/static/dota_2/ranks/divine_1.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [1]`,
              value: 4625,
            },
            {
              image: '/static/dota_2/ranks/divine_2.png',
              imageAvif: '/static/dota_2/ranks/divine_2.avif',
              imageWebp: '/static/dota_2/ranks/divine_2.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [2]`,
              value: 4825,
            },
            {
              image: '/static/dota_2/ranks/divine_3.png',
              imageAvif: '/static/dota_2/ranks/divine_3.avif',
              imageWebp: '/static/dota_2/ranks/divine_3.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [3]`,
              value: 5025,
            },
            {
              image: '/static/dota_2/ranks/divine_4.png',
              imageAvif: '/static/dota_2/ranks/divine_4.avif',
              imageWebp: '/static/dota_2/ranks/divine_4.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [4]`,
              value: 5225,
            },
            {
              image: '/static/dota_2/ranks/divine_5.png',
              imageAvif: '/static/dota_2/ranks/divine_5.avif',
              imageWebp: '/static/dota_2/ranks/divine_5.webp',
              name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [5]`,
              value: 5425,
            },
            {
              image: '/static/dota_2/ranks/immortal.png',
              imageAvif: '/static/dota_2/ranks/immortal.avif',
              imageWebp: '/static/dota_2/ranks/immortal.webp',
              name: intl.formatMessage({ id: 'dota2.rankTitan' }),
              value: 7000,
            },
          ],
          maxLength: 4,
          step: 25,
          marks: [
            {
              value: 1000,
              label: '1000',
            },
            {
              value: 2000,
              label: '2000',
            },
            {
              value: 3000,
              label: '3000',
            },
            {
              value: 4000,
              label: '4000',
            },
            {
              value: 5000,
              label: '5000',
            },
            {
              value: 6000,
              label: '6000',
            },
          ],
        },
        checkboxes: [
          {
            name: 'heroes',
            label: intl.formatMessage(
              {
                id: 'dota2.heroesPriceCheckboxLabel',
              },
              { price: '+15%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.heroesCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'dota2.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.streamCheckboxDescription',
            }),
          },
          {
            name: 'booster',
            label: intl.formatMessage(
              {
                id: 'dota2.boosterPriceCheckboxLabel',
              },
              { price: '+60%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.boosterCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getDota2BoostResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        currency,
        formValue,
        game: GAME.DOTA_2,
        locale: intl.locale,
        position: POSITION.BOOST,
        promocode,
      }}
    />
  );
};

export default Boost;
