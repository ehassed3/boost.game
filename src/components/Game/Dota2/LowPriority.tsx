import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getDota2LowPriorityResult from '@helpers/getOrderData/getResult/getDota2LowPriorityResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { Dota2LowPriorityFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '108% 40%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const LowPriority: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<Dota2LowPriorityFormValue>({
    booster: false,
    games: 5,
    heroes: false,
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.DOTA_2][POSITION.LOW_PRIORITY].rating
      ? ''
      : `/api/get_rating/?game=${GAME.DOTA_2}&position=${POSITION.LOW_PRIORITY}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.DOTA_2]: {
        ...game[GAME.DOTA_2],
        [POSITION.LOW_PRIORITY]: { rating },
      },
    });
  }, [rating]);

  return (
    <Position<Dota2LowPriorityFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.DOTA_2}
      position={POSITION.LOW_PRIORITY}
      rating={game[GAME.DOTA_2][POSITION.LOW_PRIORITY].rating}
      description={{
        info: [intl.formatMessage({ id: 'dota2LowPriority.infoDescription' })],
        eta: [intl.formatMessage({ id: 'dota2LowPriority.etaDescription' })],
        safe: [intl.formatMessage({ id: 'dota2LowPriority.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'dota2LowPriority.requirementsDescription',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'games',
          label: intl.formatMessage({
            id: 'dota2LowPriority.sliderLabel',
          }),
          min: 1,
          max: 10,
          step: 1,
          maxLength: 2,
          marks: [
            {
              value: 2,
              label: '2',
            },
            {
              value: 4,
              label: '4',
            },
            {
              value: 6,
              label: '6',
            },
            {
              value: 8,
              label: '8',
            },
          ],
        },
        checkboxes: [
          {
            name: 'heroes',
            label: intl.formatMessage(
              {
                id: 'dota2.heroesPriceCheckboxLabel',
              },
              { price: '+15%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.heroesCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'dota2.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.streamCheckboxDescription',
            }),
          },
          {
            name: 'booster',
            label: intl.formatMessage(
              {
                id: 'dota2.boosterPriceCheckboxLabel',
              },
              { price: '+60%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.boosterCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getDota2LowPriorityResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        currency,
        formValue,
        game: GAME.DOTA_2,
        locale: intl.locale,
        position: POSITION.LOW_PRIORITY,
        promocode,
      }}
    />
  );
};

export default LowPriority;
