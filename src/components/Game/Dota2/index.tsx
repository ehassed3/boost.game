import * as React from 'react';

import GamePositions from '@components/Game/Positions';
import Boost from '@components/Game/Dota2/Boost';
import Calibration from '@components/Game/Dota2/Calibration';
import LowPriority from '@components/Game/Dota2/LowPriority';

import GAME from '@constants/game';
import POSITION from '@constants/position';

interface Props {
  isVisible: boolean;
}

const Dota2: React.FC<Props> = ({ isVisible }) => (
  <>
    {isVisible && (
      <GamePositions
        game={GAME.DOTA_2}
        positions={[
          { name: POSITION.BOOST, component: Boost },
          { name: POSITION.CALIBRATION, component: Calibration },
          { name: POSITION.LOW_PRIORITY, component: LowPriority },
        ]}
      />
    )}
  </>
);

export default Dota2;
