import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import findIndex from 'lodash/findIndex';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTextField from '@material-ui/core/TextField';
import MuiTypography from '@material-ui/core/Typography';

import Position from '@components/Game/Positions/Position';
import { Adornment } from '@components/FieldSlider';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getDota2CalibrationResult from '@helpers/getOrderData/getResult/getDota2CalibrationResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { Dota2CalibrationFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '64% 100%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '48% 100%',
    },
  },
  games: {
    width: '140px',
  },
  resultCalibration: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: '28px',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'row',
      marginBottom: '40px',
      marginTop: '0',
      width: '100%',
    },

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  resultCalibrationText: {
    ...theme.typography.h4,
    marginBottom: '32px',
    textAlign: 'center',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.h5,
      marginBottom: '0',
      marginRight: '8px',
    },

    [theme.breakpoints.down('xs')]: {
      marginBottom: '8px',
    },
  },
  resultCalibrationImage: {
    height: '256px',
    width: '256px',

    [theme.breakpoints.down('sm')]: {
      height: '80px',
      width: '80px',
    },
  },
  resultCalibrationName: {
    ...theme.typography.h6,
    marginTop: '24px',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '8px',
      marginRight: '12px',
      marginTop: '0',
    },

    [theme.breakpoints.down('xs')]: {
      marginTop: '8px',
    },
  },
}));

export const getCalibrateRank = (
  formRank: number,
  ranks: Adornment[]
): Adornment => {
  const rankIndex = findIndex(ranks, (rank) => rank.value >= formRank);

  if (!rankIndex) {
    return ranks[22];
  }

  return ranks[rankIndex];
};

interface Props {
  className?: string;
  colored?: boolean;
}

const Calibration: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const ranks = React.useMemo(
    () => [
      {
        image: '/static/dota_2/ranks/uncalibrated.png',
        imageAvif: '/static/dota_2/ranks/uncalibrated.avif',
        imageWebp: '/static/dota_2/ranks/uncalibrated.webp',
        name: intl.formatMessage({ id: 'dota2.rankUncalibrated' }),
        value: 0,
      },
      {
        image: '/static/dota_2/ranks/herald_2.png',
        imageAvif: '/static/dota_2/ranks/herald_2.avif',
        imageWebp: '/static/dota_2/ranks/herald_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [2]`,
        value: 175,
      },
      {
        image: '/static/dota_2/ranks/herald_3.png',
        imageAvif: '/static/dota_2/ranks/herald_3.avif',
        imageWebp: '/static/dota_2/ranks/herald_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [3]`,
        value: 325,
      },
      {
        image: '/static/dota_2/ranks/herald_4.png',
        imageAvif: '/static/dota_2/ranks/herald_4.avif',
        imageWebp: '/static/dota_2/ranks/herald_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [4]`,
        value: 475,
      },
      {
        image: '/static/dota_2/ranks/herald_5.png',
        imageAvif: '/static/dota_2/ranks/herald_5.avif',
        imageWebp: '/static/dota_2/ranks/herald_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankHerald' })} [5]`,
        value: 625,
      },
      {
        image: '/static/dota_2/ranks/guardian_1.png',
        imageAvif: '/static/dota_2/ranks/guardian_1.avif',
        imageWebp: '/static/dota_2/ranks/guardian_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [1]`,
        value: 775,
      },
      {
        image: '/static/dota_2/ranks/guardian_2.png',
        imageAvif: '/static/dota_2/ranks/guardian_2.avif',
        imageWebp: '/static/dota_2/ranks/guardian_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [2]`,
        value: 925,
      },
      {
        image: '/static/dota_2/ranks/guardian_3.png',
        imageAvif: '/static/dota_2/ranks/guardian_3.avif',
        imageWebp: '/static/dota_2/ranks/guardian_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [3]`,
        value: 1100,
      },
      {
        image: '/static/dota_2/ranks/guardian_4.png',
        imageAvif: '/static/dota_2/ranks/guardian_4.avif',
        imageWebp: '/static/dota_2/ranks/guardian_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [4]`,
        value: 1250,
      },
      {
        image: '/static/dota_2/ranks/guardian_5.png',
        imageAvif: '/static/dota_2/ranks/guardian_5.avif',
        imageWebp: '/static/dota_2/ranks/guardian_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankGuardian' })} [5]`,
        value: 1400,
      },
      {
        image: '/static/dota_2/ranks/crusader_1.png',
        imageAvif: '/static/dota_2/ranks/crusader_1.avif',
        imageWebp: '/static/dota_2/ranks/crusader_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [1]`,
        value: 1550,
      },
      {
        image: '/static/dota_2/ranks/crusader_2.png',
        imageAvif: '/static/dota_2/ranks/crusader_2.avif',
        imageWebp: '/static/dota_2/ranks/crusader_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [2]`,
        value: 1700,
      },
      {
        image: '/static/dota_2/ranks/crusader_3.png',
        imageAvif: '/static/dota_2/ranks/crusader_3.avif',
        imageWebp: '/static/dota_2/ranks/crusader_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [3]`,
        value: 1850,
      },
      {
        image: '/static/dota_2/ranks/crusader_4.png',
        imageAvif: '/static/dota_2/ranks/crusader_4.avif',
        imageWebp: '/static/dota_2/ranks/crusader_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [4]`,
        value: 2025,
      },
      {
        image: '/static/dota_2/ranks/crusader_5.png',
        imageAvif: '/static/dota_2/ranks/crusader_5.avif',
        imageWebp: '/static/dota_2/ranks/crusader_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankCrusader' })} [5]`,
        value: 2175,
      },
      {
        image: '/static/dota_2/ranks/archon_1.png',
        imageAvif: '/static/dota_2/ranks/archon_1.avif',
        imageWebp: '/static/dota_2/ranks/archon_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [1]`,
        value: 2325,
      },
      {
        image: '/static/dota_2/ranks/archon_2.png',
        imageAvif: '/static/dota_2/ranks/archon_2.avif',
        imageWebp: '/static/dota_2/ranks/archon_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [2]`,
        value: 2475,
      },
      {
        image: '/static/dota_2/ranks/archon_3.png',
        imageAvif: '/static/dota_2/ranks/archon_3.avif',
        imageWebp: '/static/dota_2/ranks/archon_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [3]`,
        value: 2625,
      },
      {
        image: '/static/dota_2/ranks/archon_4.png',
        imageAvif: '/static/dota_2/ranks/archon_4.avif',
        imageWebp: '/static/dota_2/ranks/archon_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [4]`,
        value: 2775,
      },
      {
        image: '/static/dota_2/ranks/archon_5.png',
        imageAvif: '/static/dota_2/ranks/archon_5.avif',
        imageWebp: '/static/dota_2/ranks/archon_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankArchon' })} [5]`,
        value: 2950,
      },
      {
        image: '/static/dota_2/ranks/legend_1.png',
        imageAvif: '/static/dota_2/ranks/legend_1.avif',
        imageWebp: '/static/dota_2/ranks/legend_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [1]`,
        value: 3100,
      },
      {
        image: '/static/dota_2/ranks/legend_2.png',
        imageAvif: '/static/dota_2/ranks/legend_2.avif',
        imageWebp: '/static/dota_2/ranks/legend_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [2]`,
        value: 3250,
      },
      {
        image: '/static/dota_2/ranks/legend_3.png',
        imageAvif: '/static/dota_2/ranks/legend_3.avif',
        imageWebp: '/static/dota_2/ranks/legend_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [3]`,
        value: 3400,
      },
      {
        image: '/static/dota_2/ranks/legend_4.png',
        imageAvif: '/static/dota_2/ranks/legend_4.avif',
        imageWebp: '/static/dota_2/ranks/legend_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [4]`,
        value: 3550,
      },
      {
        image: '/static/dota_2/ranks/legend_5.png',
        imageAvif: '/static/dota_2/ranks/legend_5.avif',
        imageWebp: '/static/dota_2/ranks/legend_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankLegend' })} [5]`,
        value: 3700,
      },
      {
        image: '/static/dota_2/ranks/ancient_1.png',
        imageAvif: '/static/dota_2/ranks/ancient_1.avif',
        imageWebp: '/static/dota_2/ranks/ancient_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [1]`,
        value: 3850,
      },
      {
        image: '/static/dota_2/ranks/ancient_2.png',
        imageAvif: '/static/dota_2/ranks/ancient_2.avif',
        imageWebp: '/static/dota_2/ranks/ancient_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [2]`,
        value: 4025,
      },
      {
        image: '/static/dota_2/ranks/ancient_3.png',
        imageAvif: '/static/dota_2/ranks/ancient_3.avif',
        imageWebp: '/static/dota_2/ranks/ancient_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [3]`,
        value: 4175,
      },
      {
        image: '/static/dota_2/ranks/ancient_4.png',
        imageAvif: '/static/dota_2/ranks/ancient_4.avif',
        imageWebp: '/static/dota_2/ranks/ancient_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [4]`,
        value: 4325,
      },
      {
        image: '/static/dota_2/ranks/ancient_5.png',
        imageAvif: '/static/dota_2/ranks/ancient_5.avif',
        imageWebp: '/static/dota_2/ranks/ancient_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankAncient' })} [5]`,
        value: 4475,
      },
      {
        image: '/static/dota_2/ranks/divine_1.png',
        imageAvif: '/static/dota_2/ranks/divine_1.avif',
        imageWebp: '/static/dota_2/ranks/divine_1.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [1]`,
        value: 4625,
      },
      {
        image: '/static/dota_2/ranks/divine_2.png',
        imageAvif: '/static/dota_2/ranks/divine_2.avif',
        imageWebp: '/static/dota_2/ranks/divine_2.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [2]`,
        value: 4825,
      },
      {
        image: '/static/dota_2/ranks/divine_3.png',
        imageAvif: '/static/dota_2/ranks/divine_3.avif',
        imageWebp: '/static/dota_2/ranks/divine_3.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [3]`,
        value: 5025,
      },
      {
        image: '/static/dota_2/ranks/divine_4.png',
        imageAvif: '/static/dota_2/ranks/divine_4.avif',
        imageWebp: '/static/dota_2/ranks/divine_4.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [4]`,
        value: 5225,
      },
      {
        image: '/static/dota_2/ranks/divine_5.png',
        imageAvif: '/static/dota_2/ranks/divine_5.avif',
        imageWebp: '/static/dota_2/ranks/divine_5.webp',
        name: `${intl.formatMessage({ id: 'dota2.rankDivine' })} [5]`,
        value: 5425,
      },
      {
        image: '/static/dota_2/ranks/immortal.png',
        imageAvif: '/static/dota_2/ranks/immortal.avif',
        imageWebp: '/static/dota_2/ranks/immortal.webp',
        name: intl.formatMessage({ id: 'dota2.rankTitan' }),
        value: 7000,
      },
    ],
    [intl.locale]
  );

  const [games, setGames] = React.useState('10');

  const [formValue, setFormValue] = React.useState<Dota2CalibrationFormValue>({
    booster: false,
    games: 10,
    heroes: false,
    rank: 0,
    ranks,
    stream: false,
  });
  const calibrateRank = getCalibrateRank(formValue.rank, ranks);

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.DOTA_2][POSITION.CALIBRATION].rating
      ? ''
      : `/api/get_rating/?game=${GAME.DOTA_2}&position=${POSITION.CALIBRATION}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.DOTA_2]: {
        ...game[GAME.DOTA_2],
        [POSITION.CALIBRATION]: { rating },
      },
    });
  }, [rating]);

  return (
    <Position<Dota2CalibrationFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="center"
      game={GAME.DOTA_2}
      position={POSITION.CALIBRATION}
      rating={game[GAME.DOTA_2][POSITION.CALIBRATION].rating}
      description={{
        info: [intl.formatMessage({ id: 'dota2Calibration.infoDescription' })],
        eta: [intl.formatMessage({ id: 'dota2Calibration.etaDescription' })],
        safe: [intl.formatMessage({ id: 'dota2Calibration.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'dota2Calibration.requirementsDescription',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'rank',
          label: intl.formatMessage({ id: 'dota2Calibration.sliderLabel' }),
          min: 0,
          max: 7000,
          step: 25,
          maxLength: 4,
          adornments: ranks,
          marks: [
            {
              value: 1000,
              label: '1000',
            },
            {
              value: 2000,
              label: '2000',
            },
            {
              value: 3000,
              label: '3000',
            },
            {
              value: 4000,
              label: '4000',
            },
            {
              value: 5000,
              label: '5000',
            },
            {
              value: 6000,
              label: '6000',
            },
          ],
          extra: (
            <MuiTextField
              className={classes.games}
              label={intl.formatMessage({
                id: 'dota2Calibration.gamesLabel',
              })}
              name="games"
              variant="outlined"
              type="number"
              value={games}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                let newGames = event.target.value;

                if (Number(newGames) > Number(event.target.max)) {
                  newGames = event.target.max;
                } else if (Number(newGames) < Number(event.target.min)) {
                  newGames = event.target.min;
                }

                setFormValue({ ...formValue, games: +newGames });
                setGames(event.target.value ? newGames : '');
              }}
              InputProps={{
                inputProps: {
                  min: 1,
                  max: 10,
                },
              }}
            />
          ),
        },
        extra: (
          <div className={classes.resultCalibration}>
            <MuiTypography className={classes.resultCalibrationText}>
              {intl.formatMessage({
                id: 'dota2Calibration.resultCalibrationTitle',
              })}
            </MuiTypography>
            <img
              className={classes.resultCalibrationImage}
              src={calibrateRank.image}
              alt={calibrateRank.name}
            />
            <MuiTypography className={classes.resultCalibrationName}>
              {calibrateRank.name}
            </MuiTypography>
          </div>
        ),
        checkboxes: [
          {
            name: 'heroes',
            label: intl.formatMessage(
              {
                id: 'dota2.heroesPriceCheckboxLabel',
              },
              { price: '+15%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.heroesCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'dota2.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.streamCheckboxDescription',
            }),
          },
          {
            name: 'booster',
            label: intl.formatMessage(
              {
                id: 'dota2.boosterPriceCheckboxLabel',
              },
              { price: '+60%' }
            ),
            helperText: intl.formatMessage({
              id: 'dota2.boosterCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getDota2CalibrationResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        currency,
        formValue,
        game: GAME.DOTA_2,
        locale: intl.locale,
        position: POSITION.CALIBRATION,
        promocode,
      }}
    />
  );
};

export default Calibration;
