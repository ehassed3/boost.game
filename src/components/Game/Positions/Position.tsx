import * as React from 'react';
import { useIntl } from 'react-intl';
import { Element as ScrollElement } from 'react-scroll';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

import Rating from '@components/Rating';
import Boosters from '@components/Boosters';
import DescriptionForm, {
  Config as DescriptionFormConfig,
} from '@components/DescriptionSlider';
import Switch from '@components/Switch';
import Form, { Props as FormProps } from '@components/Game/Positions/Form';
import SupportButton from '@components/Button/Support';
import Result from '@components/Result';
import Payment from '@components/Payment';

import getPositionText from '@helpers/getPositionText';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import { DirtyOrder } from '@interfaces/order';
import RatingType from '@interfaces/rating';
import { FormattedResult } from '@interfaces/result';
import Promocode from '@interfaces/promocode';

const useStyles = makeStyles((theme: Theme) => ({
  position: {
    display: 'flex',
    paddingBottom: '80px',
    paddingTop: '48px',
    position: 'relative',

    [theme.breakpoints.down('xs')]: {
      paddingBottom: '60px',
      paddingTop: '16px',
    },
  },
  content: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '20px',

    [theme.breakpoints.down('xs')]: {
      paddingTop: '12px',
    },
  },
  contentLeft: {
    marginLeft: 'auto',
    width: '60%',

    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  contentCenter: {
    width: '100%',
  },
  contentRight: {
    marginRight: 'auto',
    width: '60%',

    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  head: {
    marginBottom: '32px',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      marginBottom: '28px',
    },
  },
  subhead: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'flex-start',
    },
  },
  title: {
    ...theme.typography.h3,

    fontWeight: 500,

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h4,
      lineHeight: '1.1',
      marginBottom: '8px',
    },
  },
  rating: {
    marginLeft: 'auto',
    marginTop: '16px',

    [theme.breakpoints.down('xs')]: {
      marginTop: '14px',
    },
  },
  boosters: {
    marginTop: '16px',

    [theme.breakpoints.down('xs')]: {
      marginTop: '8px',
    },
  },
  picture: {
    '&::after': {
      bottom: '-1px',
      boxShadow: `inset 0 0px 20px 12px ${theme.palette.background.default}, inset 0 0px 30px 30px ${theme.palette.background.default}`,
      content: '""',
      display: 'block',
      left: '0',
      position: 'absolute',
      right: '0',
      top: '-1px',

      [theme.breakpoints.down('sm')]: {
        left: '-24px',
        right: '-24px',
      },

      [theme.breakpoints.down('xs')]: {
        left: '-16px',
        right: '-16px',
      },
    },

    [theme.breakpoints.down('sm')]: {
      bottom: 0,
      display: 'block',
      height: '320px',
      marginBottom: '44px',
      position: 'relative',
      top: '0',
      width: '100%',
    },

    [theme.breakpoints.down('xs')]: {
      height: '260px',
      marginBottom: '24px',
    },
  },
  pictureLeft: {
    bottom: '64px',
    left: '0',
    position: 'absolute',
    top: '52px',
    width: '34%',

    [theme.breakpoints.down('sm')]: {
      position: 'relative',
      top: '0',
      width: '100%',
    },
  },
  pictureCenter: {
    height: '320px',
    position: 'relative',
    width: '100%',
    marginBottom: '44px',
  },
  pictureRight: {
    bottom: '72px',
    position: 'absolute',
    right: '0',
    top: '40px',
    width: '34%',

    [theme.breakpoints.down('sm')]: {
      position: 'relative',
      top: '0',
      width: '100%',
    },
  },
  pictureColored: {
    '&::after': {
      boxShadow: 'inset 0 0px 20px 12px #080808, inset 0 0px 30px 30px #080808',
    },

    [theme.breakpoints.down('xs')]: {
      '&::after': {
        boxShadow:
          'inset 0 0px 20px 12px #0a0a0a, inset 0 0px 30px 30px #0a0a0a',
      },
    },
  },
  image: {
    objectFit: 'cover',
    height: '100%',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      left: '-24px',
      position: 'absolute',
      width: '100vw',
    },

    [theme.breakpoints.down('xs')]: {
      left: '-16px',
    },
  },
  description: {
    marginBottom: '40px',
    width: '100%',
  },
  form: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  formExtra: {
    width: '60%',

    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  switch: {
    marginBottom: '24px',
  },
  final: {
    alignItems: 'flex-end',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
    marginTop: '80px',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'center',
      marginTop: '60px',
    },
  },
  supportButton: {
    marginBottom: '44px',
    marginRight: 'auto',

    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  finalContent: {
    alignItems: 'flex-start',
    display: 'flex',
    height: '100%',
    justifyContent: 'flex-end',

    [theme.breakpoints.down('xs')]: {
      flexWrap: 'wrap',
      height: 'auto',
      justifyContent: 'center',
      width: '100%',
    },
  },
  result: {
    height: '42px',
    marginBottom: '32px',
    marginLeft: '48px',
    minWidth: '160px',

    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      justifyContent: 'center',
      minWidth: 'auto',
      marginBottom: '64px',
      marginLeft: '0',
    },
  },
  payment: {
    marginBottom: '32px',
    marginLeft: '32px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '16px',
      marginLeft: '0',
      width: '100%',
    },
  },
  resultOperatorWrapper: {
    marginLeft: '48px',
    marginTop: '-24px',

    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
      marginTop: '0',
    },
  },
  resultOperator: {
    ...theme.typography.h6,

    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
  resultOperatorCaption: {
    ...theme.typography.caption,

    color: theme.palette.text.secondary,
    maxWidth: '360px',

    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
}));

const getContentClassName = (
  classes: Record<string, string>,
  direction?: 'left' | 'center' | 'right'
) => {
  switch (direction) {
    case 'left':
      return classes.contentLeft;
    case 'center':
      return classes.contentCenter;
    default:
      return classes.contentRight;
  }
};

const getPictureClassName = (
  classes: Record<string, string>,
  direction?: 'left' | 'center' | 'right'
) => {
  switch (direction) {
    case 'left':
      return classes.pictureLeft;
    case 'center':
      return classes.pictureCenter;
    default:
      return classes.pictureRight;
  }
};

interface Props<FormValue, SecondFormValue> {
  className?: string;
  imgClassName?: string;
  colored?: boolean;
  direction?: 'left' | 'center' | 'right';
  game: GAME;
  category?: CATEGORY;
  position: POSITION;
  rating?: RatingType;
  description: DescriptionFormConfig;
  form: FormProps<FormValue>;
  switchedForm?: {
    form: FormProps<SecondFormValue>;
    label?: string;
    isSwitched: boolean;
    setIsSwitched: (isSwitched: boolean) => void;
  };
  promocode?: Promocode | null;
  setPromocode?: (promocode: Promocode | null) => void;
  result: FormattedResult;
  order: DirtyOrder;
}

const Position = <FormValue, SecondFormValue = void>({
  className,
  imgClassName,
  colored,
  direction,
  game,
  category,
  position,
  rating,
  description,
  form,
  switchedForm,
  promocode,
  setPromocode,
  result,
  order,
}: React.PropsWithChildren<Props<FormValue, SecondFormValue>>): JSX.Element => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <MuiContainer
      className={
        className ? `${className} ${classes.position}` : classes.position
      }
    >
      <ScrollElement
        className={`${classes.content} ${getContentClassName(
          classes,
          direction
        )}`}
        name={position}
      >
        <div className={classes.head}>
          <MuiTypography className={classes.title}>
            {getPositionText(game, position, intl)}
          </MuiTypography>
          <div className={classes.subhead}>
            <Boosters
              className={classes.boosters}
              boosters={rating?.boosters}
            />
            <Rating className={classes.rating} rating={rating} />
          </div>
        </div>
        <picture
          className={
            colored
              ? `${classes.pictureColored} ${
                  classes.picture
                } ${getPictureClassName(classes, direction)}`
              : `${classes.picture} ${getPictureClassName(classes, direction)}`
          }
        >
          <source
            type="image/avif"
            srcSet={
              category
                ? `/static/${game}/${category}/${position}.avif`
                : `/static/${game}/${position}.avif`
            }
          />
          <source
            type="image/webp"
            srcSet={
              category
                ? `/static/${game}/${category}/${position}.webp`
                : `/static/${game}/${position}.webp`
            }
          />
          <img
            className={
              imgClassName ? `${imgClassName} ${classes.image}` : classes.image
            }
            alt={getPositionText(game, position, intl)}
            srcSet={
              category
                ? `/static/${game}/${category}/${position}.jpg`
                : `/static/${game}/${position}.jpg`
            }
          />
        </picture>
        <DescriptionForm className={classes.description} config={description} />
        <div
          className={
            form.extra ? `${classes.form} ${classes.formExtra}` : classes.form
          }
        >
          {switchedForm && (
            <Switch
              className={classes.switch}
              label={switchedForm.label}
              checked={switchedForm.isSwitched}
              onChange={(
                _: React.ChangeEvent<HTMLInputElement>,
                checked: boolean
              ) => {
                switchedForm.setIsSwitched(checked);
              }}
            />
          )}
          {switchedForm && switchedForm.isSwitched ? (
            <Form key="switchedForm" {...switchedForm.form} />
          ) : (
            <Form key="form" {...form} />
          )}
          <div className={classes.final}>
            <SupportButton
              className={classes.supportButton}
              game={game}
              position={position}
            />
            <div className={classes.finalContent}>
              {result.price.amount !== Infinity ? (
                <>
                  <Result
                    className={classes.result}
                    result={result}
                    promocode={promocode}
                    setPromocode={setPromocode}
                  />
                  <Payment
                    className={classes.payment}
                    order={order}
                    result={result}
                    disabled={!result.price.amount}
                  />
                </>
              ) : (
                <div className={classes.resultOperatorWrapper}>
                  <MuiTypography className={classes.resultOperator}>
                    {intl.formatMessage({ id: 'position.resultOperator' })}
                  </MuiTypography>
                  <MuiTypography className={classes.resultOperatorCaption}>
                    {intl.formatMessage({
                      id: 'position.resultOperatorCaption',
                    })}
                  </MuiTypography>
                </div>
              )}
            </div>
          </div>
        </div>
      </ScrollElement>
    </MuiContainer>
  );
};

export default Position;
