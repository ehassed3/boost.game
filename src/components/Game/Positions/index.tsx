import * as React from 'react';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';

import ScrollContainer from '@components/ScrollContainer';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import ScrollChip from '@components/ScrollChip';
import Diagonal from '@components/Diagonal';

import getPositionText from '@helpers/getPositionText';

import GAME from '@constants/game';
import POSITION from '@constants/position';

const useStyles = makeStyles((theme: Theme) => ({
  scrollTitle: {
    ...theme.typography.h6,

    fontWeight: 600,
    marginBottom: '8px',
    paddingTop: '12px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.body1,

      fontWeight: 600,
      marginBottom: '4px',
    },
  },
  scrollChips: {
    marginBottom: '28px',
  },
  scrollChip: {
    marginBottom: 'auto',
    marginTop: 'auto',

    '&:not(:last-child)': {
      marginRight: '12px',
    },
  },
}));

interface PositionProps {
  className?: string;
  colored?: boolean;
}

export interface Position {
  name: POSITION;
  component: React.FC<PositionProps>;
}

interface Props {
  game: GAME;
  positions: Position[];
}

const Positions: React.FC<Props> = ({ game, positions }) => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <>
      <MuiContainer>
        <MuiTypography className={classes.scrollTitle}>
          {intl.formatMessage({ id: 'positions.title' })}
        </MuiTypography>
      </MuiContainer>
      <ScrollContainer className={classes.scrollChips} id="chips">
        {map(positions, (position) => (
          <ScrollChip
            key={position.name}
            className={classes.scrollChip}
            name={position.name}
            label={getPositionText(game, position.name, intl)}
          />
        ))}
      </ScrollContainer>
      {map(positions, (position, index) =>
        index % 2 === 0 ? (
          <Diagonal key={position.name} colored>
            <position.component colored />
          </Diagonal>
        ) : (
          <position.component key={position.name} />
        )
      )}
    </>
  );
};

export default Positions;
