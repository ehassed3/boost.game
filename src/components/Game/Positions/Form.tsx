import * as React from 'react';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiCollapse from '@material-ui/core/Collapse';

import Switch from '@components/Switch';
import FieldSlider, { Config as SliderConfig } from '@components/FieldSlider';
import FieldDuoSlider, {
  Config as DuoSliderConfig,
} from '@components/FieldSlider/Duo';
import SelectList, {
  Config as SelectListConfig,
  Value as SelectListValue,
} from '@components/SelectList';
import Checkbox, { Props as CheckboxProps } from '@components/Checkbox';
import ToggleButtonGroup, {
  Config as ToggleButtonGroupConfig,
  Value as ToggleButtonGroupValue,
} from '@components/ToggleButtonGroup';

const useStyles = makeStyles((theme: Theme) => ({
  description: {
    marginBottom: '32px',
  },
  slider: {
    marginBottom: '36px',
    width: '100%',
  },
  extra: {
    position: 'absolute',
    right: '0',
    width: '36%',

    [theme.breakpoints.down('sm')]: {
      position: 'static',
      width: '100%',
    },
  },
  switch: {
    marginBottom: '28px',
  },
  collapse: {
    width: '100%',
  },
  additionalSlider: {
    marginBottom: '32px',
  },
  selectList: {
    marginBottom: '28px',
  },
  toggleButtonGroup: {
    marginBottom: '32px',
  },
  checkbox: {
    marginBottom: '20px',
  },
}));

export interface Props<FormValue> {
  description?: React.ReactNode[];
  toggleButtonGroup?: ToggleButtonGroupConfig;
  slider?: SliderConfig;
  duoSlider?: DuoSliderConfig;
  extra?: React.ReactNode;
  additional?: {
    label: string;
    checked: boolean;
    setChecked: (checked: boolean) => void;
    slider?: SliderConfig;
  };
  selectList?: SelectListConfig;
  checkboxes?: CheckboxProps[];
  value: FormValue;
  setValue: (formValue: FormValue) => void;
}

const Form = <FormValue extends Record<string, any>>({
  description,
  toggleButtonGroup,
  slider,
  duoSlider,
  extra,
  additional,
  selectList,
  checkboxes,
  value,
  setValue,
}: React.PropsWithChildren<Props<FormValue>>): JSX.Element => {
  const classes = useStyles();

  return (
    <>
      {description &&
        map(description, (descriptionItem, index) => (
          <MuiTypography key={index} className={classes.description}>
            {descriptionItem}
          </MuiTypography>
        ))}
      {toggleButtonGroup && (
        <ToggleButtonGroup
          className={classes.toggleButtonGroup}
          config={toggleButtonGroup}
          value={value[toggleButtonGroup.name]}
          setValue={(newValue: ToggleButtonGroupValue) => {
            setValue({ ...value, [toggleButtonGroup.name]: newValue });
          }}
        />
      )}
      {slider && (
        <FieldSlider
          className={classes.slider}
          config={slider}
          value={+value[slider.name]}
          setValue={(newValue: string | number) => {
            setValue({ ...value, [slider.name]: +newValue });
          }}
        />
      )}
      {duoSlider && (
        <FieldDuoSlider
          className={classes.slider}
          config={duoSlider}
          value={value[duoSlider.name]}
          setValue={(newValue: number[]) => {
            setValue({
              ...value,
              [duoSlider.name]: newValue,
            });
          }}
        />
      )}
      {extra && <div className={classes.extra}>{extra}</div>}
      {additional && (
        <>
          <Switch
            className={classes.switch}
            label={additional.label}
            checked={additional.checked}
            onChange={(
              _: React.ChangeEvent<HTMLInputElement>,
              checked: boolean
            ) => {
              additional.setChecked(checked);
            }}
          />
          <MuiCollapse className={classes.collapse} in={additional.checked}>
            {additional.slider && (
              <FieldSlider
                className={classes.additionalSlider}
                config={additional.slider}
                value={+value[additional.slider.name]}
                setValue={(newValue: string | number) => {
                  if (!additional.slider) {
                    return;
                  }

                  setValue({ ...value, [additional.slider.name]: +newValue });
                }}
              />
            )}
          </MuiCollapse>
        </>
      )}
      {selectList && (
        <SelectList
          className={classes.selectList}
          config={selectList}
          value={value[selectList.name]}
          setValue={(newValue: SelectListValue[]) => {
            setValue({
              ...value,
              [selectList.name]: newValue,
            });
          }}
        />
      )}
      {checkboxes &&
        map(checkboxes, (checkbox) => (
          <Checkbox
            key={checkbox.name}
            className={classes.checkbox}
            {...checkbox}
            color="primary"
            size="small"
            onChange={(
              _: React.ChangeEvent<HTMLInputElement>,
              checked: boolean
            ) => {
              setValue({ ...value, [checkbox.name]: checked });
            }}
          />
        ))}
    </>
  );
};

export default Form;
