import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowTorghastTowerLayerResult from '@helpers/getOrderData/getResult/getWowTorghastTowerLayerResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import getCurrencySign from '@helpers/getCurrencySign';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowTorghastTowerLayerFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 10%',
    },
  },
  wingsSelect: {
    padding: '13px 32px 12px 15px',
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const SoulAsh: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const currencySign = getCurrencySign(currency.code);
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const wings = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wowTorghastTowerLayer.oneWings' }),
        multiplier: 1,
      },
      {
        label: intl.formatMessage({ id: 'wowTorghastTowerLayer.twoWings' }),
        multiplier: 2,
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<
    WowTorghastTowerLayerFormValue
  >({
    layer: 8,
    selfplay: false,
    torghast: false,
    unlock: false,
    wings: wings[0],
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH][
      POSITION.TORGHAST_TOWER_LAYER
    ].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.TORGHAST_AND_SOUL_ASH}&position=${POSITION.TORGHAST_TOWER_LAYER}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.TORGHAST_AND_SOUL_ASH]: {
          ...game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH],
          [POSITION.TORGHAST_TOWER_LAYER]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowTorghastTowerLayerFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.TORGHAST_AND_SOUL_ASH}
      position={POSITION.TORGHAST_TOWER_LAYER}
      rating={
        game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH][
          POSITION.TORGHAST_TOWER_LAYER
        ].rating
      }
      description={{
        info: [
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info1Description' }),
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info2Description' }),
          <>
            {intl.formatMessage(
              {
                id: 'wowTorghastTowerLayer.info3Description',
              },
              {
                soulAsh: (
                  <WowHeadLink
                    href={intl.formatMessage({ id: 'wowHeadLink.soulAshLink' })}
                  >
                    {intl.formatMessage({ id: 'wowHeadLink.soulAsh' })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info4Description' }),
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info5Description' }),
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info6Description' }),
          intl.formatMessage({ id: 'wowTorghastTowerLayer.info7Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowTorghastTowerLayer.eta1Description' }),
          intl.formatMessage({ id: 'wowTorghastTowerLayer.eta2Description' }),
        ],
        safe: [
          intl.formatMessage({ id: 'wowTorghastTowerLayer.safeDescription' }),
        ],
        requirements: [
          intl.formatMessage({
            id: 'wowTorghastTowerLayer.requirements1Description',
          }),
          <>
            {intl.formatMessage({
              id: 'wowTorghastTowerLayer.requirements2Description',
            })}
          </>,
          <>
            {intl.formatMessage({
              id: 'wowTorghastTowerLayer.requirements3Description',
            })}
          </>,
          intl.formatMessage({
            id: 'wowTorghastTowerLayer.requirements4Description',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'layer',
          label: intl.formatMessage({
            id: 'wowTorghastTowerLayer.sliderLabel',
          }),
          min: 1,
          max: 8,
          step: 1,
          maxLength: 1,
          marks: [
            {
              value: 1,
              label: '1',
            },
            {
              value: 2,
              label: '2',
            },
            {
              value: 3,
              label: '3',
            },
            {
              value: 4,
              label: '4',
            },
            {
              value: 5,
              label: '5',
            },
            {
              value: 6,
              label: '6',
            },
            {
              value: 7,
              label: '7',
            },
            {
              value: 8,
              label: '8',
            },
          ],
          extra: (
            <MuiSelect
              classes={{ select: classes.wingsSelect }}
              variant="outlined"
              value={formValue.wings.label}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const foundWings = wings.find(
                  (wing) => wing.label === event.target.value
                );
                setFormValue({
                  ...formValue,
                  wings: foundWings || wings[0],
                });
              }}
            >
              {map(wings, (wing) => (
                <MuiMenuItem key={wing.label} value={wing.label}>
                  {wing.label}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          ),
        },
        checkboxes: [
          {
            name: 'unlock',
            label: intl.formatMessage(
              {
                id: 'wow.unlockLayerPriceCheckboxLabel',
              },
              { price: '+100%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.unlockLayerCheckboxDescription',
            }),
          },
          {
            name: 'selfplay',
            label: intl.formatMessage(
              {
                id: 'wow.selfplayPriceCheckboxLabel',
              },
              { price: '+10%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
          },
          {
            name: 'torghast',
            label: intl.formatMessage(
              {
                id: 'wow.torghastPriceCheckboxLabel',
              },
              {
                price: `+${Math.round(
                  3 * currency.multiplier
                )} ${currencySign}`,
              }
            ),
            helperText: intl.formatMessage({
              id: 'wow.torghastCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowTorghastTowerLayerResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.TORGHAST_AND_SOUL_ASH,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.TORGHAST_TOWER_LAYER,
        promocode,
      }}
    />
  );
};

export default SoulAsh;
