import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import { scroller } from 'react-scroll';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowSoulAshResult from '@helpers/getOrderData/getResult/getWowSoulAshResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowSoulAshFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 10%',
    },
  },
  link: {
    display: 'inline-block',
    color: theme.palette.primary.main,
    cursor: 'pointer',
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const SoulAsh: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowSoulAshFormValue>({
    selfplay: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH][POSITION.SOUL_ASH].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.TORGHAST_AND_SOUL_ASH}&position=${POSITION.SOUL_ASH}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.TORGHAST_AND_SOUL_ASH]: {
          ...game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH],
          [POSITION.SOUL_ASH]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowSoulAshFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW}
      category={CATEGORY.TORGHAST_AND_SOUL_ASH}
      position={POSITION.SOUL_ASH}
      rating={
        game[GAME.WOW][CATEGORY.TORGHAST_AND_SOUL_ASH][POSITION.SOUL_ASH].rating
      }
      description={{
        info: [
          intl.formatMessage(
            {
              id: 'wowSoulAsh.info1Description',
            },
            {
              soulAsh: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.soulAshLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.soulAsh' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowSoulAsh.info2Description' }),
          intl.formatMessage({ id: 'wowSoulAsh.info3Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowSoulAsh.eta1Description' }),
          intl.formatMessage({ id: 'wowSoulAsh.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowSoulAsh.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowSoulAsh.requirements1Description',
          }),
          <>
            {intl.formatMessage({
              id: 'wowSoulAsh.requirements2Description',
            })}
          </>,
          <>
            {intl.formatMessage({
              id: 'wowSoulAsh.requirements3Description',
            })}
          </>,
          <>
            {intl.formatMessage(
              {
                id: 'wowSoulAsh.requirements4Description',
              },
              {
                link: (
                  <MuiTypography
                    className={classes.link}
                    onClick={() => {
                      scroller.scrollTo(POSITION.TORGHAST_TOWER_LAYER, {
                        smooth: true,
                      });
                    }}
                  >
                    {intl.formatMessage({
                      id: 'wowSoulAsh.requirements4DescriptionLink',
                    })}
                  </MuiTypography>
                ),
              }
            )}
          </>,
        ],
      }}
      form={{
        checkboxes: [
          {
            name: 'selfplay',
            label: intl.formatMessage(
              {
                id: 'wow.selfplayPriceCheckboxLabel',
              },
              { price: '+10%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowSoulAshResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.TORGHAST_AND_SOUL_ASH,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.SOUL_ASH,
        promocode,
      }}
    />
  );
};

export default SoulAsh;
