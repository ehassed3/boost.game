import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowArenaResult from '@helpers/getOrderData/getResult/getWowArenaResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowArenaFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 18%',
    },
  },
  playerSelect: {
    padding: '13px 32px 12px 15px',
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Arena: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const players = React.useMemo<
    {
      label: string;
      name: '2vs2' | '3vs3';
    }[]
  >(
    () => [
      {
        label: intl.formatMessage({ id: 'wowArena.twoPlayer' }),
        name: '2vs2',
      },
      {
        label: intl.formatMessage({ id: 'wowArena.threePlayer' }),
        name: '3vs3',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<WowArenaFormValue>({
    player: players[0],
    rating: 1600,
    selfplay: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.PVP][POSITION.ARENA].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.PVP}&position=${POSITION.ARENA}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.PVP]: {
          ...game[GAME.WOW][CATEGORY.PVP],
          [POSITION.ARENA]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowArenaFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.PVP}
      position={POSITION.ARENA}
      rating={game[GAME.WOW][CATEGORY.PVP][POSITION.ARENA].rating}
      description={{
        info: [
          intl.formatMessage({ id: 'wowArena.info1Description' }),
          intl.formatMessage({ id: 'wowArena.info2Description' }),
          intl.formatMessage({ id: 'wowArena.info3Description' }),
          intl.formatMessage(
            { id: 'wowArena.info4Description' },
            {
              honor: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.honorLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.honor' })}
                </WowHeadLink>
              ),
              conquest: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.conquestLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.conquest' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage(
            { id: 'wowArena.info5Description' },
            {
              rival: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.rivalLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.rival' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowArena.info6Description' }),
          intl.formatMessage({ id: 'wowArena.info7Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowArena.eta1Description' }),
          intl.formatMessage({ id: 'wowArena.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowArena.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowArena.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowArena.requirements2Description',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'rating',
          label: intl.formatMessage({
            id: 'wowArena.sliderLabel',
          }),
          min: 0,
          max: 2400,
          maxLength: 4,
          step: null,
          marks: [
            {
              value: 0,
              label: '0',
            },
            {
              value: 1400,
              label: '1400',
            },
            {
              value: 1600,
              label: '1600',
            },
            {
              value: 1800,
              label: '1800',
            },
            {
              value: 2100,
              label: '2100',
            },
            {
              value: 2400,
              label: '2400',
            },
          ],
          extra: (
            <MuiSelect
              classes={{ select: classes.playerSelect }}
              variant="outlined"
              value={formValue.player.name}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const foundPlayer = players.find(
                  (player) => player.name === event.target.value
                );
                setFormValue({
                  ...formValue,
                  player: foundPlayer || players[0],
                });
              }}
            >
              {map(players, (player) => (
                <MuiMenuItem key={player.name} value={player.name}>
                  {player.label}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          ),
        },
        checkboxes: [
          {
            name: 'selfplay',
            label: intl.formatMessage(
              {
                id: 'wow.selfplayPriceCheckboxLabel',
              },
              { price: '+10%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowArenaResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.PVP,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.ARENA,
        promocode,
      }}
    />
  );
};

export default Arena;
