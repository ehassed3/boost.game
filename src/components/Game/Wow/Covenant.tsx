import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowCovenantResult from '@helpers/getOrderData/getResult/getWowCovenantResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowCovenantFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 64%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '100% 56%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Covenant: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const covenants = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.covenantKyrian' }),
        name: 'kyrian',
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantNecrolords' }),
        name: 'necrolords',
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantNightFae' }),
        name: 'night_fae',
      },
      {
        label: intl.formatMessage({ id: 'wow.covenantVenthyr' }),
        name: 'venthyr',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<WowCovenantFormValue>({
    covenant: covenants[2],
    isSwitched: false,
    levels: [1, 40],
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.CHARACTER_BOOST][POSITION.COVENANT].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.CHARACTER_BOOST}&position=${POSITION.COVENANT}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.CHARACTER_BOOST]: {
          ...game[GAME.WOW][CATEGORY.CHARACTER_BOOST],
          [POSITION.COVENANT]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowCovenantFormValue, WowCovenantFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW}
      category={CATEGORY.CHARACTER_BOOST}
      position={POSITION.COVENANT}
      rating={
        game[GAME.WOW][CATEGORY.CHARACTER_BOOST][POSITION.COVENANT].rating
      }
      description={{
        info: [
          <>
            {intl.formatMessage(
              {
                id: 'wowCovenant.info1Description',
              },
              {
                renown: (
                  <WowHeadLink
                    href={intl.formatMessage({ id: 'wowHeadLink.renownLink' })}
                  >
                    {intl.formatMessage({ id: 'wowHeadLink.renown' })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          <>
            {intl.formatMessage(
              {
                id: 'wowCovenant.info2Description',
              },
              {
                deepeningBond: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.deepeningBondLink',
                    })}
                    toy
                  >
                    {intl.formatMessage({ id: 'wowHeadLink.deepeningBond' })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          intl.formatMessage({ id: 'wowCovenant.info3Description' }),
          <>
            {intl.formatMessage(
              {
                id: 'wowCovenant.info4Description',
              },
              {
                eternalPhalynxOfPurity: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.eternalPhalynxOfPurityLink',
                    })}
                    mythic
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.eternalPhalynxOfPurity',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          <>
            {intl.formatMessage(
              {
                id: 'wowCovenant.info5Description',
              },
              {
                kyrianHearthstone: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.kyrianHearthstoneLink',
                    })}
                    toy
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.kyrianHearthstone',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          intl.formatMessage({ id: 'wowCovenant.info6Description' }),
        ],
        eta: [intl.formatMessage({ id: 'wowCovenant.etaDescription' })],
        safe: [intl.formatMessage({ id: 'wowCovenant.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowCovenant.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowCovenant.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowCovenant.requirements3Description',
          }),
        ],
      }}
      form={{
        toggleButtonGroup: {
          game: GAME.WOW,
          name: 'covenant',
          list: covenants,
        },
        checkboxes: [
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wow.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      switchedForm={{
        form: {
          toggleButtonGroup: {
            game: GAME.WOW,
            name: 'covenant',
            list: covenants,
          },
          duoSlider: {
            name: 'levels',
            min: 1,
            max: 40,
            startLabel: intl.formatMessage({
              id: 'wowCovenant.sliderStartLabel',
            }),
            endLabel: intl.formatMessage({
              id: 'wowCovenant.sliderEndLabel',
            }),
            maxLength: 2,
            step: 1,
            marks: [
              {
                value: 10,
                label: '10',
              },
              {
                value: 20,
                label: '20',
              },
              {
                value: 30,
                label: '30',
              },
              {
                value: 40,
                label: '40',
              },
            ],
          },
          checkboxes: [
            {
              name: 'stream',
              label: intl.formatMessage(
                {
                  id: 'wow.streamPriceCheckboxLabel',
                },
                { price: '+20%' }
              ),
              helperText: intl.formatMessage({
                id: 'wow.streamCheckboxDescription',
              }),
            },
          ],
          value: formValue,
          setValue: setFormValue,
        },
        label: intl.formatMessage({ id: 'wowCovenant.switchFormLabel' }),
        isSwitched: formValue.isSwitched,
        setIsSwitched: (newIsSwitched) => {
          setFormValue({ ...formValue, isSwitched: newIsSwitched });
        },
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowCovenantResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.CHARACTER_BOOST,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.COVENANT,
        promocode,
      }}
    />
  );
};

export default Covenant;
