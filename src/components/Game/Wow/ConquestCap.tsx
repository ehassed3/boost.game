import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowConquestCapResult from '@helpers/getOrderData/getResult/getWowConquestCapResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowConquestCapFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 26%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '100% 24%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const ConquestCap: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowConquestCapFormValue>({
    amount: 40,
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.PVP][POSITION.CONQUEST_CAP].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.PVP}&position=${POSITION.CONQUEST_CAP}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.PVP]: {
          ...game[GAME.WOW][CATEGORY.PVP],
          [POSITION.CONQUEST_CAP]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowConquestCapFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.PVP}
      position={POSITION.CONQUEST_CAP}
      rating={game[GAME.WOW][CATEGORY.PVP][POSITION.CONQUEST_CAP].rating}
      description={{
        info: [
          intl.formatMessage(
            { id: 'wowConquestCap.info1Description' },
            {
              conquest: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.conquestLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.conquest' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowConquestCap.info2Description' }),
          intl.formatMessage({ id: 'wowConquestCap.info3Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowConquestCap.eta1Description' }),
          intl.formatMessage({ id: 'wowConquestCap.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowConquestCap.safeDescription' })],
        requirements: [
          intl.formatMessage({ id: 'wowConquestCap.requirements1Description' }),
          intl.formatMessage({ id: 'wowConquestCap.requirements2Description' }),
        ],
      }}
      form={{
        description: [intl.formatMessage({ id: 'wowConquestCap.description' })],
        checkboxes: [
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wow.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowConquestCapResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.PVP,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.CONQUEST_CAP,
        promocode,
      }}
    />
  );
};

export default ConquestCap;
