import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowMythic8DungeonsResult from '@helpers/getOrderData/getResult/getWowMythic8DungeonsResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowMythic8DungeonsFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 52%',
    },
  },
}));

enum TRADER {
  CLOTH = 'cloth',
  LEATHER = 'leather',
  MAIL = 'mail',
  PLATE = 'plate',
}

interface Props {
  className?: string;
  colored?: boolean;
}

const Mythic8Dungeons: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const fractions = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.fractionHorde' }),
        name: 'horde',
      },
      {
        label: intl.formatMessage({ id: 'wow.fractionAlliance' }),
        name: 'alliance',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<WowMythic8DungeonsFormValue>(
    {
      fraction: fractions[0],
      selfplay: false,
      traders: [],
    }
  );

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.MYTHIC_8_DUNGEONS]
      .rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.DUNGEONS_AND_RAIDS}&position=${POSITION.MYTHIC_8_DUNGEONS}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.DUNGEONS_AND_RAIDS]: {
          ...game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS],
          [POSITION.MYTHIC_8_DUNGEONS]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowMythic8DungeonsFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.DUNGEONS_AND_RAIDS}
      position={POSITION.MYTHIC_8_DUNGEONS}
      rating={
        game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.MYTHIC_8_DUNGEONS]
          .rating
      }
      description={{
        info: [
          intl.formatMessage({ id: 'wowMythic8Dungeons.info1Description' }),
          intl.formatMessage({ id: 'wowMythic8Dungeons.info2Description' }),
          <>
            {intl.formatMessage(
              {
                id: 'wowMythic8Dungeons.info3Description',
              },
              {
                shadowlandsDungeonHero: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.shadowlandsDungeonHeroLink',
                    })}
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.shadowlandsDungeonHero',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          intl.formatMessage({ id: 'wowMythic8Dungeons.info4Description' }),
          intl.formatMessage({ id: 'wowMythic8Dungeons.info5Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowMythic8Dungeons.eta1Description' }),
          intl.formatMessage({ id: 'wowMythic8Dungeons.eta2Description' }),
        ],
        safe: [
          intl.formatMessage({ id: 'wowMythic8Dungeons.safeDescription' }),
        ],
        requirements: [
          intl.formatMessage({
            id: 'wowMythic8Dungeons.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowMythic8Dungeons.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowMythic8Dungeons.requirements3Description',
          }),
        ],
      }}
      form={{
        toggleButtonGroup: {
          game: GAME.WOW,
          name: 'fraction',
          list: fractions,
        },
        selectList: {
          list: [
            {
              value: TRADER.CLOTH,
              amount: 15,
              label: intl.formatMessage({ id: 'wow.traderCloth' }),
            },
            {
              value: TRADER.LEATHER,
              amount: 15,
              label: intl.formatMessage({ id: 'wow.traderLeather' }),
            },
            {
              value: TRADER.MAIL,
              amount: 15,
              label: intl.formatMessage({ id: 'wow.traderMail' }),
            },
            {
              value: TRADER.PLATE,
              amount: 15,
              label: intl.formatMessage({ id: 'wow.traderPlate' }),
            },
          ],
          game: GAME.WOW,
          name: 'traders',
          label: intl.formatMessage({ id: 'wow.traderLabel' }),
          buttonLabel: intl.formatMessage({ id: 'wow.traderButton' }),
          max: 4,
        },
        checkboxes: [
          {
            name: 'selfplay',
            label: intl.formatMessage(
              {
                id: 'wow.selfplayPriceCheckboxLabel',
              },
              { price: '+10%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowMythic8DungeonsResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.DUNGEONS_AND_RAIDS,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.MYTHIC_8_DUNGEONS,
        promocode,
      }}
    />
  );
};

export default Mythic8Dungeons;
