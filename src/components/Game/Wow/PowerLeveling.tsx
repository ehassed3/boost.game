import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowPowerLevelingResult from '@helpers/getOrderData/getResult/getWowPowerLevelingResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import getCurrencySign from '@helpers/getCurrencySign';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowPowerLevelingFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 20%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '100% 16%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const PowerLeveling: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const currencySign = getCurrencySign(currency.code);
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowPowerLevelingFormValue>({
    fast: false,
    gear: 180,
    isGear: false,
    levels: [50, 60],
    stream: false,
    torghast: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.CHARACTER_BOOST][POSITION.POWER_LEVELING].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.CHARACTER_BOOST}&position=${POSITION.POWER_LEVELING}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.CHARACTER_BOOST]: {
          ...game[GAME.WOW][CATEGORY.CHARACTER_BOOST],
          [POSITION.POWER_LEVELING]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowPowerLevelingFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.CHARACTER_BOOST}
      position={POSITION.POWER_LEVELING}
      rating={
        game[GAME.WOW][CATEGORY.CHARACTER_BOOST][POSITION.POWER_LEVELING].rating
      }
      description={{
        info: [
          intl.formatMessage(
            {
              id: 'wowPowerLeveling.info1Description',
            },
            {
              level60: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.level60Link' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.level60' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage(
            {
              id: 'wowPowerLeveling.info2Description',
            },
            {
              level60: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.level60Link' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.level60' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({
            id: 'wowPowerLeveling.info3Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.info4Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.info5Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.info6Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.info7Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.info8Description',
          }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowPowerLeveling.eta1Description' }),
          intl.formatMessage({ id: 'wowPowerLeveling.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowPowerLeveling.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowPowerLeveling.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowPowerLeveling.requirements2Description',
          }),
        ],
      }}
      form={{
        duoSlider: {
          name: 'levels',
          min: 1,
          max: 60,
          startLabel: intl.formatMessage({
            id: 'wowPowerLeveling.sliderStartLabel',
          }),
          endLabel: intl.formatMessage({
            id: 'wowPowerLeveling.sliderEndLabel',
          }),
          maxLength: 2,
          step: 1,
          marks: [
            {
              value: 10,
              label: '10',
            },
            {
              value: 20,
              label: '20',
            },
            {
              value: 30,
              label: '30',
            },
            {
              value: 40,
              label: '40',
            },
            {
              value: 50,
              label: '50',
            },
            {
              value: 60,
              label: '60',
            },
          ],
        },
        additional: {
          label: intl.formatMessage({
            id: 'wowPowerLeveling.additionalLabel',
          }),
          checked: formValue.isGear,
          setChecked: (newIsGear) => {
            setFormValue({ ...formValue, isGear: newIsGear });
          },
          slider: {
            name: 'gear',
            min: 170,
            max: 210,
            label: intl.formatMessage({
              id: 'wowPowerLeveling.additionalLabel',
            }),
            maxLength: 3,
            step: 1,
            marks: [
              {
                value: 170,
                label: '170',
              },
              {
                value: 175,
                label: '175',
              },
              {
                value: 180,
                label: '180',
              },
              {
                value: 185,
                label: '185',
              },
              {
                value: 190,
                label: '190',
              },
              {
                value: 195,
                label: '195',
              },
              {
                value: 200,
                label: '200',
              },
              {
                value: 205,
                label: '205',
              },
              {
                value: 210,
                label: '210',
              },
            ],
          },
        },
        checkboxes: [
          {
            name: 'torghast',
            label: intl.formatMessage(
              {
                id: 'wow.torghastPriceCheckboxLabel',
              },
              {
                price: `+${Math.round(
                  3 * currency.multiplier
                )} ${currencySign}`,
              }
            ),
            helperText: intl.formatMessage({
              id: 'wow.torghastCheckboxDescription',
            }),
          },
          {
            name: 'fast',
            label: intl.formatMessage(
              {
                id: 'wow.fastPriceCheckboxLabel',
              },
              { price: '+30%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.fastCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wow.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowPowerLevelingResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.CHARACTER_BOOST,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.POWER_LEVELING,
        promocode,
      }}
    />
  );
};

export default PowerLeveling;
