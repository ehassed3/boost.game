import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowHeroResult from '@helpers/getOrderData/getResult/getWowHeroResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowHeroFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 26%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '100% 24%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Hero: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowHeroFormValue>({
    amount: 110,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.ACHIEVEMENTS][POSITION.HERO].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.ACHIEVEMENTS}&position=${POSITION.HERO}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.ACHIEVEMENTS]: {
          ...game[GAME.WOW][CATEGORY.ACHIEVEMENTS],
          [POSITION.HERO]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowHeroFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW}
      category={CATEGORY.ACHIEVEMENTS}
      position={POSITION.HERO}
      rating={game[GAME.WOW][CATEGORY.ACHIEVEMENTS][POSITION.HERO].rating}
      description={{
        info: [
          <>
            {intl.formatMessage(
              {
                id: 'wowHero.info1Description',
              },
              {
                gloryOfTheShadowlandsHero: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.gloryOfTheShadowlandsHeroLink',
                    })}
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.gloryOfTheShadowlandsHero',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          <>
            {intl.formatMessage(
              {
                id: 'wowHero.info2Description',
              },
              {
                shadowlandsDungeonHero: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.shadowlandsDungeonHeroLink',
                    })}
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.shadowlandsDungeonHero',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          <>
            {intl.formatMessage(
              {
                id: 'wowHero.info3Description',
              },
              {
                voraciousGorger: (
                  <WowHeadLink
                    href={intl.formatMessage({
                      id: 'wowHeadLink.voraciousGorgerLink',
                    })}
                    mythic
                  >
                    {intl.formatMessage({
                      id: 'wowHeadLink.voraciousGorger',
                    })}
                  </WowHeadLink>
                ),
              }
            )}
          </>,
          intl.formatMessage({ id: 'wowHero.info4Description' }),
          intl.formatMessage({ id: 'wowHero.info5Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowHero.eta1Description' }),
          intl.formatMessage({ id: 'wowHero.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowHero.safeDescription' })],
        requirements: [
          intl.formatMessage({ id: 'wowHero.requirements1Description' }),
          intl.formatMessage({ id: 'wowHero.requirements2Description' }),
        ],
      }}
      form={{
        description: [intl.formatMessage({ id: 'wowHero.description' })],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowHeroResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.ACHIEVEMENTS,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.HERO,
        promocode,
      }}
    />
  );
};

export default Hero;
