import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowHonorResult from '@helpers/getOrderData/getResult/getWowHonorResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowHonorFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 28%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Honor: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowHonorFormValue>({
    honor: 15,
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.PVP][POSITION.HONOR].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.PVP}&position=${POSITION.HONOR}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.PVP]: {
          ...game[GAME.WOW][CATEGORY.PVP],
          [POSITION.HONOR]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowHonorFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW}
      category={CATEGORY.PVP}
      position={POSITION.HONOR}
      rating={game[GAME.WOW][CATEGORY.PVP][POSITION.HONOR].rating}
      description={{
        info: [
          intl.formatMessage(
            { id: 'wowHonor.info1Description' },
            {
              honor: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.honorLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.honor' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowHonor.info2Description' }),
          intl.formatMessage({ id: 'wowHonor.info3Description' }),
          intl.formatMessage({ id: 'wowHonor.info4Description' }),
          intl.formatMessage({ id: 'wowHonor.info5Description' }),
          intl.formatMessage({ id: 'wowHonor.info6Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowHonor.eta1Description' }),
          intl.formatMessage({ id: 'wowHonor.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowHonor.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowHonor.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowHonor.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowHonor.requirements3Description',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'honor',
          label: intl.formatMessage({ id: 'wowHonor.sliderLabel' }),
          min: 0,
          max: 50,
          step: 5,
          maxLength: 3,
          marks: [
            {
              value: 0,
              label: '0',
            },
            {
              value: 10,
              label: '10',
            },
            {
              value: 20,
              label: '20',
            },
            {
              value: 30,
              label: '30',
            },
            {
              value: 40,
              label: '40',
            },
            {
              value: 50,
              label: '50',
            },
          ],
        },
        checkboxes: [
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wow.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowHonorResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.PVP,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.HONOR,
        promocode,
      }}
    />
  );
};

export default Honor;
