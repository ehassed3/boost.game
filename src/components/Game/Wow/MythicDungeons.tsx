import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowMythicDungeonsResult from '@helpers/getOrderData/getResult/getWowMythicDungeonsResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import getCurrencySign from '@helpers/getCurrencySign';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowMythicDungeonsFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 52%',
    },
  },
  mythicSelect: {
    padding: '13px 32px 12px 15px',
  },
}));

const MYTHICS = [
  {
    level: 10,
    amount: 15,
  },
  {
    level: 11,
    amount: 17,
  },
  {
    level: 12,
    amount: 22,
  },
  {
    level: 13,
    amount: 27,
  },
  {
    level: 14,
    amount: 30,
  },
  {
    level: 15,
    amount: 32,
  },
  {
    level: 16,
    amount: 42,
  },
  {
    level: 17,
    amount: 54,
  },
  {
    level: 18,
    amount: 85,
  },
  {
    level: 19,
    amount: 99,
  },
  {
    level: 20,
    amount: 120,
  },
];

enum TRADER {
  CLOTH = 'cloth',
  LEATHER = 'leather',
  MAIL = 'mail',
  PLATE = 'plate',
}

const getTraderAmount = (level: number) => {
  switch (level) {
    case 10:
      return 2;
    case 11:
      return 2;
    case 12:
      return 3;
    case 13:
      return 4;
    case 14:
      return 4;
    case 15:
      return 5;
    case 16:
      return 7;
    case 17:
      return 10;
    case 18:
      return 13;
    case 19:
      return 17;
    case 20:
      return 20;
    default:
      return Infinity;
  }
};

interface Props {
  className?: string;
  colored?: boolean;
}

const MythicDungeons: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const currencySign = getCurrencySign(currency.code);
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const fractions = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.fractionHorde' }),
        name: 'horde',
      },
      {
        label: intl.formatMessage({ id: 'wow.fractionAlliance' }),
        name: 'alliance',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<WowMythicDungeonsFormValue>({
    fraction: fractions[0],
    mythic: MYTHICS[0],
    runs: 1,
    selfplay: false,
    specific: false,
    timer: false,
    traders: [],
  });

  const traders = [
    {
      value: TRADER.CLOTH,
      amount: getTraderAmount(formValue.mythic.level),
      label: intl.formatMessage({ id: 'wow.traderCloth' }),
    },
    {
      value: TRADER.LEATHER,
      amount: getTraderAmount(formValue.mythic.level),
      label: intl.formatMessage({ id: 'wow.traderLeather' }),
    },
    {
      value: TRADER.MAIL,
      amount: getTraderAmount(formValue.mythic.level),
      label: intl.formatMessage({ id: 'wow.traderMail' }),
    },
    {
      value: TRADER.PLATE,
      amount: getTraderAmount(formValue.mythic.level),
      label: intl.formatMessage({ id: 'wow.traderPlate' }),
    },
  ];
  React.useEffect(() => {
    const updatedTraders = formValue.traders.map((trader) => {
      const foundTrader = traders.find(
        (traderItem) => traderItem.value === trader.value
      );
      return foundTrader || trader;
    });

    setFormValue({
      ...formValue,
      traders: updatedTraders,
    });
  }, [formValue.mythic]);

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.MYTHIC_DUNGEONS].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.DUNGEONS_AND_RAIDS}&position=${POSITION.MYTHIC_DUNGEONS}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.DUNGEONS_AND_RAIDS]: {
          ...game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS],
          [POSITION.MYTHIC_DUNGEONS]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowMythicDungeonsFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW}
      category={CATEGORY.DUNGEONS_AND_RAIDS}
      position={POSITION.MYTHIC_DUNGEONS}
      rating={
        game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.MYTHIC_DUNGEONS]
          .rating
      }
      description={{
        info: [
          intl.formatMessage({ id: 'wowMythicDungeons.info1Description' }),
          intl.formatMessage({ id: 'wowMythicDungeons.info2Description' }),
          intl.formatMessage(
            {
              id: 'wowMythicDungeons.info3Description',
            },
            {
              shadowlandsKeystoneConquerorSeasonOne: (
                <WowHeadLink
                  href={intl.formatMessage({
                    id: 'wowHeadLink.shadowlandsKeystoneConquerorSeasonOneLink',
                  })}
                >
                  {intl.formatMessage({
                    id: 'wowHeadLink.shadowlandsKeystoneConquerorSeasonOne',
                  })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowMythicDungeons.info4Description' }),
          intl.formatMessage({ id: 'wowMythicDungeons.info5Description' }),
          intl.formatMessage({ id: 'wowMythicDungeons.info6Description' }),
          intl.formatMessage({ id: 'wowMythicDungeons.info7Description' }),
          intl.formatMessage({ id: 'wowMythicDungeons.info8Description' }),
        ],
        eta: [intl.formatMessage({ id: 'wowMythicDungeons.etaDescription' })],
        safe: [intl.formatMessage({ id: 'wowMythicDungeons.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowMythicDungeons.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowMythicDungeons.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowMythicDungeons.requirements3Description',
          }),
        ],
      }}
      form={{
        toggleButtonGroup: {
          game: GAME.WOW,
          name: 'fraction',
          list: fractions,
        },
        slider: {
          name: 'runs',
          label: intl.formatMessage({ id: 'wowMythicDungeons.sliderLabel' }),
          min: 1,
          max: 12,
          step: 1,
          maxLength: 2,
          marks: [
            {
              value: 1,
              label: '1',
            },
            {
              value: 2,
              label: '2',
            },
            {
              value: 3,
              label: '3',
            },
            {
              value: 4,
              label: '4',
            },
            {
              value: 5,
              label: '5',
            },
            {
              value: 6,
              label: '6',
            },
            {
              value: 7,
              label: '7',
            },
            {
              value: 8,
              label: '8',
            },
            {
              value: 9,
              label: '9',
            },
            {
              value: 10,
              label: '10',
            },
            {
              value: 11,
              label: '11',
            },
            {
              value: 12,
              label: '12',
            },
          ],
          extra: (
            <MuiSelect
              classes={{ select: classes.mythicSelect }}
              variant="outlined"
              value={formValue.mythic.level}
              renderValue={() =>
                intl.formatMessage(
                  { id: 'wowMythicDungeons.mythicSelect' },
                  {
                    level: formValue.mythic.level,
                    price: `${Math.round(
                      formValue.mythic.amount * currency.multiplier
                    )} ${currencySign}`,
                  }
                )
              }
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const foundMythic = MYTHICS.find(
                  (mythic) => mythic.level === event.target.value
                );
                setFormValue({
                  ...formValue,
                  mythic: foundMythic || MYTHICS[0],
                });
              }}
            >
              {map(MYTHICS, (mythic) => (
                <MuiMenuItem key={mythic.level} value={mythic.level}>
                  {intl.formatMessage(
                    { id: 'wowMythicDungeons.mythicSelect' },
                    {
                      level: mythic.level,
                      price: `${Math.round(
                        mythic.amount * currency.multiplier
                      )} ${currencySign}`,
                    }
                  )}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          ),
        },
        selectList: {
          list: traders,
          game: GAME.WOW,
          name: 'traders',
          label: intl.formatMessage({ id: 'wow.traderLabel' }),
          buttonLabel: intl.formatMessage({ id: 'wow.traderButton' }),
          max: 4,
        },
        checkboxes: [
          {
            name: 'selfplay',
            label: intl.formatMessage(
              {
                id: 'wow.selfplayPriceCheckboxLabel',
              },
              { price: '+10%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
          },
          {
            name: 'specific',
            label: intl.formatMessage(
              {
                id: 'wow.specificPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.specificCheckboxDescription',
            }),
          },
          {
            name: 'timer',
            label: intl.formatMessage(
              {
                id: 'wow.timerPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wow.timerCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowMythicDungeonsResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.DUNGEONS_AND_RAIDS,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.MYTHIC_DUNGEONS,
        promocode,
      }}
    />
  );
};

export default MythicDungeons;
