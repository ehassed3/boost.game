import * as React from 'react';
import { useIntl } from 'react-intl';

import GameCategories from '@components/Game/Categories';
import PowerLeveling from '@components/Game/Wow/PowerLeveling';
import Covenant from '@components/Game/Wow/Covenant';
import TorghastTowerLayer from '@components/Game/Wow/TorghastTowerLayer';
import Arena from '@components/Game/Wow/Arena';
import Honor from '@components/Game/Wow/Honor';
import ConquestCap from '@components/Game/Wow/ConquestCap';
import SoulAsh from '@components/Game/Wow/SoulAsh';
import Raids from '@components/Game/Wow/Raids';
import MythicDungeons from '@components/Game/Wow/MythicDungeons';
import Mythic8Dungeons from '@components/Game/Wow/Mythic8Dungeons';
import Hero from '@components/Game/Wow/Hero';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

interface Props {
  initial?: CATEGORY;
  isVisible: boolean;
}

const Wow: React.FC<Props> = ({ initial, isVisible }) => {
  const intl = useIntl();

  const categories = React.useMemo(
    () => [
      {
        name: CATEGORY.CHARACTER_BOOST,
        positions: [
          { name: POSITION.POWER_LEVELING, component: PowerLeveling },
          { name: POSITION.COVENANT, component: Covenant },
        ],
        title: intl.formatMessage({ id: 'wow.characterBoostCategory' }),
      },
      {
        name: CATEGORY.PVP,
        positions: [
          { name: POSITION.ARENA, component: Arena },
          { name: POSITION.HONOR, component: Honor },
          { name: POSITION.CONQUEST_CAP, component: ConquestCap },
        ],
        title: intl.formatMessage({ id: 'wow.pvpCategory' }),
      },
      {
        name: CATEGORY.DUNGEONS_AND_RAIDS,
        positions: [
          { name: POSITION.RAIDS, component: Raids },
          { name: POSITION.MYTHIC_DUNGEONS, component: MythicDungeons },
          { name: POSITION.MYTHIC_8_DUNGEONS, component: Mythic8Dungeons },
        ],
        title: intl.formatMessage({ id: 'wow.dungeonsAndRaidsCategory' }),
      },
      {
        name: CATEGORY.TORGHAST_AND_SOUL_ASH,
        positions: [
          {
            name: POSITION.TORGHAST_TOWER_LAYER,
            component: TorghastTowerLayer,
          },
          { name: POSITION.SOUL_ASH, component: SoulAsh },
        ],
        title: intl.formatMessage({ id: 'wow.torghastAndSoulAshCategory' }),
      },
      {
        name: CATEGORY.ACHIEVEMENTS,
        positions: [{ name: POSITION.HERO, component: Hero }],
        title: intl.formatMessage({ id: 'wow.achievementsCategory' }),
      },
      {
        name: CATEGORY.OTHER,
        positions: [],
        title: intl.formatMessage({ id: 'wow.otherCategory' }),
      },
    ],
    [intl.locale]
  );
  const initialCategory = React.useMemo(
    () =>
      categories.find((category) => category.name === initial) || categories[0],
    [intl.locale]
  );
  const currentCategories = React.useMemo(
    () => [
      initialCategory,
      ...categories.filter(
        (category) => category.name !== initialCategory.name
      ),
    ],
    [intl.locale]
  );

  return (
    <>
      {isVisible && (
        <GameCategories
          initial={initialCategory}
          game={GAME.WOW}
          categories={currentCategories}
        />
      )}
    </>
  );
};

export default Wow;
