import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowRaidsResult from '@helpers/getOrderData/getResult/getWowRaidsResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowRaidsFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 52%',
    },
  },
  difficultySelect: {
    padding: '13px 32px 12px 15px',
  },
}));

enum TRADER {
  CLOTH = 'cloth',
  LEATHER = 'leather',
  MAIL = 'mail',
  PLATE = 'plate',
}

const getTraderAmount = (difficulty: 'normal' | 'heroic' | 'mythic') => {
  switch (difficulty) {
    case 'normal':
      return 10;
    case 'heroic':
      return 18;
    case 'mythic':
      return Infinity;
    default:
      return Infinity;
  }
};

interface Props {
  className?: string;
  colored?: boolean;
}

const Raids: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const difficults = React.useMemo<
    {
      label: string;
      name: 'normal' | 'heroic' | 'mythic';
    }[]
  >(
    () => [
      {
        label: intl.formatMessage({ id: 'wowRaids.normalDifficulty' }),
        name: 'normal',
      },
      {
        label: intl.formatMessage({ id: 'wowRaids.heroicDifficulty' }),
        name: 'heroic',
      },
      {
        label: intl.formatMessage({ id: 'wowRaids.mythicDifficulty' }),
        name: 'mythic',
      },
    ],
    [intl.locale]
  );

  const fractions = React.useMemo(
    () => [
      {
        label: intl.formatMessage({ id: 'wow.fractionHorde' }),
        name: 'horde',
      },
      {
        label: intl.formatMessage({ id: 'wow.fractionAlliance' }),
        name: 'alliance',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<WowRaidsFormValue>({
    difficulty: difficults[0],
    fraction: fractions[0],
    raids: 10,
    selfplay: true,
    traders: [],
  });

  const traders = [
    {
      value: TRADER.CLOTH,
      amount: getTraderAmount(formValue.difficulty.name),
      label: intl.formatMessage({ id: 'wow.traderCloth' }),
    },
    {
      value: TRADER.LEATHER,
      amount: getTraderAmount(formValue.difficulty.name),
      label: intl.formatMessage({ id: 'wow.traderLeather' }),
    },
    {
      value: TRADER.MAIL,
      amount: getTraderAmount(formValue.difficulty.name),
      label: intl.formatMessage({ id: 'wow.traderMail' }),
    },
    {
      value: TRADER.PLATE,
      amount: getTraderAmount(formValue.difficulty.name),
      label: intl.formatMessage({ id: 'wow.traderPlate' }),
    },
  ];
  React.useEffect(() => {
    const updatedTraders = formValue.traders.map((trader) => {
      const foundTrader = traders.find(
        (traderItem) => traderItem.value === trader.value
      );
      return foundTrader || trader;
    });

    setFormValue({
      ...formValue,
      traders: updatedTraders,
    });
  }, [formValue.difficulty]);

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.RAIDS].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW}&category=${CATEGORY.DUNGEONS_AND_RAIDS}&position=${POSITION.RAIDS}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW]: {
        ...game[GAME.WOW],
        [CATEGORY.DUNGEONS_AND_RAIDS]: {
          ...game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS],
          [POSITION.RAIDS]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowRaidsFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW}
      category={CATEGORY.DUNGEONS_AND_RAIDS}
      position={POSITION.RAIDS}
      rating={
        game[GAME.WOW][CATEGORY.DUNGEONS_AND_RAIDS][POSITION.RAIDS].rating
      }
      description={{
        info: [
          intl.formatMessage(
            {
              id: 'wowRaids.info1Description',
            },
            {
              castleNathria: (
                <WowHeadLink
                  href={intl.formatMessage({
                    id: 'wowHeadLink.castleNathriaLink',
                  })}
                >
                  {intl.formatMessage({
                    id: 'wowHeadLink.castleNathria',
                  })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowRaids.info2Description' }),
          intl.formatMessage({ id: 'wowRaids.info3Description' }),
          intl.formatMessage(
            {
              id: 'wowRaids.info4Description',
            },
            {
              castleNathria: (
                <WowHeadLink
                  href={intl.formatMessage({
                    id: 'wowHeadLink.castleNathriaLink',
                  })}
                >
                  {intl.formatMessage({
                    id: 'wowHeadLink.castleNathria',
                  })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowRaids.info5Description' }),
          intl.formatMessage({ id: 'wowRaids.info6Description' }),
          intl.formatMessage({ id: 'wowRaids.info7Description' }),
          intl.formatMessage({ id: 'wowRaids.info8Description' }),
        ],
        eta: [intl.formatMessage({ id: 'wowRaids.etaDescription' })],
        safe: [intl.formatMessage({ id: 'wowRaids.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowRaids.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowRaids.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowRaids.requirements3Description',
          }),
        ],
      }}
      form={{
        toggleButtonGroup: {
          game: GAME.WOW,
          name: 'fraction',
          list: fractions,
        },
        slider: {
          name: 'raids',
          label: intl.formatMessage({ id: 'wowRaids.sliderLabel' }),
          min: 3,
          max: 10,
          step: null,
          maxLength: 2,
          marks: [
            {
              value: 3,
              label: '3',
            },
            {
              value: 7,
              label: '7',
            },
            {
              value: 10,
              label: '10',
            },
          ],
          extra: (
            <MuiSelect
              classes={{ select: classes.difficultySelect }}
              variant="outlined"
              value={formValue.difficulty.name}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const foundDifficulty = difficults.find(
                  (difficulty) => difficulty.name === event.target.value
                );
                setFormValue({
                  ...formValue,
                  difficulty: foundDifficulty || difficults[0],
                });
              }}
            >
              {map(difficults, (difficulty) => (
                <MuiMenuItem key={difficulty.name} value={difficulty.name}>
                  {difficulty.label}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          ),
        },
        selectList: {
          list: traders,
          game: GAME.WOW,
          name: 'traders',
          label: intl.formatMessage({ id: 'wow.traderLabel' }),
          buttonLabel: intl.formatMessage({ id: 'wow.traderButton' }),
          max: 4,
        },
        checkboxes: [
          {
            name: 'selfplay',
            label: intl.formatMessage({
              id: 'wow.selfplayCheckboxLabel',
            }),
            helperText: intl.formatMessage({
              id: 'wow.selfplayCheckboxDescription',
            }),
            disabled: true,
            checked: formValue.selfplay,
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowRaidsResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.DUNGEONS_AND_RAIDS,
        currency,
        formValue,
        game: GAME.WOW,
        locale: intl.locale,
        position: POSITION.RAIDS,
        promocode,
      }}
    />
  );
};

export default Raids;
