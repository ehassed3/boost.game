import * as React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import getGameTitle from '@helpers/getGameTitle';

import GAME from '@constants/game';

const useStyles = makeStyles(() => ({
  image: {
    display: 'block',
    height: '100%',
    objectFit: 'cover',
    width: '100%',
  },
}));

interface Props {
  className?: string;
  game: GAME;
  icon?: boolean;
}

const GameLogo: React.FC<Props> = ({ className, game, icon }) => {
  const classes = useStyles();

  return game === GAME.POE || game === GAME.WOW || game === GAME.WOW_CLASSIC ? (
    <picture className={className}>
      <source
        type="image/avif"
        srcSet={`/static/${game}/${icon ? 'icon' : 'logo'}.avif`}
      />
      <source
        type="image/webp"
        srcSet={`/static/${game}/${icon ? 'icon' : 'logo'}.webp`}
      />
      <img
        className={classes.image}
        alt={getGameTitle(game)}
        src={`/static/${game}/${icon ? 'icon' : 'logo'}.png`}
      />
    </picture>
  ) : (
    <img
      className={className}
      alt={`${getGameTitle(game)} ${icon ? 'icon' : 'logo'}`}
      src={`/static/${game}/${icon ? 'icon' : 'logo'}.svg`}
      height={90}
      width={196}
    />
  );
};

export default GameLogo;
