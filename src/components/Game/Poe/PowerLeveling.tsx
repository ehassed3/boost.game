import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';

import Position from '@components/Game/Positions/Position';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getPoePowerLevelingResult from '@helpers/getOrderData/getResult/getPoePowerLevelingResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { PoePowerLevelingFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 20%',
    },

    [theme.breakpoints.down('xs')]: {
      objectPosition: '100% 16%',
    },
  },
  difficulty: {
    marginBottom: '24px',
  },
  difficultySelect: {
    padding: '17px 32px 16px 15px',
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const PowerLeveling: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const difficults = React.useMemo<
    {
      label: string;
      name: 'softcore' | 'hardcore';
    }[]
  >(
    () => [
      {
        label: intl.formatMessage({
          id: 'poePowerLeveling.softcoreDifficulty',
        }),
        name: 'softcore',
      },
      {
        label: intl.formatMessage({
          id: 'poePowerLeveling.hardcoreDifficulty',
        }),
        name: 'hardcore',
      },
    ],
    [intl.locale]
  );

  const [formValue, setFormValue] = React.useState<PoePowerLevelingFormValue>({
    difficulty: difficults[0],
    levels: [1, 70],
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.POE][POSITION.POWER_LEVELING].rating
      ? ''
      : `/api/get_rating/?game=${GAME.POE}&position=${POSITION.POWER_LEVELING}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.POE]: {
        ...game[GAME.POE],
        [POSITION.POWER_LEVELING]: { rating },
      },
    });
  }, [rating]);

  return (
    <Position<PoePowerLevelingFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.POE}
      position={POSITION.POWER_LEVELING}
      rating={game[GAME.POE][POSITION.POWER_LEVELING].rating}
      description={{
        info: [
          intl.formatMessage({
            id: 'poePowerLeveling.info1Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info2Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info3Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info4Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info5Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info6Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info7Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info8Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.info9Description',
          }),
        ],
        eta: [
          intl.formatMessage({ id: 'poePowerLeveling.eta1Description' }),
          intl.formatMessage({ id: 'poePowerLeveling.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'poePowerLeveling.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'poePowerLeveling.requirements1Description',
          }),
          intl.formatMessage({
            id: 'poePowerLeveling.requirements2Description',
          }),
        ],
      }}
      form={{
        duoSlider: {
          name: 'levels',
          min: 1,
          max: 90,
          startLabel: intl.formatMessage({
            id: 'poePowerLeveling.sliderStartLabel',
          }),
          endLabel: intl.formatMessage({
            id: 'poePowerLeveling.sliderEndLabel',
          }),
          maxLength: 2,
          step: 1,
          marks: [
            {
              value: 10,
              label: '10',
            },
            {
              value: 20,
              label: '20',
            },
            {
              value: 30,
              label: '30',
            },
            {
              value: 40,
              label: '40',
            },
            {
              value: 50,
              label: '50',
            },
            {
              value: 60,
              label: '60',
            },
            {
              value: 70,
              label: '70',
            },
            {
              value: 80,
              label: '80',
            },
            {
              value: 90,
              label: '90',
            },
          ],
          extra: (
            <MuiSelect
              className={classes.difficulty}
              classes={{
                select: classes.difficultySelect,
              }}
              variant="outlined"
              value={formValue.difficulty.name}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const foundDifficulty = difficults.find(
                  (difficulty) => difficulty.name === event.target.value
                );
                setFormValue({
                  ...formValue,
                  difficulty: foundDifficulty || difficults[0],
                });
              }}
            >
              {map(difficults, (difficulty) => (
                <MuiMenuItem key={difficulty.name} value={difficulty.name}>
                  {difficulty.label}
                </MuiMenuItem>
              ))}
            </MuiSelect>
          ),
        },
        checkboxes: [
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'poe.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'poe.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getPoePowerLevelingResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        currency,
        formValue,
        game: GAME.POE,
        locale: intl.locale,
        position: POSITION.POWER_LEVELING,
        promocode,
      }}
    />
  );
};

export default PowerLeveling;
