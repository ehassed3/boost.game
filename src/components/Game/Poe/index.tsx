import * as React from 'react';

import GamePositions from '@components/Game/Positions';
import PowerLeveling from '@components/Game/Poe/PowerLeveling';

import GAME from '@constants/game';
import POSITION from '@constants/position';

interface Props {
  isVisible: boolean;
}

const Poe: React.FC<Props> = ({ isVisible }) => (
  <>
    {isVisible && (
      <GamePositions
        game={GAME.POE}
        positions={[
          { name: POSITION.POWER_LEVELING, component: PowerLeveling },
        ]}
      />
    )}
  </>
);

export default Poe;
