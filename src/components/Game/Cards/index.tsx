import * as React from 'react';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { scroller, Element as ScrollElement } from 'react-scroll';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

import GameCard from '@components/Game/Cards/Card';
import ScrollContainer from '@components/ScrollContainer';

import GAMES from '@constants/games';
import GAME from '@constants/game';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h5,

    marginBottom: '24px',
    fontWeight: 600,

    [theme.breakpoints.down('xs')]: {
      marginBottom: '18px',
    },
  },
  card: {
    flexShrink: 0,

    '&:not(:last-child)': {
      marginRight: '36px',
    },

    [theme.breakpoints.down('xs')]: {
      '&:not(:last-child)': {
        marginRight: '32px',
      },
    },
  },
  scrollElement: {
    marginTop: '32px',

    [theme.breakpoints.down('xs')]: {
      marginTop: '24px',
    },
  },
}));

const GAME_CARDS = [
  GAME.WOW,
  GAME.WOW_CLASSIC,
  GAME.POE,
  GAME.DOTA_2,
  GAME.LOL,
  GAME.CS_GO,
  GAME.VALORANT,
  GAME.OVERWATCH,
  GAME.HEARTHSTONE,
  GAME.DESTINY_2,
];

interface Props {
  className?: string;
  initial: GAME;
  onChange?: (card: GAME) => void;
}

const GameCards: React.FC<Props> = ({ className, initial, onChange }) => {
  const intl = useIntl();
  const classes = useStyles();

  const router = useRouter();

  const [shouldUpdateCard, setShouldUpdateCard] = React.useState(false);
  const [activeGameCard, setActiveGameCard] = React.useState(initial);
  const gameCards = React.useMemo(
    () => [initial, ...GAME_CARDS.filter((gameCard) => gameCard !== initial)],
    []
  );

  const onGameCardClick = (gameCard: GAME) => {
    if (activeGameCard === gameCard) {
      scroller.scrollTo('toCard', { smooth: true });
      return;
    }

    router.push(`/${gameCard}`, `/${gameCard}`);
  };

  React.useEffect(() => {
    if (!shouldUpdateCard) {
      return;
    }

    const page = router.pathname.split('/')[1] || initial;

    if (
      activeGameCard === page ||
      gameCards.every((gamecard) => gamecard !== page)
    ) {
      return;
    }

    setActiveGameCard(page as GAME);

    scroller.scrollTo(page as GAME, {
      containerId: 'cards',
      delay: 800,
      horizontal: true,
      offset: -60,
      smooth: true,
      ignoreCancelEvents: true,
    });

    scroller.scrollTo('toCard', {
      delay: 800,
      smooth: true,
    });

    if (onChange) {
      onChange(page as GAME);
    }
  }, [router.pathname]);

  React.useEffect(() => {
    setShouldUpdateCard(true);
  }, []);

  return (
    <>
      <MuiContainer>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'cards.title' })}
        </MuiTypography>
      </MuiContainer>
      <ScrollContainer className={className} id="cards">
        {map(gameCards, (gameCard) => (
          <GameCard
            key={gameCard}
            className={classes.card}
            name={gameCard}
            active={activeGameCard === gameCard}
            disabled={GAMES.indexOf(gameCard) === -1}
            onClick={onGameCardClick}
          />
        ))}
      </ScrollContainer>
      <ScrollElement className={classes.scrollElement} name="toCard" />
    </>
  );
};

export default GameCards;
