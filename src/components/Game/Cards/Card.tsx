import * as React from 'react';
import { useIntl } from 'react-intl';
import { Element as ScrollElement } from 'react-scroll';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiCardActionArea from '@material-ui/core/CardActionArea';
import MuiTypography from '@material-ui/core/Typography';

import GameLogo from '@components/Game/Logo';

import getGameTitle from '@helpers/getGameTitle';

import GAME from '@constants/game';

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '16px',
    height: '420px',
    marginRight: '8px',
    marginTop: '12px',
    overflow: 'visible',
    position: 'relative',
    width: '280px',

    '&::before': {
      backgroundColor: theme.palette.primary.main,
      borderRadius: '16px',
      bottom: 0,
      content: '""',
      left: 0,
      position: 'absolute',
      right: 0,
      top: 0,
    },

    [theme.breakpoints.down('sm')]: {
      height: '368px',
      width: '240px',
    },
  },
  soon: {
    alignItems: 'flex-start',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    bottom: '0',
    display: 'flex',
    left: '0',
    position: 'absolute',
    right: '0',
    top: '0',
    zIndex: 1,
  },
  soonText: {
    ...theme.typography.body2,

    backgroundColor: theme.palette.secondary.dark,
    borderRadius: '6px',
    marginLeft: 'auto',
    marginRight: '10px',
    marginTop: '10px',
    padding: '2px 8px',
    pointerEvents: 'none',
  },
  cardActionArea: {
    borderRadius: '16px',
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    position: 'relative',
    transition: 'transform 0.7s',
  },
  picture: {
    borderRadius: '16px',
    backgroundColor: theme.palette.background.paper,
    bottom: '0',
    left: '0',
    position: 'absolute',
    right: '0',
    top: '0',
  },
  image: {
    borderRadius: '16px',
    display: 'block',
    height: '100%',
    width: '100%',
  },
  cardLogo: {
    padding: '0 24px',
    pointerEvents: 'none',
    width: '100%',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      padding: '0 12px',
    },
  },
  poeLogo: {
    padding: '0 48px',

    [theme.breakpoints.down('sm')]: {
      padding: '0 28px',
    },
  },
}));

interface Props {
  className?: string;
  name: GAME;
  active?: boolean;
  disabled?: boolean;
  onClick: (card: GAME) => void;
}

const GameCard: React.FC<Props> = ({
  className,
  name,
  active,
  disabled,
  onClick,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <ScrollElement
      className={className ? `${className} ${classes.card}` : classes.card}
      name={name}
    >
      {disabled && (
        <div className={classes.soon}>
          <MuiTypography className={classes.soonText}>
            {intl.formatMessage({ id: 'card.soon' })}
          </MuiTypography>
        </div>
      )}
      <MuiCardActionArea
        className={classes.cardActionArea}
        style={{
          transform: active ? 'translate(8px, -12px)' : 'translate(0px, 0px)',
        }}
        disabled={disabled}
        onClick={() => onClick(name)}
      >
        <picture className={classes.picture}>
          <source type="image/avif" srcSet={`/static/${name}/card.avif`} />
          <source type="image/webp" srcSet={`/static/${name}/card.webp`} />
          <img
            className={classes.image}
            alt={getGameTitle(name)}
            src={`/static/${name}/card.jpg`}
          />
        </picture>
        <GameLogo
          className={
            name === GAME.POE
              ? ` ${classes.poeLogo} ${classes.cardLogo}`
              : classes.cardLogo
          }
          game={name}
        />
      </MuiCardActionArea>
    </ScrollElement>
  );
};

export default GameCard;
