import * as React from 'react';
import { useIntl } from 'react-intl';
import { Element as ScrollElement } from 'react-scroll';

import { makeStyles, Theme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import MuiCardActionArea from '@material-ui/core/CardActionArea';
import MuiTypography from '@material-ui/core/Typography';

import { Position } from '@components/Game/Positions';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '16px',
    flexShrink: 0,
    height: '120px',
    marginRight: '8px',
    marginTop: '12px',
    overflow: 'visible',
    position: 'relative',
    width: '220px',

    '&::before': {
      backgroundColor: theme.palette.primary.main,
      borderRadius: '16px',
      bottom: 0,
      content: '""',
      left: 0,
      position: 'absolute',
      right: 0,
      top: 0,
    },

    [theme.breakpoints.down('sm')]: {
      height: '120px',
      width: '220px',
    },
  },
  soon: {
    alignItems: 'flex-start',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    bottom: '0',
    display: 'flex',
    left: '0',
    position: 'absolute',
    right: '0',
    top: '0',
    zIndex: 1,
  },
  soonText: {
    ...theme.typography.body2,

    backgroundColor: theme.palette.secondary.dark,
    borderRadius: '6px',
    marginLeft: 'auto',
    marginRight: '10px',
    marginTop: '10px',
    padding: '2px 8px',
    pointerEvents: 'none',
  },
  cardActionArea: {
    borderRadius: '16px',
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    position: 'relative',
    transition: 'transform 0.7s',
  },
  picture: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '16px',
    bottom: '0',
    left: '0',
    position: 'absolute',
    right: '0',
    top: '0',
  },
  image: {
    borderRadius: '16px',
    display: 'block',
    height: '100%',
    width: '100%',
  },
  cardLogo: {
    padding: '0 24px',
    pointerEvents: 'none',
    width: '100%',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      padding: '0 12px',
    },
  },
  title: {
    ...theme.typography.h6,

    backgroundColor: fade(theme.palette.background.paper, 0.8),
    borderRadius: '8px',
    margin: '20px',
    lineHeight: '1.4',
    padding: '4px 12px 6px',
    textAlign: 'center',
    zIndex: 1,
    width: 'min-content',
  },
}));

export interface Category {
  name: CATEGORY;
  title: string;
  positions: Position[];
}

interface Props {
  className?: string;
  game: GAME;
  category: Category;
  active?: boolean;
  onClick: (category: Category) => void;
}

const GameCategory: React.FC<Props> = ({
  className,
  game,
  category,
  active,
  onClick,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const disabled = !category.positions.length;

  return (
    <ScrollElement
      className={className ? `${className} ${classes.card}` : classes.card}
      name={category.name}
    >
      {disabled && (
        <div className={classes.soon}>
          <MuiTypography className={classes.soonText}>
            {intl.formatMessage({ id: 'category.soon' })}
          </MuiTypography>
        </div>
      )}
      <MuiCardActionArea
        className={classes.cardActionArea}
        style={{
          transform: active ? 'translate(6px, -8px)' : 'translate(0px, 0px)',
        }}
        disabled={disabled}
        onClick={() => onClick(category)}
      >
        <picture className={classes.picture}>
          <source
            type="image/avif"
            srcSet={`/static/${game}/categories/${category.name}.avif`}
          />
          <source
            type="image/webp"
            srcSet={`/static/${game}/categories/${category.name}.webp`}
          />
          <img
            className={classes.image}
            alt={category.title}
            src={`/static/${game}/categories/${category.name}.jpg`}
          />
        </picture>
        <MuiTypography className={classes.title}>
          {category.title}
        </MuiTypography>
      </MuiCardActionArea>
    </ScrollElement>
  );
};

export default GameCategory;
