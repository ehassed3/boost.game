import * as React from 'react';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { scroller, Element as ScrollElement } from 'react-scroll';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

import ScrollContainer from '@components/ScrollContainer';

import GameCategory, { Category } from '@components/Game/Categories/Category';
import GamePositions from '@components/Game/Positions';

import GAME from '@constants/game';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h6,

    paddingTop: '12px',
    marginBottom: '12px',
    fontWeight: 600,
  },
  category: {
    '&:not(:last-child)': {
      marginRight: '28px',
    },
  },
  scrollElement: {
    marginTop: '32px',
  },
}));

interface Props {
  initial: Category;
  game: GAME;
  categories: Category[];
}

const GameCategories: React.FC<Props> = ({ initial, game, categories }) => {
  const intl = useIntl();
  const classes = useStyles();

  const router = useRouter();

  const [shouldUpdateCategory, setShouldUpdateCategory] = React.useState(false);
  const [shouldScrollToCategory, setShouldScrollToCategory] = React.useState(
    false
  );

  const [activeCategory, setActiveCategory] = React.useState(initial);
  const [positions, setPositions] = React.useState(initial.positions);
  const [isVisibleForm, setIsVisibleForm] = React.useState(true);

  const onCategoryClick = (category: Category) => {
    if (activeCategory === category) {
      scroller.scrollTo('toCategory', {
        smooth: true,
      });
      return;
    }

    router.push(`/${game}/${category.name}`, `/${game}/${category.name}`);
    setShouldScrollToCategory(true);
  };

  React.useEffect(() => {
    const items = router.pathname.split('/');

    if (!!items[1] && items[1] !== game) {
      return;
    }

    const newCategory =
      categories.find((category) => category.name === items[2]) ||
      categories[0];

    if (activeCategory.name === newCategory.name) {
      return;
    }

    setActiveCategory(newCategory);

    scroller.scrollTo(newCategory.name, {
      containerId: 'categories',
      delay: 800,
      horizontal: true,
      offset: -60,
      smooth: true,
      ignoreCancelEvents: true,
    });

    setPositions(newCategory.positions);

    if (shouldUpdateCategory) {
      if (shouldScrollToCategory) {
        scroller.scrollTo('toCategory', {
          delay: 800,
          smooth: true,
        });
        setShouldScrollToCategory(false);
      } else {
        scroller.scrollTo('toCard', {
          delay: 800,
          smooth: true,
        });
      }

      setIsVisibleForm(false);
    }
  }, [router.pathname]);

  React.useEffect(() => {
    setShouldUpdateCategory(true);
  }, []);

  return (
    <>
      <MuiContainer>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'categories.title' })}
        </MuiTypography>
      </MuiContainer>
      <ScrollContainer id="categories">
        {map(categories, (category) => (
          <GameCategory
            key={category.name}
            className={classes.category}
            game={game}
            category={category}
            active={category.name === activeCategory.name}
            onClick={onCategoryClick}
          />
        ))}
      </ScrollContainer>
      <ScrollElement className={classes.scrollElement} name="toCategory" />
      <AnimatePresence
        initial={false}
        onExitComplete={() => {
          setIsVisibleForm(true);
        }}
      >
        {isVisibleForm && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.4 } }}
          >
            <GamePositions game={game} positions={positions} />
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default GameCategories;
