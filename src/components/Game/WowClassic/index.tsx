import * as React from 'react';
import { useIntl } from 'react-intl';

import GameCategories from '@components/Game/Categories';
import PowerLeveling from '@components/Game/WowClassic/PowerLeveling';
import Rank from '@components/Game/WowClassic/Rank';
import Honor from '@components/Game/WowClassic/Honor';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

interface Props {
  initial?: CATEGORY;
  isVisible: boolean;
}

const WowClassic: React.FC<Props> = ({ initial, isVisible }) => {
  const intl = useIntl();

  const categories = React.useMemo(
    () => [
      {
        name: CATEGORY.POWER_LEVELING,
        positions: [
          { name: POSITION.POWER_LEVELING, component: PowerLeveling },
        ],
        title: intl.formatMessage({ id: 'wowClassic.powerLevelingCategory' }),
      },
      {
        name: CATEGORY.PVP,
        positions: [
          { name: POSITION.HONOR, component: Honor },
          { name: POSITION.RANK, component: Rank },
        ],
        title: intl.formatMessage({ id: 'wowClassic.pvpCategory' }),
      },
      {
        name: CATEGORY.PVE,
        positions: [],
        title: intl.formatMessage({ id: 'wowClassic.pveCategory' }),
      },
      {
        name: CATEGORY.OTHER,
        positions: [],
        title: intl.formatMessage({ id: 'wowClassic.otherCategory' }),
      },
    ],
    [intl.locale]
  );
  const initialCategory = React.useMemo(
    () =>
      categories.find((category) => category.name === initial) || categories[0],
    [intl.locale]
  );
  const currentCategories = React.useMemo(
    () => [
      initialCategory,
      ...categories.filter(
        (category) => category.name !== initialCategory.name
      ),
    ],
    [intl.locale]
  );

  return (
    <>
      {isVisible && (
        <GameCategories
          initial={initialCategory}
          game={GAME.WOW_CLASSIC}
          categories={currentCategories}
        />
      )}
    </>
  );
};

export default WowClassic;
