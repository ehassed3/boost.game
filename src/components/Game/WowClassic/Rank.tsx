import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowClassicRankResult from '@helpers/getOrderData/getResult/getWowClassicRankResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowClassicRankFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 28%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Rank: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowClassicRankFormValue>({
    ranks: [0, 3],
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW_CLASSIC][CATEGORY.PVP][POSITION.RANK].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW_CLASSIC}&category=${CATEGORY.PVP}&position=${POSITION.RANK}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW_CLASSIC]: {
        ...game[GAME.WOW_CLASSIC],
        [CATEGORY.PVP]: {
          ...game[GAME.WOW_CLASSIC][CATEGORY.PVP],
          [POSITION.RANK]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowClassicRankFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="left"
      game={GAME.WOW_CLASSIC}
      category={CATEGORY.PVP}
      position={POSITION.RANK}
      rating={game[GAME.WOW_CLASSIC][CATEGORY.PVP][POSITION.RANK].rating}
      description={{
        info: [
          intl.formatMessage({ id: 'wowClassicRank.info1Description' }),
          intl.formatMessage({ id: 'wowClassicRank.info2Description' }),
          intl.formatMessage({ id: 'wowClassicRank.info3Description' }),
          intl.formatMessage({ id: 'wowClassicRank.info4Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowClassicRank.eta1Description' }),
          intl.formatMessage({ id: 'wowClassicRank.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowClassicRank.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowClassicRank.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowClassicRank.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowClassicRank.requirements3Description',
          }),
        ],
      }}
      form={{
        duoSlider: {
          name: 'ranks',
          min: 0,
          max: 14,
          startLabel: intl.formatMessage({
            id: 'wowClassicRank.sliderStartLabel',
          }),
          endLabel: intl.formatMessage({ id: 'wowClassicRank.sliderEndLabel' }),
          maxLength: 2,
          step: 1,
          marks: [
            {
              value: 2,
              label: '2',
            },
            {
              value: 4,
              label: '4',
            },
            {
              value: 6,
              label: '6',
            },
            {
              value: 8,
              label: '8',
            },
            {
              value: 10,
              label: '10',
            },
            {
              value: 12,
              label: '12',
            },
            {
              value: 14,
              label: '14',
            },
          ],
        },
        checkboxes: [
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wowClassic.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wowClassic.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowClassicRankResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.PVP,
        currency,
        formValue,
        game: GAME.WOW_CLASSIC,
        locale: intl.locale,
        position: POSITION.RANK,
        promocode,
      }}
    />
  );
};

export default Rank;
