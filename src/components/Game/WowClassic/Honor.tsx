import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';
import WowHeadLink from '@components/Link/WowHead';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowClassicHonorResult from '@helpers/getOrderData/getResult/getWowClassicHonorResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowClassicHonorFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 28%',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Honor: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<WowClassicHonorFormValue>({
    fast: false,
    honor: 200,
    hours: 12,
    isSwitched: false,
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW_CLASSIC][CATEGORY.PVP][POSITION.HONOR].rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW_CLASSIC}&category=${CATEGORY.PVP}&position=${POSITION.HONOR}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW_CLASSIC]: {
        ...game[GAME.WOW_CLASSIC],
        [CATEGORY.PVP]: {
          ...game[GAME.WOW_CLASSIC][CATEGORY.PVP],
          [POSITION.HONOR]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowClassicHonorFormValue, WowClassicHonorFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW_CLASSIC}
      category={CATEGORY.PVP}
      position={POSITION.HONOR}
      rating={game[GAME.WOW_CLASSIC][CATEGORY.PVP][POSITION.HONOR].rating}
      description={{
        info: [
          intl.formatMessage(
            { id: 'wowClassicHonor.info1Description' },
            {
              honor: (
                <WowHeadLink
                  href={intl.formatMessage({ id: 'wowHeadLink.honorLink' })}
                >
                  {intl.formatMessage({ id: 'wowHeadLink.honor' })}
                </WowHeadLink>
              ),
            }
          ),
          intl.formatMessage({ id: 'wowClassicHonor.info2Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.info3Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.info4Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.info5Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.info6Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.info7Description' }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowClassicHonor.eta1Description' }),
          intl.formatMessage({ id: 'wowClassicHonor.eta2Description' }),
        ],
        safe: [intl.formatMessage({ id: 'wowClassicHonor.safeDescription' })],
        requirements: [
          intl.formatMessage({
            id: 'wowClassicHonor.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowClassicHonor.requirements2Description',
          }),
          intl.formatMessage({
            id: 'wowClassicHonor.requirements3Description',
          }),
        ],
      }}
      form={{
        slider: {
          name: 'honor',
          label: intl.formatMessage({ id: 'wowClassicHonor.sliderLabel' }),
          min: 0,
          max: 700,
          step: 50,
          maxLength: 3,
          marks: [
            {
              value: 100,
              label: '100',
            },
            {
              value: 200,
              label: '200',
            },
            {
              value: 300,
              label: '300',
            },
            {
              value: 400,
              label: '400',
            },
            {
              value: 500,
              label: '500',
            },
            {
              value: 600,
              label: '600',
            },
            {
              value: 700,
              label: '700',
            },
          ],
        },
        checkboxes: [
          {
            name: 'fast',
            label: intl.formatMessage(
              {
                id: 'wowClassic.fastPriceCheckboxLabel',
              },
              { price: '+30%' }
            ),
            helperText: intl.formatMessage({
              id: 'wowClassic.fastCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wowClassic.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wowClassic.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      switchedForm={{
        form: {
          slider: {
            name: 'hours',
            label: intl.formatMessage({
              id: 'wowClassicHonor.sliderHourlyLabel',
            }),
            min: 1,
            max: 50,
            step: 1,
            maxLength: 2,
            marks: [
              {
                value: 10,
                label: '10',
              },
              {
                value: 20,
                label: '20',
              },
              {
                value: 30,
                label: '30',
              },
              {
                value: 40,
                label: '40',
              },
              {
                value: 50,
                label: '50',
              },
            ],
          },
          checkboxes: [
            {
              name: 'stream',
              label: intl.formatMessage(
                {
                  id: 'wowClassic.streamPriceCheckboxLabel',
                },
                { price: '+20%' }
              ),
              helperText: intl.formatMessage({
                id: 'wowClassic.streamCheckboxDescription',
              }),
            },
          ],
          value: formValue,
          setValue: setFormValue,
        },
        label: intl.formatMessage({ id: 'wowClassicHonor.switchFormLabel' }),
        isSwitched: formValue.isSwitched,
        setIsSwitched: (newIsSwitched) => {
          setFormValue({ ...formValue, isSwitched: newIsSwitched });
        },
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowClassicHonorResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.PVP,
        currency,
        formValue,
        game: GAME.WOW_CLASSIC,
        locale: intl.locale,
        position: POSITION.HONOR,
        promocode,
      }}
    />
  );
};

export default Honor;
