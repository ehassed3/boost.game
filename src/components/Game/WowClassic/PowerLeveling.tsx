import * as React from 'react';
import useSWR from 'swr';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';

import Position from '@components/Game/Positions/Position';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';
import { useGame } from '@context/game';

import getWowClassicPowerLevelingResult from '@helpers/getOrderData/getResult/getWowClassicPowerLevelingResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';

import getCurrencySign from '@helpers/getCurrencySign';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import Rating from '@interfaces/rating';
import Promocode from '@interfaces/promocode';
import { WowClassicPowerLevelingFormValue } from '@interfaces/formValue';

const useStyles = makeStyles((theme: Theme) => ({
  image: {
    [theme.breakpoints.down('sm')]: {
      objectPosition: '100% 10%',
    },
  },
}));

export enum PROFESSION {
  ALCHEMY = 'alchemy',
  BLACKSMITHING = 'blacksmithing',
  COOKING = 'cooking',
  ENCHANTING = 'enchanting',
  ENGINEERING = 'engineering',
  FIRST_AID = 'first_aid',
  FISHING = 'fishing',
  HERBALISM = 'herbalism',
  LEATHERWORKING = 'leatherworking',
  LOCKPICKING = 'lockpicking',
  MINING = 'mining',
  SKINNING = 'skinning',
  TAILORING = 'tailoring',
}

interface Props {
  className?: string;
  colored?: boolean;
}

const PowerLeveling: React.FC<Props> = ({ className, colored }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const currencySign = getCurrencySign(currency.code);
  const { userStatus } = useUserStatus();
  const [promocode, setPromocode] = React.useState<Promocode | null>(null);

  const [formValue, setFormValue] = React.useState<
    WowClassicPowerLevelingFormValue
  >({
    fast: false,
    levels: [1, 60],
    mount: false,
    professions: [],
    stream: false,
  });

  const { game, setGame } = useGame();
  const { data: rating } = useSWR<Rating>(
    game[GAME.WOW_CLASSIC][CATEGORY.POWER_LEVELING][POSITION.POWER_LEVELING]
      .rating
      ? ''
      : `/api/get_rating/?game=${GAME.WOW_CLASSIC}&category=${CATEGORY.POWER_LEVELING}&position=${POSITION.POWER_LEVELING}`
  );
  React.useEffect(() => {
    if (!rating) {
      return;
    }

    setGame({
      ...game,
      [GAME.WOW_CLASSIC]: {
        ...game[GAME.WOW_CLASSIC],
        [CATEGORY.POWER_LEVELING]: {
          ...game[GAME.WOW_CLASSIC][CATEGORY.POWER_LEVELING],
          [POSITION.POWER_LEVELING]: { rating },
        },
      },
    });
  }, [rating]);

  return (
    <Position<WowClassicPowerLevelingFormValue>
      className={className}
      imgClassName={classes.image}
      colored={colored}
      direction="right"
      game={GAME.WOW_CLASSIC}
      category={CATEGORY.POWER_LEVELING}
      position={POSITION.POWER_LEVELING}
      rating={
        game[GAME.WOW_CLASSIC][CATEGORY.POWER_LEVELING][POSITION.POWER_LEVELING]
          .rating
      }
      description={{
        info: [
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info1Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info2Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info3Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info4Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info5Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info6Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info7Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.info8Description',
          }),
        ],
        eta: [
          intl.formatMessage({ id: 'wowClassicPowerLeveling.eta1Description' }),
          intl.formatMessage({ id: 'wowClassicPowerLeveling.eta2Description' }),
        ],
        safe: [
          intl.formatMessage({ id: 'wowClassicPowerLeveling.safeDescription' }),
        ],
        requirements: [
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.requirements1Description',
          }),
          intl.formatMessage({
            id: 'wowClassicPowerLeveling.requirements2Description',
          }),
        ],
      }}
      form={{
        duoSlider: {
          name: 'levels',
          min: 1,
          max: 60,
          startLabel: intl.formatMessage({
            id: 'wowClassicPowerLeveling.sliderStartLabel',
          }),
          endLabel: intl.formatMessage({
            id: 'wowClassicPowerLeveling.sliderEndLabel',
          }),
          maxLength: 2,
          step: 1,
          marks: [
            {
              value: 10,
              label: '10',
            },
            {
              value: 20,
              label: '20',
            },
            {
              value: 30,
              label: '30',
            },
            {
              value: 40,
              label: '40',
            },
            {
              value: 50,
              label: '50',
            },
            {
              value: 60,
              label: '60',
            },
          ],
        },
        selectList: {
          list: [
            {
              amount: 90,
              value: PROFESSION.BLACKSMITHING,
              label: intl.formatMessage({
                id: 'wowClassic.professionBlacksmithing',
              }),
            },
            {
              amount: 90,
              value: PROFESSION.ENGINEERING,
              label: intl.formatMessage({
                id: 'wowClassic.professionEngineering',
              }),
            },
            {
              amount: 40,
              value: PROFESSION.MINING,
              label: intl.formatMessage({ id: 'wowClassic.professionMining' }),
            },
            {
              amount: 40,
              value: PROFESSION.HERBALISM,
              label: intl.formatMessage({
                id: 'wowClassic.professionHerbalism',
              }),
            },
            {
              amount: 90,
              value: PROFESSION.ALCHEMY,
              label: intl.formatMessage({ id: 'wowClassic.professionAlchemy' }),
            },
            {
              amount: 40,
              value: PROFESSION.SKINNING,
              label: intl.formatMessage({
                id: 'wowClassic.professionSkinning',
              }),
            },
            {
              amount: 90,
              value: PROFESSION.TAILORING,
              label: intl.formatMessage({
                id: 'wowClassic.professionTailoring',
              }),
            },
            {
              amount: 110,
              value: PROFESSION.ENCHANTING,
              label: intl.formatMessage({
                id: 'wowClassic.professionEnchanting',
              }),
            },
            {
              amount: 40,
              value: PROFESSION.FIRST_AID,
              label: intl.formatMessage({
                id: 'wowClassic.professionFirstAid',
              }),
            },
            {
              amount: 80,
              value: PROFESSION.LEATHERWORKING,
              label: intl.formatMessage({
                id: 'wowClassic.professionLeatherworking',
              }),
            },
            {
              amount: 40,
              value: PROFESSION.COOKING,
              label: intl.formatMessage({ id: 'wowClassic.professionCooking' }),
            },
            {
              amount: 40,
              value: PROFESSION.FISHING,
              label: intl.formatMessage({ id: 'wowClassic.professionFishing' }),
            },
            {
              amount: 110,
              value: PROFESSION.LOCKPICKING,
              label: intl.formatMessage({
                id: 'wowClassic.professionLockpicking',
              }),
            },
          ],
          game: GAME.WOW_CLASSIC,
          name: 'professions',
          label: intl.formatMessage({ id: 'wowClassic.professionLabel' }),
          buttonLabel: intl.formatMessage({
            id: 'wowClassic.professionButton',
          }),
          max: 2,
          difference: true,
        },
        checkboxes: [
          {
            name: 'mount',
            label:
              formValue.levels[1] < 40
                ? intl.formatMessage(
                    {
                      id: 'wowClassic.mountPriceCheckboxLabel',
                    },
                    {
                      price: `${Math.round(
                        20 * currency.multiplier
                      )} ${currencySign}`,
                    }
                  )
                : intl.formatMessage({
                    id: 'wowClassic.mountCheckboxLabel',
                  }),
            helperText: intl.formatMessage({
              id: 'wowClassic.mountCheckboxDescription',
            }),
            checked: formValue.levels[1] >= 40 || formValue.mount,
            disabled: formValue.levels[1] >= 40,
          },
          {
            name: 'fast',
            label: intl.formatMessage(
              {
                id: 'wowClassic.fastPriceCheckboxLabel',
              },
              { price: '+30%' }
            ),
            helperText: intl.formatMessage({
              id: 'wowClassic.fastCheckboxDescription',
            }),
          },
          {
            name: 'stream',
            label: intl.formatMessage(
              {
                id: 'wowClassic.streamPriceCheckboxLabel',
              },
              { price: '+20%' }
            ),
            helperText: intl.formatMessage({
              id: 'wowClassic.streamCheckboxDescription',
            }),
          },
        ],
        value: formValue,
        setValue: setFormValue,
      }}
      promocode={promocode}
      setPromocode={setPromocode}
      result={getFormattedResult(
        intl,
        currency,
        getWowClassicPowerLevelingResult(formValue),
        userStatus?.percent,
        promocode?.percent
      )}
      order={{
        category: CATEGORY.POWER_LEVELING,
        currency,
        formValue,
        game: GAME.WOW_CLASSIC,
        locale: intl.locale,
        position: POSITION.POWER_LEVELING,
        promocode,
      }}
    />
  );
};

export default PowerLeveling;
