import * as React from 'react';
import { useIntl } from 'react-intl';
import useSWR from 'swr';
import map from 'lodash/map';
import { motion, AnimatePresence } from 'framer-motion';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

import Diagonal from '@components/Diagonal';
import Order from '@components/Order';

import { PublicOrder } from '@interfaces/order';

const useStyles = makeStyles((theme: Theme) => ({
  session: {
    marginBottom: '160px',
    maxWidth: '960px',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '80px',
    },
  },
  title: {
    ...theme.typography.h3,

    fontWeight: 400,
    textAlign: 'center',
  },
  search: {
    alignItems: 'center',
    display: 'flex',
    width: '100%',
  },
  searchField: {
    display: 'flex',
    flexGrow: 1,
    marginRight: '36px',
  },
  button: {
    position: 'relative',
  },
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
  scrollChips: {
    marginBottom: '40px',
  },
  scrollChip: {
    marginBottom: 'auto',
    marginTop: 'auto',

    '&:not(:last-child)': {
      marginRight: '12px',
    },
  },
  order: {
    maxWidth: '1300px',
  },
}));

const Market: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  const { data: orders, mutate, isValidating: loading } = useSWR<PublicOrder[]>(
    '/api/get_orders/market/'
  );

  return (
    <>
      <MuiContainer className={classes.session}>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'market.title' })}
        </MuiTypography>
      </MuiContainer>
      <AnimatePresence>
        {!!orders && !!orders.length && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.6 } }}
          >
            {map(orders, (order, index) =>
              index % 2 === 0 ? (
                <Diagonal key={order.shortId} colored>
                  <Order
                    className={classes.order}
                    name={order.shortId}
                    order={order}
                    take
                    reload={mutate}
                    loading={loading}
                  />
                </Diagonal>
              ) : (
                <Order
                  key={order.shortId}
                  className={classes.order}
                  name={order.shortId}
                  order={order}
                  take
                  reload={mutate}
                  loading={loading}
                />
              )
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default Market;
