import * as React from 'react';
import Head from 'next/head';
import { NextRouter } from 'next/router';
import { useIntl, IntlShape } from 'react-intl';

import Header from '@components/Header';
import BottomNavigation from '@components/BottomNavigation';
import Chatra from '@components/Chatra';
import CookieSnackbar from '@components/Snackbar/Cookie';
import PaymentStatusForm from '@components/PaymentStatusForm';

import getValueFromCookie from '@helpers/getValueFromCookie';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';

const getHead = (pathname: string, intl: IntlShape) => {
  const pages = pathname.split('/');

  switch (pages[1] || '') {
    case GAME.POE:
      return {
        title: intl.formatMessage({ id: 'head.poePowerLevelingTitle' }),
        description: intl.formatMessage({
          id: 'head.poePowerLevelingDescription',
        }),
      };
    case GAME.WOW:
    case '':
      switch (pages[2] || '') {
        case CATEGORY.CHARACTER_BOOST:
        case '':
          return {
            title: intl.formatMessage({ id: 'head.wowCharacterBoostTitle' }),
            description: intl.formatMessage({
              id: 'head.wowCharacterBoostDescription',
            }),
          };
        case CATEGORY.PVP:
          return {
            title: intl.formatMessage({ id: 'head.wowPvPTitle' }),
            description: intl.formatMessage({ id: 'head.wowPvPDescription' }),
          };
        case CATEGORY.DUNGEONS_AND_RAIDS:
          return {
            title: intl.formatMessage({
              id: 'head.wowDungeonsAndRaidsBoostTitle',
            }),
            description: intl.formatMessage({
              id: 'head.wowDungeonsAndRaidsBoostDescription',
            }),
          };
        case CATEGORY.TORGHAST_AND_SOUL_ASH:
          return {
            title: intl.formatMessage({
              id: 'head.wowTorghastAndSoulAshTitle',
            }),
            description: intl.formatMessage({
              id: 'head.wowTorghastAndSoulAshDescription',
            }),
          };
        case CATEGORY.ACHIEVEMENTS:
          return {
            title: intl.formatMessage({ id: 'head.wowAchievementsTitle' }),
            description: intl.formatMessage({
              id: 'head.wowAchievementsDescription',
            }),
          };
        default:
          return {
            title: intl.formatMessage({ id: 'head.defaultTitle' }),
            description: intl.formatMessage({ id: 'head.defaultDescription' }),
          };
      }
    case GAME.WOW_CLASSIC:
      switch (pages[2] || '') {
        case CATEGORY.POWER_LEVELING:
        case '':
          return {
            title: intl.formatMessage({
              id: 'head.wowClassicPowerLevelingTitle',
            }),
            description: intl.formatMessage({
              id: 'head.wowClassicPowerLevelingDescription',
            }),
          };
        case CATEGORY.PVP:
          return {
            title: intl.formatMessage({ id: 'head.wowClassicPvPTitle' }),
            description: intl.formatMessage({
              id: 'head.wowClassicPvPDescription',
            }),
          };
        default:
          return {
            title: intl.formatMessage({ id: 'head.defaultTitle' }),
            description: intl.formatMessage({ id: 'head.defaultDescription' }),
          };
      }
    case GAME.DOTA_2:
      return {
        title: intl.formatMessage({ id: 'head.dota2Title' }),
        description: intl.formatMessage({ id: 'head.dota2Description' }),
      };
    case 'account':
      return {
        title: intl.formatMessage({ id: 'head.accountTitle' }),
        description: intl.formatMessage({ id: 'head.defaultDescription' }),
      };
    case 'agreement':
      return {
        title: intl.formatMessage({ id: 'head.userAgreementTitle' }),
        description: intl.formatMessage({ id: 'head.defaultDescription' }),
      };
    case 'privacy':
      return {
        title: intl.formatMessage({ id: 'head.privacyPolicyTitle' }),
        description: intl.formatMessage({ id: 'head.defaultDescription' }),
      };
    default:
      return {
        title: intl.formatMessage({ id: 'head.defaultTitle' }),
        description: intl.formatMessage({ id: 'head.defaultDescription' }),
      };
  }
};

interface Props {
  router: NextRouter;
}

const Layout: React.FC<Props> = ({ router, children }) => {
  const intl = useIntl();

  const head = React.useMemo(() => getHead(router.pathname, intl), [
    router.pathname,
    intl.locale,
  ]);

  const [showCookieSnackbar, setShowCookieSnackbar] = React.useState(false);
  React.useEffect(() => {
    if (!getValueFromCookie('privacy')) {
      setShowCookieSnackbar(true);
    }
  }, []);

  return (
    <>
      <Head>
        <title>{head.title}</title>
        <meta name="description" content={head.description} />
      </Head>
      <Header
        locale={router.locale || 'en'}
        locales={router.locales || ['en']}
      />
      <BottomNavigation asPath={router.asPath} pathname={router.pathname} />
      {children}
      <Chatra />
      {showCookieSnackbar && <CookieSnackbar />}
      {router.query.form && router.query.status && router.query.account && (
        <PaymentStatusForm
          status={String(router.query.status)}
          id={String(router.query.account)}
        />
      )}
    </>
  );
};

export default Layout;
