import React from 'react';
import { useIntl } from 'react-intl';
import { useRouter } from 'next/router';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiDialog from '@material-ui/core/Dialog';
import MuiTypography from '@material-ui/core/Typography';
import MuiLink from '@material-ui/core/Link';
import MuiButton from '@material-ui/core/Button';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import MuiCheckCircleIcon from '@material-ui/icons/CheckCircle';
import MuiErrorIcon from '@material-ui/icons/Error';

import getShortMongoId from '@helpers/getShortMongoId';

import STATUS from '@constants/status';
import URL from '@constants/url';

const useStyles = makeStyles((theme: Theme) => ({
  dialogPaper: {
    borderRadius: '16px',
    padding: '12px 20px 16px',
  },
  successIcon: {
    color: theme.palette.success.main,
    height: '36px',
    left: '16px',
    position: 'absolute',
    top: '14px',
    width: '36px',
  },
  failIcon: {
    color: theme.palette.error.main,
    height: '36px',
    left: '16px',
    position: 'absolute',
    top: '14px',
    width: '36px',
  },
  successTitle: {
    ...theme.typography.h4,

    color: theme.palette.success.main,
    fontWeight: 600,
    marginBottom: '20px',
    marginLeft: '40px',
  },
  failTitle: {
    ...theme.typography.h4,

    color: theme.palette.error.main,
    fontWeight: 600,
    marginBottom: '20px',
    marginLeft: '40px',
  },
  id: {
    ...theme.typography.h6,

    marginBottom: '12px',
  },
  text: {
    marginBottom: '12px',
  },
  link: {
    color: theme.palette.text.primary,
    textDecoration: 'underline',
  },
  button: {
    marginLeft: 'auto',
    marginTop: '20px',
    width: '100%',
  },
  close: {
    position: 'absolute',
    right: '12px',
    top: '10px',
  },
}));

interface Props {
  status: string;
  id: string;
}

const PaymentStatusForm: React.FC<Props> = ({ status, id }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(true);
  const router = useRouter();

  const onClose = () => {
    setOpen(false);

    delete router.query.form;
    delete router.query.status;
    delete router.query.account;

    router.replace(
      {
        pathname: router.pathname,
        query: router.query,
      },
      undefined,
      {
        shallow: true,
      }
    );
  };

  React.useEffect(() => {
    if (open && status === STATUS.SUCCESS) {
      gtag('event', 'click', {
        event_category: 'payment',
        event_label: 'Success',
      });
    }
  }, [open]);

  return (
    <MuiDialog
      classes={{ paper: classes.dialogPaper }}
      fullWidth
      maxWidth="xs"
      open={open}
      onClose={onClose}
    >
      <MuiIconButton className={classes.close} onClick={onClose}>
        <MuiCloseIcon fontSize="small" />
      </MuiIconButton>
      {status === STATUS.SUCCESS ? (
        <>
          <MuiCheckCircleIcon className={classes.successIcon} />
          <MuiTypography className={classes.successTitle}>
            {intl.formatMessage({ id: 'paymentStatusForm.successTitle' })}
          </MuiTypography>
          <MuiTypography className={classes.id}>
            {`${intl.formatMessage({
              id: 'paymentStatusForm.order',
            })} #${getShortMongoId(id)}`}
          </MuiTypography>
          <MuiTypography className={classes.text}>
            {intl.formatMessage({ id: 'paymentStatusForm.startSuccessText' })}
          </MuiTypography>
          <MuiTypography>
            {intl.formatMessage(
              {
                id: 'paymentStatusForm.discordSuccessText',
              },
              {
                channel: (
                  <MuiLink
                    className={classes.link}
                    href={URL.DISCORD_GUILD}
                    rel="noopener"
                    target="_blank"
                  >
                    {intl.formatMessage({
                      id: 'paymentStatusForm.discordChannel',
                    })}
                  </MuiLink>
                ),
              }
            )}
          </MuiTypography>
          <MuiTypography>
            {intl.formatMessage({ id: 'paymentStatusForm.privateText' })}
          </MuiTypography>
        </>
      ) : (
        <>
          <MuiErrorIcon className={classes.failIcon} />
          <MuiTypography className={classes.failTitle}>
            {intl.formatMessage({ id: 'paymentStatusForm.failTitle' })}
          </MuiTypography>
          <MuiTypography className={classes.id}>
            {`${intl.formatMessage({
              id: 'paymentStatusForm.order',
            })} #${getShortMongoId(id)}`}
          </MuiTypography>
          <MuiTypography className={classes.text}>
            {intl.formatMessage({ id: 'paymentStatusForm.startFailText' })}
          </MuiTypography>
          <MuiTypography>
            {intl.formatMessage(
              {
                id: 'paymentStatusForm.discordFailText',
              },
              {
                channel: (
                  <MuiLink
                    className={classes.link}
                    href={URL.DISCORD_GUILD}
                    rel="noopener"
                    variant="body1"
                    target="_blank"
                  >
                    {intl.formatMessage({
                      id: 'paymentStatusForm.discordChannel',
                    })}
                  </MuiLink>
                ),
              }
            )}
          </MuiTypography>
          <MuiTypography>
            {intl.formatMessage({ id: 'paymentStatusForm.problemText' })}
          </MuiTypography>
        </>
      )}
      <MuiButton
        className={classes.button}
        variant="contained"
        onClick={onClose}
      >
        {intl.formatMessage({ id: 'paymentStatusForm.button' })}
      </MuiButton>
    </MuiDialog>
  );
};

export default PaymentStatusForm;
