import React from 'react';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiMenu from '@material-ui/core/Menu';
import MuiMenuItem from '@material-ui/core/MenuItem';
import MuiTypography from '@material-ui/core/Typography';

import { useCurrency } from '@context/currency';

import getCurrencySign from '@helpers/getCurrencySign';

import CURRENCIES from '@constants/currencies';

import CurrencyType from '@interfaces/currency';

const useStyles = makeStyles((theme: Theme) => ({
  currency: {
    padding: '16px',

    [theme.breakpoints.down('xs')]: {
      padding: '12px',
    },
  },
  label: {
    height: '22px',
    width: '22px',

    [theme.breakpoints.down('xs')]: {
      height: '20px',
      width: '20px',
    },
  },
  item: {
    boxSizing: 'content-box',
    display: 'flex',
    height: '22px',
    justifyContent: 'center',
    padding: '15px',
    width: '22px',

    [theme.breakpoints.down('xs')]: {
      height: '20px',
      minHeight: '20px',
      padding: '12px',
      width: '20px',
    },
  },
  sign: {
    fontSize: '22px',
    fontWeight: 600,
  },
}));

interface Props {
  className?: string;
}

const CurrencyButton: React.FC<Props> = ({ className }) => {
  const classes = useStyles();
  const { currency, setCurrency } = useCurrency();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const onClick = (newCurrency: CurrencyType) => () => {
    setCurrency(newCurrency);
    setAnchorEl(null);
  };

  return (
    <>
      <MuiIconButton
        classes={{
          root: className
            ? `${className} ${classes.currency}`
            : classes.currency,
          label: classes.label,
        }}
        onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
          setAnchorEl(event.currentTarget);
        }}
      >
        <MuiTypography className={classes.sign}>
          {getCurrencySign(currency.code)}
        </MuiTypography>
      </MuiIconButton>
      <MuiMenu
        anchorEl={anchorEl}
        keepMounted
        open={!!anchorEl}
        onClose={() => {
          setAnchorEl(null);
        }}
      >
        {map(CURRENCIES, (currencyItem) => (
          <MuiMenuItem
            key={currencyItem.code}
            className={classes.item}
            onClick={onClick(currencyItem)}
            selected={currency.code === currencyItem.code}
          >
            <MuiTypography className={classes.sign}>
              {getCurrencySign(currencyItem.code)}
            </MuiTypography>
          </MuiMenuItem>
        ))}
      </MuiMenu>
    </>
  );
};

export default CurrencyButton;
