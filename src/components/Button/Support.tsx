import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiButton from '@material-ui/core/Button';

import { useSupport } from '@context/support';

import GAME from '@constants/game';
import POSITION from '@constants/position';

const useStyles = makeStyles((theme: Theme) => ({
  button: {
    alignItems: 'center',
    border: `1px solid ${theme.palette.secondary.main}`,
    display: 'flex',
    whiteSpace: 'nowrap',
  },
}));

interface Props {
  className?: string;
  game?: GAME;
  position?: POSITION;
}

const SupportButton: React.FC<Props> = ({ className, game, position }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { setSupport } = useSupport();

  return (
    <>
      <MuiButton
        className={
          className ? `${className} ${classes.button}` : classes.button
        }
        color="secondary"
        variant="outlined"
        size="large"
        onClick={() => {
          gtag('event', 'click', {
            event_category: 'support',
            event_label: `Show service chat`,
          });
          setSupport({ open: true, message: '', game, position });
        }}
      >
        {intl.formatMessage({ id: 'supportButton.title' })}
      </MuiButton>
    </>
  );
};

export default SupportButton;
