import React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiLink from '@material-ui/core/Link';
import MuiButton from '@material-ui/core/Button';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiTypography from '@material-ui/core/Typography';

import DiscordIcon from '@icons/Discord';
import DiscordSymbolIcon from '@icons/Discord/Symbol';

import URL from '@constants/url';

const useStyles = makeStyles((theme: Theme) => ({
  discordButton: {
    color: 'inherit',
    padding: '3px 0 4px',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  discordMobileButton: {
    display: 'none',
    padding: '10px',

    [theme.breakpoints.down('xs')]: {
      display: 'block',
    },
  },
  discordLabel: {
    alignItems: 'flex-end',
    display: 'flex',
    flexDirection: 'column',
  },
  discordIcon: {
    display: 'block',
    height: '36px',
    width: '120px',

    [theme.breakpoints.down('xs')]: {
      height: '32px',
      width: '96px',
    },
  },
  discordText: {
    fontSize: '15px',
    fontWeight: 600,
    marginRight: '12px',
    marginTop: '-9px',
    textTransform: 'none',

    [theme.breakpoints.down('xs')]: {
      fontSize: '13px',
      marginRight: '5px',
    },
  },
}));

interface Props {
  className?: string;
}

const DiscordButton: React.FC<Props> = ({ className }) => {
  const classes = useStyles();

  return (
    <>
      <MuiButton
        className={
          className
            ? `${className} ${classes.discordButton}`
            : classes.discordButton
        }
        classes={{ label: classes.discordLabel }}
        color="inherit"
        component={MuiLink}
        underline="none"
        href={URL.DISCORD_INVITE}
        rel="noopener"
        target="_blank"
      >
        <DiscordIcon className={classes.discordIcon} />
        <MuiTypography className={classes.discordText}>Join us</MuiTypography>
      </MuiButton>
      <MuiIconButton
        className={
          className
            ? `${className} ${classes.discordMobileButton}`
            : classes.discordMobileButton
        }
        component={MuiLink}
        href={URL.DISCORD_INVITE}
        rel="noopener"
        target="_blank"
      >
        <DiscordSymbolIcon />
      </MuiIconButton>
    </>
  );
};

export default DiscordButton;
