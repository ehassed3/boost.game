import * as React from 'react';
import { useRouter } from 'next/router';
import { useSession, signIn } from 'next-auth/client';
import { motion } from 'framer-motion';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiPersonIcon from '@material-ui/icons/Person';

import Avatar from '@components/Avatar';

const useStyles = makeStyles((theme: Theme) => ({
  account: {
    padding: '13px',
  },
  personLabel: {
    position: 'relative',
  },
  personIcon: {
    height: '28px',
    width: '28px',
  },
  avatar: {
    height: '28px',
    position: 'absolute',
    right: '-12px',
    top: '10px',
    width: '28px',

    [theme.breakpoints.down('xs')]: {
      right: '-8px',
      height: '24px',
      width: '24px',
    },
  },
}));

interface Props {
  className?: string;
}

const AccountButton: React.FC<Props> = ({ className }) => {
  const classes = useStyles();

  const router = useRouter();
  const [session, loading] = useSession();
  const [clicked, setClicked] = React.useState(false);

  React.useEffect(() => {
    if (loading || !clicked) {
      return;
    }

    setClicked(false);

    if (session) {
      router.push('/account');
      return;
    }

    signIn('discord');
  }, [loading]);

  return (
    <MuiIconButton
      classes={{
        root: className ? `${className} ${classes.account}` : classes.account,
        label: classes.personLabel,
      }}
      onClick={() => {
        if (loading) {
          setClicked(true);
          return;
        }

        if (session) {
          router.push('/account');
          return;
        }

        signIn('discord');
      }}
    >
      <MuiPersonIcon className={classes.personIcon} />
      {!!session && (
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <Avatar className={classes.avatar} src={session.user.image} />
        </motion.div>
      )}
    </MuiIconButton>
  );
};

export default AccountButton;
