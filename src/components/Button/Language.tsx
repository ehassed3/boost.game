import React from 'react';
import { useRouter } from 'next/router';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiMenu from '@material-ui/core/Menu';
import MuiMenuItem from '@material-ui/core/MenuItem';

import FlagEn from '@icons/Flag/En';
import FlagDe from '@icons/Flag/De';
import FlagRu from '@icons/Flag/Ru';

import { useCurrency } from '@context/currency';

import getCurrencyByLocale from '@helpers/getCurrencyByLocale';

const useStyles = makeStyles((theme: Theme) => ({
  language: {
    padding: '16px',

    [theme.breakpoints.down('xs')]: {
      padding: '12px',
    },
  },
  item: {
    padding: '15px',

    [theme.breakpoints.down('xs')]: {
      padding: '12px',
    },
  },
  flag: {
    height: '22px',
    width: '22px',

    [theme.breakpoints.down('xs')]: {
      height: '20px',
      width: '20px',
    },
  },
}));

const getFlag = (locale: string) => {
  switch (locale) {
    case 'en':
      return FlagEn;
    case 'de':
      return FlagDe;
    case 'ru':
      return FlagRu;
    default:
      return FlagEn;
  }
};

interface Props {
  className?: string;
  locale: string;
  locales: string[];
}

const LanguageButton: React.FC<Props> = ({ className, locale, locales }) => {
  const classes = useStyles();

  const router = useRouter();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const [language, setLanguage] = React.useState(locale);
  const { setCurrency } = useCurrency();

  const onClick = (locale: string) => () => {
    setLanguage(locale);
    setCurrency(getCurrencyByLocale(locale));
    router.replace(router.asPath, router.asPath, { locale });
    document.cookie = `NEXT_LOCALE=${locale}; path=/; expires=Tue, 19 Jan 2038 03:14:07 UTC;`;

    setAnchorEl(null);
  };

  const CurrentFlag = getFlag(language);

  return (
    <>
      <MuiIconButton
        className={
          className ? `${className} ${classes.language}` : classes.language
        }
        onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
          setAnchorEl(event.currentTarget);
        }}
      >
        <CurrentFlag className={classes.flag} />
      </MuiIconButton>
      <MuiMenu
        anchorEl={anchorEl}
        keepMounted
        open={!!anchorEl}
        onClose={() => {
          setAnchorEl(null);
        }}
      >
        {map(locales, (localeItem) => {
          const Flag = getFlag(localeItem);

          return (
            <MuiMenuItem
              key={localeItem}
              className={classes.item}
              onClick={onClick(localeItem)}
              selected={language === localeItem}
            >
              <Flag className={classes.flag} />
            </MuiMenuItem>
          );
        })}
      </MuiMenu>
    </>
  );
};

export default LanguageButton;
