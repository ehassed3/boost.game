import React from 'react';
import { useIntl } from 'react-intl';
import SwipeableViews from 'react-swipeable-views';
import { Magnifier, TOUCH_ACTIVATION } from 'react-image-magnifiers';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTooltip from '@material-ui/core/Tooltip';
import MuiButton from '@material-ui/core/Button';
import MuiDialog from '@material-ui/core/Dialog';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiTypography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';

import MuiCloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme: Theme) => ({
  button: {
    alignItems: 'center',
    display: 'flex',
    width: '100%',
  },
  imageWrapper: {
    alignItems: 'center',
    display: 'flex',
    height: '80vh',
    justifyContent: 'center',
  },
  image: {
    alignItems: 'center',
    display: 'flex',
    height: '100%',
    maxHeight: '100%',
    maxWidth: '100%',

    '& div': {
      display: 'flex',
      height: '100%',
    },

    '& img': {
      margin: 'auto',
      width: '100%',
    },
  },
  close: {
    position: 'absolute',
    right: '16px',
    top: '16px',

    [theme.breakpoints.down('xs')]: {
      right: '0',
      top: '0',
    },
  },
  caption: {
    color: theme.palette.text.secondary,
    left: '16px',
    position: 'absolute',
    pointerEvents: 'none',
    top: '24px',

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  touchCaption: {
    ...theme.typography.body2,

    bottom: '28px',
    color: theme.palette.text.secondary,
    display: 'none',
    left: '50%',
    position: 'absolute',
    pointerEvents: 'none',
    textAlign: 'center',
    transform: 'translateX(-50%)',
    width: '148px',

    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },
  pagination: {
    bottom: '16px',
    position: 'absolute',
    right: '16px',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
}));

interface Props {
  className?: string;
  screenshots?: string[];
  size?: 'medium' | 'large' | 'small';
  disabled?: boolean;
  tooltip?: string;
}

const ScreenshotsButton: React.FC<Props> = ({
  className,
  screenshots = [],
  size,
  disabled,
  tooltip,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [activeStep, setActiveStep] = React.useState(0);
  const [swipeDisabled, setSwipeDisabled] = React.useState(false);

  const onChange = (_: React.ChangeEvent<unknown>, paginationStep: number) => {
    setActiveStep(paginationStep - 1);
  };

  return (
    <>
      <MuiTooltip title={tooltip || ''}>
        <div className={className}>
          <MuiButton
            className={classes.button}
            color="inherit"
            size={size}
            variant="outlined"
            disabled={disabled}
            onClick={() => setOpen(true)}
          >
            {intl.formatMessage({ id: 'screenshots.title' })}
          </MuiButton>
        </div>
      </MuiTooltip>
      {!!screenshots.length && open && (
        <MuiDialog
          maxWidth="md"
          fullWidth
          open={open}
          onClose={() => setOpen(false)}
        >
          <SwipeableViews
            index={activeStep}
            onChangeIndex={(step: number) => {
              setActiveStep(step);
            }}
            enableMouseEvents
            disabled={swipeDisabled}
          >
            {screenshots.map((screenshot) => (
              <div key={screenshot} className={classes.imageWrapper}>
                <Magnifier
                  className={classes.image}
                  imageSrc={screenshot}
                  imageAlt="screenshot"
                  touchActivation={TOUCH_ACTIVATION.DOUBLE_TAP}
                  onZoomStart={() => {
                    setSwipeDisabled(true);
                  }}
                  onZoomEnd={() => {
                    setSwipeDisabled(false);
                  }}
                />
              </div>
            ))}
          </SwipeableViews>
          <MuiIconButton
            className={classes.close}
            onClick={() => setOpen(false)}
          >
            <MuiCloseIcon fontSize="small" />
          </MuiIconButton>
          {!swipeDisabled && (
            <MuiTypography className={classes.caption}>
              {intl.formatMessage({ id: 'screenshots.clickZoom' })}
            </MuiTypography>
          )}
          {!swipeDisabled && (
            <MuiTypography className={classes.touchCaption}>
              {intl.formatMessage({ id: 'screenshots.tapZoom' })}
            </MuiTypography>
          )}
          <Pagination
            className={classes.pagination}
            count={screenshots.length}
            color="primary"
            size="large"
            page={activeStep + 1}
            onChange={onChange}
          />
        </MuiDialog>
      )}
    </>
  );
};

export default ScreenshotsButton;
