import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';
import MuiCheckbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  checkbox: {
    alignItems: 'flex-start',
  },
  label: {
    margin: 'auto',
  },
  helperText: {
    display: 'flex',
  },
}));

export interface Props extends CheckboxProps {
  name: string;
  label?: React.ReactNode;
  helperText?: React.ReactNode;
  checked?: boolean;
}

const Checkbox: React.FC<Props> = ({
  className,
  name,
  label,
  helperText,
  checked,
  ...props
}) => {
  const classes = useStyles();

  return (
    <MuiFormControlLabel
      control={<MuiCheckbox {...props} />}
      classes={{
        root: className ? `${className} ${classes.checkbox}` : classes.checkbox,
        label: classes.label,
      }}
      name={name}
      label={
        helperText ? (
          <>
            <MuiTypography>{label}</MuiTypography>
            {helperText && (
              <MuiTypography
                className={classes.helperText}
                variant="caption"
                color="textSecondary"
              >
                {helperText}
              </MuiTypography>
            )}
          </>
        ) : (
          label
        )
      }
      checked={checked}
    />
  );
};

export default Checkbox;
