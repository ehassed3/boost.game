import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';

import Link from '@components/Link';

import Logo from '@icons/Logo';
import VisaIcon from '@icons/PaymentSystem/Visa';
import MasterCardIcon from '@icons/PaymentSystem/MasterCard';
import ApplePayIcon from '@icons/PaymentSystem/ApplePay';
import GooglePayIcon from '@icons/PaymentSystem/GooglePay';

const useStyles = makeStyles((theme: Theme) => ({
  footer: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 'auto',
    paddingBottom: '72px',
    paddingTop: '200px',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
      flexDirection: 'column',
      paddingBottom: '68px',
      paddingTop: '120px',
    },
  },
  address: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
    },
  },
  policy: {
    alignItems: 'flex-end',
    display: 'flex',
    flexDirection: 'column',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
      flexWrap: 'wrap',
      justifyContent: 'center',
      margin: '20px 20px 0',
    },
  },
  policyLinks: {
    alignItems: 'center',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',

    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
    },
  },
  terms: {
    ...theme.typography.body1,

    color: theme.palette.text.primary,
    marginRight: '12px',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.body2,

      margin: '0 6px 6px',
    },
  },
  policyDivider: {
    margin: '0',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  privacy: {
    ...theme.typography.body1,

    color: theme.palette.text.primary,
    marginLeft: '12px',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.body2,

      margin: '0 6px 6px',
    },
  },
  paymentSystems: {
    alignItems: 'center',
    display: 'flex',
    height: '36px',
    marginTop: '12px',

    [theme.breakpoints.down('xs')]: {
      height: '32px',
      marginBottom: '16px',
    },
  },
  visa: {
    height: '76px',
    marginRight: '16px',
    width: '62px',
  },
  masterCard: {
    height: '32px',
    marginRight: '20px',
    width: '41px',
  },
  applePay: {
    height: '29px',
    marginRight: '20px',
    width: '46px',
  },
  googlePay: {
    height: '23px',
    width: '54px',
  },
}));

const Footer: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <MuiContainer className={classes.footer} component="footer">
      <section className={classes.address}>
        <Logo />
      </section>
      <section className={classes.policy}>
        <div className={classes.policyLinks}>
          <Link className={classes.terms} href="/agreement">
            {intl.formatMessage({ id: 'footer.agreement' })}
          </Link>
          <p className={classes.policyDivider}>•</p>
          <Link className={classes.privacy} href="/privacy">
            {intl.formatMessage({ id: 'footer.privacy' })}
          </Link>
        </div>
        <div className={classes.paymentSystems}>
          <VisaIcon className={classes.visa} />
          <MasterCardIcon className={classes.masterCard} />
          <ApplePayIcon className={classes.applePay} />
          <GooglePayIcon className={classes.googlePay} />
        </div>
      </section>
    </MuiContainer>
  );
};

export default Footer;
