import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';
import MuiTypography from '@material-ui/core/Typography';
import MuiButton from '@material-ui/core/Button';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import STATUS from '@constants/status';
import SERVER from '@constants/server';

import { PublicOrder } from '@interfaces/order';

const useStyles = makeStyles((theme: Theme) => ({
  panel: {
    display: 'flex',
    flexDirection: 'column',
  },
  status: {
    display: 'flex',
  },
  statusTitle: {
    ...theme.typography.h6,

    marginTop: '4px',
    marginRight: '20px',
  },
  statusItem: {
    ...theme.typography.h6,

    textTransform: 'capitalize',
  },
  statusSelect: {
    ...theme.typography.h6,

    flexGrow: 1,
    textTransform: 'capitalize',
    width: '120px',
  },
  saveButton: {
    marginTop: '32px',
    position: 'relative',
  },
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
  order: PublicOrder;
  reload?: () => void;
  loading?: boolean;
}

const BoosterPanel: React.FC<Props> = ({
  className,
  order,
  reload,
  loading,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [panelStatus, setPanelStatus] = React.useState(order.status);

  const [loadingData, setLoadingData] = React.useState(false);

  React.useEffect(() => {
    if (loading) {
      return;
    }

    setLoadingData(false);
  }, [loading]);

  const onSaveClick = () => {
    if (order.status === panelStatus) {
      return;
    }

    setLoadingData(true);

    fetch(`${SERVER}/api/update_order/booster/`, {
      method: 'POST',
      body: JSON.stringify({
        shortId: order.shortId,
        status: panelStatus,
      }),
    })
      .then(() => {
        if (reload) {
          reload();
          return;
        }

        setLoadingData(false);
      })
      .catch(() => {
        setLoadingData(false);
      });
  };

  return (
    <div
      className={className ? `${className} ${classes.panel}` : classes.panel}
    >
      <div className={classes.status}>
        <MuiTypography className={classes.statusTitle}>
          {intl.formatMessage({ id: 'orderPanel.statusTitle' })}
        </MuiTypography>
        <MuiSelect
          className={classes.statusSelect}
          value={panelStatus}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
            setPanelStatus(event.target.value as STATUS);
          }}
        >
          <MuiMenuItem className={classes.statusItem} value={STATUS.PAUSED}>
            {STATUS.PAUSED}
          </MuiMenuItem>
          <MuiMenuItem className={classes.statusItem} value={STATUS.LIVE}>
            {STATUS.LIVE}
          </MuiMenuItem>
        </MuiSelect>
      </div>
      <MuiButton
        className={classes.saveButton}
        disabled={loadingData || order.status === panelStatus}
        variant="contained"
        size="large"
        onClick={onSaveClick}
      >
        {intl.formatMessage({ id: 'orderPanel.saveButton' })}
        {loadingData && <MuiLinearProgress className={classes.loader} />}
      </MuiButton>
    </div>
  );
};

export default BoosterPanel;
