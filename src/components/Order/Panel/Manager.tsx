import * as React from 'react';
import { useIntl } from 'react-intl';
import map from 'lodash/map';
import uniqueId from 'lodash/uniqueId';
import isEqual from 'lodash/isEqual';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';
import MuiTypography from '@material-ui/core/Typography';
import MuiTextField from '@material-ui/core/TextField';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import MuiCloseIcon from '@material-ui/icons/Close';

import Avatar from '@components/Avatar';

import getPositonBoosters from '@helpers/getPositionBoosters';

import STATUS from '@constants/status';
import SERVER from '@constants/server';

import { PublicOrder } from '@interfaces/order';
import { Booster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  panel: {
    display: 'flex',
    flexDirection: 'column',
  },
  status: {
    display: 'flex',
  },
  statusTitle: {
    ...theme.typography.h6,

    marginTop: '4px',
    marginRight: '20px',
  },
  statusItem: {
    ...theme.typography.h6,

    textTransform: 'capitalize',
  },
  statusSelect: {
    ...theme.typography.h6,

    flexGrow: 1,
    textTransform: 'capitalize',
    width: '120px',
  },
  booster: {
    display: 'flex',
    marginTop: '24px',
  },
  boosterTitle: {
    ...theme.typography.h6,

    marginTop: '4px',
    marginRight: '20px',
  },
  boosterSelect: {
    ...theme.typography.h6,

    flexGrow: 1,
  },
  boosterItem: {
    display: 'flex',
  },
  boosterAvatar: {
    height: '28px',
    width: '28px',
  },
  boosterName: {
    ...theme.typography.h6,

    marginLeft: '8px',
  },
  screenshots: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '36px',
  },
  screenshotsTitle: {
    ...theme.typography.h6,
  },
  screenshot: {
    alignItems: 'center',
    display: 'flex',
    marginTop: '16px',
  },
  panelScreenshot: {
    flexGrow: 1,
    marginRight: '20px',
  },
  addScreenshotButton: {
    marginTop: '12px',
    marginLeft: 'auto',
  },
  saveButton: {
    marginTop: '32px',
    position: 'relative',
  },
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
  boosters: Booster[];
  order: PublicOrder;
  reload?: () => void;
  loading?: boolean;
}

const ManagerPanel: React.FC<Props> = ({
  className,
  boosters,
  order,
  reload,
  loading,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const positionBoosters = getPositonBoosters(
    boosters,
    order.game,
    order.position,
    order.category
  );

  const [panelBooster, setPanelBooster] = React.useState(
    positionBoosters
      ? positionBoosters.find((booster) => booster.name === order.booster?.name)
      : undefined
  );
  const [panelScreenshots, setPanelScreenshots] = React.useState(
    map(order.screenshots || [], (screenshot) => ({
      key: uniqueId(),
      value: screenshot,
    }))
  );
  const [panelStatus, setPanelStatus] = React.useState(order.status);

  const disabledBooster = order.booster?.name === panelBooster?.name;
  const screenshots = map(
    panelScreenshots,
    (panelScreenshot) => panelScreenshot.value
  );
  const disabledScreenshots = isEqual(order.screenshots || [], screenshots);
  const disabledStatus = order.status === panelStatus;
  const disabled = disabledBooster && disabledScreenshots && disabledStatus;

  const [loadingData, setLoadingData] = React.useState(false);

  React.useEffect(() => {
    if (loading) {
      return;
    }

    setLoadingData(false);
  }, [loading]);

  const onSaveClick = () => {
    if (disabled) {
      return;
    }

    setLoadingData(true);

    fetch(`${SERVER}/api/update_order/manager/`, {
      method: 'POST',
      body: JSON.stringify({
        shortId: order.shortId,
        ...(!disabledBooster && panelBooster
          ? { booster: panelBooster.email }
          : {}),
        ...(!disabledScreenshots ? { screenshots } : {}),
        ...(!disabledStatus ? { status: panelStatus } : {}),
      }),
    })
      .then(() => {
        if (reload) {
          reload();
          return;
        }

        setLoadingData(false);
      })
      .catch(() => {
        setLoadingData(false);
      });
  };

  return (
    <div
      className={className ? `${className} ${classes.panel}` : classes.panel}
    >
      <div className={classes.status}>
        <MuiTypography className={classes.statusTitle}>
          {intl.formatMessage({ id: 'orderPanel.statusTitle' })}
        </MuiTypography>
        <MuiSelect
          className={classes.statusSelect}
          value={panelStatus}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
            setPanelStatus(event.target.value as STATUS);
          }}
        >
          <MuiMenuItem className={classes.statusItem} value={STATUS.PAUSED}>
            {STATUS.PAUSED}
          </MuiMenuItem>
          <MuiMenuItem className={classes.statusItem} value={STATUS.LIVE}>
            {STATUS.LIVE}
          </MuiMenuItem>
          <MuiMenuItem className={classes.statusItem} value={STATUS.CLOSED}>
            {STATUS.CLOSED}
          </MuiMenuItem>
        </MuiSelect>
      </div>
      {!!positionBoosters.length && (
        <div className={classes.booster}>
          <MuiTypography className={classes.boosterTitle}>
            {intl.formatMessage({ id: 'orderPanel.boosterTitle' })}
          </MuiTypography>
          <MuiSelect
            className={classes.boosterSelect}
            value={panelBooster?.email || ''}
            renderValue={() =>
              panelBooster ? (
                <div className={classes.boosterItem}>
                  <Avatar
                    className={classes.boosterAvatar}
                    src={panelBooster.image}
                    alt={panelBooster.name}
                  />
                  <MuiTypography className={classes.boosterName}>
                    {panelBooster.name}
                  </MuiTypography>
                </div>
              ) : (
                <div />
              )
            }
            onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
              const newBooster = positionBoosters.find(
                (boosterItem) => boosterItem.email === event.target.value
              );

              if (!newBooster) {
                return;
              }

              setPanelBooster(newBooster);
            }}
          >
            {map(positionBoosters, (booster) => (
              <MuiMenuItem
                key={booster.email}
                className={classes.boosterItem}
                value={booster.email}
              >
                <Avatar
                  className={classes.boosterAvatar}
                  src={booster.image}
                  alt={booster.name}
                />
                <MuiTypography className={classes.boosterName}>
                  {booster.name}
                </MuiTypography>
              </MuiMenuItem>
            ))}
          </MuiSelect>
        </div>
      )}
      <div className={classes.screenshots}>
        <MuiTypography className={classes.screenshotsTitle}>
          {intl.formatMessage({ id: 'orderPanel.screenshotsTitle' })}
        </MuiTypography>
        {map(panelScreenshots, (panelScreenshot, index) => (
          <div key={panelScreenshot.key} className={classes.screenshot}>
            <MuiTextField
              className={classes.panelScreenshot}
              value={panelScreenshot.value}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                const newPanelScreenshots = [...panelScreenshots];
                newPanelScreenshots[index].value = event.target.value;
                setPanelScreenshots(newPanelScreenshots);
              }}
              onKeyPress={(event) => {
                const element = event.target as HTMLInputElement;

                if (!element || event.key !== 'Enter') {
                  return;
                }

                element.blur();
              }}
            />
            <MuiIconButton
              onClick={() => {
                const newPanelScreenshots = [...panelScreenshots];
                newPanelScreenshots.splice(index, 1);
                setPanelScreenshots(newPanelScreenshots);
              }}
            >
              <MuiCloseIcon fontSize="small" />
            </MuiIconButton>
          </div>
        ))}
        <MuiButton
          className={classes.addScreenshotButton}
          color="inherit"
          onClick={() => {
            setPanelScreenshots([
              ...panelScreenshots,
              { key: uniqueId(), value: '' },
            ]);
          }}
        >
          {intl.formatMessage({ id: 'orderPanel.screenshotsButton' })}
        </MuiButton>
      </div>
      <MuiButton
        className={classes.saveButton}
        disabled={loadingData || disabled}
        variant="contained"
        size="large"
        onClick={onSaveClick}
      >
        {intl.formatMessage({ id: 'orderPanel.saveButton' })}
        {loadingData && <MuiLinearProgress className={classes.loader} />}
      </MuiButton>
    </div>
  );
};

export default ManagerPanel;
