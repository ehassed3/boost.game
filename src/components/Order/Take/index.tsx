import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles } from '@material-ui/core/styles';
import MuiButton from '@material-ui/core/Button';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import SERVER from '@constants/server';

const useStyles = makeStyles(() => ({
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
  shortId: string;
  reload?: () => void;
  loading?: boolean;
}

const TakeOrder: React.FC<Props> = ({
  className,
  shortId,
  reload,
  loading,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [loadingData, setLoadingData] = React.useState(false);

  React.useEffect(() => {
    if (loading) {
      return;
    }

    setLoadingData(false);
  }, [loading]);

  const onClick = () => {
    setLoadingData(true);

    fetch(`${SERVER}/api/take_order/`, {
      method: 'POST',
      body: JSON.stringify({ shortId }),
    })
      .then(() => {
        if (reload) {
          reload();
          return;
        }

        setLoadingData(false);
      })
      .catch(() => {
        setLoadingData(false);
      });
  };

  return (
    <MuiButton
      className={className}
      variant="contained"
      color="primary"
      size="large"
      disabled={loadingData}
      onClick={onClick}
    >
      {intl.formatMessage({ id: 'takeOrder.button' })}
      {loadingData && <MuiLinearProgress className={classes.loader} />}
    </MuiButton>
  );
};

export default TakeOrder;
