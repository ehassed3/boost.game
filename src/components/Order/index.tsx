import * as React from 'react';
import { useIntl, IntlShape } from 'react-intl';
import { Element as ScrollElement } from 'react-scroll';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import MuiTooltip from '@material-ui/core/Tooltip';
import MuiButton from '@material-ui/core/Button';
import MuiRating from '@material-ui/lab/Rating';

import Link from '@components/Link';
import GameLogo from '@components/Game/Logo';
import Avatar from '@components/Avatar';
import Result from '@components/Result';
import ScreenshotsButton from '@components/Button/Screenshots';
import AddReview from '@components/Reviews/Add';
import Take from '@components/Order/Take';
import AdminPanel from '@components/Order/Panel/Admin';
import BoosterPanel from '@components/Order/Panel/Booster';
import ManagerPanel from '@components/Order/Panel/Manager';

import DiscordTextIcon from '@icons/Discord/Text';
import TwitchTextIcon from '@icons/Twitch/Text';
import PulseSecondary from '@icons/Pulse/Secondary';
import PulseWarning from '@icons/Pulse/Warning';

import { GUILD_ID } from '@constants/discord';
import ROLE from '@constants/role';
import STATUS from '@constants/status';

import { PublicOrder } from '@interfaces/order';
import { Manager, PublicBooster, Booster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  order: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingBottom: '56px',
    paddingTop: '32px',
    position: 'relative',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      paddingBottom: '80px',
      paddingTop: '44px',
    },

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      paddingBottom: '40px',
      paddingTop: '20px',
    },
  },
  content: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    marginRight: '20px',
    paddingTop: '20px',
    width: '52%',

    [theme.breakpoints.down('sm')]: {
      marginRight: '0',
      width: '100%',
    },
  },
  head: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: '40px',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      marginTop: '20px',
      width: '100%',
    },
  },
  headText: {
    ...theme.typography.h3,

    fontWeight: 500,
    marginLeft: '28px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h4,

      marginLeft: '0',
      marginTop: '28px',
    },
  },
  headDivider: {
    ...theme.typography.h3,

    marginLeft: '28px',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  game: {
    flexGrow: 1,
    height: '108px',
    maxWidth: '280px',
  },
  gameImage: {
    height: '100%',
    width: '100%',
  },
  title: {
    ...theme.typography.h5,

    fontWeight: 500,
    marginBottom: '12px',
    wordBreak: 'break-word',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,
    },
  },
  details: {
    margin: '12px 28px',
    padding: '0',
  },
  detail: {
    ...theme.typography.h6,

    marginBottom: '8px',
  },
  booster: {
    display: 'flex',
    marginBottom: '8px',
    marginTop: 'auto',
    paddingTop: '120px',

    [theme.breakpoints.down('xs')]: {
      paddingTop: '60px',
    },
  },
  manager: {
    display: 'flex',
    marginBottom: '60px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '80px',
    },
  },
  roleTitle: {
    ...theme.typography.h6,

    marginRight: '16px',
    marginTop: '5px',
  },
  roleLink: {
    color: theme.palette.text.primary,
    display: 'flex',
    textDecoration: 'none',
    textTransform: 'none',

    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      textDecoration: 'none',
    },
  },
  roleAvatar: {
    height: '28px',
    width: '28px',
  },
  roleName: {
    ...theme.typography.h6,

    marginLeft: '8px',
  },
  controls: {
    display: 'flex',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  screenshots: {
    marginRight: 'auto',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '16px',
      width: '100%',
    },
  },
  discordChannel: {
    marginLeft: '20px',

    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
      width: '100%',
    },
  },
  discordButton: {
    backgroundColor: '#7289da',
    padding: '11px 17px',
    width: '100%',

    '&:hover': {
      backgroundColor: '#8ea1e1',
    },
  },
  discordIcon: {
    height: '20px',
    width: '82px',
  },
  twitchChannel: {
    [theme.breakpoints.down('xs')]: {
      marginBottom: '16px',
      width: '100%',
    },
  },
  twitchButton: {
    backgroundColor: '#6441a4',
    padding: '7px 17px 8px',
    width: '100%',

    '&:hover': {
      backgroundColor: '#a66bff',
    },
  },
  twitchIcon: {
    height: '27px',
    width: '71px',
  },
  result: {
    marginBottom: '28px',
    marginLeft: 'auto',
  },
  info: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 'auto',
    minWidth: '400px',
    paddingTop: '20px',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      margin: '80px 0 0',
    },

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      margin: '60px 0 0',
      minWidth: 'auto',
    },
  },
  status: {
    alignItems: 'center',
    display: 'flex',
    transform: 'rotate(-4deg)',
  },
  statusValue: {
    ...theme.typography.h1,

    fontWeight: 500,
    textTransform: 'uppercase',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.h2,

      fontWeight: 500,
    },
  },
  statusLiveValue: {
    color: theme.palette.secondary.main,
  },
  statusPauseValue: {
    color: theme.palette.warning.main,
  },
  statusClosedValue: {
    border: '4px solid',
    borderRadius: '8px',
    color: theme.palette.success.main,
    padding: '8px 16px',
  },
  statusPulse: {
    marginRight: '20px',
    height: '16px',
    width: '16px',
  },
  addReview: {
    margin: 'auto',

    [theme.breakpoints.down('sm')]: {
      margin: '0',
    },

    [theme.breakpoints.down('xs')]: {
      margin: '76px 0 40px',
    },
  },
  review: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '44px',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '32px',
      marginTop: '0',
    },

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
      margin: '40px 0',
    },
  },
  reviewText: {
    ...theme.typography.h6,

    marginTop: '26px',
    width: '360px',

    [theme.breakpoints.down('sm')]: {
      width: 'auto',
    },
  },
  take: {
    marginTop: '100px',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '32px',
      marginTop: '0',
    },

    [theme.breakpoints.down('xs')]: {
      margin: '48px 0 40px',
      width: '100%',
    },
  },
  panel: {
    marginTop: '52px',
    width: '360px',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '32px',
      marginTop: '0',
    },

    [theme.breakpoints.down('xs')]: {
      margin: '48px 0 40px',
      width: '100%',
    },
  },
}));

const getScreenshotsDisabledReason = (
  intl: IntlShape,
  status: STATUS,
  role?: ROLE,
  showScreenshots?: boolean,
  screenshots?: string[]
): string => {
  if (
    role === ROLE.ADMIN ||
    role === ROLE.MANAGER ||
    role === ROLE.BOOSTER ||
    role === ROLE.CLIENT
  ) {
    return screenshots && screenshots.length
      ? ''
      : intl.formatMessage({ id: 'order.noScreenshotsDisabledReason' });
  }

  if (status !== STATUS.CLOSED) {
    return intl.formatMessage({ id: 'order.notClosed' });
  }

  if (!showScreenshots) {
    return intl.formatMessage({ id: 'order.noUserScreenshotsDisabledReason' });
  }

  return screenshots && screenshots.length
    ? ''
    : intl.formatMessage({ id: 'order.noScreenshotsDisabledReason' });
};

const getDiscordDisabledReason = (
  intl: IntlShape,
  status: STATUS,
  channel?: string
): string => {
  if (status === STATUS.CLOSED) {
    return intl.formatMessage({ id: 'order.closedDiscordDisabledReason' });
  }

  if (!channel) {
    return intl.formatMessage({ id: 'order.channelDiscordDisabledReason' });
  }

  return '';
};

const getTwitchDisabledReason = (
  intl: IntlShape,
  booster?: PublicBooster,
  stream?: boolean
): string => {
  if (!stream) {
    return intl.formatMessage({ id: 'order.streamTwitchDisabledReason' });
  }

  if (!booster) {
    return intl.formatMessage({ id: 'order.boosterTwitchDisabledReason' });
  }

  return '';
};

const getStatus = (
  status: STATUS,
  classes: ReturnType<typeof useStyles>,
  intl: IntlShape
) => {
  switch (status) {
    case STATUS.CLOSED:
      return (
        <div className={classes.status}>
          <MuiTypography
            className={`${classes.statusValue} ${classes.statusClosedValue}`}
          >
            {intl.formatMessage({ id: 'orderStatus.closed' })}
          </MuiTypography>
        </div>
      );
    case STATUS.PAUSED:
      return (
        <div className={classes.status}>
          <PulseWarning className={classes.statusPulse} />
          <MuiTypography
            className={`${classes.statusValue} ${classes.statusPauseValue}`}
          >
            {intl.formatMessage({ id: 'orderStatus.paused' })}
          </MuiTypography>
        </div>
      );
    case STATUS.LIVE:
      return (
        <div className={classes.status}>
          <PulseSecondary className={classes.statusPulse} />
          <MuiTypography
            className={`${classes.statusValue} ${classes.statusLiveValue}`}
          >
            {intl.formatMessage({ id: 'orderStatus.live' })}
          </MuiTypography>
        </div>
      );
    default:
      return (
        <div className={classes.status}>
          <MuiTypography
            className={`${classes.statusValue} ${classes.statusLiveValue}`}
          >
            {intl.formatMessage({ id: 'orderStatus.live' })}
          </MuiTypography>
        </div>
      );
  }
};

interface Props {
  className?: string;
  name: string;
  order: PublicOrder;
  take?: boolean;
  addReview?: boolean;
  managers?: Manager[];
  boosters?: Booster[];
  role?: ROLE;
  reload?: () => void;
  loading?: boolean;
}

const Order: React.FC<Props> = ({
  className,
  name,
  order,
  take,
  managers,
  boosters,
  role,
  reload,
  loading,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const screenshotsDisabledReason = getScreenshotsDisabledReason(
    intl,
    order.status,
    role,
    order.review?.showScreenshots,
    order.screenshots
  );
  const discordDisabledReason = getDiscordDisabledReason(
    intl,
    order.status,
    order.channelId
  );
  const twitchDisabledReason = getTwitchDisabledReason(
    intl,
    order.booster,
    order.stream
  );

  return (
    <MuiContainer
      className={className ? `${className} ${classes.order}` : classes.order}
    >
      <ScrollElement className={classes.content} name={name}>
        <div className={classes.head}>
          <Link className={classes.game} href={`/${order.game}`}>
            <GameLogo className={classes.gameImage} game={order.game} />
          </Link>
          <MuiTypography className={classes.headDivider}>/</MuiTypography>
          <MuiTypography className={classes.headText}>
            {`#${order.shortId}`}
          </MuiTypography>
        </div>
        <MuiTypography className={classes.title}>{order.title}</MuiTypography>
        {!!order.details && !!order.details.length && (
          <ul className={classes.details}>
            {map(order.details, (detail) => (
              <MuiTypography
                key={detail}
                className={classes.detail}
                component="li"
              >
                {detail}
              </MuiTypography>
            ))}
          </ul>
        )}
        {order.booster ? (
          <div className={classes.booster}>
            <MuiTypography className={classes.roleTitle}>
              {intl.formatMessage({ id: 'order.boosterTitle' })}
            </MuiTypography>
            <Link
              className={classes.roleLink}
              component={MuiButton}
              href={`/boosters/${order.booster.name}`}
            >
              <Avatar
                className={classes.roleAvatar}
                src={order.booster.image}
                alt={order.booster.name}
                status={order.status}
              />
              <MuiTypography className={classes.roleName}>
                {order.booster.name}
              </MuiTypography>
            </Link>
          </div>
        ) : (
          <MuiTypography className={classes.booster} color="textSecondary">
            {intl.formatMessage({ id: 'order.boosterAssigning' })}
          </MuiTypography>
        )}
        {order.manager ? (
          <div className={classes.manager}>
            <MuiTypography className={classes.roleTitle}>
              {intl.formatMessage({ id: 'order.managerTitle' })}
            </MuiTypography>
            <Link
              className={classes.roleLink}
              component={MuiButton}
              href={`/managers/${order.manager.name}`}
            >
              <Avatar
                className={classes.roleAvatar}
                src={order.manager.image}
                alt={order.manager.name}
              />
              <MuiTypography className={classes.roleName}>
                {order.manager.name}
              </MuiTypography>
            </Link>
          </div>
        ) : (
          <MuiTypography className={classes.manager} color="textSecondary">
            {intl.formatMessage({ id: 'order.managerAssigning' })}
          </MuiTypography>
        )}
        {(role === ROLE.ADMIN ||
          role === ROLE.MANAGER ||
          role === ROLE.CLIENT) && (
          <Result className={classes.result} result={order.result} />
        )}
        <div className={classes.controls}>
          <ScreenshotsButton
            className={classes.screenshots}
            screenshots={order.screenshots}
            size="large"
            disabled={!!screenshotsDisabledReason}
            tooltip={screenshotsDisabledReason}
          />
          <MuiTooltip title={twitchDisabledReason}>
            <div className={classes.twitchChannel}>
              <MuiButton
                className={classes.twitchButton}
                component="a"
                href={`https://twitch.tv/${order.booster?.streamChannel}`}
                rel="noopener"
                target="_blank"
                variant="contained"
                disabled={!!twitchDisabledReason}
              >
                <TwitchTextIcon className={classes.twitchIcon} />
              </MuiButton>
            </div>
          </MuiTooltip>
          {(role === ROLE.ADMIN ||
            role === ROLE.MANAGER ||
            role === ROLE.CLIENT) && (
            <MuiTooltip title={discordDisabledReason}>
              <div className={classes.discordChannel}>
                <MuiButton
                  className={classes.discordButton}
                  component="a"
                  href={`https://discord.com/channels/${GUILD_ID}/${order.channelId}`}
                  rel="noopener"
                  target="_blank"
                  variant="contained"
                  disabled={!!discordDisabledReason}
                >
                  <DiscordTextIcon className={classes.discordIcon} />
                </MuiButton>
              </div>
            </MuiTooltip>
          )}
        </div>
      </ScrollElement>
      <div className={classes.info}>
        {getStatus(order.status, classes, intl)}
        {order.review ? (
          <div className={classes.review}>
            <MuiRating size="large" readOnly value={order.review.rating} />
            <MuiTypography className={classes.reviewText}>
              {order.review.text}
            </MuiTypography>
          </div>
        ) : (
          role === ROLE.CLIENT && (
            <AddReview
              className={classes.addReview}
              shortId={order.shortId}
              disabled={order.status !== STATUS.CLOSED}
              tooltip={
                order.status !== STATUS.CLOSED
                  ? intl.formatMessage({ id: 'order.notClosed' })
                  : ''
              }
              reload={reload}
              loading={loading}
            />
          )
        )}
        {take && !order.manager && (
          <Take
            className={classes.take}
            shortId={order.shortId}
            reload={reload}
            loading={loading}
          />
        )}
        {role === ROLE.ADMIN && boosters && managers && (
          <AdminPanel
            className={classes.panel}
            managers={managers}
            boosters={boosters}
            order={order}
            reload={reload}
            loading={loading}
          />
        )}
        {role === ROLE.MANAGER && boosters && (
          <ManagerPanel
            className={classes.panel}
            boosters={boosters}
            order={order}
            reload={reload}
            loading={loading}
          />
        )}
        {role === ROLE.BOOSTER && order.status !== STATUS.CLOSED && (
          <BoosterPanel
            className={classes.panel}
            order={order}
            reload={reload}
            loading={loading}
          />
        )}
      </div>
    </MuiContainer>
  );
};

export default Order;
