import * as React from 'react';
import map from 'lodash/map';
import differenceBy from 'lodash/differenceBy';

import { makeStyles } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiSelect from '@material-ui/core/Select';
import MuiMenuItem from '@material-ui/core/MenuItem';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';

import MuiCloseIcon from '@material-ui/icons/Close';

import { useCurrency } from '@context/currency';

import getCurrencySign from '@helpers/getCurrencySign';

import GAME from '@constants/game';

const useStyles = makeStyles(() => ({
  selectList: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  picture: {
    height: '32px',
    marginRight: '12px',
    marginTop: '2px',
    width: '40px',
  },
  image: {
    borderRadius: '8px',
    height: '100%',
    width: '100%',
  },
  label: {
    marginBottom: '16px',
    marginRight: '20px',
    marginTop: '6px',
  },
  control: {
    alignItems: 'flex-end',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 'auto',
  },
  itemWrapper: {
    display: 'flex',
    marginBottom: '16px',
  },
  item: {
    display: 'flex',
  },
  itemPicture: {
    borderRadius: '4px',
    flexShrink: 0,
    height: '24px',
    marginRight: '12px',
    width: '24px',
  },
  itemImage: {
    height: '100%',
    width: '100%',
  },
  close: {
    padding: '8px',
    marginLeft: '12px',
  },
}));

export interface Value {
  amount?: number;
  label: string;
  percent?: number;
  value: string;
}

export interface Config {
  game: GAME;
  name: string;
  list: Value[];
  label: string;
  buttonLabel: string;
  max?: number;
  difference?: boolean;
}

interface Props {
  className?: string;
  config: Config;
  value: Value[];
  setValue: (newValue: Value[]) => void;
}

const SelectList: React.FC<Props> = ({
  className,
  config,
  value,
  setValue,
}) => {
  const classes = useStyles();
  const { currency } = useCurrency();

  const [list, setList] = React.useState(config.list);

  React.useEffect(() => {
    setList(
      config.difference
        ? differenceBy(config.list, value, 'value')
        : config.list
    );
  }, [config.list, value]);

  return (
    <div
      className={
        className ? `${className} ${classes.selectList}` : classes.selectList
      }
    >
      <picture className={classes.picture}>
        <source
          type="image/avif"
          srcSet={`/static/${config.game}/${config.name}/${config.name}.avif`}
        />
        <source
          type="image/webp"
          srcSet={`/static/${config.game}/${config.name}/${config.name}.webp`}
        />
        <img
          className={classes.image}
          srcSet={`/static/${config.game}/${config.name}/${config.name}.png`}
          alt={config.label}
        />
      </picture>
      <MuiTypography className={classes.label}>{config.label}</MuiTypography>
      <div className={classes.control}>
        {map(value, (item, index) => (
          <div key={`${item.value} ${index}`} className={classes.itemWrapper}>
            <MuiSelect
              value={item.value}
              renderValue={() => {
                const foundItem =
                  list.find((listItem) => listItem.value === item.value) ||
                  item;

                return (
                  <div className={classes.item}>
                    <picture className={classes.itemPicture}>
                      <source
                        type="image/avif"
                        srcSet={`/static/${config.game}/${config.name}/${foundItem.value}.avif`}
                      />
                      <source
                        type="image/webp"
                        srcSet={`/static/${config.game}/${config.name}/${foundItem.value}.webp`}
                      />
                      <img
                        className={classes.itemImage}
                        srcSet={`/static/${config.game}/${config.name}/${foundItem.value}.jpg`}
                        alt={foundItem.label}
                      />
                    </picture>
                    <MuiTypography>
                      {foundItem.amount
                        ? `${foundItem.label}${
                            foundItem.amount !== Infinity
                              ? `, ${Math.round(
                                  foundItem.amount * currency.multiplier
                                )} ${getCurrencySign(currency.code)}`
                              : ''
                          }`
                        : `${foundItem.label}${
                            foundItem.percent ? `, +${foundItem.percent}%` : ''
                          }`}
                    </MuiTypography>
                  </div>
                );
              }}
              onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
                const newItem = list.find(
                  (listItem) => listItem.value === event.target.value
                );

                if (!newItem) {
                  return;
                }

                const newValue = [...value];
                newValue[index] = newItem;
                setValue(newValue);
              }}
            >
              {map(list, (listItem) => (
                <MuiMenuItem
                  key={listItem.value}
                  className={classes.item}
                  value={listItem.value}
                >
                  <picture className={classes.itemPicture}>
                    <source
                      type="image/avif"
                      srcSet={`/static/${config.game}/${config.name}/${listItem.value}.avif`}
                    />
                    <source
                      type="image/webp"
                      srcSet={`/static/${config.game}/${config.name}/${listItem.value}.webp`}
                    />
                    <img
                      className={classes.itemImage}
                      srcSet={`/static/${config.game}/${config.name}/${listItem.value}.jpg`}
                      alt={listItem.label}
                    />
                  </picture>
                  <MuiTypography>
                    {listItem.amount
                      ? `${listItem.label}${
                          listItem.amount !== Infinity
                            ? `, ${Math.round(
                                listItem.amount * currency.multiplier
                              )} ${getCurrencySign(currency.code)}`
                            : ''
                        }`
                      : `${listItem.label}${
                          listItem.percent ? `, +${listItem.percent}%` : ''
                        }`}
                  </MuiTypography>
                </MuiMenuItem>
              ))}
            </MuiSelect>
            <MuiIconButton
              className={classes.close}
              onClick={() => {
                const newValue = [...value];
                newValue.splice(index, 1);
                setValue(newValue);
              }}
            >
              <MuiCloseIcon fontSize="small" />
            </MuiIconButton>
          </div>
        ))}
        {(!config.max || value.length < config.max) && (
          <MuiButton
            color="inherit"
            variant="outlined"
            onClick={() => {
              setValue([...value, list[0]]);
            }}
          >
            {config.buttonLabel}
          </MuiButton>
        )}
      </div>
    </div>
  );
};

export default SelectList;
