import * as React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';

import DiscordButton from '@components/Button/Discord';
import LanguageButton from '@components/Button/Language';
import CurrencyButton from '@components/Button/Currency';
import AccountButton from '@components/Button/Account';

import Logo from '@icons/Logo';

const useStyles = makeStyles((theme: Theme) => ({
  header: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: '20px',
    paddingBottom: '20px',
    paddingLeft: '16px',
    paddingTop: '20px',
    position: 'relative',

    [theme.breakpoints.down('xs')]: {
      paddingBottom: '10px',
      paddingLeft: '16px',
      paddingRight: '8px',
      paddingTop: '10px',
    },
  },
  logo: {
    marginRight: 'auto',
    padding: '0 0 4px',

    [theme.breakpoints.down('sm')]: {
      padding: '0',
    },
  },
  discord: {
    marginRight: '28px',

    [theme.breakpoints.down('sm')]: {
      marginRight: '18px',
    },

    [theme.breakpoints.down('xs')]: {
      marginRight: '10px',
    },
  },
  language: {
    marginRight: '16px',

    [theme.breakpoints.down('sm')]: {
      marginRight: '8px',
    },

    [theme.breakpoints.down('xs')]: {
      marginRight: '6px',
    },
  },
  currency: {
    marginRight: '16px',

    [theme.breakpoints.down('sm')]: {
      marginRight: '8px',
    },

    [theme.breakpoints.down('xs')]: {
      marginRight: '0',
    },
  },
  account: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
}));

interface Props {
  locale: string;
  locales: string[];
}

const Header: React.FC<Props> = ({ locale, locales }) => {
  const classes = useStyles();

  return (
    <MuiContainer className={classes.header} component="header">
      <Logo className={classes.logo} />
      <DiscordButton className={classes.discord} />
      <LanguageButton
        className={classes.language}
        locale={locale}
        locales={locales}
      />
      <CurrencyButton className={classes.currency} />
      <AccountButton className={classes.account} />
    </MuiContainer>
  );
};

export default Header;
