import * as React from 'react';
import { useIntl } from 'react-intl';
import { signIn } from 'next-auth/client';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) => ({
  withoutSession: {
    alignItems: 'center',
    display: 'flex',
    height: '52vh',
    justifyContent: 'center',
    padding: '0 36px',
  },
  text: {
    ...theme.typography.h5,

    textAlign: 'center',
  },
  link: {
    ...theme.typography.h5,

    color: theme.palette.text.primary,
    cursor: 'pointer',
    textDecoration: 'underline',
    whiteSpace: 'nowrap',
  },
}));

const WithoutSession: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <MuiContainer className={classes.withoutSession}>
      <MuiTypography className={classes.text}>
        {intl.formatMessage(
          { id: 'withoutSession.title' },
          {
            link: (
              <MuiTypography
                className={classes.link}
                component="span"
                onClick={() => {
                  signIn('discord');
                }}
              >
                {intl.formatMessage({ id: 'withoutSession.link' })}
              </MuiTypography>
            ),
          }
        )}
      </MuiTypography>
    </MuiContainer>
  );
};

export default WithoutSession;
