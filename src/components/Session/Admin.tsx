import * as React from 'react';
import { useIntl } from 'react-intl';
import useSWR from 'swr';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import MuiTextField from '@material-ui/core/TextField';
import MuiButton from '@material-ui/core/Button';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import Diagonal from '@components/Diagonal';
import Order from '@components/Order';

import ROLE from '@constants/role';
import SERVER from '@constants/server';

import { PublicOrder } from '@interfaces/order';
import { Manager, Booster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  session: {
    marginBottom: '160px',
    maxWidth: '960px',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '80px',
    },
  },
  title: {
    ...theme.typography.h4,

    marginBottom: '48px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,
    },
  },
  search: {
    alignItems: 'center',
    display: 'flex',
    width: '100%',
  },
  searchField: {
    display: 'flex',
    flexGrow: 1,
    marginRight: '36px',
  },
  button: {
    position: 'relative',
  },
  loader: {
    bottom: '-12px',
    left: '0',
    position: 'absolute',
    right: '0',
  },
  order: {
    maxWidth: '1300px',
  },
}));

const AdminSession: React.FC = () => {
  const intl = useIntl();
  const classes = useStyles();

  const [mongoId, setMongoId] = React.useState('');

  const [orders, setOrders] = React.useState<PublicOrder[]>([]);

  const [loading, setLoading] = React.useState(false);

  const { data: boosters } = useSWR<Booster[]>('/api/get_boosters/');
  const { data: managers } = useSWR<Manager[]>('/api/get_managers/');

  const reload = () => {
    setLoading(true);

    fetch(`${SERVER}/api/get_orders/admin/?_id=${mongoId}`)
      .then((response) => response.json())
      .then((orders) => {
        setLoading(false);
        setOrders(orders);
      })
      .catch(() => {
        setLoading(false);
        setOrders([]);
      });
  };

  return (
    <>
      <MuiContainer className={classes.session}>
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'adminSession.searchTitle' })}
        </MuiTypography>
        <div className={classes.search}>
          <MuiTextField
            className={classes.searchField}
            variant="outlined"
            label="Mongo ID"
            value={mongoId}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setMongoId(event.target.value);
            }}
            onKeyPress={(event) => {
              const element = event.target as HTMLInputElement;

              if (!element || event.key !== 'Enter') {
                return;
              }

              element.blur();
            }}
          />
          <MuiButton
            className={classes.button}
            variant="contained"
            size="large"
            disabled={loading}
            onClick={reload}
          >
            {intl.formatMessage({ id: 'adminSession.searchButton' })}
            {loading && <MuiLinearProgress className={classes.loader} />}
          </MuiButton>
        </div>
      </MuiContainer>
      <AnimatePresence>
        {!!orders && !!orders.length && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.6 } }}
          >
            {map(orders, (order, index) =>
              index % 2 === 0 ? (
                <Diagonal key={order.shortId} colored>
                  <Order
                    className={classes.order}
                    name={order.shortId}
                    order={order}
                    managers={managers}
                    boosters={boosters}
                    role={ROLE.ADMIN}
                    reload={reload}
                    loading={loading}
                  />
                </Diagonal>
              ) : (
                <Order
                  key={order.shortId}
                  className={classes.order}
                  name={order.shortId}
                  order={order}
                  managers={managers}
                  boosters={boosters}
                  role={ROLE.ADMIN}
                  reload={reload}
                  loading={loading}
                />
              )
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default AdminSession;
