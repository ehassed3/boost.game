import * as React from 'react';
import { useIntl } from 'react-intl';
import useSWR from 'swr';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import MuiRating from '@material-ui/lab/Rating';
import MuiSkeleton from '@material-ui/lab/Skeleton';
import MuiButton from '@material-ui/core/Button';

import Avatar from '@components/Avatar';
import Diagonal from '@components/Diagonal';
import Order from '@components/Order';

import TwitchTextIcon from '@icons/Twitch/Text';

import ROLE from '@constants/role';

import { PublicOrder } from '@interfaces/order';
import { Booster, PublicBooster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  session: {
    alignItems: 'flex-start',
    display: 'flex',
    marginBottom: '100px',
    maxWidth: '1300px',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      marginBottom: '80px',
    },
  },
  avatar: {
    flexShrink: 0,
    height: '200px',
    width: '200px',

    [theme.breakpoints.down('sm')]: {
      height: '160px',
      width: '160px',
    },

    [theme.breakpoints.down('xs')]: {
      height: '140px',
      width: '140px',
    },
  },
  info: {
    marginLeft: '40px',
    width: 'calc(100% - 240px)',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '28px',
      width: 'calc(100% - 188px)',
    },

    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
      marginTop: '28px',
      width: '100%',
    },
  },
  title: {
    ...theme.typography.h6,
  },
  name: {
    ...theme.typography.h4,

    marginBottom: '24px',
    overflow: 'hidden',
    paddingRight: '180px',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,
    },
  },
  description: {
    ...theme.typography.h5,

    marginTop: '8px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  rating: {
    position: 'absolute',
    right: '24px',
    top: '38px',

    [theme.breakpoints.down('xs')]: {
      right: '16px',
      top: '55px',
    },
  },
  twitchButton: {
    backgroundColor: '#6441a4',
    marginTop: '20px',
    padding: '8px 16px 9px',

    '&:hover': {
      backgroundColor: '#a66bff',
    },
  },
  twitchIcon: {
    height: '27px',
    width: '71px',
  },
  order: {
    maxWidth: '1300px',
  },
  moreButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    margin: '100px auto',
    maxWidth: '1300px',
  },
}));

interface Props {
  booster: Booster | PublicBooster;
  role: ROLE;
}

const BoosterSession: React.FC<Props> = ({ booster, role }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { data: orders, mutate, isValidating: loading } = useSWR<PublicOrder[]>(
    booster ? `/api/get_orders/booster/?name=${booster.name}` : ''
  );
  const [ordersEnd, setOrdersEnd] = React.useState(5);

  return (
    <>
      <MuiContainer className={classes.session}>
        {booster ? (
          <Avatar className={classes.avatar} src={booster.image} />
        ) : (
          <MuiSkeleton className={classes.avatar} variant="circular" />
        )}
        <div className={classes.info}>
          <MuiTypography className={classes.title} color="textSecondary">
            {intl.formatMessage({ id: 'boosterSession.title' })}
          </MuiTypography>
          {booster ? (
            <MuiTypography className={classes.name}>
              {booster.name}
            </MuiTypography>
          ) : (
            <MuiSkeleton className={classes.name} variant="text" />
          )}
          <MuiTypography className={classes.title} color="textSecondary">
            {intl.formatMessage({ id: 'boosterSession.description' })}
          </MuiTypography>
          {booster ? (
            <MuiTypography className={classes.description}>
              {booster.description[intl.locale || 'en'] ||
                booster.description.en}
            </MuiTypography>
          ) : (
            <>
              <MuiSkeleton className={classes.description} variant="text" />
              <MuiSkeleton className={classes.description} variant="text" />
            </>
          )}
          {!!booster.streamChannel && (
            <MuiButton
              className={classes.twitchButton}
              component="a"
              href={`https://twitch.tv/${booster.streamChannel}`}
              rel="noopener"
              target="_blank"
              variant="contained"
            >
              <TwitchTextIcon className={classes.twitchIcon} />
            </MuiButton>
          )}
        </div>
        {!!booster.rating && (
          <MuiRating
            className={classes.rating}
            size="large"
            readOnly
            value={booster.rating}
            precision={0.1}
          />
        )}
      </MuiContainer>
      <AnimatePresence>
        {!!orders && !!orders.length && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.6 } }}
          >
            {map(orders.slice(0, ordersEnd), (order, index) =>
              index % 2 === 0 ? (
                <Diagonal key={order.shortId} colored>
                  <Order
                    className={classes.order}
                    name={order.shortId}
                    order={order}
                    role={role}
                    reload={mutate}
                    loading={loading}
                  />
                </Diagonal>
              ) : (
                <Order
                  key={order.shortId}
                  className={classes.order}
                  name={order.shortId}
                  order={order}
                  role={role}
                  reload={mutate}
                  loading={loading}
                />
              )
            )}
            {orders.length > ordersEnd && (
              <MuiContainer className={classes.moreButtonContainer}>
                <MuiButton
                  variant="contained"
                  size="large"
                  onClick={() => {
                    setOrdersEnd(orders.length);
                  }}
                >
                  {intl.formatMessage(
                    { id: 'session.moreButton' },
                    { count: orders.length - ordersEnd }
                  )}
                </MuiButton>
              </MuiContainer>
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default BoosterSession;
