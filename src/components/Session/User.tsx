import * as React from 'react';
import { useIntl } from 'react-intl';
import useSWR from 'swr';
import { signOut } from 'next-auth/client';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import MuiButton from '@material-ui/core/Button';
import MuiSkeleton from '@material-ui/lab/Skeleton';

import MuiExitToApp from '@material-ui/icons/ExitToApp';

import Avatar from '@components/Avatar';
import Status from '@components/Status';
import Diagonal from '@components/Diagonal';
import Order from '@components/Order';

import ROLE from '@constants/role';

import { PublicOrder } from '@interfaces/order';
import { User, Client } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  session: {
    marginBottom: '100px',
    maxWidth: '1300px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '80px',
    },
  },
  inner: {
    alignItems: 'flex-start',
    display: 'flex',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  avatar: {
    height: '200px',
    width: '200px',

    [theme.breakpoints.down('sm')]: {
      height: '160px',
      width: '160px',
    },

    [theme.breakpoints.down('xs')]: {
      height: '140px',
      width: '140px',
    },
  },
  user: {
    marginLeft: '40px',
    width: 'calc(100% - 240px)',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '28px',
      width: 'calc(100% - 188px)',
    },

    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
      marginTop: '28px',
      width: '100%',
    },
  },
  userTitle: {
    ...theme.typography.h6,
  },
  userName: {
    ...theme.typography.h4,

    marginBottom: '20px',
    overflow: 'hidden',
    paddingRight: '180px',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,
    },
  },
  userDiscriminator: {
    opacity: 0.5,
  },
  userEmail: {
    ...theme.typography.h4,

    marginBottom: '20px',
    maxWidth: '460px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.h5,
    },
  },
  userCaption: {
    maxWidth: '460px',
  },
  signOut: {
    position: 'absolute',
    right: '0',
    top: '20px',
  },
  status: {
    marginTop: '44px',
    marginLeft: '240px',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
    },
  },
  order: {
    maxWidth: '1300px',
  },
  moreButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    margin: '100px auto',
    maxWidth: '1300px',
  },
}));

interface Props {
  user?: User & Client;
}

const UserSession: React.FC<Props> = ({ user }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { data: orders, mutate } = useSWR<PublicOrder[]>(
    user ? '/api/get_orders/user/' : ''
  );
  const [ordersEnd, setOrdersEnd] = React.useState(5);

  return (
    <>
      <MuiContainer className={classes.session}>
        <div className={classes.inner}>
          {user ? (
            <Avatar className={classes.avatar} src={user.image} />
          ) : (
            <MuiSkeleton className={classes.avatar} variant="circular" />
          )}
          <div className={classes.user}>
            <MuiTypography className={classes.userTitle} color="textSecondary">
              {intl.formatMessage({ id: 'userSession.title' })}
            </MuiTypography>
            {user ? (
              <MuiTypography className={classes.userName}>
                {user.name}
                <span className={classes.userDiscriminator}>
                  #{user.discriminator}
                </span>
              </MuiTypography>
            ) : (
              <MuiSkeleton className={classes.userName} variant="text" />
            )}
            <MuiTypography className={classes.userTitle} color="textSecondary">
              {intl.formatMessage({ id: 'userSession.email' })}
            </MuiTypography>
            {user ? (
              <MuiTypography className={classes.userEmail}>
                {user.email}
              </MuiTypography>
            ) : (
              <MuiSkeleton className={classes.userEmail} variant="text" />
            )}
            <MuiTypography
              className={classes.userCaption}
              color="textSecondary"
            >
              {intl.formatMessage({ id: 'userSession.caption' })}
            </MuiTypography>
          </div>
          {user && (
            <MuiButton
              className={classes.signOut}
              variant="contained"
              color="secondary"
              endIcon={<MuiExitToApp />}
              onClick={() => signOut({ callbackUrl: '/' })}
            >
              {intl.formatMessage({ id: 'userSession.logOutButton' })}
            </MuiButton>
          )}
        </div>
        <Status className={classes.status} />
      </MuiContainer>
      <AnimatePresence>
        {!!orders && !!orders.length && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.6 } }}
          >
            {map(orders.slice(0, ordersEnd), (order, index) =>
              index % 2 === 0 ? (
                <Diagonal key={order.shortId} colored>
                  <Order
                    className={classes.order}
                    name={order.shortId}
                    order={order}
                    role={ROLE.CLIENT}
                    reload={mutate}
                  />
                </Diagonal>
              ) : (
                <Order
                  key={order.shortId}
                  className={classes.order}
                  name={order.shortId}
                  order={order}
                  role={ROLE.CLIENT}
                  reload={mutate}
                />
              )
            )}
            {orders.length > ordersEnd && (
              <MuiContainer className={classes.moreButtonContainer}>
                <MuiButton
                  variant="contained"
                  size="large"
                  onClick={() => {
                    setOrdersEnd(orders.length);
                  }}
                >
                  {intl.formatMessage(
                    { id: 'session.moreButton' },
                    { count: orders.length - ordersEnd }
                  )}
                </MuiButton>
              </MuiContainer>
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default UserSession;
