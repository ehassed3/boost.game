import * as React from 'react';
import { useIntl } from 'react-intl';
import useSWR from 'swr';
import { motion, AnimatePresence } from 'framer-motion';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiTypography from '@material-ui/core/Typography';
import MuiRating from '@material-ui/lab/Rating';
import MuiSkeleton from '@material-ui/lab/Skeleton';
import MuiButton from '@material-ui/core/Button';

import Avatar from '@components/Avatar';
import CreatePromocode from '@components/Promocode/Create';
import Diagonal from '@components/Diagonal';
import Order from '@components/Order';

import ROLE from '@constants/role';

import { PublicOrder } from '@interfaces/order';
import { Manager, PublicManager, Booster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  session: {
    alignItems: 'flex-start',
    display: 'flex',
    marginBottom: '100px',
    maxWidth: '1300px',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      marginBottom: '80px',
    },
  },
  avatar: {
    flexShrink: 0,
    height: '200px',
    width: '200px',

    [theme.breakpoints.down('sm')]: {
      height: '160px',
      width: '160px',
    },

    [theme.breakpoints.down('xs')]: {
      height: '140px',
      width: '140px',
    },
  },
  info: {
    marginLeft: '40px',
    width: 'calc(100% - 240px)',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '28px',
      width: 'calc(100% - 188px)',
    },

    [theme.breakpoints.down('xs')]: {
      marginLeft: '0',
      marginTop: '28px',
      width: '100%',
    },
  },
  title: {
    ...theme.typography.h6,
  },
  name: {
    ...theme.typography.h4,

    marginBottom: '24px',
    overflow: 'hidden',
    paddingRight: '180px',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,
    },
  },
  description: {
    ...theme.typography.h5,

    marginTop: '8px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.h5,
    },
  },
  createPromocode: {
    marginTop: '32px',
    width: '320px',

    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  rating: {
    position: 'absolute',
    right: '24px',
    top: '38px',

    [theme.breakpoints.down('xs')]: {
      right: '16px',
      top: '55px',
    },
  },
  order: {
    maxWidth: '1300px',
  },
  moreButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    margin: '100px auto',
    maxWidth: '1300px',
  },
}));

interface Props {
  manager: Manager | PublicManager;
  role: ROLE;
}

const ManagerSession: React.FC<Props> = ({ manager, role }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { data: orders, mutate, isValidating: loading } = useSWR<PublicOrder[]>(
    manager ? `/api/get_orders/manager/?name=${manager.name}` : ''
  );
  const [ordersEnd, setOrdersEnd] = React.useState(5);

  const { data: boosters } = useSWR<Booster[]>(
    role === ROLE.MANAGER ? '/api/get_boosters/' : ''
  );

  return (
    <>
      <MuiContainer className={classes.session}>
        {manager ? (
          <Avatar className={classes.avatar} src={manager.image} />
        ) : (
          <MuiSkeleton className={classes.avatar} variant="circular" />
        )}
        <div className={classes.info}>
          <MuiTypography className={classes.title} color="textSecondary">
            {intl.formatMessage({ id: 'managerSession.title' })}
          </MuiTypography>
          {manager ? (
            <MuiTypography className={classes.name}>
              {manager.name}
            </MuiTypography>
          ) : (
            <MuiSkeleton className={classes.name} variant="text" />
          )}
          {(!manager ||
            manager.description[intl.locale || 'en'] ||
            manager.description.en) && (
            <>
              <MuiTypography className={classes.title} color="textSecondary">
                {intl.formatMessage({ id: 'managerSession.description' })}
              </MuiTypography>
              {manager ? (
                <MuiTypography className={classes.description}>
                  {manager.description[intl.locale || 'en'] ||
                    manager.description.en}
                </MuiTypography>
              ) : (
                <>
                  <MuiSkeleton className={classes.description} variant="text" />
                  <MuiSkeleton className={classes.description} variant="text" />
                </>
              )}
            </>
          )}
          {manager && role === ROLE.MANAGER && (
            <CreatePromocode className={classes.createPromocode} />
          )}
        </div>
        {!!manager.rating && (
          <MuiRating
            className={classes.rating}
            size="large"
            readOnly
            value={manager.rating}
            precision={0.1}
          />
        )}
      </MuiContainer>
      <AnimatePresence>
        {!!orders && !!orders.length && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 0.5 } }}
            exit={{ opacity: 0, transition: { duration: 0.6 } }}
          >
            {map(orders.slice(0, ordersEnd), (order, index) =>
              index % 2 === 0 ? (
                <Diagonal key={order.shortId} colored>
                  <Order
                    className={classes.order}
                    name={order.shortId}
                    order={order}
                    boosters={boosters}
                    role={role}
                    reload={mutate}
                    loading={loading}
                  />
                </Diagonal>
              ) : (
                <Order
                  key={order.shortId}
                  className={classes.order}
                  name={order.shortId}
                  order={order}
                  boosters={boosters}
                  role={role}
                  reload={mutate}
                  loading={loading}
                />
              )
            )}
            {orders.length > ordersEnd && (
              <MuiContainer className={classes.moreButtonContainer}>
                <MuiButton
                  variant="contained"
                  size="large"
                  onClick={() => {
                    setOrdersEnd(orders.length);
                  }}
                >
                  {intl.formatMessage(
                    { id: 'session.moreButton' },
                    { count: orders.length - ordersEnd }
                  )}
                </MuiButton>
              </MuiContainer>
            )}
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default ManagerSession;
