import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiLinearProgress from '@material-ui/core/LinearProgress';
import MuiSkeleton from '@material-ui/lab/Skeleton';

import WarriorStatus from '@icons/Status/Warrior';
import MasterStatus from '@icons/Status/Master';
import ShadowStatus from '@icons/Status/Shadow';

import { useCurrency } from '@context/currency';
import { useUserStatus } from '@context/userStatus';

import getCurrencySign from '@helpers/getCurrencySign';

import { USER_STATUS_AMOUNT } from '@constants/userStatus';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    ...theme.typography.h6,

    marginBottom: '28px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '16px',
    },
  },
  amount: {
    ...theme.typography.h6,
  },
  amountSkeleton: {
    ...theme.typography.h6,

    display: 'inline-block',
    width: '44px',
  },
  inner: {
    display: 'flex',
    position: 'relative',
    marginLeft: '-66px',

    [theme.breakpoints.down('sm')]: {
      marginLeft: '-58px',
    },

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      marginLeft: '0',
    },
  },
  info: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    width: '212px',

    '&:not(:last-of-type)': {
      marginRight: '64px',
    },

    [theme.breakpoints.down('sm')]: {
      width: '179px',

      '&:not(:last-of-type)': {
        marginRight: '32px',
      },
    },

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'row',
      height: '108px',
      width: 'auto',

      '&:not(:last-of-type)': {
        marginBottom: '28px',
        marginRight: '0',
      },
    },
  },
  icon: {
    height: '80px',
    width: '80px',

    [theme.breakpoints.down('sm')]: {
      height: '60px',
      width: '60px',
    },

    [theme.breakpoints.down('xs')]: {
      marginRight: '24px',
    },
  },
  content: {
    [theme.breakpoints.down('xs')]: {
      flexGrow: 1,
    },
  },
  name: {
    ...theme.typography.h6,

    fontWeight: 600,
    marginTop: '12px',
    textAlign: 'center',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.body1,

      fontWeight: 600,
    },

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h6,

      fontWeight: 600,
      marginTop: '0',
      textAlign: 'left',
    },
  },
  description: {
    marginTop: '4px',
    textAlign: 'center',

    [theme.breakpoints.down('sm')]: {
      ...theme.typography.body2,
    },

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.body1,

      textAlign: 'left',
    },
  },
  progressOne: {
    left: '146px',
    position: 'absolute',
    top: '40px',
    width: '196px',

    [theme.breakpoints.down('sm')]: {
      left: '120px',
      top: '30px',
      width: '151px',
    },

    [theme.breakpoints.down('xs')]: {
      left: '-8px',
      top: '120px',
      transform: 'rotate(90deg)',
      width: '76px',
    },
  },
  progressTwo: {
    left: '422px',
    position: 'absolute',
    top: '40px',
    width: '196px',

    [theme.breakpoints.down('sm')]: {
      left: '331px',
      top: '30px',
      width: '151px',
    },

    [theme.breakpoints.down('xs')]: {
      left: '-8px',
      top: '256px',
      transform: 'rotate(90deg)',
      width: '76px',
    },
  },
  bar: {
    transition: 'none',
  },
}));

const getAmountPercent = (amount = 0, min: number, max: number) => {
  if (amount < min) {
    return 0;
  }

  if (amount > max) {
    return 100;
  }

  const maxAmount = max - min;
  const minAmount = amount - min;
  return ((maxAmount - (maxAmount - minAmount)) / maxAmount) * 100;
};

interface Props {
  className?: string;
}

const Status: React.FC<Props> = ({ className }) => {
  const intl = useIntl();
  const classes = useStyles();

  const { currency } = useCurrency();
  const currencySign = getCurrencySign(currency.code);

  const { userStatus } = useUserStatus();

  const amountPercentOne = getAmountPercent(
    userStatus?.amount,
    USER_STATUS_AMOUNT.WARRIOR,
    USER_STATUS_AMOUNT.MASTER
  );
  const amountPercentTwo = getAmountPercent(
    userStatus?.amount,
    USER_STATUS_AMOUNT.MASTER + 1,
    USER_STATUS_AMOUNT.SHADOW
  );

  return (
    <div className={className}>
      <MuiTypography className={classes.title}>
        {intl.formatMessage(
          { id: 'status.title' },
          {
            price: userStatus ? (
              <MuiTypography
                className={classes.amount}
                component="span"
              >{`${Math.round(
                (userStatus.amount || 0) * currency.multiplier
              )} ${currencySign}`}</MuiTypography>
            ) : (
              <MuiSkeleton className={classes.amountSkeleton} variant="text" />
            ),
          }
        )}
      </MuiTypography>
      <div className={classes.inner}>
        <div className={classes.info}>
          <WarriorStatus
            className={classes.icon}
            style={{
              filter: amountPercentOne > 0 ? undefined : 'grayscale(100%)',
            }}
          />
          <div className={classes.content}>
            <MuiTypography
              className={classes.name}
              style={{ opacity: amountPercentOne > 0 ? 1 : 0.7 }}
            >
              {intl.formatMessage({
                id: 'status.warriorTitle',
              })}
            </MuiTypography>
          </div>
        </div>
        <MuiLinearProgress
          classes={{
            root: classes.progressOne,
            bar: classes.bar,
          }}
          variant="determinate"
          value={amountPercentOne}
        />
        <div className={classes.info}>
          <MasterStatus
            className={classes.icon}
            style={{
              filter: amountPercentOne === 100 ? undefined : 'grayscale(100%)',
            }}
          />
          <div className={classes.content}>
            <MuiTypography
              className={classes.name}
              style={{ opacity: amountPercentOne === 100 ? 1 : 0.7 }}
            >
              {intl.formatMessage(
                {
                  id: 'status.masterTitle',
                },
                {
                  price: `${Math.round(
                    USER_STATUS_AMOUNT.MASTER * currency.multiplier
                  )} ${currencySign}`,
                }
              )}
            </MuiTypography>
            <MuiTypography
              className={classes.description}
              style={{ opacity: amountPercentOne === 100 ? 1 : 0.7 }}
            >
              {intl.formatMessage({ id: 'status.masterDescription' })}
            </MuiTypography>
          </div>
        </div>
        <MuiLinearProgress
          classes={{
            root: classes.progressTwo,
            bar: classes.bar,
          }}
          variant="determinate"
          value={amountPercentOne === 100 ? amountPercentTwo : 0}
        />
        <div className={classes.info}>
          <ShadowStatus
            className={classes.icon}
            style={{
              filter: amountPercentTwo === 100 ? undefined : 'grayscale(100%)',
            }}
          />
          <div className={classes.content}>
            <MuiTypography
              className={classes.name}
              style={{ opacity: amountPercentTwo === 100 ? 1 : 0.7 }}
            >
              {intl.formatMessage(
                {
                  id: 'status.shadowTitle',
                },
                {
                  price: `${Math.round(
                    USER_STATUS_AMOUNT.SHADOW * currency.multiplier
                  )} ${currencySign}`,
                }
              )}
            </MuiTypography>
            <MuiTypography
              className={classes.description}
              style={{ opacity: amountPercentTwo === 100 ? 1 : 0.7 }}
            >
              {intl.formatMessage({ id: 'status.shadowDescription' })}
            </MuiTypography>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Status;
