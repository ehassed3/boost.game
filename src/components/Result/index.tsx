import React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';

import ApplyPromocode from '@components/Promocode/Apply';

import DiscountIcon from '@icons/Discount';

import getCurrencySign from '@helpers/getCurrencySign';

import { FormattedResult } from '@interfaces/result';
import Promocode from '@interfaces/promocode';

const useStyles = makeStyles((theme: Theme) => ({
  result: {
    alignItems: 'center',
    display: 'flex',
    position: 'relative',
    whiteSpace: 'nowrap',
  },
  time: {
    ...theme.typography.h6,

    marginLeft: '12px',
  },
  discountPrice: {
    display: 'flex',
    left: '0',
    position: 'absolute',
    top: '-18px',
  },
  fullPrice: {
    marginRight: '12px',
    textDecoration: 'line-through',
  },
  price: {
    ...theme.typography.h5,

    fontWeight: 500,
  },
  promocode: {
    bottom: '-40px',
    position: 'absolute',
    transform: 'rotate(4deg)',
  },
}));

interface Props {
  className?: string;
  result: FormattedResult;
  promocode?: Promocode | null;
  setPromocode?: (promocode: Promocode | null) => void;
}

const Result: React.FC<Props> = ({
  className,
  result,
  promocode,
  setPromocode,
}) => {
  const classes = useStyles();

  return (
    <div
      className={className ? `${className} ${classes.result}` : classes.result}
    >
      {!!result.discountPercent && (
        <div className={classes.discountPrice}>
          <MuiTypography className={classes.fullPrice} color="textSecondary">
            {`${result.fullPrice.amount} ${getCurrencySign(
              result.fullPrice.currency
            )}`}
          </MuiTypography>
          <DiscountIcon percent={result.discountPercent} />
        </div>
      )}
      <MuiTypography className={classes.price} variant="h6">
        {`${result.price.amount} ${getCurrencySign(result.price.currency)}`}
      </MuiTypography>
      {!!setPromocode && (
        <ApplyPromocode
          className={classes.promocode}
          promocode={promocode}
          setPromocode={setPromocode}
        />
      )}
      {!!result.time && (
        <MuiTypography
          className={classes.time}
          color="textSecondary"
          variant="body2"
        >
          {`≈ ${result.time}`}
        </MuiTypography>
      )}
    </div>
  );
};

export default Result;
