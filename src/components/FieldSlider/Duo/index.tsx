import React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTextField from '@material-ui/core/TextField';
import MuiInputAdornment from '@material-ui/core/InputAdornment';
import MuiTypography from '@material-ui/core/Typography';
import MuiSlider, { Mark } from '@material-ui/core/Slider';

const useStyles = makeStyles((theme: Theme) => ({
  fieldDuoSlider: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '6px',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
    },
  },
  fields: {
    alignItems: 'flex-start',
    display: 'flex',
    flexWrap: 'wrap',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'normal',
      flexDirection: 'column',
      width: '100%',
    },
  },
  field: {
    minWidth: '260px',

    '&:not(:last-child)': {
      marginRight: '20px',
    },

    [theme.breakpoints.down('xs')]: {
      '&:not(:last-child)': {
        marginRight: '0',
      },
    },
  },
  adornment: {
    position: 'relative',
  },
  adornmentImage: {
    height: '36px',
    pointerEvents: 'none',
    width: '36px',
  },
  adornmentText: {
    bottom: '-56px',
    position: 'absolute',
    right: '-10px',
  },
  slider: {
    margin: '40px 0 28px',

    [theme.breakpoints.down('sm')]: {
      margin: '36px 0 8px',
    },

    [theme.breakpoints.down('xs')]: {
      width: '95%',
    },
  },
  sliderMark: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

interface Adornment {
  image: string;
  imageAvif: string;
  imageWebp: string;
  name: string;
  value: number;
}

export interface Config {
  name: string;
  min: number;
  max: number;
  startLabel?: string;
  endLabel?: string;
  adornments?: Adornment[];
  maxLength?: number;
  step?: number | null;
  marks?: boolean | Mark[];
  extra?: React.ReactNode;
}

interface Props {
  className?: string;
  config: Config;
  value: number[];
  setValue: (value: number[]) => void;
}

const FieldDuoSlider: React.FC<Props> = ({
  className,
  config,
  value,
  setValue,
}) => {
  const classes = useStyles();

  const startRef = React.useRef<HTMLInputElement>(null);
  const [startValue, setStartValue] = React.useState(
    value ? String(value[0]) : '0'
  );

  const endRef = React.useRef<HTMLInputElement>(null);
  const [endValue, setEndValue] = React.useState(
    value ? String(value[1]) : '0'
  );

  const onSliderChange = (
    _: React.ChangeEvent<unknown>,
    newSliderValue: number | number[]
  ) => {
    if (!Array.isArray(newSliderValue)) {
      return;
    }

    setValue(newSliderValue);
    setStartValue(String(newSliderValue[0]));
    setEndValue(String(newSliderValue[1]));
  };

  const onStartChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = event.target.value;

    if (newValue.length > event.target.maxLength) {
      return;
    }

    if (Number(newValue) > Number(event.target.max)) {
      newValue = event.target.max;
    }

    setStartValue(newValue);

    if (Number(newValue) < Number(event.target.min)) {
      newValue = event.target.min;
    }

    if (Number(newValue) <= value[1]) {
      setValue([
        Number(newValue),
        value[1] !== Number(endValue) && Number(newValue) <= Number(endValue)
          ? Number(endValue)
          : value[1],
      ]);
    }
  };

  const onEndChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = event.target.value;

    if (newValue.length > event.target.maxLength) {
      return;
    }

    if (Number(newValue) > Number(event.target.max)) {
      newValue = event.target.max;
    }

    setEndValue(newValue);

    if (Number(newValue) < Number(event.target.min)) {
      newValue = event.target.min;
    }

    if (Number(newValue) >= value[0]) {
      setValue([
        value[0] !== Number(startValue) &&
        Number(startValue) <= Number(newValue)
          ? Number(startValue)
          : value[0],
        Number(newValue),
      ]);
    }
  };

  const onFieldKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const element = event.target as HTMLInputElement;

    if (!element || event.key !== 'Enter') {
      return;
    }

    element.blur();
  };

  const startFieldAdornment =
    config.adornments &&
    config.adornments.find(
      (adornment) => adornment.value >= Number(startValue)
    );
  const endFieldAdornment =
    config.adornments &&
    config.adornments.find((adornment) => adornment.value >= Number(endValue));

  return (
    <div
      className={
        className
          ? `${className} ${classes.fieldDuoSlider}`
          : classes.fieldDuoSlider
      }
    >
      <div className={classes.fields}>
        <MuiTextField
          className={classes.field}
          style={{
            marginBottom:
              startFieldAdornment || endFieldAdornment ? '44px' : '24px',
          }}
          label={config.startLabel}
          variant="outlined"
          color={
            Number(startValue) <= Number(endValue) ? 'primary' : 'secondary'
          }
          type="number"
          value={startValue}
          onChange={onStartChange}
          onKeyPress={onFieldKeyPress}
          InputProps={{
            endAdornment: startFieldAdornment && (
              <MuiInputAdornment className={classes.adornment} position="end">
                <picture>
                  <source
                    type="image/avif"
                    srcSet={startFieldAdornment.imageAvif}
                  />
                  <source
                    type="image/webp"
                    srcSet={startFieldAdornment.imageWebp}
                  />
                  <img
                    className={classes.adornmentImage}
                    alt={startFieldAdornment.name}
                    src={startFieldAdornment.image}
                  />
                </picture>
                <MuiTypography
                  className={classes.adornmentText}
                  variant="caption"
                >
                  {startFieldAdornment.name}
                </MuiTypography>
              </MuiInputAdornment>
            ),
            inputProps: {
              maxLength: config.maxLength,
              min: config.min,
              max: config.max,
              ref: startRef,
            },
          }}
        />
        <MuiTextField
          className={classes.field}
          style={{
            marginBottom:
              startFieldAdornment || endFieldAdornment ? '44px' : '24px',
          }}
          label={config.endLabel}
          variant="outlined"
          color={
            Number(endValue) >= Number(startValue) ? 'primary' : 'secondary'
          }
          type="number"
          value={endValue}
          onChange={onEndChange}
          onKeyPress={onFieldKeyPress}
          InputProps={{
            endAdornment: endFieldAdornment && (
              <MuiInputAdornment className={classes.adornment} position="end">
                <picture>
                  <source
                    type="image/avif"
                    srcSet={endFieldAdornment.imageAvif}
                  />
                  <source
                    type="image/webp"
                    srcSet={endFieldAdornment.imageWebp}
                  />
                  <img
                    className={classes.adornmentImage}
                    alt={endFieldAdornment.name}
                    src={endFieldAdornment.image}
                  />
                </picture>
                <MuiTypography
                  className={classes.adornmentText}
                  variant="caption"
                >
                  {endFieldAdornment.name}
                </MuiTypography>
              </MuiInputAdornment>
            ),
            inputProps: {
              maxLength: config.maxLength,
              min: config.min,
              max: config.max,
              ref: endRef,
            },
          }}
        />
        {config.extra}
      </div>
      <MuiSlider
        className={classes.slider}
        classes={{ markLabel: classes.sliderMark }}
        value={value}
        onChange={onSliderChange}
        valueLabelDisplay="on"
        marks={config.marks}
        min={config.min}
        max={config.max}
        step={config.step}
      />
    </div>
  );
};

export default FieldDuoSlider;
