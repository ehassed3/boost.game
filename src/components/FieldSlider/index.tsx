import React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTextField from '@material-ui/core/TextField';
import MuiInputAdornment from '@material-ui/core/InputAdornment';
import MuiTypography from '@material-ui/core/Typography';
import MuiSlider, { Mark } from '@material-ui/core/Slider';

const useStyles = makeStyles((theme: Theme) => ({
  fieldSlider: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '6px',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
    },
  },
  fields: {
    display: 'flex',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      width: '100%',
    },
  },
  field: {
    minWidth: '260px',

    '&:not(:last-child)': {
      marginRight: '20px',
    },

    [theme.breakpoints.down('xs')]: {
      '&:not(:last-child)': {
        marginBottom: '24px',
        marginRight: '0',
      },
    },
  },
  adornment: {
    position: 'relative',
  },
  adornmentImage: {
    height: '36px',
    pointerEvents: 'none',
    width: '36px',
  },
  adornmentText: {
    bottom: '-56px',
    position: 'absolute',
    right: '-10px',
  },
  slider: {
    margin: '40px 0 28px',

    [theme.breakpoints.down('sm')]: {
      margin: '36px 0 8px',
    },

    [theme.breakpoints.down('xs')]: {
      width: '95%',
    },
  },
  sliderMark: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

export interface Adornment {
  image: string;
  imageAvif: string;
  imageWebp: string;
  name: string;
  value: number;
}

export interface Config {
  min: number;
  max: number;
  label?: string;
  name: string;
  adornments?: Adornment[];
  maxLength?: number;
  step?: number | null;
  marks?: boolean | Mark[];
  extra?: React.ReactNode;
}

export interface Props {
  className?: string;
  config: Config;
  value: string | number;
  setValue: (value: string | number) => void;
}

const FieldSlider: React.FC<Props> = ({
  className,
  config,
  value,
  setValue,
}) => {
  const classes = useStyles();

  const [fieldValue, setFieldValue] = React.useState(
    value ? String(value) : '0'
  );

  const onFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = event.target.value;

    if (newValue.length > event.target.maxLength) {
      return;
    }

    if (Number(newValue) > Number(event.target.max)) {
      newValue = event.target.max;
    }

    setFieldValue(newValue);

    if (Number(newValue) < Number(event.target.min)) {
      newValue = event.target.min;
    }

    setValue(
      Number(newValue) <= Number(event.target.min)
        ? Number(event.target.min)
        : Number(newValue)
    );
  };

  const onFieldKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const element = event.target as HTMLInputElement;

    if (!element || event.key !== 'Enter') {
      return;
    }

    element.blur();
  };

  const onSliderChange = (
    _: React.ChangeEvent<unknown>,
    newSliderValue: number | number[]
  ) => {
    if (Array.isArray(newSliderValue)) {
      return;
    }

    setFieldValue(String(newSliderValue));
    setValue(newSliderValue);
  };

  const fieldAdornment =
    config.adornments &&
    config.adornments.find((adornment) => adornment.value >= Number(value));

  return (
    <div
      className={
        className ? `${className} ${classes.fieldSlider}` : classes.fieldSlider
      }
    >
      <div
        className={classes.fields}
        style={{
          marginBottom: fieldAdornment ? '44px' : '20px',
        }}
      >
        <MuiTextField
          className={classes.field}
          label={config.label}
          variant="outlined"
          type="number"
          value={fieldValue}
          onChange={onFieldChange}
          onKeyPress={onFieldKeyPress}
          InputProps={{
            endAdornment: fieldAdornment && (
              <MuiInputAdornment className={classes.adornment} position="end">
                <picture>
                  <source type="image/avif" srcSet={fieldAdornment.imageAvif} />
                  <source type="image/webp" srcSet={fieldAdornment.imageWebp} />
                  <img
                    className={classes.adornmentImage}
                    alt={fieldAdornment.name}
                    src={fieldAdornment.image}
                  />
                </picture>
                <MuiTypography
                  className={classes.adornmentText}
                  variant="caption"
                >
                  {fieldAdornment.name}
                </MuiTypography>
              </MuiInputAdornment>
            ),
            inputProps: {
              maxLength: config.maxLength,
              min: config.min,
              max: config.max,
            },
          }}
        />
        {config.extra}
      </div>
      <MuiSlider
        className={classes.slider}
        classes={{ markLabel: classes.sliderMark }}
        value={Number(fieldValue)}
        onChange={onSliderChange}
        valueLabelDisplay="on"
        marks={config.marks}
        min={config.min}
        max={config.max}
        step={config.step}
      />
    </div>
  );
};

export default FieldSlider;
