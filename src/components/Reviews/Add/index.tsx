import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles } from '@material-ui/core/styles';
import MuiTooltip from '@material-ui/core/Tooltip';
import MuiButton from '@material-ui/core/Button';
import MuiDialog from '@material-ui/core/Dialog';
import MuiRating from '@material-ui/lab/Rating';
import MuiTextField from '@material-ui/core/TextField';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiSnackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import MuiCloseIcon from '@material-ui/icons/Close';

import Checkbox from '@components/Checkbox';

import SERVER from '@constants/server';

const useStyles = makeStyles(() => ({
  buttonRating: {
    color: '#faaf00',
  },
  dialogPaper: {
    margin: '16px',
    padding: '16px',
    width: 'calc(100% - 32px)',
  },
  topControls: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '20px',
  },
  controls: {
    alignItems: 'center',
    display: 'flex',
  },
  text: {
    marginBottom: '28px',
  },
  textField: {
    marginBottom: '16px',
  },
  sendButton: {
    marginLeft: 'auto',
  },
  loader: {
    bottom: '0',
    left: '0',
    position: 'absolute',
    right: '0',
  },
}));

interface Props {
  className?: string;
  shortId: string;
  disabled?: boolean;
  tooltip?: string;
  reload?: () => void;
  loading?: boolean;
}

const AddReview: React.FC<Props> = ({
  className,
  shortId,
  disabled,
  tooltip,
  reload,
  loading,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const [loadingData, setLoadingData] = React.useState(false);
  const [error, setError] = React.useState('');

  const [text, setText] = React.useState('');
  const [rating, setRating] = React.useState(0);
  const [showScreenshots, setShowScreenshots] = React.useState(true);

  const onSubmit = () => {
    setLoadingData(true);

    fetch(`${SERVER}/api/set_review/`, {
      method: 'POST',
      body: JSON.stringify({
        shortId,
        rating,
        showScreenshots,
        text,
        locale: intl.locale,
      }),
    })
      .then((res) => {
        if (reload) {
          reload();
        }

        setLoadingData(false);
        setOpen(false);

        return res.ok ? '' : res.text();
      })
      .then((message) => {
        if (!message) {
          return;
        }

        setError(message);
      })
      .catch(() => {
        setError(intl.formatMessage({ id: 'addReview.error' }));
      });
  };

  return (
    <>
      <MuiTooltip title={tooltip || ''}>
        <div className={className}>
          <MuiButton
            color="primary"
            variant="contained"
            disabled={disabled}
            size="large"
            onClick={() => setOpen(true)}
          >
            {intl.formatMessage({ id: 'addReview.button' })}
          </MuiButton>
        </div>
      </MuiTooltip>
      {!disabled && open && (
        <MuiDialog
          classes={{ paper: classes.dialogPaper }}
          fullWidth
          open={open}
          onClose={() => setOpen(false)}
        >
          <>
            <div className={classes.topControls}>
              <MuiRating
                name="rating"
                size="large"
                disabled={loading || loadingData}
                value={rating}
                onChange={(
                  _: React.ChangeEvent<unknown>,
                  value: number | null
                ) => {
                  setRating(value || 0);
                }}
              />
              <MuiIconButton onClick={() => setOpen(false)}>
                <MuiCloseIcon fontSize="small" />
              </MuiIconButton>
            </div>
            <MuiTextField
              className={classes.textField}
              placeholder={intl.formatMessage({
                id: 'addReview.textFieldPlaceholder',
              })}
              variant="outlined"
              multiline
              rows={7}
              disabled={loading || loadingData}
              InputProps={{ inputProps: { maxLength: 280 } }}
              value={text}
              onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
                setText(event.target.value);
              }}
              onKeyPress={(event) => {
                const element = event.target as HTMLTextAreaElement;

                if (!element || event.key !== 'Enter') {
                  return;
                }

                element.blur();
              }}
            />
            <div className={classes.controls}>
              <Checkbox
                label={intl.formatMessage({
                  id: 'addReview.screenshotsCheckbox',
                })}
                name="showScreenshots"
                color="primary"
                checked={showScreenshots}
                onChange={(
                  _: React.ChangeEvent<HTMLInputElement>,
                  checked: boolean
                ) => {
                  setShowScreenshots(checked);
                }}
              />
              <MuiTooltip
                title={
                  !text || !rating
                    ? intl.formatMessage({ id: 'addReview.tooltipSendButton' })
                    : ''
                }
              >
                <div className={classes.sendButton}>
                  <MuiButton
                    color="primary"
                    variant="contained"
                    disabled={loading || loadingData || !text || !rating}
                    onClick={onSubmit}
                  >
                    {intl.formatMessage({ id: 'addReview.sendButton' })}
                  </MuiButton>
                </div>
              </MuiTooltip>
            </div>
            {(loading || loadingData) && (
              <MuiLinearProgress className={classes.loader} />
            )}
          </>
        </MuiDialog>
      )}
      {error && (
        <MuiSnackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          autoHideDuration={6000}
          open={!!error}
          onClose={() => {
            setError('');
          }}
        >
          <MuiAlert
            onClose={() => {
              setError('');
            }}
            severity="error"
          >
            {error}
          </MuiAlert>
        </MuiSnackbar>
      )}
    </>
  );
};

export default AddReview;
