import * as React from 'react';
import { useIntl, IntlShape } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiRating from '@material-ui/lab/Rating';

import Avatar from '@components/Avatar';
import ScreenshotsButton from '@components/Button/Screenshots';

import { FullReview } from '@interfaces/review';

const useStyles = makeStyles((theme: Theme) => ({
  review: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    padding: '0 8px',
  },
  user: {
    display: 'flex',
    padding: '6px 0',
  },
  avatar: {
    height: '28px',
    width: '28px',
  },
  name: {
    ...theme.typography.h6,

    marginLeft: '8px',
  },
  title: {
    ...theme.typography.body2,

    color: theme.palette.text.secondary,
    marginBottom: '12px',
  },
  text: {
    marginBottom: '12px',
  },
  controls: {
    alignItems: 'center',
    display: 'flex',
    width: '100%',
  },
  rating: {
    marginRight: 'auto',
  },
}));

const getScreenshotsDisabledReason = (
  intl: IntlShape,
  showScreenshots: boolean,
  screenshots?: string[]
): string => {
  if (!showScreenshots) {
    return intl.formatMessage({ id: 'review.noScreenshotsDisabledReason' });
  }

  if (!screenshots || !screenshots.length) {
    return intl.formatMessage({ id: 'review.noUserScreenshotsDisabledReason' });
  }

  return '';
};

interface Props {
  className?: string;
  review: FullReview;
}

const Review: React.FC<Props> = ({ className, review }) => {
  const intl = useIntl();
  const classes = useStyles();

  const screenshotsDisabledReason = getScreenshotsDisabledReason(
    intl,
    review.showScreenshots,
    review.screenshots
  );

  return (
    <div
      className={className ? `${className} ${classes.review}` : classes.review}
    >
      <div className={classes.user}>
        <Avatar
          className={classes.avatar}
          src={review.image}
          alt={review.name}
        />
        <MuiTypography className={classes.name}>{review.name}</MuiTypography>
      </div>
      <MuiTypography className={classes.title}>{review.title}</MuiTypography>
      <MuiTypography className={classes.text}>{review.text}</MuiTypography>
      <div className={classes.controls}>
        <MuiRating className={classes.rating} readOnly value={review.rating} />
        <ScreenshotsButton
          screenshots={review.screenshots}
          disabled={!!screenshotsDisabledReason}
          tooltip={screenshotsDisabledReason}
        />
      </div>
    </div>
  );
};

export default Review;
