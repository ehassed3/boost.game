import React from 'react';
import { useIntl } from 'react-intl';
import { useSession } from 'next-auth/client';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiTextField from '@material-ui/core/TextField';
import MuiButtonBase from '@material-ui/core/ButtonBase';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import SERVER from '@constants/server';

import { DirtyOrder } from '@interfaces/order';

const useStyles = makeStyles((theme: Theme) => ({
  standardPayment: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  title: {
    ...theme.typography.h5,

    display: 'flex',
    fontWeight: 600,
    marginBottom: '2px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h6,

      fontWeight: 600,
    },
  },
  description: {
    ...theme.typography.body2,

    color: theme.palette.text.secondary,
    fontWeight: 600,
  },
  email: {
    marginBottom: '24px',
    marginTop: '24px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '20px',
      marginTop: '12px',
    },
  },
  helperText: {
    bottom: '-20px',
    position: 'absolute',
  },
  button: {
    ...theme.palette.action.focus.big,

    backgroundColor: theme.palette.primary.main,
    borderRadius: '4px',
    cursor: 'pointer',
    height: '42px',
    marginBottom: '6px',
    minWidth: '180px',
    overflow: 'hidden',
    transform: 'translate3d(0px, 0%, 0px)',
    position: 'relative',
    transitionDelay: '0.6s',
    width: '100%',

    '&::before': {
      backgroundColor: '#421561',
      borderRadius: '50% 50% 0 0',
      content: '""',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      transform: 'translateY(100%) scaleY(.5)',
      transition: 'all .6s ease',
      width: '100%',
    },

    '&::after': {
      backgroundColor: theme.palette.primary.main,
      borderRadius: '0',
      content: '""',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      transform: 'translateY(0) scaleY(1)',
      transition: 'all .6s ease',
      width: '100%',
    },

    '&:hover, &:focus': {
      backgroundColor: '#421561',
      transition: 'background .2s linear',
      transitionDelay: '.6s',

      '&::before': {
        borderRadius: '0',
        transform: 'translateY(0) scaleY(1)',
        transitionDelay: '0',
      },

      '&::after': {
        borderRadius: '0 0 50% 50%',
        transform: 'translateY(-100%) scaleY(.5)',
        transitionDelay: '0',
      },

      '& $text': {
        '&:first-child': {
          transform: 'translateY(0)',
        },

        '&:last-child': {
          transform: 'translateY(-24px)',
        },
      },
    },

    '&:disabled': {
      backgroundColor: 'rgba(255, 255, 255, 0.12)',
      transition: 'none',
      boxShadow: 'none',

      '& $content span': {
        color: 'rgba(255, 255, 255, 0.3)',
      },

      '&::before': {
        backgroundColor: 'transparent',
        transform: 'none',
      },

      '&::after': {
        backgroundColor: 'transparent',
        transform: 'none',
      },
    },
  },
  content: {
    display: 'flex',
    height: '26px',
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'relative',
    textAlign: 'center',
    textTransform: 'uppercase',
    top: '2px',
    width: '100%',
  },
  text: {
    ...theme.typography.body1,

    fontWeight: 500,
    position: 'absolute',
    top: '0',
    transition: 'transform .5s ease',
    width: '100%',
    zIndex: 1,

    '&:first-child': {
      color: '#ffffff',
      transform: 'translateY(24px)',
    },

    '&:last-child': {
      color: '#ffffff',
      transform: 'translateY(0)',
    },
  },
  loader: {
    bottom: '-6px',
    left: '0',
    position: 'absolute',
    right: '20px',

    [theme.breakpoints.down('xs')]: {
      bottom: '10px',
      right: '0',
    },
  },
}));

const EMAIL_REGEX = /^(([^<>()\\[\]\\.,;:\s@\\"]+(\.[^<>()\\[\]\\.,;:\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\]\\.,;:\s@\\"]+\.)+[^<>()[\]\\.,;:\s@\\"]{2,})$/;

interface Props {
  className?: string;
  order: DirtyOrder;
}

const DiscordPayment: React.FC<Props> = ({ className, order }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [session, loading] = useSession();

  const [email, setEmail] = React.useState('');
  const [error, setError] = React.useState('');
  const [loadingButton, setLoadingButton] = React.useState(false);

  React.useEffect(() => {
    if (!session?.user) {
      return;
    }

    setEmail(session.user.email);
  }, [session?.user]);

  return (
    <>
      <div
        className={
          className
            ? `${className} ${classes.standardPayment}`
            : classes.standardPayment
        }
      >
        <MuiTypography className={classes.title}>
          {intl.formatMessage({ id: 'payment.standardTitle' })}
        </MuiTypography>
        <MuiTypography className={classes.description}>
          {intl.formatMessage({ id: 'payment.standardDescription' })}
        </MuiTypography>
        <MuiTextField
          className={classes.email}
          FormHelperTextProps={{ className: classes.helperText }}
          label={intl.formatMessage({ id: 'payment.emailLabel' })}
          name="email"
          type="email"
          variant="filled"
          size="small"
          value={email}
          error={!!error}
          helperText={error}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            if (error) {
              setError('');
            }

            setEmail(event.target.value);
          }}
        />
        <MuiButtonBase
          className={classes.button}
          focusRipple
          disabled={loading}
          onClick={() => {
            const isValid = EMAIL_REGEX.test(email);

            if (!isValid) {
              setError(intl.formatMessage({ id: 'payment.emailError' }));
              return;
            }

            gtag('event', 'click', {
              event_category: 'button',
              event_label: 'Payment with email',
            });
            setLoadingButton(true);
            fetch(`${SERVER}/api/set_raw_order/`, {
              method: 'POST',
              body: JSON.stringify({ ...order, user: email }),
            })
              .then((response) => response.text())
              .then((url) => {
                window.location.href = url;
              })
              .catch(() => {
                setLoadingButton(false);
              });
          }}
        >
          <div className={classes.content}>
            <span className={classes.text}>
              {intl.formatMessage({ id: 'payment.button' })}
            </span>
            <span className={classes.text}>
              {intl.formatMessage({ id: 'payment.button' })}
            </span>
          </div>
        </MuiButtonBase>
        {loadingButton && <MuiLinearProgress className={classes.loader} />}
      </div>
    </>
  );
};

export default DiscordPayment;
