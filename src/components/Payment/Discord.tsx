import React from 'react';
import { useIntl } from 'react-intl';
import { useSession, signIn } from 'next-auth/client';
import qs from 'qs';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiButtonBase from '@material-ui/core/ButtonBase';
import MuiLinearProgress from '@material-ui/core/LinearProgress';

import DiscordDescription from '@components/Payment/DiscordDescription';

import DiscordTextIcon from '@icons/Discord/Text';

import SERVER from '@constants/server';

import { DirtyOrder } from '@interfaces/order';

const useStyles = makeStyles((theme: Theme) => ({
  discordPayment: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  title: {
    ...theme.typography.h5,

    display: 'flex',
    fontWeight: 600,
    marginBottom: '2px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h6,

      fontWeight: 600,
    },
  },
  icon: {
    height: '31px',
    marginLeft: '4px',
    width: '103px',

    [theme.breakpoints.down('xs')]: {
      height: '32px',
      width: '85px',
    },
  },
  description: {
    ...theme.typography.body2,

    color: theme.palette.text.secondary,
    fontWeight: 600,
    marginBottom: 'auto',
  },
  note: {
    marginBottom: '4px',
    marginRight: 'auto',
    marginTop: '28px',

    [theme.breakpoints.down('xs')]: {
      marginTop: '12px',
    },
  },
  button: {
    ...theme.palette.action.focus.big,

    backgroundColor: '#7289da',
    borderRadius: '4px',
    cursor: 'pointer',
    height: '42px',
    marginBottom: '6px',
    minWidth: '180px',
    overflow: 'hidden',
    position: 'relative',
    transform: 'translate3d(0px, 0%, 0px)',
    transitionDelay: '0.6s',
    width: '100%',

    '&::before': {
      backgroundColor: '#421561',
      borderRadius: '50% 50% 0 0',
      content: '""',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      transform: 'translateY(100%) scaleY(.5)',
      transition: 'all .6s ease',
      width: '100%',
    },

    '&::after': {
      backgroundColor: '#7289da',
      borderRadius: '0',
      content: '""',
      height: '100%',
      left: '0',
      position: 'absolute',
      top: '0',
      transform: 'translateY(0) scaleY(1)',
      transition: 'all .6s ease',
      width: '100%',
    },

    '&:hover, &:focus': {
      backgroundColor: '#421561',
      transition: 'background .2s linear',
      transitionDelay: '.6s',

      '&::before': {
        borderRadius: '0',
        transform: 'translateY(0) scaleY(1)',
        transitionDelay: '0',
      },

      '&::after': {
        borderRadius: '0 0 50% 50%',
        transform: 'translateY(-100%) scaleY(.5)',
        transitionDelay: '0',
      },

      '& $text': {
        '&:first-child': {
          transform: 'translateY(0)',
        },

        '&:last-child': {
          transform: 'translateY(-24px)',
        },
      },
    },

    '&:disabled': {
      backgroundColor: 'rgba(255, 255, 255, 0.12)',
      transition: 'none',
      boxShadow: 'none',

      '& $content span': {
        color: 'rgba(255, 255, 255, 0.3)',
      },

      '&::before': {
        backgroundColor: 'transparent',
        transform: 'none',
      },

      '&::after': {
        backgroundColor: 'transparent',
        transform: 'none',
      },
    },
  },
  content: {
    display: 'flex',
    height: '26px',
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'relative',
    textAlign: 'center',
    textTransform: 'uppercase',
    top: '2px',
    width: '100%',
  },
  text: {
    ...theme.typography.body1,

    fontWeight: 500,
    position: 'absolute',
    top: '0',
    transition: 'transform .5s ease',
    width: '100%',
    zIndex: 1,

    '&:first-child': {
      color: '#ffffff',
      transform: 'translateY(24px)',
    },

    '&:last-child': {
      color: '#ffffff',
      transform: 'translateY(0)',
    },
  },
  loader: {
    bottom: '-6px',
    left: '20px',
    position: 'absolute',
    right: '0',

    [theme.breakpoints.down('xs')]: {
      left: '0',
    },
  },
}));

interface Props {
  className?: string;
  order: DirtyOrder;
}

const DiscordPayment: React.FC<Props> = ({ className, order }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [session, loading] = useSession();

  const [loadingButton, setLoadingButton] = React.useState(false);

  const onClick = () => {
    if (!session) {
      gtag('event', 'click', {
        event_category: 'button',
        event_label: 'Payment without session',
      });
      signIn('discord', {
        callbackUrl: `${SERVER}/api/set_raw_order/?${qs.stringify(order)}`,
      });
      return;
    }

    gtag('event', 'click', {
      event_category: 'button',
      event_label: 'Payment with session',
    });
    setLoadingButton(true);
    fetch(`${SERVER}/api/set_raw_order/`, {
      method: 'POST',
      body: JSON.stringify(order),
    })
      .then((response) => response.text())
      .then((url) => {
        window.location.href = url;
      })
      .catch(() => {
        setLoadingButton(false);
      });
  };

  return (
    <>
      <div
        className={
          className
            ? `${className} ${classes.discordPayment}`
            : classes.discordPayment
        }
      >
        <MuiTypography className={classes.title}>
          {intl.formatMessage(
            { id: 'payment.discordTitle' },
            { discord: <DiscordTextIcon className={classes.icon} /> }
          )}
        </MuiTypography>
        <MuiTypography className={classes.description}>
          {intl.formatMessage({ id: 'payment.discordDescription' })}
        </MuiTypography>
        <DiscordDescription className={classes.note} />
        <MuiButtonBase
          className={classes.button}
          focusRipple
          disabled={loading}
          onClick={onClick}
        >
          <div className={classes.content}>
            <span className={classes.text}>
              {intl.formatMessage({ id: 'payment.button' })}
            </span>
            <span className={classes.text}>
              {intl.formatMessage({ id: 'payment.button' })}
            </span>
          </div>
        </MuiButtonBase>
        {loadingButton && <MuiLinearProgress className={classes.loader} />}
      </div>
    </>
  );
};

export default DiscordPayment;
