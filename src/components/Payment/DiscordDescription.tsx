import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import MuiDialog from '@material-ui/core/Dialog';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import DiscordTextIcon from '@icons/Discord/Text';

const useStyles = makeStyles((theme: Theme) => ({
  discordDescription: {
    cursor: 'pointer',
    fontWeight: 600,
    textDecoration: 'underline',
  },
  dialog: {
    padding: '16px 20px 0',
  },
  close: {
    position: 'absolute',
    right: '12px',
    top: '10px',
  },
  dialogTitle: {
    ...theme.typography.h5,

    fontWeight: 600,
    marginBottom: '16px',
    marginRight: '36px',
    minHeight: '32px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',

    [theme.breakpoints.down('xs')]: {
      fontSize: '0',
    },
  },
  dialogTitleIcon: {
    height: '25px',
    margin: '3px 0 -4px',
    width: '102px',

    [theme.breakpoints.down('xs')]: {
      margin: '3px 0 -4px -4px',
    },
  },
  dialogContent: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'scroll',
    padding: '4px 0 16px',
  },
  dialogDescription: {
    fontWeight: 500,
    marginBottom: '12px',
  },
  dialogItemLine: {
    flexShrink: 0,
    height: '100px',
    marginTop: '8px',
    width: '1px',

    [theme.breakpoints.down('xs')]: {
      height: '60px',
    },
  },
  dialogAuthLine: {
    backgroundImage: `linear-gradient(${theme.palette.background.paper}, #019ae9)`,
  },
  dialogTrackingLine: {
    backgroundImage: `linear-gradient(${theme.palette.background.paper}, #9a1fB8)`,
  },
  dialogClosingLine: {
    backgroundImage: `linear-gradient(${theme.palette.background.paper}, #f9cb28)`,
  },
  dialogItemIcon: {
    alignItems: 'center',
    borderRadius: '100%',
    display: 'inline-flex',
    flexShrink: 0,
    fontWeight: 600,
    height: '40px',
    justifyContent: 'center',
    marginBottom: '12px',
    width: '40px',
  },
  dialogAuthIcon: {
    backgroundImage: 'linear-gradient(90deg, #007cf0, #00dfd8)',
  },
  dialogTrackingIcon: {
    backgroundImage: 'linear-gradient(90deg, #7928ca, #ff0080)',
  },
  dialogClosingIcon: {
    backgroundImage: 'linear-gradient(90deg, #ff4d4d, #f9cb28)',
  },
  dialogItemTitle: {
    ...theme.typography.h5,

    '-webkit-background-clip': 'text',
    '-webkit-text-fill-color': 'transparent',
    backgroundClip: 'text',
    fontWeight: 500,
    textAlign: 'center',
  },
  dialogAuthTitle: {
    backgroundImage: 'linear-gradient(90deg, #007cf0, #00dfd8)',
  },
  dialogTrackingTitle: {
    backgroundImage: 'linear-gradient(90deg, #7928ca, #ff0080)',
  },
  dialogClosingTitle: {
    backgroundImage: 'linear-gradient(90deg, #ff4d4d, #f9cb28)',
  },
  dialogItem: {
    display: 'flex',
    marginBottom: '20px',
    flexDirection: 'column',
    marginTop: '20px',
  },
  dialogItemImages: {
    flexShrink: 0,
    marginTop: '12px',
    marginBottom: '60px',
    position: 'relative',
    width: '100%',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '28px',
      marginTop: '0',
    },
  },
  dialogItemFirstPicture: {
    backgroundColor: '#333333',
    borderRadius: '8px',
    boxShadow: '0px 8px 30px #000000',
    display: 'block',
    minHeight: '292px',
    width: 'calc(100% - 40px)',

    [theme.breakpoints.down('xs')]: {
      minHeight: '110px',
      width: 'calc(100% - 20px)',
    },
  },
  dialogItemSecondPicture: {
    backgroundColor: '#333333',
    borderRadius: '8px',
    boxShadow: '0px 8px 30px #000000',
    display: 'block',
    height: '100%',
    left: '40px',
    minHeight: '292px',
    position: 'absolute',
    top: '60px',
    width: 'calc(100% - 40px)',

    [theme.breakpoints.down('xs')]: {
      left: '20px',
      minHeight: '110px',
      top: '28px',
      width: 'calc(100% - 20px)',
    },
  },
  dialogItemFirstImage: {
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '8px',
    display: 'block',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center top',
    width: '100%',
  },
  dialogItemSecondImage: {
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '8px',
    display: 'block',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center',
    width: '100%',
  },
  dialogItemDescription: {
    fontWeight: 500,
    marginTop: '28px',

    [theme.breakpoints.down('xs')]: {
      marginTop: '20px',
    },
  },
}));

interface Props {
  className?: string;
}

const DiscordDescription: React.FC<Props> = ({ className }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  return (
    <>
      <MuiTypography
        className={
          className
            ? `${className} ${classes.discordDescription}`
            : classes.discordDescription
        }
        onClick={() => {
          setOpen(true);
        }}
      >
        {intl.formatMessage({ id: 'authDescription.note' })}
      </MuiTypography>
      <MuiDialog
        classes={{ paper: classes.dialog }}
        maxWidth="sm"
        fullWidth
        open={open}
        onClose={() => setOpen(false)}
      >
        <MuiIconButton className={classes.close} onClick={() => setOpen(false)}>
          <MuiCloseIcon fontSize="small" />
        </MuiIconButton>
        <MuiTypography className={classes.dialogTitle}>
          {intl.formatMessage(
            { id: 'payment.discordTitle' },
            { discord: <DiscordTextIcon className={classes.dialogTitleIcon} /> }
          )}
        </MuiTypography>
        <div className={classes.dialogContent}>
          <div>
            <MuiTypography className={classes.dialogDescription}>
              {intl.formatMessage({ id: 'authDescription.description1' })}
            </MuiTypography>
            <MuiTypography className={classes.dialogDescription}>
              {intl.formatMessage({ id: 'authDescription.description2' })}
            </MuiTypography>
          </div>
          <div
            className={`${classes.dialogItemLine} ${classes.dialogAuthLine}`}
          />
          <div
            className={`${classes.dialogItemIcon} ${classes.dialogAuthIcon}`}
          >
            1
          </div>
          <MuiTypography
            className={`${classes.dialogItemTitle} ${classes.dialogAuthTitle}`}
          >
            {intl.formatMessage({ id: 'authDescription.authTitle' })}
          </MuiTypography>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/auth_1_background.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/auth_1_background.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="First auth"
                  src="/static/auth/auth_1_background.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source type="image/avif" srcSet="/static/auth/auth_1.avif" />
                <source type="image/webp" srcSet="/static/auth/auth_1.webp" />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="First auth"
                  src="/static/auth/auth_1.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({ id: 'authDescription.authDescription1' })}
            </MuiTypography>
          </div>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/auth_2_background.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/auth_2_background.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="Second auth"
                  src="/static/auth/auth_2_background.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source type="image/avif" srcSet="/static/auth/auth_2.avif" />
                <source type="image/webp" srcSet="/static/auth/auth_2.webp" />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="Second auth"
                  src="/static/auth/auth_2.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({ id: 'authDescription.authDescription2' })}
            </MuiTypography>
          </div>
          <div
            className={`${classes.dialogItemLine} ${classes.dialogTrackingLine}`}
          />
          <div
            className={`${classes.dialogItemIcon} ${classes.dialogTrackingIcon}`}
          >
            2
          </div>
          <MuiTypography
            className={`${classes.dialogItemTitle} ${classes.dialogTrackingTitle}`}
          >
            {intl.formatMessage({ id: 'authDescription.trackingTitle' })}
          </MuiTypography>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/tracking_1_background.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/tracking_1_background.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="First tracking"
                  src="/static/auth/tracking_1_background.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/tracking_1.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/tracking_1.webp"
                />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="First tracking"
                  src="/static/auth/tracking_1.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({
                id: 'authDescription.trackingDescription1',
              })}
            </MuiTypography>
          </div>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/tracking_2_background.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/tracking_2_background.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="Second tracking"
                  src="/static/auth/tracking_2_background.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/tracking_2.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/tracking_2.webp"
                />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="Second tracking"
                  src="/static/auth/tracking_2.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({
                id: 'authDescription.trackingDescription2',
              })}
            </MuiTypography>
          </div>
          <div
            className={`${classes.dialogItemLine} ${classes.dialogClosingLine}`}
          />
          <div
            className={`${classes.dialogItemIcon} ${classes.dialogClosingIcon}`}
          >
            3
          </div>
          <MuiTypography
            className={`${classes.dialogItemTitle} ${classes.dialogClosingTitle}`}
          >
            {intl.formatMessage({ id: 'authDescription.closingTitle' })}
          </MuiTypography>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/closing_1_background.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/closing_1_background.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="First closing"
                  src="/static/auth/closing_1_background.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/closing_1.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/closing_1.webp"
                />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="First closing"
                  src="/static/auth/closing_1.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({
                id: 'authDescription.closingDescription1',
              })}
            </MuiTypography>
          </div>
          <div className={classes.dialogItem}>
            <div className={classes.dialogItemImages}>
              <picture className={classes.dialogItemFirstPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/closing_2.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/closing_2.webp"
                />
                <img
                  className={classes.dialogItemFirstImage}
                  alt="Second closing"
                  src="/static/auth/closing_2.jpg"
                />
              </picture>
              <picture className={classes.dialogItemSecondPicture}>
                <source
                  type="image/avif"
                  srcSet="/static/auth/closing_2.avif"
                />
                <source
                  type="image/webp"
                  srcSet="/static/auth/closing_2.webp"
                />
                <img
                  className={classes.dialogItemSecondImage}
                  alt="Second closing"
                  src="/static/auth/closing_2.jpg"
                />
              </picture>
            </div>
            <MuiTypography className={classes.dialogItemDescription}>
              {intl.formatMessage({
                id: 'authDescription.closingDescription2',
              })}
            </MuiTypography>
          </div>
        </div>
      </MuiDialog>
    </>
  );
};

export default DiscordDescription;
