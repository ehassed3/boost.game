import React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiButton from '@material-ui/core/Button';
import MuiDialog from '@material-ui/core/Dialog';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiTypography from '@material-ui/core/Typography';

import MuiCloseIcon from '@material-ui/icons/Close';

import Result from '@components/Result';
import StandardPayment from '@components/Payment/Standard';
import DiscordPayment from '@components/Payment/Discord';

import { DirtyOrder } from '@interfaces/order';
import { FormattedResult } from '@interfaces/result';

const useStyles = makeStyles((theme: Theme) => ({
  payment: {
    position: 'relative',
  },
  dialog: {
    alignItems: 'center',
    overflow: 'visible',
    padding: '16px 24px 20px',

    [theme.breakpoints.down('xs')]: {
      alignItems: 'flex-start',
      margin: 'auto 0 0',
      padding: '16px 16px 0',
      width: '100%',
    },
  },
  close: {
    position: 'absolute',
    right: '20px',
    top: '16px',

    [theme.breakpoints.down('xs')]: {
      right: '4px',
      top: '10px',
    },
  },
  dialogTitle: {
    ...theme.typography.h4,

    fontWeight: 600,
    marginBottom: '36px',

    [theme.breakpoints.down('xs')]: {
      ...theme.typography.h5,

      fontWeight: 600,
      marginBottom: '20px',
      paddingRight: '28px',
    },
  },
  result: {
    marginBottom: '28px',

    [theme.breakpoints.down('xs')]: {
      marginBottom: '16px',
      marginTop: '12px',
    },
  },
  dialogContent: {
    display: 'flex',

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      overflow: 'scroll',
      paddingBottom: '16px',
      width: '100%',
    },
  },
  standard: {
    paddingRight: '20px',
    width: '50%',

    [theme.breakpoints.down('xs')]: {
      paddingRight: '0',
      paddingBottom: '16px',
      width: '100%',
    },
  },
  discord: {
    borderLeft: `1px solid ${theme.palette.divider}`,
    paddingLeft: '20px',
    width: '50%',

    [theme.breakpoints.down('xs')]: {
      borderLeft: '0',
      borderTop: `1px solid ${theme.palette.divider}`,
      paddingLeft: '0',
      paddingTop: '16px',
      width: '100%',
    },
  },
  button: {
    minWidth: '180px',

    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
}));

interface Props {
  className?: string;
  order: DirtyOrder;
  result: FormattedResult;
  disabled?: boolean;
}

const PaymentButton: React.FC<Props> = ({
  className,
  order,
  result,
  disabled,
}) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  return (
    <div
      className={
        className ? `${className} ${classes.payment}` : classes.payment
      }
    >
      <MuiButton
        className={classes.button}
        color="primary"
        size="large"
        variant="contained"
        disabled={disabled}
        onClick={() => setOpen(true)}
      >
        {intl.formatMessage({ id: 'payment.button' })}
      </MuiButton>
      {open && (
        <MuiDialog
          classes={{ paper: classes.dialog }}
          maxWidth="md"
          fullWidth
          open={open}
          transitionDuration={600}
          onClose={() => setOpen(false)}
        >
          <MuiIconButton
            className={classes.close}
            onClick={() => setOpen(false)}
          >
            <MuiCloseIcon fontSize="small" />
          </MuiIconButton>
          <MuiTypography className={classes.dialogTitle}>
            {intl.formatMessage({ id: 'payment.title' })}
          </MuiTypography>
          <Result className={classes.result} result={result} />
          <div className={classes.dialogContent}>
            <StandardPayment className={classes.standard} order={order} />
            <DiscordPayment className={classes.discord} order={order} />
          </div>
        </MuiDialog>
      )}
    </div>
  );
};

export default PaymentButton;
