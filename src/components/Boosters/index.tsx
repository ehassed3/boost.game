import * as React from 'react';
import { useIntl } from 'react-intl';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiAvatarGroup from '@material-ui/lab/AvatarGroup';
import MuiDialog from '@material-ui/core/Dialog';
import MuiTypography from '@material-ui/core/Typography';
import MuiIconButton from '@material-ui/core/IconButton';

import MuiCloseIcon from '@material-ui/icons/Close';

import Avatar from '@components/Avatar';
import Booster from '@components/Boosters/Booster';

import { PublicBooster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  '@keyframes pulse': {
    '0%': {
      opacity: 1,
    },
    '50%': {
      opacity: 0.4,
    },
    '100%': {
      opacity: 1,
    },
  },
  avatarGroup: {
    cursor: 'pointer',
    position: 'relative',
    outline: 0,
  },
  avatarGroupSkeleton: {
    animation: '$pulse 1.5s ease-in-out 0.5s infinite',
    position: 'relative',
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.text.secondary,
    fontSize: '16px',
    height: '36px',
    width: '36px',

    [theme.breakpoints.down('xs')]: {
      marginLeft: '-16px',
    },
  },
  avatarSkeleton: {
    backgroundColor: '#282828',
    borderRadius: '50%',
    color: theme.palette.text.secondary,
    fontSize: '16px',
    height: '36px',
    width: '36px',

    [theme.breakpoints.down('xs')]: {
      marginLeft: '-16px',
    },
  },
  title: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    borderRadius: '8px',
    bottom: '-12px',
    color: theme.palette.primary.main,
    fontWeight: 600,
    padding: '0 4px',
    position: 'absolute',
    right: '0',
    zIndex: 1,
    whiteSpace: 'nowrap',
  },
  titleWrapperSkeleton: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    borderRadius: '8px',
    bottom: '-12px',
    padding: '7px 5px',
    position: 'absolute',
    right: '0',
    zIndex: 1,
  },
  titleSkeleton: {
    backgroundColor: '#282828',
    borderRadius: '4px',
    height: '10px',
    width: '80px',
  },
  dialog: {
    padding: '16px 12px 0',
  },
  dialogTitle: {
    ...theme.typography.h5,

    fontWeight: 600,
    paddingLeft: '8px',
    marginBottom: '20px',
  },
  boosters: {
    overflowY: 'scroll',
    paddingBottom: '16px',
  },
  booster: {
    '&:not(:last-of-type)': {
      borderBottom: `1px solid ${theme.palette.divider}`,
      marginBottom: '20px',
      paddingBottom: '20px',
    },
  },
  close: {
    position: 'absolute',
    right: '12px',
    top: '10px',
  },
}));

interface Props {
  className?: string;
  boosters?: PublicBooster[];
}

const Boosters: React.FC<Props> = ({ className, boosters }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  return boosters ? (
    <>
      {boosters.length > 0 && (
        <>
          <div
            className={
              className
                ? `${className} ${classes.avatarGroup}`
                : classes.avatarGroup
            }
            onClick={() => {
              setOpen(true);
            }}
            onKeyPress={(event: React.KeyboardEvent<HTMLDivElement>) => {
              if (event.key !== 'Enter') {
                return;
              }

              setOpen(true);
            }}
            role="button"
            tabIndex={0}
          >
            <MuiAvatarGroup classes={{ avatar: classes.avatar }} max={5}>
              {map(boosters, (booster) => (
                <Avatar
                  key={booster.name}
                  className={classes.avatar}
                  src={booster.image}
                  alt={booster.name}
                />
              ))}
            </MuiAvatarGroup>
            {boosters.length > 3 && (
              <MuiTypography className={classes.title}>
                {intl.formatMessage(
                  { id: 'boosters.title' },
                  { boosters: boosters.length }
                )}
              </MuiTypography>
            )}
          </div>
          {open && (
            <MuiDialog
              classes={{ paper: classes.dialog }}
              maxWidth="xs"
              fullWidth
              open={open}
              onClose={() => {
                setOpen(false);
              }}
            >
              <MuiIconButton
                className={classes.close}
                onClick={() => setOpen(false)}
              >
                <MuiCloseIcon fontSize="small" />
              </MuiIconButton>
              <MuiTypography className={classes.dialogTitle}>
                {intl.formatMessage({ id: 'boosters.dialogTitle' })}
              </MuiTypography>
              <div className={classes.boosters}>
                {boosters.map((booster) => (
                  <Booster
                    key={booster.name}
                    className={classes.booster}
                    booster={booster}
                  />
                ))}
              </div>
            </MuiDialog>
          )}
        </>
      )}
    </>
  ) : (
    <div
      className={
        className
          ? `${className} ${classes.avatarGroupSkeleton}`
          : classes.avatarGroupSkeleton
      }
    >
      <MuiAvatarGroup classes={{ avatar: classes.avatarSkeleton }} max={5}>
        <div className={classes.avatarSkeleton} />
        <div className={classes.avatarSkeleton} />
        <div className={classes.avatarSkeleton} />
        <div className={classes.avatarSkeleton} />
        <div className={classes.avatarSkeleton} />
      </MuiAvatarGroup>
      <div className={classes.titleWrapperSkeleton}>
        <div className={classes.titleSkeleton} />
      </div>
    </div>
  );
};

export default Boosters;
