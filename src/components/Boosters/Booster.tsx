import * as React from 'react';
import { useIntl } from 'react-intl';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiButton from '@material-ui/core/Button';
import MuiTypography from '@material-ui/core/Typography';
import MuiRating from '@material-ui/lab/Rating';

import Avatar from '@components/Avatar';
import Link from '@components/Link';

import TwitchTextIcon from '@icons/Twitch/Text';

import { PublicBooster } from '@interfaces/user';

const useStyles = makeStyles((theme: Theme) => ({
  booster: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
  },
  boosterRole: {
    color: theme.palette.text.primary,
    display: 'flex',
    marginBottom: '16px',
    textDecoration: 'none',
    textTransform: 'none',

    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      textDecoration: 'none',
    },
  },
  avatar: {
    height: '28px',
    width: '28px',
  },
  name: {
    ...theme.typography.h6,

    marginLeft: '8px',
  },
  text: {
    padding: '0 8px',
    marginBottom: '12px',
  },
  controls: {
    padding: '0 8px',
    alignItems: 'center',
    display: 'flex',
    width: '100%',
  },
  twitchButton: {
    backgroundColor: '#6441a4',
    marginLeft: 'auto',
    padding: '5px 12px 7px',

    '&:hover': {
      backgroundColor: '#a66bff',
    },
  },
  twitchIcon: {
    height: '24px',
    width: '63px',
  },
}));

interface Props {
  className?: string;
  booster: PublicBooster;
}

const Boosters: React.FC<Props> = ({ className, booster }) => {
  const intl = useIntl();
  const classes = useStyles();

  return (
    <div
      className={
        className ? `${className} ${classes.booster}` : classes.booster
      }
    >
      <Link
        className={classes.boosterRole}
        component={MuiButton}
        href={`/boosters/${booster.name}`}
      >
        <Avatar
          className={classes.avatar}
          src={booster.image}
          alt={booster.name}
        />
        <MuiTypography className={classes.name}>{booster.name}</MuiTypography>
      </Link>
      <MuiTypography className={classes.text}>
        {booster.description[intl.locale || 'en'] || booster.description.en}
      </MuiTypography>
      <div className={classes.controls}>
        {!!booster.rating && (
          <MuiRating readOnly value={booster.rating} precision={0.1} />
        )}
        <MuiButton
          className={classes.twitchButton}
          component="a"
          href={`https://twitch.tv/${booster.streamChannel}`}
          rel="noopener"
          target="_blank"
          variant="contained"
          disabled={!booster.streamChannel}
        >
          <TwitchTextIcon className={classes.twitchIcon} />
        </MuiButton>
      </div>
    </div>
  );
};

export default Boosters;
