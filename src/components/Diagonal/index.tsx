import React from 'react';

import { makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    position: 'relative',
  },
  diagonalTop: {
    borderColor: `transparent ${theme.palette.background.default} transparent transparent`,
    borderStyle: 'solid',
    borderWidth: '52px 100vw 0 0',
    height: '0',
    width: '0',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      borderWidth: '20px 100vw 0 0',
    },
  },
  diagonalTopColored: {
    borderColor: 'transparent #080808 transparent transparent',

    [theme.breakpoints.down('xs')]: {
      borderColor: 'transparent #0a0a0a transparent transparent',
    },
  },
  containerColored: {
    backgroundColor: '#080808',

    [theme.breakpoints.down('xs')]: {
      backgroundColor: '#0a0a0a',
    },
  },
  diagonalBottom: {
    borderColor: `${theme.palette.background.default} transparent transparent transparent`,
    borderStyle: 'solid',
    borderWidth: '52px 100vw 0 0',
    height: '0',
    width: '0',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      borderWidth: '20px 100vw 0 0',
    },
  },
  diagonalBottomColored: {
    borderColor: '#080808 transparent transparent transparent',

    [theme.breakpoints.down('xs')]: {
      borderColor: '#0a0a0a transparent transparent transparent',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
}

const Diagonal: React.FC<Props> = ({ children, colored }) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div
        className={
          colored
            ? `${classes.diagonalTopColored} ${classes.diagonalTop}`
            : classes.diagonalTop
        }
      />
      <div className={colored ? classes.containerColored : undefined}>
        {children}
      </div>
      <div
        className={
          colored
            ? `${classes.diagonalBottomColored} ${classes.diagonalBottom}`
            : classes.diagonalBottom
        }
      />
    </div>
  );
};

export default Diagonal;
