import * as React from 'react';
import { useIntl } from 'react-intl';
import SwipeableViews from 'react-swipeable-views';
import map from 'lodash/map';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTabs from '@material-ui/core/Tabs';
import MuiTab from '@material-ui/core/Tab';
import MuiTypography from '@material-ui/core/Typography';

import MuiTimerIcon from '@material-ui/icons/Timer';
import MuiSecurityIcon from '@material-ui/icons/Security';
import MuiErrorOutlineOutlinedIcon from '@material-ui/icons/ErrorOutlineOutlined';
import MuiInfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

const useStyles = makeStyles((theme: Theme) => ({
  tabs: {
    marginBottom: '24px',
  },
  tab: {
    minWidth: '60px',

    '&:not(:last-child)': {
      marginRight: '16px',
    },
  },
  helpTab: {
    color: theme.palette.secondary.light,
    minWidth: '60px',
  },
  title: {
    ...theme.typography.h5,

    color: theme.palette.primary.main,
    fontWeight: 500,
    marginBottom: '16px',
  },
  descriptionList: {
    margin: '0',
    paddingLeft: '20px',
  },
  descriptionItem: {
    '&:not(:last-child)': {
      marginBottom: '10px',
    },
  },
}));

export interface Config {
  info?: React.ReactNode[];
  eta?: React.ReactNode[];
  safe?: React.ReactNode[];
  requirements?: React.ReactNode[];
}

interface Props {
  className?: string;
  config: Config;
}

const DescriptionSlider: React.FC<Props> = ({ className, config }) => {
  const intl = useIntl();
  const classes = useStyles();

  const [tabValue, setTabValue] = React.useState(0);

  return (
    <div className={className}>
      <MuiTabs
        className={classes.tabs}
        indicatorColor="primary"
        value={tabValue}
        onChange={(_: React.ChangeEvent<unknown>, newTabValue: number) => {
          setTabValue(newTabValue);
        }}
      >
        {config.info && (
          <MuiTab className={classes.tab} icon={<MuiInfoOutlinedIcon />} />
        )}
        {config.eta && (
          <MuiTab className={classes.tab} icon={<MuiTimerIcon />} />
        )}
        {config.safe && (
          <MuiTab className={classes.tab} icon={<MuiSecurityIcon />} />
        )}
        {config.requirements && (
          <MuiTab
            className={classes.tab}
            icon={<MuiErrorOutlineOutlinedIcon />}
          />
        )}
      </MuiTabs>
      <SwipeableViews
        containerStyle={{
          transition: 'transform 0.4s cubic-bezier(0.15, 0.3, 0.25, 1) 0s',
        }}
        index={tabValue}
        onChangeIndex={(newTabValue: number) => {
          setTabValue(newTabValue);
        }}
      >
        {config.info && (
          <>
            <MuiTypography className={classes.title}>
              {intl.formatMessage({ id: 'descriptionSlider.infoTitle' })}
            </MuiTypography>
            <ul className={classes.descriptionList}>
              {map(config.info, (infoItem, index) => (
                <MuiTypography
                  key={index}
                  className={classes.descriptionItem}
                  component="li"
                >
                  {infoItem}
                </MuiTypography>
              ))}
            </ul>
          </>
        )}
        {config.eta && (
          <>
            <MuiTypography className={classes.title}>
              {intl.formatMessage({ id: 'descriptionSlider.etaTitle' })}
            </MuiTypography>
            <ul className={classes.descriptionList}>
              {map(config.eta, (etaItem, index) => (
                <MuiTypography
                  key={index}
                  className={classes.descriptionItem}
                  component="li"
                >
                  {etaItem}
                </MuiTypography>
              ))}
            </ul>
          </>
        )}
        {config.safe && (
          <>
            <MuiTypography className={classes.title}>
              {intl.formatMessage({ id: 'descriptionSlider.safeTitle' })}
            </MuiTypography>
            <ul className={classes.descriptionList}>
              {map(config.safe, (safeItem, index) => (
                <MuiTypography
                  key={index}
                  className={classes.descriptionItem}
                  component="li"
                >
                  {safeItem}
                </MuiTypography>
              ))}
            </ul>
          </>
        )}
        {config.requirements && (
          <>
            <MuiTypography className={classes.title}>
              {intl.formatMessage({
                id: 'descriptionSlider.requirementsTitle',
              })}
            </MuiTypography>
            <ul className={classes.descriptionList}>
              {map(config.requirements, (requirementsItem, index) => (
                <MuiTypography
                  key={index}
                  className={classes.descriptionItem}
                  component="li"
                >
                  {requirementsItem}
                </MuiTypography>
              ))}
            </ul>
          </>
        )}
      </SwipeableViews>
    </div>
  );
};

export default DescriptionSlider;
