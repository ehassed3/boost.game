import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';
import MuiSwitch, { SwitchProps } from '@material-ui/core/Switch';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  helperText: {
    display: 'flex',
  },
}));

export interface Props extends SwitchProps {
  label?: React.ReactNode;
  helperText?: React.ReactNode;
}

const Switch: React.FC<Props> = ({
  className,
  label,
  helperText,
  ...props
}) => {
  const classes = useStyles();

  return (
    <MuiFormControlLabel
      control={<MuiSwitch {...props} />}
      className={className}
      label={
        helperText ? (
          <>
            <MuiTypography>{label}</MuiTypography>
            {helperText && (
              <MuiTypography
                className={classes.helperText}
                variant="caption"
                color="textSecondary"
              >
                {helperText}
              </MuiTypography>
            )}
          </>
        ) : (
          label
        )
      }
    />
  );
};

export default Switch;
