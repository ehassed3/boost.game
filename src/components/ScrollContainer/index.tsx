import * as React from 'react';
import { animateScroll } from 'react-scroll';

import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiIconButton from '@material-ui/core/IconButton';
import MuiArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import MuiArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: 'flex',
    minHeight: '48px',
    position: 'relative',

    '&::before': {
      backgroundImage: `linear-gradient(
        to right,
        ${theme.palette.background.default},
        transparent
      )`,
      content: '""',
      height: '100%',
      left: '0',
      pointerEvents: 'none',
      position: 'absolute',
      top: '0',
      width: '24px',
      zIndex: 1,

      [theme.breakpoints.down('xs')]: {
        width: '20px',
      },
    },

    '&::after': {
      backgroundImage: `linear-gradient(
        to left,
        ${theme.palette.background.default},
        transparent
      )`,
      content: '""',
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      right: '0',
      top: '0',
      width: '24px',
      zIndex: 1,

      [theme.breakpoints.down('xs')]: {
        width: '20px',
      },
    },

    [theme.breakpoints.down('sm')]: {
      padding: '0',
    },
  },
  containerColored: {
    '&::before': {
      backgroundImage: 'linear-gradient(to right, #080808, transparent)',
    },

    '&::after': {
      backgroundImage: 'linear-gradient(to left, #080808, transparent)',
    },

    [theme.breakpoints.down('xs')]: {
      '&::before': {
        backgroundImage: 'linear-gradient(to right, #0a0a0a, transparent)',
      },

      '&::after': {
        backgroundImage: 'linear-gradient(to left, #0a0a0a, transparent)',
      },
    },
  },
  inner: {
    alignItems: 'flex-start',
    display: 'flex',
    minHeight: '48px',
    overflow: 'scroll',
    zIndex: 0,

    [theme.breakpoints.down('sm')]: {
      '&::before, &::after': {
        content: '""',
        flexShrink: 0,
        height: '100%',
        width: '24px',
      },
    },

    [theme.breakpoints.down('xs')]: {
      '&::before, &::after': {
        content: '""',
        flexShrink: 0,
        height: '100%',
        width: '16px',
      },
    },
  },
  backShadow: {
    backgroundImage: `linear-gradient(
      to right,
      ${theme.palette.background.default},
      transparent
    )`,
    height: '100%',
    left: '22px',
    position: 'absolute',
    pointerEvents: 'none',
    top: '0',
    width: '40px',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  backShadowColored: {
    backgroundImage: 'linear-gradient(to right, #080808, transparent)',

    [theme.breakpoints.down('xs')]: {
      backgroundImage: 'linear-gradient(to right, #0a0a0a, transparent)',
    },
  },
  forwardShadow: {
    backgroundImage: `linear-gradient(
      to left,
      ${theme.palette.background.default},
      transparent
    )`,
    height: '100%',
    right: '22px',
    position: 'absolute',
    pointerEvents: 'none',
    top: '0',
    width: '40px',
    zIndex: 1,

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  forwardShadowColored: {
    backgroundImage: 'linear-gradient(to left, #080808, transparent)',

    [theme.breakpoints.down('xs')]: {
      backgroundImage: 'linear-gradient(to left, #0a0a0a, transparent)',
    },
  },
  backButton: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '50%',
    left: '8px',
    position: 'absolute',
    top: 'calc(50% - 24px)',
    zIndex: 2,

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  forwardButton: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '50%',
    position: 'absolute',
    right: '8px',
    top: 'calc(50% - 24px)',
    zIndex: 2,

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

interface Props {
  className?: string;
  colored?: boolean;
  id: string;
}

const ScrollContainer: React.FC<Props> = ({
  className,
  colored,
  id,
  children,
}) => {
  const classes = useStyles();

  const [shouldShowBackButton, setShouldShowBackButton] = React.useState(false);
  const [shouldShowForwardButton, setShouldShowForwardButton] = React.useState(
    true
  );

  const ref = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (!ref || !ref.current) {
      return;
    }

    setShouldShowForwardButton(
      ref.current.offsetWidth + ref.current.scrollLeft < ref.current.scrollWidth
    );
  }, [ref, children]);

  const onScroll = () => {
    if (!ref || !ref.current) {
      return;
    }

    if (ref.current.scrollLeft === 0) {
      setShouldShowBackButton(false);
    } else if (ref.current.scrollLeft > 0) {
      setShouldShowBackButton(true);
    }

    const scrolledWidth = ref.current.offsetWidth + ref.current.scrollLeft;
    if (scrolledWidth === ref.current.scrollWidth) {
      setShouldShowForwardButton(false);
    } else if (scrolledWidth < ref.current.scrollWidth) {
      setShouldShowForwardButton(true);
    }
  };

  React.useEffect(() => {
    if (ref && ref.current) {
      ref.current.addEventListener('scroll', onScroll, { passive: true });
    }

    return () => {
      if (ref && ref.current) {
        ref.current.removeEventListener('scroll', onScroll);
      }
    };
  }, [ref]);

  const onButtonClick = (toX: number) => () => {
    if (!ref || !ref.current) {
      return;
    }

    animateScroll.scrollTo(ref.current.scrollLeft + toX, {
      containerId: id,
      duration: 600,
      horizontal: true,
      smooth: true,
      ignoreCancelEvents: true,
    });
  };

  return (
    <MuiContainer
      className={
        colored
          ? `${classes.containerColored} ${classes.container}`
          : classes.container
      }
      classes={{ root: className }}
    >
      {shouldShowBackButton && (
        <>
          <div
            className={
              colored
                ? `${classes.backShadowColored} ${classes.backShadow}`
                : classes.backShadow
            }
          />
          <div className={classes.backButton}>
            <MuiIconButton color="inherit" onClick={onButtonClick(-800)}>
              <MuiArrowBackRoundedIcon />
            </MuiIconButton>
          </div>
        </>
      )}
      {shouldShowForwardButton && (
        <>
          <div
            className={
              colored
                ? `${classes.forwardShadowColored} ${classes.forwardShadow}`
                : classes.forwardShadow
            }
          />
          <div className={classes.forwardButton}>
            <MuiIconButton color="inherit" onClick={onButtonClick(800)}>
              <MuiArrowForwardRoundedIcon />
            </MuiIconButton>
          </div>
        </>
      )}
      <div className={classes.inner} id={id} ref={ref}>
        {children}
      </div>
    </MuiContainer>
  );
};

export default ScrollContainer;
