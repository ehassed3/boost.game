import { ObjectID } from 'bson';

const SYMBOLS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-'.split(
  ''
);

const toBase = (num: number, base: number) => {
  let decimal = num;
  let temp;
  let conversion = '';

  if (base > SYMBOLS.length || base <= 1) {
    throw new RangeError(
      `Radix must be less than ${SYMBOLS.length} and greater than 1`
    );
  }

  while (decimal > 0) {
    temp = Math.floor(decimal / base);
    conversion = SYMBOLS[decimal - base * temp] + conversion;
    decimal = temp;
  }

  return conversion;
};

const getShortMongoId = (id: string): string => {
  let str = '';

  if (!ObjectID.isValid(id)) {
    return str;
  }

  const objectId = new ObjectID(id);

  // creation date
  const date = objectId.getTimestamp();

  // time in milliseconds (with precision in seconds)
  let time = date.getTime();

  // hexadecimal counter converted to a decimal
  let counter = parseInt(objectId.toHexString().slice(-6), 16);

  // only use the last 3 digits of the counter to serve as our "milliseconds"
  counter = parseInt(counter.toString().slice(-3), 10);

  // add counter as our millisecond precision to our time
  time += counter;

  // convert to 64 base string (not strict base64)
  str = toBase(time, 64);

  // slice off the first, least variating, character
  // this lowers the entropy, but brings us to 6 characters, which is nice.
  // This will cause a roll-over once every two years, but the counter and the rest of the timestamp should make it unique (enough)
  str = str.slice(1);

  // reverse the string so that the first characters have the most variation
  str = str.split('').reverse().join('');

  return str;
};

export default getShortMongoId;
