import { IntlShape } from 'react-intl';

import getGameTitle from '@helpers/getGameTitle';
import getCurrencySign from '@helpers/getCurrencySign';

import SERVER from '@constants/server';
import STATUS from '@constants/status';
import THEME from '@constants/theme';

import { Order } from '@interfaces/order';
import { User } from '@interfaces/user';

interface Embed {
  color?: number;
  description?: string;
  image?: { url: string };
  title?: string;
  url?: string;
}

export const getMarketEmbed = (order: Order, user: User): Embed => {
  const { game, category, position, title } = order;
  const description = order.details
    ? order.details.reduce(
        (acc, detail) => `${acc}\n- ${detail}`,
        `Mongo ID ${order._id}\n\n${getGameTitle(game)}\n\n${title}\n`
      )
    : `Mongo ID ${order._id}\n\n${getGameTitle(game)}\n\n${title}`;

  const { name, discriminator, email } = user;
  const userDescription = `${name}#${discriminator}\n${order.user || email}`;

  const price = `${order.result.price.amount} ${getCurrencySign(
    order.result.price.currency
  )}`;
  const result = order.result.time ? `${price}, ${order.result.time}` : price;

  return {
    description: `${description}\n\n${userDescription}\n\n${result}`,
    image: {
      url: category
        ? `${SERVER}/static/${game}/${category}/${position}.jpg`
        : `${SERVER}/static/${game}/${position}.jpg`,
    },
    title: `#${order.shortId}`,
    url: `${SERVER}/market`,
  };
};

export const getOrderEmbed = (order: Order): Embed => {
  const { game, category, position, title } = order;
  const description = order.details
    ? order.details.reduce(
        (acc, detail) => `${acc}\n- ${detail}`,
        `${getGameTitle(game)}\n\n${title}\n`
      )
    : `${getGameTitle(game)}\n\n${title}`;

  return {
    description,
    image: {
      url: category
        ? `${SERVER}/static/${game}/${category}/${position}.jpg`
        : `${SERVER}/static/${game}/${position}.jpg`,
    },
    title: `#${order.shortId}`,
  };
};

export const getStatusUpdatedEmbed = (
  intl: IntlShape,
  status: STATUS
): Embed => {
  switch (status) {
    case STATUS.LIVE:
      return {
        color: parseInt(THEME.palette.secondary.main.substr(1), 16),
        description: intl.formatMessage({
          id: 'updatedOrder.statusLiveDescription',
        }),
        title: intl.formatMessage({ id: 'updatedOrder.statusLiveTitle' }),
      };
    case STATUS.PAUSED:
      return {
        color: parseInt(THEME.palette.warning.main.substr(1), 16),
        description: intl.formatMessage({
          id: 'updatedOrder.statusPausedDescription',
        }),
        title: intl.formatMessage({ id: 'updatedOrder.statusPausedTitle' }),
        url: `${SERVER}/account`,
      };
    case STATUS.CLOSED:
      return {
        color: parseInt(THEME.palette.success.main.substr(1), 16),
        description: intl.formatMessage({
          id: 'updatedOrder.statusClosedDescription',
        }),
        title: intl.formatMessage({ id: 'updatedOrder.statusClosedTitle' }),
        url: `${SERVER}/account`,
      };
    default:
      return {};
  }
};
