import GAMES from '@constants/games';

const isGamePage = (path: string, locale?: string): boolean => {
  const pages = path.split('/');
  const currentPage = pages[1] === locale ? pages[2] : pages[1];
  return !currentPage || GAMES.some((game) => game === currentPage);
};

export default isGamePage;
