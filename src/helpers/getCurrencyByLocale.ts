import { EUR, RUB } from '@constants/currencies';

import Currency from '@interfaces/currency';

const getCurrencyByLocale = (locale?: string): Currency => {
  switch (locale) {
    case 'en':
      return EUR;
    case 'de':
      return EUR;
    case 'ru':
      return RUB;
    default:
      return EUR;
  }
};

export default getCurrencyByLocale;
