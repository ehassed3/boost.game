import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import { Booster } from '@interfaces/user';

const getPositionBoosters = (
  boosters: Booster[],
  game: GAME,
  position: POSITION,
  category?: CATEGORY
): Booster[] =>
  boosters.filter((booster) =>
    category
      ? booster.games.some((boosterGame) => boosterGame === game) &&
        booster.categories.some(
          (boosterCategory) => boosterCategory === category
        ) &&
        booster.positions.some(
          (boosterPosition) => boosterPosition === position
        )
      : booster.games.some((boosterGame) => boosterGame === game) &&
        booster.positions.some(
          (boosterPosition) => boosterPosition === position
        )
  );

export default getPositionBoosters;
