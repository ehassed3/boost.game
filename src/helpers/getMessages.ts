const getMessages = (locale?: string): Record<string, string> => {
  try {
    return require(`../lang/${locale}.json`);
  } catch (error) {
    return require(`../lang/en.json`);
  }
};

export default getMessages;
