import { ROLE_ID } from '@constants/discord';
import { USER_STATUS_AMOUNT } from '@constants/userStatus';

import { User, Manager } from '@interfaces/user';

const getUserRole = (
  amount: number,
  admin?: User,
  manager?: Manager
): ROLE_ID => {
  if (admin) {
    return ROLE_ID.ADMIN;
  }

  if (manager) {
    return ROLE_ID.MANAGER;
  }

  if (amount > USER_STATUS_AMOUNT.SHADOW) {
    return ROLE_ID.SHADOW;
  }

  if (amount > USER_STATUS_AMOUNT.MASTER) {
    return ROLE_ID.MASTER;
  }

  return ROLE_ID.WARRIOR;
};

export default getUserRole;
