import {
  USER_STATUS,
  USER_STATUS_AMOUNT,
  USER_STATUS_PERCENT,
} from '@constants/userStatus';

import UserStatus from '@interfaces/userStatus';

const getUserStatus = (amount: number): UserStatus => {
  if (amount > USER_STATUS_AMOUNT.SHADOW) {
    return {
      amount,
      status: USER_STATUS.SHADOW,
      percent: USER_STATUS_PERCENT.SHADOW,
    };
  }

  if (amount > USER_STATUS_AMOUNT.MASTER) {
    return {
      amount,
      status: USER_STATUS.MASTER,
      percent: USER_STATUS_PERCENT.MASTER,
    };
  }

  return {
    amount,
    status: USER_STATUS.WARRIOR,
    percent: USER_STATUS_PERCENT.WARRIOR,
  };
};

export default getUserStatus;
