import { USD, EUR, RUB } from '@constants/currencies';

import Price from '@interfaces/price';

const getUSDAmount = (price: Price): number => {
  switch (price.currency) {
    case 'USD':
      return Math.round(price.amount / USD.multiplier);
    case 'EUR':
      return Math.round(price.amount / EUR.multiplier);
    case 'RUB':
      return Math.round(price.amount / RUB.multiplier);
    default:
      return Math.round(price.amount / USD.multiplier);
  }
};

export default getUSDAmount;
