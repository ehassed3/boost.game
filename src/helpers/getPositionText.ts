import { IntlShape } from 'react-intl';

import GAME from '@constants/game';
import POSITION from '@constants/position';

const getPositionText = (
  game: GAME,
  position: POSITION,
  intl: IntlShape
): string => {
  switch (game) {
    case GAME.POE:
      switch (position) {
        case POSITION.POWER_LEVELING:
          return intl.formatMessage({ id: 'poe.powerLevelingPosition' });
        default:
          return '';
      }
    case GAME.WOW:
      switch (position) {
        case POSITION.POWER_LEVELING:
          return intl.formatMessage({ id: 'wow.powerLevelingPosition' });
        case POSITION.COVENANT:
          return intl.formatMessage({ id: 'wow.covenantPosition' });
        case POSITION.ARENA:
          return intl.formatMessage({ id: 'wow.arenaPosition' });
        case POSITION.HONOR:
          return intl.formatMessage({ id: 'wow.honorPosition' });
        case POSITION.CONQUEST_CAP:
          return intl.formatMessage({ id: 'wow.conquestCapPosition' });
        case POSITION.RAIDS:
          return intl.formatMessage({ id: 'wow.raidsPosition' });
        case POSITION.MYTHIC_DUNGEONS:
          return intl.formatMessage({ id: 'wow.mythicDungeonsPosition' });
        case POSITION.MYTHIC_8_DUNGEONS:
          return intl.formatMessage({ id: 'wow.mythic8DungeonsPosition' });
        case POSITION.TORGHAST_TOWER_LAYER:
          return intl.formatMessage({ id: 'wow.torghastTowerLayerPosition' });
        case POSITION.SOUL_ASH:
          return intl.formatMessage({ id: 'wow.soulAshPosition' });
        case POSITION.HERO:
          return intl.formatMessage({ id: 'wow.heroPosition' });
        default:
          return '';
      }
    case GAME.WOW_CLASSIC:
      switch (position) {
        case POSITION.POWER_LEVELING:
          return intl.formatMessage({ id: 'wowClassic.powerLevelingPosition' });
        case POSITION.HONOR:
          return intl.formatMessage({ id: 'wowClassic.honorPosition' });
        case POSITION.RANK:
          return intl.formatMessage({ id: 'wowClassic.rankPosition' });
        default:
          return '';
      }
    case GAME.DOTA_2:
      switch (position) {
        case POSITION.BOOST:
          return intl.formatMessage({ id: 'dota2.boostPosition' });
        case POSITION.CALIBRATION:
          return intl.formatMessage({ id: 'dota2.calibrationPosition' });
        case POSITION.LOW_PRIORITY:
          return intl.formatMessage({ id: 'dota2.lowPriorityPosition' });
        default:
          return '';
      }
    default:
      return '';
  }
};

export default getPositionText;
