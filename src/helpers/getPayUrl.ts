import { sha256 } from 'js-sha256';

import getGameTitle from '@helpers/getGameTitle';

import { InsertedRawOrder } from '@interfaces/order';

const getPayLocale = (order: InsertedRawOrder): string => {
  if (order.locale === 'ru') {
    return order.locale;
  }

  return 'en';
};

const getPayUrl = (order: InsertedRawOrder): string => {
  const { _id, game, title, user } = order;
  const { amount, currency } = order.result.price;

  const desc = `${getGameTitle(game)}. ${title}.`;
  const locale = getPayLocale(order);

  const publicKey = process.env.UNITPAY_PUBLIC_KEY;
  const secretKey = process.env.UNITPAY_SECRET_KEY;

  const signature = sha256(
    `${_id}{up}${currency}{up}${desc}{up}${amount}{up}${secretKey}`
  );

  const cashItems = Buffer.from(
    JSON.stringify({
      name: desc,
      count: 1,
      price: amount,
      nds: 'none',
      type: 'service',
      paymentMethod: 'full_prepayment',
    })
  ).toString('base64');

  return encodeURI(
    `https://unitpay.ru/pay/${publicKey}/card?account=${_id}&currency=${currency}&desc=${desc}&sum=${amount}&signature=${signature}&locale=${locale}&customerEmail=${user}&cashItems=${cashItems}`
  );
};

export default getPayUrl;
