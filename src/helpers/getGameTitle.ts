import GAME from '@constants/game';

const getGameTitle = (game?: GAME): string => {
  switch (game) {
    case GAME.POE:
      return 'Path of Exile';
    case GAME.WOW:
      return 'World of Warcraft: Shadowlands';
    case GAME.WOW_CLASSIC:
      return 'World of Warcraft: Classic';
    case GAME.DOTA_2:
      return 'Dota 2';
    default:
      return '';
  }
};

export default getGameTitle;
