import { createIntl, createIntlCache, IntlShape } from 'react-intl';

import getMessages from '@helpers/getMessages';

const intlCache = createIntlCache();

const createServerIntl = (locale: string): IntlShape => {
  return createIntl({ locale, messages: getMessages(locale) }, intlCache);
};

export default createServerIntl;
