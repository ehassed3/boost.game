import cookie from 'cookie';

const getValueFromCookie = (cookieName: string): string => {
  let item = '';

  if (process.browser) {
    const parsedCookies = cookie.parse(document.cookie);
    item = parsedCookies[cookieName] || '';
  }

  return item;
};

export default getValueFromCookie;
