import { IntlShape } from 'react-intl';

import { WowSoulAshFormValue } from '@interfaces/formValue';

export const getWowSoulAshDetails = (
  intl: IntlShape,
  formValue: WowSoulAshFormValue
): string[] => {
  const details = [];

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  return details;
};

export default getWowSoulAshDetails;
