import { IntlShape } from 'react-intl';

import { WowRaidsFormValue } from '@interfaces/formValue';

export const getWowRaidsDetails = (
  intl: IntlShape,
  formValue: WowRaidsFormValue
): string[] => {
  const details = [formValue.fraction.label];

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  if (formValue.traders) {
    formValue.traders.forEach((trader) => {
      details.push(
        intl.formatMessage({ id: 'wow.traderTitle' }, { title: trader.label })
      );
    });
  }

  return details;
};

export default getWowRaidsDetails;
