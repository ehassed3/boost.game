import { IntlShape } from 'react-intl';

import { WowClassicPowerLevelingFormValue } from '@interfaces/formValue';

export const getWowClassicPowerLevelingDetails = (
  intl: IntlShape,
  formValue: WowClassicPowerLevelingFormValue
): string[] => {
  const details = [];

  if (formValue.professions) {
    formValue.professions.forEach((profession) => {
      details.push(
        intl.formatMessage(
          { id: 'wowClassic.professionTitle' },
          { title: profession.title }
        )
      );
    });
  }

  if (formValue.mount) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.mountCheckboxLabel',
      })
    );
  }

  if (formValue.fast) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.fastCheckboxLabel',
      })
    );
  }

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.streamCheckboxLabel',
      })
    );
  }

  return details;
};

export default getWowClassicPowerLevelingDetails;
