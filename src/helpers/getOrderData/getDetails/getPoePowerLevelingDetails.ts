import { IntlShape } from 'react-intl';

import { PoePowerLevelingFormValue } from '@interfaces/formValue';

export const getPoePowerLevelingDetails = (
  intl: IntlShape,
  formValue: PoePowerLevelingFormValue
): string[] => {
  const details = [];

  if (formValue.stream) {
    details.push(intl.formatMessage({ id: 'poe.streamCheckboxLabel' }));
  }

  return details;
};

export default getPoePowerLevelingDetails;
