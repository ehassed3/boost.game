import { IntlShape } from 'react-intl';

import { WowPowerLevelingFormValue } from '@interfaces/formValue';

export const getWowPowerLevelingDetails = (
  intl: IntlShape,
  formValue: WowPowerLevelingFormValue
): string[] => {
  const details = [];

  if (formValue.fast) {
    details.push(intl.formatMessage({ id: 'wow.fastCheckboxLabel' }));
  }

  if (formValue.stream) {
    details.push(intl.formatMessage({ id: 'wow.streamCheckboxLabel' }));
  }

  if (formValue.torghast) {
    details.push(intl.formatMessage({ id: 'wow.torghastCheckboxLabel' }));
  }

  return details;
};

export default getWowPowerLevelingDetails;
