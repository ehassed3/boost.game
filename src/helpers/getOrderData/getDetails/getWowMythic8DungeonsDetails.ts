import { IntlShape } from 'react-intl';

import { WowMythic8DungeonsFormValue } from '@interfaces/formValue';

export const getWowMythic8DungeonsDetails = (
  intl: IntlShape,
  formValue: WowMythic8DungeonsFormValue
): string[] => {
  const details = [formValue.fraction.label];

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  formValue.traders.forEach((trader) => {
    details.push(
      intl.formatMessage({ id: 'wow.traderTitle' }, { title: trader.label })
    );
  });

  return details;
};

export default getWowMythic8DungeonsDetails;
