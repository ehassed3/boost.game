import { IntlShape } from 'react-intl';

import { WowCovenantFormValue } from '@interfaces/formValue';

export const getWowCovenantDetails = (
  intl: IntlShape,
  formValue: WowCovenantFormValue
): string[] => {
  const details = [];

  if (formValue.stream) {
    details.push(intl.formatMessage({ id: 'wow.streamCheckboxLabel' }));
  }

  return details;
};

export default getWowCovenantDetails;
