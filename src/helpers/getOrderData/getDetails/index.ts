import { IntlShape } from 'react-intl';

import getPoePowerLevelingDetails from '@helpers/getOrderData/getDetails/getPoePowerLevelingDetails';

import getWowPowerLevelingDetails from '@helpers/getOrderData/getDetails/getWowPowerLevelingDetails';
import getWowCovenantDetails from '@helpers/getOrderData/getDetails/getWowCovenantDetails';
import getWowArenaDetails from '@helpers/getOrderData/getDetails/getWowArenaDetails';
import getWowHonorDetails from '@helpers/getOrderData/getDetails/getWowHonorDetails';
import getWowConquestCapDetails from '@helpers/getOrderData/getDetails/getWowConquestCapDetails';
import getWowRaidsDetails from '@helpers/getOrderData/getDetails/getWowRaidsDetails';
import getWowMythicDungeonsDetails from '@helpers/getOrderData/getDetails/getWowMythicDungeonsDetails';
import getWowMythic8DungeonsDetails from '@helpers/getOrderData/getDetails/getWowMythic8DungeonsDetails';
import getWowTorghastTowerLayerDetails from '@helpers/getOrderData/getDetails/getWowTorghastTowerLayerDetails';
import getWowSoulAshDetails from '@helpers/getOrderData/getDetails/getWowSoulAshDetails';
import getWowHeroDetails from '@helpers/getOrderData/getDetails/getWowHeroDetails';

import getWowClassicPowerLevelingDetails from '@helpers/getOrderData/getDetails/getWowClassicPowerLevelingDetails';
import getWowClassicHonorDetails from '@helpers/getOrderData/getDetails/getWowClassicHonorDetails';
import getWowClassicRankDetails from '@helpers/getOrderData/getDetails/getWowClassicRankDetails';

import getDota2BoostDetails from '@helpers/getOrderData/getDetails/getDota2BoostDetails';
import getDota2CalibrationDetails from '@helpers/getOrderData/getDetails/getDota2CalibrationDetails';
import getDota2LowPriorityDetails from '@helpers/getOrderData/getDetails/getDota2LowPriorityDetails';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import { DirtyOrder } from '@interfaces/order';
import {
  PoePowerLevelingFormValue,
  WowPowerLevelingFormValue,
  WowCovenantFormValue,
  WowArenaFormValue,
  WowHonorFormValue,
  WowConquestCapFormValue,
  WowRaidsFormValue,
  WowMythicDungeonsFormValue,
  WowMythic8DungeonsFormValue,
  WowTorghastTowerLayerFormValue,
  WowSoulAshFormValue,
  WowClassicPowerLevelingFormValue,
  WowClassicHonorFormValue,
  WowClassicRankFormValue,
  Dota2BoostFormValue,
  Dota2CalibrationFormValue,
  Dota2LowPriorityFormValue,
} from '@interfaces/formValue';

const getDetails = (intl: IntlShape, order: DirtyOrder): string[] => {
  switch (order.game) {
    case GAME.POE:
      switch (order.position) {
        case POSITION.POWER_LEVELING:
          return getPoePowerLevelingDetails(
            intl,
            order.formValue as PoePowerLevelingFormValue
          );
        default:
          return [];
      }
    case GAME.WOW:
      switch (order.category) {
        case CATEGORY.CHARACTER_BOOST:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowPowerLevelingDetails(
                intl,
                order.formValue as WowPowerLevelingFormValue
              );
            case POSITION.COVENANT:
              return getWowCovenantDetails(
                intl,
                order.formValue as WowCovenantFormValue
              );
            default:
              return [];
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.ARENA:
              return getWowArenaDetails(
                intl,
                order.formValue as WowArenaFormValue
              );
            case POSITION.HONOR:
              return getWowHonorDetails(
                intl,
                order.formValue as WowHonorFormValue
              );
            case POSITION.CONQUEST_CAP:
              return getWowConquestCapDetails(
                intl,
                order.formValue as WowConquestCapFormValue
              );
            default:
              return [];
          }
        case CATEGORY.DUNGEONS_AND_RAIDS:
          switch (order.position) {
            case POSITION.RAIDS:
              return getWowRaidsDetails(
                intl,
                order.formValue as WowRaidsFormValue
              );
            case POSITION.MYTHIC_DUNGEONS:
              return getWowMythicDungeonsDetails(
                intl,
                order.formValue as WowMythicDungeonsFormValue
              );
            case POSITION.MYTHIC_8_DUNGEONS:
              return getWowMythic8DungeonsDetails(
                intl,
                order.formValue as WowMythic8DungeonsFormValue
              );
            default:
              return [];
          }
        case CATEGORY.TORGHAST_AND_SOUL_ASH:
          switch (order.position) {
            case POSITION.TORGHAST_TOWER_LAYER:
              return getWowTorghastTowerLayerDetails(
                intl,
                order.formValue as WowTorghastTowerLayerFormValue
              );
            case POSITION.SOUL_ASH:
              return getWowSoulAshDetails(
                intl,
                order.formValue as WowSoulAshFormValue
              );
            default:
              return [];
          }
        case CATEGORY.ACHIEVEMENTS:
          switch (order.position) {
            case POSITION.HERO:
              return getWowHeroDetails();
            default:
              return [];
          }
        default:
          return [];
      }
    case GAME.WOW_CLASSIC:
      switch (order.category) {
        case CATEGORY.POWER_LEVELING:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowClassicPowerLevelingDetails(
                intl,
                order.formValue as WowClassicPowerLevelingFormValue
              );
            default:
              return [];
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.HONOR:
              return getWowClassicHonorDetails(
                intl,
                order.formValue as WowClassicHonorFormValue
              );
            case POSITION.RANK:
              return getWowClassicRankDetails(
                intl,
                order.formValue as WowClassicRankFormValue
              );
            default:
              return [];
          }
        default:
          return [];
      }
    case GAME.DOTA_2:
      switch (order.position) {
        case POSITION.BOOST:
          return getDota2BoostDetails(
            intl,
            order.formValue as Dota2BoostFormValue
          );
        case POSITION.CALIBRATION:
          return getDota2CalibrationDetails(
            intl,
            order.formValue as Dota2CalibrationFormValue
          );
        case POSITION.LOW_PRIORITY:
          return getDota2LowPriorityDetails(
            intl,
            order.formValue as Dota2LowPriorityFormValue
          );
        default:
          return [];
      }
    default:
      return [];
  }
};

export default getDetails;
