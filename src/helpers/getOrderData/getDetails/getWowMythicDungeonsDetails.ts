import { IntlShape } from 'react-intl';

import { WowMythicDungeonsFormValue } from '@interfaces/formValue';

export const getWowMythicDungeonsDetails = (
  intl: IntlShape,
  formValue: WowMythicDungeonsFormValue
): string[] => {
  const details = [formValue.fraction.label];

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  if (formValue.timer) {
    details.push(intl.formatMessage({ id: 'wow.timerCheckboxLabel' }));
  }

  if (formValue.specific) {
    details.push(intl.formatMessage({ id: 'wow.specificCheckboxLabel' }));
  }

  if (formValue.traders) {
    formValue.traders.forEach((trader) => {
      details.push(
        intl.formatMessage({ id: 'wow.traderTitle' }, { title: trader.label })
      );
    });
  }

  return details;
};

export default getWowMythicDungeonsDetails;
