import { IntlShape } from 'react-intl';

import { WowClassicRankFormValue } from '@interfaces/formValue';

export const getWowClassicRankDetails = (
  intl: IntlShape,
  formValue: WowClassicRankFormValue
): string[] => {
  const details = [];

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.streamCheckboxLabel',
      })
    );
  }

  return details;
};

export default getWowClassicRankDetails;
