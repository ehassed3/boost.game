import { IntlShape } from 'react-intl';

import { Dota2BoostFormValue } from '@interfaces/formValue';

export const getDota2BoostDetails = (
  intl: IntlShape,
  formValue: Dota2BoostFormValue
): string[] => {
  const details = [];

  if (formValue.heroes) {
    details.push(
      intl.formatMessage({
        id: 'dota2.heroesCheckboxLabel',
      })
    );
  }

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'dota2.streamCheckboxLabel',
      })
    );
  }

  if (formValue.booster) {
    details.push(
      intl.formatMessage({
        id: 'dota2.boosterCheckboxLabel',
      })
    );
  }

  return details;
};

export default getDota2BoostDetails;
