import { IntlShape } from 'react-intl';

import { WowArenaFormValue } from '@interfaces/formValue';

export const getWowArenaDetails = (
  intl: IntlShape,
  formValue: WowArenaFormValue
): string[] => {
  const details = [];

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  return details;
};

export default getWowArenaDetails;
