import { IntlShape } from 'react-intl';

import { WowConquestCapFormValue } from '@interfaces/formValue';

export const getWowConquestCapDetails = (
  intl: IntlShape,
  formValue: WowConquestCapFormValue
): string[] => {
  const details = [];

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'wow.streamCheckboxLabel',
      })
    );
  }

  return details;
};

export default getWowConquestCapDetails;
