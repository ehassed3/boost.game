import { IntlShape } from 'react-intl';

import { WowClassicHonorFormValue } from '@interfaces/formValue';

export const getWowClassicHonorDetails = (
  intl: IntlShape,
  formValue: WowClassicHonorFormValue
): string[] => {
  const details = [];

  if (formValue.isSwitched) {
    if (formValue.stream) {
      details.push(
        intl.formatMessage({
          id: 'wowClassic.streamCheckboxLabel',
        })
      );
    }

    return details;
  }

  if (formValue.fast) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.fastCheckboxLabel',
      })
    );
  }

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'wowClassic.streamCheckboxLabel',
      })
    );
  }

  return details;
};

export default getWowClassicHonorDetails;
