import { IntlShape } from 'react-intl';

import { WowHonorFormValue } from '@interfaces/formValue';

export const getWowHonorDetails = (
  intl: IntlShape,
  formValue: WowHonorFormValue
): string[] => {
  const details = [];

  if (formValue.stream) {
    details.push(
      intl.formatMessage({
        id: 'wow.streamCheckboxLabel',
      })
    );
  }

  return details;
};

export default getWowHonorDetails;
