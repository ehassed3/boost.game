import { IntlShape } from 'react-intl';

import { WowTorghastTowerLayerFormValue } from '@interfaces/formValue';

export const getWowTorghastTowerLayerDetails = (
  intl: IntlShape,
  formValue: WowTorghastTowerLayerFormValue
): string[] => {
  const details = [];

  if (formValue.unlock) {
    details.push(intl.formatMessage({ id: 'wow.unlockLayerCheckboxLabel' }));
  }

  if (formValue.selfplay) {
    details.push(intl.formatMessage({ id: 'wow.selfplayCheckboxLabel' }));
  }

  if (formValue.torghast) {
    details.push(intl.formatMessage({ id: 'wow.torghastCheckboxLabel' }));
  }

  return details;
};

export default getWowTorghastTowerLayerDetails;
