import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowClassicRankFormValue } from '@interfaces/formValue';

export const MENU: Record<number, Result> = {
  0: {
    amount: 0,
    hours: 0,
  },
  1: {
    amount: 20,
    hours: 40,
  },
  2: {
    amount: 40,
    hours: 40,
  },
  3: {
    amount: 60,
    hours: 40,
  },
  4: {
    amount: 70,
    hours: 40,
  },
  5: {
    amount: 80,
    hours: 40,
  },
  6: {
    amount: 160,
    hours: 40,
  },
  7: {
    amount: 180,
    hours: 40,
  },
  8: {
    amount: 220,
    hours: 40,
  },
  9: {
    amount: 240,
    hours: 40,
  },
  10: {
    amount: 280,
    hours: 80,
  },
  11: {
    amount: 370,
    hours: 80,
  },
  12: {
    amount: 420,
    hours: 120,
  },
  13: {
    amount: 460,
    hours: 160,
  },
  14: {
    amount: 540,
    hours: 160,
  },
};

export const getWowClassicRankResult = (
  formValue: WowClassicRankFormValue
): Result => {
  if (formValue.ranks[0] === formValue.ranks[1]) {
    return { amount: 0, hours: 0 };
  }

  const rankRange = range(formValue.ranks[0] + 1, formValue.ranks[1] + 1, 1);

  let amount = rankRange.reduce((acc, rank) => acc + MENU[rank].amount, 0);

  if (formValue.stream) {
    amount += amount * 0.2;
  }

  return { discountPercent: 10, amount, hours: 0 };
};

export default getWowClassicRankResult;
