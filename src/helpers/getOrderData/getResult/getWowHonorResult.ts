import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowHonorFormValue } from '@interfaces/formValue';

const MENU: Record<number, Result> = {
  0: {
    amount: 0,
    hours: 0,
  },
  5: {
    amount: 10,
    hours: 4,
  },
  10: {
    amount: 10,
    hours: 4,
  },
  15: {
    amount: 10,
    hours: 4,
  },
  20: {
    amount: 10,
    hours: 4,
  },
  25: {
    amount: 10,
    hours: 4,
  },
  30: {
    amount: 10,
    hours: 4,
  },
  35: {
    amount: 10,
    hours: 4,
  },
  40: {
    amount: 10,
    hours: 4,
  },
  45: {
    amount: 10,
    hours: 4,
  },
  50: {
    amount: 10,
    hours: 4,
  },
};

export const getWowHonorResult = (formValue: WowHonorFormValue): Result => {
  const honorRange = range(0, formValue.honor + 5, 5);

  const result = honorRange.reduce(
    (acc, honor) => ({
      amount: acc.amount + MENU[honor].amount,
      hours: acc.hours + MENU[honor].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  return {
    discountPercent: 10,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getWowHonorResult;
