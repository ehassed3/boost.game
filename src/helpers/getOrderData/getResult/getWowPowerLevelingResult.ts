import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowPowerLevelingFormValue } from '@interfaces/formValue';

const MENU: Record<number, Result> = {
  1: {
    amount: 0,
    hours: 0,
  },
  2: {
    amount: 0.74,
    hours: 0.48,
  },
  3: {
    amount: 0.74,
    hours: 0.48,
  },
  4: {
    amount: 0.74,
    hours: 0.48,
  },
  5: {
    amount: 0.74,
    hours: 0.48,
  },
  6: {
    amount: 0.74,
    hours: 0.48,
  },
  7: {
    amount: 0.74,
    hours: 0.48,
  },
  8: {
    amount: 0.74,
    hours: 0.48,
  },
  9: {
    amount: 0.74,
    hours: 0.48,
  },
  10: {
    amount: 0.74,
    hours: 0.48,
  },
  11: {
    amount: 0.74,
    hours: 0.48,
  },
  12: {
    amount: 0.74,
    hours: 0.48,
  },
  13: {
    amount: 0.74,
    hours: 0.48,
  },
  14: {
    amount: 0.74,
    hours: 0.48,
  },
  15: {
    amount: 0.74,
    hours: 0.48,
  },
  16: {
    amount: 0.74,
    hours: 0.48,
  },
  17: {
    amount: 0.74,
    hours: 0.48,
  },
  18: {
    amount: 0.74,
    hours: 0.48,
  },
  19: {
    amount: 0.74,
    hours: 0.48,
  },
  20: {
    amount: 0.74,
    hours: 0.48,
  },
  21: {
    amount: 0.74,
    hours: 0.48,
  },
  22: {
    amount: 0.74,
    hours: 0.48,
  },
  23: {
    amount: 0.74,
    hours: 0.48,
  },
  24: {
    amount: 0.74,
    hours: 0.48,
  },
  25: {
    amount: 0.74,
    hours: 0.48,
  },
  26: {
    amount: 0.74,
    hours: 0.48,
  },
  27: {
    amount: 0.74,
    hours: 0.48,
  },
  28: {
    amount: 0.74,
    hours: 0.48,
  },
  29: {
    amount: 0.74,
    hours: 0.48,
  },
  30: {
    amount: 0.74,
    hours: 0.48,
  },
  31: {
    amount: 0.74,
    hours: 0.48,
  },
  32: {
    amount: 0.74,
    hours: 0.48,
  },
  33: {
    amount: 0.74,
    hours: 0.48,
  },
  34: {
    amount: 0.74,
    hours: 0.48,
  },
  35: {
    amount: 0.74,
    hours: 0.48,
  },
  36: {
    amount: 0.74,
    hours: 0.48,
  },
  37: {
    amount: 0.74,
    hours: 0.48,
  },
  38: {
    amount: 0.74,
    hours: 0.48,
  },
  39: {
    amount: 0.74,
    hours: 0.48,
  },
  40: {
    amount: 0.74,
    hours: 0.48,
  },
  41: {
    amount: 0.74,
    hours: 0.48,
  },
  42: {
    amount: 0.74,
    hours: 0.48,
  },
  43: {
    amount: 0.74,
    hours: 0.48,
  },
  44: {
    amount: 0.74,
    hours: 0.48,
  },
  45: {
    amount: 0.74,
    hours: 0.48,
  },
  46: {
    amount: 0.74,
    hours: 0.48,
  },
  47: {
    amount: 0.74,
    hours: 0.48,
  },
  48: {
    amount: 0.74,
    hours: 0.48,
  },
  49: {
    amount: 0.74,
    hours: 0.48,
  },
  50: {
    amount: 0.74,
    hours: 0.48,
  },
  51: {
    amount: 3.5,
    hours: 2.4,
  },
  52: {
    amount: 3.5,
    hours: 2.4,
  },
  53: {
    amount: 3.5,
    hours: 2.4,
  },
  54: {
    amount: 3.5,
    hours: 2.4,
  },
  55: {
    amount: 3.5,
    hours: 2.4,
  },
  56: {
    amount: 3.5,
    hours: 2.4,
  },
  57: {
    amount: 3.5,
    hours: 2.4,
  },
  58: {
    amount: 3.5,
    hours: 2.4,
  },
  59: {
    amount: 3.5,
    hours: 2.4,
  },
  60: {
    amount: 3.5,
    hours: 2.4,
  },
};

const MENU_GEAR: Record<number, Result> = {
  170: {
    amount: 55,
    hours: 0,
  },
  171: {
    amount: 3,
    hours: 0,
  },
  172: {
    amount: 3,
    hours: 0,
  },
  173: {
    amount: 3,
    hours: 0,
  },
  174: {
    amount: 3,
    hours: 0,
  },
  175: {
    amount: 3,
    hours: 0,
  },
  176: {
    amount: 3,
    hours: 0,
  },
  177: {
    amount: 3,
    hours: 0,
  },
  178: {
    amount: 3,
    hours: 0,
  },
  179: {
    amount: 3,
    hours: 0,
  },
  180: {
    amount: 3.7,
    hours: 0,
  },
  181: {
    amount: 3.7,
    hours: 0,
  },
  182: {
    amount: 3.7,
    hours: 0,
  },
  183: {
    amount: 3.7,
    hours: 0,
  },
  184: {
    amount: 3.7,
    hours: 0,
  },
  185: {
    amount: 3.7,
    hours: 0,
  },
  186: {
    amount: 3.7,
    hours: 0,
  },
  187: {
    amount: 3.7,
    hours: 0,
  },
  188: {
    amount: 3.7,
    hours: 0,
  },
  189: {
    amount: 3.7,
    hours: 0,
  },
  190: {
    amount: 3.7,
    hours: 0,
  },
  191: {
    amount: 5.7,
    hours: 0,
  },
  192: {
    amount: 5.7,
    hours: 0,
  },
  193: {
    amount: 5.7,
    hours: 0,
  },
  194: {
    amount: 5.7,
    hours: 0,
  },
  195: {
    amount: 5.7,
    hours: 0,
  },
  196: {
    amount: 5.7,
    hours: 0,
  },
  197: {
    amount: 5.7,
    hours: 0,
  },
  198: {
    amount: 5.7,
    hours: 0,
  },
  199: {
    amount: 5.7,
    hours: 0,
  },
  200: {
    amount: 5.7,
    hours: 0,
  },
  201: {
    amount: 10,
    hours: 0,
  },
  202: {
    amount: 10,
    hours: 0,
  },
  203: {
    amount: 10,
    hours: 0,
  },
  204: {
    amount: 10,
    hours: 0,
  },
  205: {
    amount: 10,
    hours: 0,
  },
  206: {
    amount: 10,
    hours: 0,
  },
  207: {
    amount: 10,
    hours: 0,
  },
  208: {
    amount: 10,
    hours: 0,
  },
  209: {
    amount: 10,
    hours: 0,
  },
  210: {
    amount: 10,
    hours: 0,
  },
};

export const getWowPowerLevelingResult = (
  formValue: WowPowerLevelingFormValue
): Result => {
  if (!formValue.isGear && formValue.levels[0] === formValue.levels[1]) {
    return {
      discountPercent: 0,
      amount: 0,
      hours: 0,
    };
  }

  const levelRange = range(formValue.levels[0] + 1, formValue.levels[1] + 1, 1);

  const result = levelRange.reduce(
    (acc, level) => ({
      amount: acc.amount + MENU[level].amount,
      hours: acc.hours + MENU[level].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.isGear) {
    const gearRange = range(170, formValue.gear + 1, 1);

    gearRange.forEach((gear) => {
      result.amount += MENU_GEAR[gear].amount;
    });
    result.hours = 0;
  }

  if (formValue.torghast) {
    result.amount += 3;
  }

  if (formValue.fast) {
    result.amount += result.amount * 0.3;
    result.hours -= result.hours * 0.5;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  return {
    discountPercent: 20,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getWowPowerLevelingResult;
