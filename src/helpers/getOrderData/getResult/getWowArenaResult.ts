import find from 'lodash/find';

import { Result } from '@interfaces/result';
import { WowArenaFormValue } from '@interfaces/formValue';

const MENU: Record<'2vs2' | '3vs3', Record<number, Result>> = {
  '2vs2': {
    0: {
      amount: 0,
      hours: 0,
    },
    1400: {
      amount: 55,
      hours: 0,
    },
    1600: {
      amount: 80,
      hours: 0,
    },
    1800: {
      amount: 137,
      hours: 0,
    },
    2100: {
      amount: 330,
      hours: 0,
    },
    2400: {
      amount: Infinity,
      hours: 0,
    },
  },
  '3vs3': {
    0: {
      amount: 0,
      hours: 0,
    },
    1400: {
      amount: 80,
      hours: 0,
    },
    1600: {
      amount: 105,
      hours: 0,
    },
    1800: {
      amount: 165,
      hours: 0,
    },
    2100: {
      amount: 384,
      hours: 0,
    },
    2400: {
      amount: Infinity,
      hours: 0,
    },
  },
};

export const getWowArenaResult = (formValue: WowArenaFormValue): Result => {
  const menu = MENU[formValue.player.name];
  let rating = menu[formValue.rating];

  if (!rating) {
    rating = find(menu, (_, key) => Number(key) > formValue.rating) || menu[0];
  }

  let { amount } = rating;

  if (formValue.selfplay) {
    amount += amount * 0.1;
  }

  return {
    discountPercent: 15,
    amount,
    hours: 0,
  };
};

export default getWowArenaResult;
