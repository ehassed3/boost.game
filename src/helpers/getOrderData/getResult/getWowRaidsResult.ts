import find from 'lodash/find';

import { Result } from '@interfaces/result';
import { WowRaidsFormValue } from '@interfaces/formValue';

const MENU: Record<'normal' | 'heroic' | 'mythic', Record<number, Result>> = {
  normal: {
    3: {
      amount: 27,
      hours: 0,
    },
    7: {
      amount: 28,
      hours: 0,
    },
    10: {
      amount: 30,
      hours: 0,
    },
  },
  heroic: {
    3: {
      amount: 29,
      hours: 0,
    },
    7: {
      amount: 39,
      hours: 0,
    },
    10: {
      amount: 48,
      hours: 0,
    },
  },
  mythic: {
    3: {
      amount: 175,
      hours: 0,
    },
    7: {
      amount: 599,
      hours: 0,
    },
    10: {
      amount: Infinity,
      hours: 0,
    },
  },
};

export const getWowRaidsResult = (formValue: WowRaidsFormValue): Result => {
  const menu = MENU[formValue.difficulty.name];
  let raids = menu[formValue.raids];

  if (!raids) {
    raids = find(menu, (_, key) => Number(key) > formValue.raids) || menu[0];
  }

  let { amount } = raids;

  if (formValue.traders) {
    formValue.traders.forEach((trader) => {
      amount += trader.amount;
    });
  }

  return { discountPercent: 10, amount, hours: 0 };
};

export default getWowRaidsResult;
