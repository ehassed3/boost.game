import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowTorghastTowerLayerFormValue } from '@interfaces/formValue';

const MENU: Record<number, Result> = {
  1: {
    amount: 3,
    hours: 0,
  },
  2: {
    amount: 4,
    hours: 0,
  },
  3: {
    amount: 5,
    hours: 0,
  },
  4: {
    amount: 6,
    hours: 0,
  },
  5: {
    amount: 7,
    hours: 0,
  },
  6: {
    amount: 9,
    hours: 0,
  },
  7: {
    amount: 12,
    hours: 0,
  },
  8: {
    amount: 16,
    hours: 0,
  },
};

export const getWowTorghastTowerLayerResult = (
  formValue: WowTorghastTowerLayerFormValue
): Result => {
  const layerRange = range(1, formValue.layer + 1, 1);

  let amount = layerRange.reduce((acc, layer) => acc + MENU[layer].amount, 0);

  if (formValue.torghast) {
    amount += 3;
  }

  if (formValue.selfplay) {
    amount += amount * 0.1;
  }

  if (formValue.unlock) {
    amount += amount;
  }

  return {
    discountPercent: 10,
    amount: amount * formValue.wings.multiplier,
    hours: 0,
  };
};

export default getWowTorghastTowerLayerResult;
