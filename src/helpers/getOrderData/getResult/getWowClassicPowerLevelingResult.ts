import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowClassicPowerLevelingFormValue } from '@interfaces/formValue';

export const MENU: Record<number, Result> = {
  1: {
    amount: 0,
    hours: 0,
  },
  2: {
    amount: 1,
    hours: 1,
  },
  3: {
    amount: 1,
    hours: 1,
  },
  4: {
    amount: 1,
    hours: 1,
  },
  5: {
    amount: 1,
    hours: 1,
  },
  6: {
    amount: 1,
    hours: 1,
  },
  7: {
    amount: 1,
    hours: 1,
  },
  8: {
    amount: 1,
    hours: 1,
  },
  9: {
    amount: 1,
    hours: 1,
  },
  10: {
    amount: 1,
    hours: 1,
  },
  11: {
    amount: 1.5,
    hours: 1.5,
  },
  12: {
    amount: 1.5,
    hours: 1.5,
  },
  13: {
    amount: 1.5,
    hours: 1.5,
  },
  14: {
    amount: 1.5,
    hours: 1.5,
  },
  15: {
    amount: 1.5,
    hours: 1.5,
  },
  16: {
    amount: 1.5,
    hours: 1.5,
  },
  17: {
    amount: 1.5,
    hours: 1.5,
  },
  18: {
    amount: 1.5,
    hours: 1.5,
  },
  19: {
    amount: 1.5,
    hours: 1.5,
  },
  20: {
    amount: 1.5,
    hours: 1.5,
  },
  21: {
    amount: 2,
    hours: 4,
  },
  22: {
    amount: 2,
    hours: 4,
  },
  23: {
    amount: 2,
    hours: 4,
  },
  24: {
    amount: 2,
    hours: 4,
  },
  25: {
    amount: 2,
    hours: 4,
  },
  26: {
    amount: 2,
    hours: 4,
  },
  27: {
    amount: 2,
    hours: 4,
  },
  28: {
    amount: 2,
    hours: 4,
  },
  29: {
    amount: 2,
    hours: 4,
  },
  30: {
    amount: 2,
    hours: 4,
  },
  31: {
    amount: 4,
    hours: 12,
  },
  32: {
    amount: 4,
    hours: 12,
  },
  33: {
    amount: 4,
    hours: 12,
  },
  34: {
    amount: 4,
    hours: 12,
  },
  35: {
    amount: 4,
    hours: 12,
  },
  36: {
    amount: 4,
    hours: 12,
  },
  37: {
    amount: 4,
    hours: 12,
  },
  38: {
    amount: 4,
    hours: 12,
  },
  39: {
    amount: 4,
    hours: 12,
  },
  40: {
    amount: 4,
    hours: 12,
  },
  41: {
    amount: 5.7,
    hours: 14,
  },
  42: {
    amount: 5.7,
    hours: 14,
  },
  43: {
    amount: 5.7,
    hours: 14,
  },
  44: {
    amount: 5.7,
    hours: 14,
  },
  45: {
    amount: 5.7,
    hours: 14,
  },
  46: {
    amount: 5.7,
    hours: 14,
  },
  47: {
    amount: 5.7,
    hours: 14,
  },
  48: {
    amount: 5.7,
    hours: 14,
  },
  49: {
    amount: 5.7,
    hours: 14,
  },
  50: {
    amount: 5.7,
    hours: 14,
  },
  51: {
    amount: 7.6,
    hours: 18,
  },
  52: {
    amount: 7.6,
    hours: 18,
  },
  53: {
    amount: 7.6,
    hours: 18,
  },
  54: {
    amount: 7.6,
    hours: 18,
  },
  55: {
    amount: 7.6,
    hours: 18,
  },
  56: {
    amount: 7.6,
    hours: 18,
  },
  57: {
    amount: 7.6,
    hours: 18,
  },
  58: {
    amount: 7.6,
    hours: 18,
  },
  59: {
    amount: 7.6,
    hours: 18,
  },
  60: {
    amount: 7.6,
    hours: 18,
  },
};

export const getWowClassicPowerLevelingResult = (
  formValue: WowClassicPowerLevelingFormValue
): Result => {
  if (formValue.levels[0] === formValue.levels[1]) {
    return { amount: 0, hours: 0 };
  }

  const levelRange = range(formValue.levels[0] + 1, formValue.levels[1] + 1, 1);

  const result = levelRange.reduce(
    (acc, level) => ({
      amount: acc.amount + MENU[level].amount,
      hours: acc.hours + MENU[level].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.professions) {
    formValue.professions.forEach((profession) => {
      result.amount += profession.amount || 0;
    });
  }

  if (formValue.mount) {
    result.amount += formValue.levels[1] >= 40 ? 0 : 20;
  }

  if (formValue.fast) {
    result.amount += result.amount * 0.3;
    result.hours -= result.hours * 0.3;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  return {
    discountPercent: 10,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getWowClassicPowerLevelingResult;
