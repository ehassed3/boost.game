import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowCovenantFormValue } from '@interfaces/formValue';

const MENU: Record<number, Result> = {
  1: {
    amount: 0,
    hours: 0,
  },
  2: {
    amount: 2.75,
    hours: 0,
  },
  3: {
    amount: 2.75,
    hours: 0,
  },
  4: {
    amount: 2.75,
    hours: 0,
  },
  5: {
    amount: 2.75,
    hours: 0,
  },
  6: {
    amount: 2.75,
    hours: 0,
  },
  7: {
    amount: 2.75,
    hours: 0,
  },
  8: {
    amount: 2.75,
    hours: 0,
  },
  9: {
    amount: 2.75,
    hours: 0,
  },
  10: {
    amount: 2.75,
    hours: 0,
  },
  11: {
    amount: 2.75,
    hours: 0,
  },
  12: {
    amount: 2.75,
    hours: 0,
  },
  13: {
    amount: 2.75,
    hours: 0,
  },
  14: {
    amount: 2.75,
    hours: 0,
  },
  15: {
    amount: 2.75,
    hours: 0,
  },
  16: {
    amount: 2.75,
    hours: 0,
  },
  17: {
    amount: 2.75,
    hours: 0,
  },
  18: {
    amount: 2.75,
    hours: 0,
  },
  19: {
    amount: 2.75,
    hours: 0,
  },
  20: {
    amount: 2.75,
    hours: 0,
  },
  21: {
    amount: 2.75,
    hours: 0,
  },
  22: {
    amount: 2.75,
    hours: 0,
  },
  23: {
    amount: 2.75,
    hours: 0,
  },
  24: {
    amount: 2.75,
    hours: 0,
  },
  25: {
    amount: 2.75,
    hours: 0,
  },
  26: {
    amount: 2.75,
    hours: 0,
  },
  27: {
    amount: 2.75,
    hours: 0,
  },
  28: {
    amount: 2.75,
    hours: 0,
  },
  29: {
    amount: 2.75,
    hours: 0,
  },
  30: {
    amount: 2.75,
    hours: 0,
  },
  31: {
    amount: 2.75,
    hours: 0,
  },
  32: {
    amount: 2.75,
    hours: 0,
  },
  33: {
    amount: 2.75,
    hours: 0,
  },
  34: {
    amount: 2.75,
    hours: 0,
  },
  35: {
    amount: 2.75,
    hours: 0,
  },
  36: {
    amount: 2.75,
    hours: 0,
  },
  37: {
    amount: 2.75,
    hours: 0,
  },
  38: {
    amount: 2.75,
    hours: 0,
  },
  39: {
    amount: 2.75,
    hours: 0,
  },
  40: {
    amount: 2.75,
    hours: 0,
  },
};

export const getWowCovenantResult = (
  formValue: WowCovenantFormValue
): Result => {
  if (formValue.isSwitched) {
    if (formValue.levels[0] === formValue.levels[1]) {
      return { amount: 0, hours: 0 };
    }

    const levelRange = range(
      formValue.levels[0] + 1,
      formValue.levels[1] + 1,
      1
    );
    let amount = levelRange.reduce((acc, level) => acc + MENU[level].amount, 0);

    if (formValue.stream) {
      amount += amount * 0.2;
    }

    return { discountPercent: 10, amount, hours: 0 };
  }

  let amount = 45;

  if (formValue.stream) {
    amount += amount * 0.2;
  }

  return { discountPercent: 10, amount, hours: 0 };
};

export default getWowCovenantResult;
