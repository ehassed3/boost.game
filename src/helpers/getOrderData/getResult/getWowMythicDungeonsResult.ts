import { Result } from '@interfaces/result';
import { WowMythicDungeonsFormValue } from '@interfaces/formValue';

export const getWowMythicDungeonsResult = (
  formValue: WowMythicDungeonsFormValue
): Result => {
  let amount = formValue.mythic.amount * formValue.runs;

  if (formValue.traders) {
    formValue.traders.forEach((trader) => {
      amount += trader.amount;
    });
  }

  if (formValue.selfplay) {
    amount += amount * 0.1;
  }

  if (formValue.timer) {
    amount += amount * 0.2;
  }

  if (formValue.specific) {
    amount += amount * 0.2;
  }

  return { discountPercent: formValue.runs > 4 ? 20 : 15, amount, hours: 0 };
};

export default getWowMythicDungeonsResult;
