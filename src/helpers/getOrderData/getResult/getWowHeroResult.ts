import { Result } from '@interfaces/result';
import { WowHeroFormValue } from '@interfaces/formValue';

export const getWowHeroResult = (formValue: WowHeroFormValue): Result => ({
  discountPercent: 10,
  amount: formValue.amount,
  hours: 0,
});

export default getWowHeroResult;
