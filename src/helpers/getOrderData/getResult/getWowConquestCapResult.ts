import { Result } from '@interfaces/result';
import { WowConquestCapFormValue } from '@interfaces/formValue';

export const getWowConquestCapResult = (
  formValue: WowConquestCapFormValue
): Result => {
  let { amount } = formValue;

  if (formValue.stream) {
    amount += amount * 0.2;
  }

  return { discountPercent: 10, amount, hours: 0 };
};

export default getWowConquestCapResult;
