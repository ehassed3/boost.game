import { Result } from '@interfaces/result';
import { WowMythic8DungeonsFormValue } from '@interfaces/formValue';

export const getWowMythic8DungeonsResult = (
  formValue: WowMythic8DungeonsFormValue
): Result => {
  let amount = 135;

  if (formValue.traders) {
    formValue.traders.forEach((trader) => {
      amount += trader.amount;
    });
  }

  if (formValue.selfplay) {
    amount += amount * 0.1;
  }

  return { discountPercent: 15, amount, hours: 0 };
};

export default getWowMythic8DungeonsResult;
