import { Result } from '@interfaces/result';
import { Dota2LowPriorityFormValue } from '@interfaces/formValue';

export const getDota2LowPriorityResult = (
  formValue: Dota2LowPriorityFormValue
): Result => {
  const result = {
    amount: 1.5 * formValue.games,
    hours: formValue.games,
  };

  if (formValue.heroes) {
    result.amount += result.amount * 0.15;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  if (formValue.booster) {
    result.amount += result.amount * 0.6;
  }

  return {
    discountPercent: 10,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getDota2LowPriorityResult;
