import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { WowClassicHonorFormValue } from '@interfaces/formValue';

export const MENU: Record<number, Result> = {
  0: {
    amount: 0,
    hours: 0,
  },
  50: {
    amount: 19,
    hours: 12,
  },
  100: {
    amount: 19,
    hours: 12,
  },
  150: {
    amount: 19,
    hours: 12,
  },
  200: {
    amount: 19,
    hours: 12,
  },
  250: {
    amount: 19,
    hours: 12,
  },
  300: {
    amount: 19,
    hours: 12,
  },
  350: {
    amount: 19,
    hours: 12,
  },
  400: {
    amount: 19,
    hours: 12,
  },
  450: {
    amount: 19,
    hours: 12,
  },
  500: {
    amount: 19,
    hours: 12,
  },
  550: {
    amount: 19,
    hours: 12,
  },
  600: {
    amount: 19,
    hours: 12,
  },
  650: {
    amount: 19,
    hours: 12,
  },
  700: {
    amount: 19,
    hours: 12,
  },
};

export const getWowClassicHonorResult = (
  formValue: WowClassicHonorFormValue
): Result => {
  if (formValue.isSwitched) {
    let amount = 3 * formValue.hours;

    if (formValue.stream) {
      amount += amount * 0.2;
    }

    return { amount, hours: 0 };
  }

  const honorRange = range(0, formValue.honor + 50, 50);

  const result = honorRange.reduce(
    (acc, honor) => ({
      amount: acc.amount + MENU[honor].amount,
      hours: acc.hours + MENU[honor].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.fast) {
    result.amount += result.amount * 0.3;
    result.hours -= result.hours * 0.3;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  return {
    discountPercent: 20,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getWowClassicHonorResult;
