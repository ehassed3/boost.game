import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { PoePowerLevelingFormValue } from '@interfaces/formValue';

const MENU: Record<'softcore' | 'hardcore', Record<number, Result>> = {
  softcore: {
    1: {
      amount: 0,
      hours: 0,
    },
    2: {
      amount: 0.35,
      hours: 0.125,
    },
    3: {
      amount: 0.35,
      hours: 0.125,
    },
    4: {
      amount: 0.35,
      hours: 0.125,
    },
    5: {
      amount: 0.35,
      hours: 0.125,
    },
    6: {
      amount: 0.35,
      hours: 0.125,
    },
    7: {
      amount: 0.35,
      hours: 0.125,
    },
    8: {
      amount: 0.35,
      hours: 0.125,
    },
    9: {
      amount: 0.35,
      hours: 0.125,
    },
    10: {
      amount: 0.35,
      hours: 0.125,
    },
    11: {
      amount: 0.35,
      hours: 0.125,
    },
    12: {
      amount: 0.35,
      hours: 0.125,
    },
    13: {
      amount: 0.35,
      hours: 0.125,
    },
    14: {
      amount: 0.35,
      hours: 0.125,
    },
    15: {
      amount: 0.35,
      hours: 0.125,
    },
    16: {
      amount: 0.35,
      hours: 0.125,
    },
    17: {
      amount: 0.35,
      hours: 0.125,
    },
    18: {
      amount: 0.35,
      hours: 0.125,
    },
    19: {
      amount: 0.35,
      hours: 0.125,
    },
    20: {
      amount: 0.35,
      hours: 0.125,
    },
    21: {
      amount: 0.35,
      hours: 0.125,
    },
    22: {
      amount: 0.35,
      hours: 0.125,
    },
    23: {
      amount: 0.35,
      hours: 0.125,
    },
    24: {
      amount: 0.35,
      hours: 0.125,
    },
    25: {
      amount: 0.35,
      hours: 0.125,
    },
    26: {
      amount: 0.35,
      hours: 0.125,
    },
    27: {
      amount: 0.35,
      hours: 0.125,
    },
    28: {
      amount: 0.35,
      hours: 0.125,
    },
    29: {
      amount: 0.35,
      hours: 0.125,
    },
    30: {
      amount: 0.35,
      hours: 0.125,
    },
    31: {
      amount: 0.35,
      hours: 0.125,
    },
    32: {
      amount: 0.35,
      hours: 0.125,
    },
    33: {
      amount: 0.35,
      hours: 0.125,
    },
    34: {
      amount: 0.74,
      hours: 0.48,
    },
    35: {
      amount: 0.35,
      hours: 0.125,
    },
    36: {
      amount: 0.35,
      hours: 0.125,
    },
    37: {
      amount: 0.35,
      hours: 0.125,
    },
    38: {
      amount: 0.35,
      hours: 0.125,
    },
    39: {
      amount: 0.35,
      hours: 0.125,
    },
    40: {
      amount: 0.35,
      hours: 0.125,
    },
    41: {
      amount: 0.7,
      hours: 0.2,
    },
    42: {
      amount: 0.7,
      hours: 0.2,
    },
    43: {
      amount: 0.7,
      hours: 0.2,
    },
    44: {
      amount: 0.7,
      hours: 0.2,
    },
    45: {
      amount: 0.7,
      hours: 0.2,
    },
    46: {
      amount: 0.7,
      hours: 0.2,
    },
    47: {
      amount: 0.7,
      hours: 0.2,
    },
    48: {
      amount: 0.7,
      hours: 0.2,
    },
    49: {
      amount: 0.7,
      hours: 0.2,
    },
    50: {
      amount: 0.7,
      hours: 0.2,
    },
    51: {
      amount: 0.8,
      hours: 0.2,
    },
    52: {
      amount: 0.8,
      hours: 0.2,
    },
    53: {
      amount: 0.8,
      hours: 0.2,
    },
    54: {
      amount: 0.8,
      hours: 0.2,
    },
    55: {
      amount: 0.8,
      hours: 0.2,
    },
    56: {
      amount: 0.8,
      hours: 0.2,
    },
    57: {
      amount: 0.8,
      hours: 0.2,
    },
    58: {
      amount: 0.8,
      hours: 0.2,
    },
    59: {
      amount: 0.8,
      hours: 0.2,
    },
    60: {
      amount: 0.8,
      hours: 0.2,
    },
    61: {
      amount: 1.3,
      hours: 0.4,
    },
    62: {
      amount: 1.3,
      hours: 0.4,
    },
    63: {
      amount: 1.3,
      hours: 0.4,
    },
    64: {
      amount: 1.3,
      hours: 0.4,
    },
    65: {
      amount: 1.3,
      hours: 0.4,
    },
    66: {
      amount: 1.3,
      hours: 0.4,
    },
    67: {
      amount: 1.3,
      hours: 0.4,
    },
    68: {
      amount: 1.3,
      hours: 0.4,
    },
    69: {
      amount: 1.3,
      hours: 0.4,
    },
    70: {
      amount: 1.3,
      hours: 0.5,
    },
    71: {
      amount: 1.8,
      hours: 0.5,
    },
    72: {
      amount: 1.8,
      hours: 0.5,
    },
    73: {
      amount: 1.8,
      hours: 0.5,
    },
    74: {
      amount: 1.8,
      hours: 0.5,
    },
    75: {
      amount: 1.8,
      hours: 0.5,
    },
    76: {
      amount: 1.8,
      hours: 0.5,
    },
    77: {
      amount: 1.8,
      hours: 0.5,
    },
    78: {
      amount: 1.8,
      hours: 0.5,
    },
    79: {
      amount: 1.8,
      hours: 0.5,
    },
    80: {
      amount: 1.8,
      hours: 0.5,
    },
    81: {
      amount: 4.5,
      hours: 4.8,
    },
    82: {
      amount: 4.5,
      hours: 4.8,
    },
    83: {
      amount: 4.5,
      hours: 4.8,
    },
    84: {
      amount: 4.5,
      hours: 4.8,
    },
    85: {
      amount: 4.5,
      hours: 4.8,
    },
    86: {
      amount: 4.5,
      hours: 4.8,
    },
    87: {
      amount: 4.5,
      hours: 4.8,
    },
    88: {
      amount: 4.5,
      hours: 4.8,
    },
    89: {
      amount: 4.5,
      hours: 4.8,
    },
    90: {
      amount: 4.5,
      hours: 4.8,
    },
  },
  hardcore: {
    1: {
      amount: 0,
      hours: 0,
    },
    2: {
      amount: 0.45,
      hours: 0.15,
    },
    3: {
      amount: 0.45,
      hours: 0.15,
    },
    4: {
      amount: 0.45,
      hours: 0.15,
    },
    5: {
      amount: 0.45,
      hours: 0.15,
    },
    6: {
      amount: 0.45,
      hours: 0.15,
    },
    7: {
      amount: 0.45,
      hours: 0.15,
    },
    8: {
      amount: 0.45,
      hours: 0.15,
    },
    9: {
      amount: 0.45,
      hours: 0.15,
    },
    10: {
      amount: 0.45,
      hours: 0.15,
    },
    11: {
      amount: 0.45,
      hours: 0.15,
    },
    12: {
      amount: 0.45,
      hours: 0.15,
    },
    13: {
      amount: 0.45,
      hours: 0.15,
    },
    14: {
      amount: 0.45,
      hours: 0.15,
    },
    15: {
      amount: 0.45,
      hours: 0.15,
    },
    16: {
      amount: 0.45,
      hours: 0.15,
    },
    17: {
      amount: 0.45,
      hours: 0.15,
    },
    18: {
      amount: 0.45,
      hours: 0.15,
    },
    19: {
      amount: 0.45,
      hours: 0.15,
    },
    20: {
      amount: 0.45,
      hours: 0.15,
    },
    21: {
      amount: 0.45,
      hours: 0.15,
    },
    22: {
      amount: 0.45,
      hours: 0.15,
    },
    23: {
      amount: 0.45,
      hours: 0.15,
    },
    24: {
      amount: 0.45,
      hours: 0.15,
    },
    25: {
      amount: 0.45,
      hours: 0.15,
    },
    26: {
      amount: 0.45,
      hours: 0.15,
    },
    27: {
      amount: 0.45,
      hours: 0.15,
    },
    28: {
      amount: 0.45,
      hours: 0.15,
    },
    29: {
      amount: 0.45,
      hours: 0.15,
    },
    30: {
      amount: 0.45,
      hours: 0.15,
    },
    31: {
      amount: 0.45,
      hours: 0.15,
    },
    32: {
      amount: 0.45,
      hours: 0.15,
    },
    33: {
      amount: 0.45,
      hours: 0.15,
    },
    34: {
      amount: 0.74,
      hours: 0.48,
    },
    35: {
      amount: 0.45,
      hours: 0.15,
    },
    36: {
      amount: 0.45,
      hours: 0.15,
    },
    37: {
      amount: 0.45,
      hours: 0.15,
    },
    38: {
      amount: 0.45,
      hours: 0.15,
    },
    39: {
      amount: 0.45,
      hours: 0.15,
    },
    40: {
      amount: 0.45,
      hours: 0.15,
    },
    41: {
      amount: 0.8,
      hours: 0.3,
    },
    42: {
      amount: 0.8,
      hours: 0.3,
    },
    43: {
      amount: 0.8,
      hours: 0.3,
    },
    44: {
      amount: 0.8,
      hours: 0.3,
    },
    45: {
      amount: 0.8,
      hours: 0.3,
    },
    46: {
      amount: 0.8,
      hours: 0.3,
    },
    47: {
      amount: 0.8,
      hours: 0.3,
    },
    48: {
      amount: 0.8,
      hours: 0.3,
    },
    49: {
      amount: 0.8,
      hours: 0.3,
    },
    50: {
      amount: 0.8,
      hours: 0.3,
    },
    51: {
      amount: 1.1,
      hours: 0.3,
    },
    52: {
      amount: 1.1,
      hours: 0.3,
    },
    53: {
      amount: 1.1,
      hours: 0.3,
    },
    54: {
      amount: 1.1,
      hours: 0.3,
    },
    55: {
      amount: 1.1,
      hours: 0.3,
    },
    56: {
      amount: 1.1,
      hours: 0.3,
    },
    57: {
      amount: 1.1,
      hours: 0.3,
    },
    58: {
      amount: 1.1,
      hours: 0.3,
    },
    59: {
      amount: 1.1,
      hours: 0.3,
    },
    60: {
      amount: 1.1,
      hours: 0.3,
    },
    61: {
      amount: 1.6,
      hours: 0.5,
    },
    62: {
      amount: 1.6,
      hours: 0.5,
    },
    63: {
      amount: 1.6,
      hours: 0.5,
    },
    64: {
      amount: 1.6,
      hours: 0.5,
    },
    65: {
      amount: 1.6,
      hours: 0.5,
    },
    66: {
      amount: 1.6,
      hours: 0.5,
    },
    67: {
      amount: 1.6,
      hours: 0.5,
    },
    68: {
      amount: 1.6,
      hours: 0.5,
    },
    69: {
      amount: 1.6,
      hours: 0.5,
    },
    70: {
      amount: 1.6,
      hours: 0.5,
    },
    71: {
      amount: 1.8,
      hours: 0.6,
    },
    72: {
      amount: 1.8,
      hours: 0.6,
    },
    73: {
      amount: 1.8,
      hours: 0.6,
    },
    74: {
      amount: 1.8,
      hours: 0.6,
    },
    75: {
      amount: 1.8,
      hours: 0.6,
    },
    76: {
      amount: 1.8,
      hours: 0.6,
    },
    77: {
      amount: 1.8,
      hours: 0.6,
    },
    78: {
      amount: 1.8,
      hours: 0.6,
    },
    79: {
      amount: 1.8,
      hours: 0.6,
    },
    80: {
      amount: 1.8,
      hours: 0.6,
    },
    81: {
      amount: 5.2,
      hours: 7.2,
    },
    82: {
      amount: 5.2,
      hours: 7.2,
    },
    83: {
      amount: 5.2,
      hours: 7.2,
    },
    84: {
      amount: 5.2,
      hours: 7.2,
    },
    85: {
      amount: 5.2,
      hours: 7.2,
    },
    86: {
      amount: 5.2,
      hours: 7.2,
    },
    87: {
      amount: 5.2,
      hours: 7.2,
    },
    88: {
      amount: 5.2,
      hours: 7.2,
    },
    89: {
      amount: 5.2,
      hours: 7.2,
    },
    90: {
      amount: 5.2,
      hours: 7.2,
    },
  },
};

export const getPoePowerLevelingResult = (
  formValue: PoePowerLevelingFormValue
): Result => {
  if (formValue.levels[0] === formValue.levels[1]) {
    return {
      discountPercent: 0,
      amount: 0,
      hours: 0,
    };
  }

  const levelRange = range(formValue.levels[0] + 1, formValue.levels[1] + 1, 1);

  const result = levelRange.reduce(
    (acc, level) => ({
      amount: acc.amount + MENU[formValue.difficulty.name][level].amount,
      hours: acc.hours + MENU[formValue.difficulty.name][level].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  return {
    discountPercent: 10,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getPoePowerLevelingResult;
