import range from 'lodash/range';

import { Result } from '@interfaces/result';
import { Dota2BoostFormValue } from '@interfaces/formValue';

const MENU: Record<number, Result> = {
  0: {
    amount: 0,
    hours: 0,
  },
  25: {
    amount: 1.2,
    hours: 3,
  },
  50: {
    amount: 1.2,
    hours: 3,
  },
  75: {
    amount: 1.2,
    hours: 3,
  },
  100: {
    amount: 1.2,
    hours: 3,
  },
  125: {
    amount: 1.2,
    hours: 3,
  },
  150: {
    amount: 1.2,
    hours: 3,
  },
  175: {
    amount: 1.2,
    hours: 3,
  },
  200: {
    amount: 1.2,
    hours: 3,
  },
  225: {
    amount: 1.2,
    hours: 3,
  },
  250: {
    amount: 1.2,
    hours: 3,
  },
  275: {
    amount: 1.2,
    hours: 3,
  },
  300: {
    amount: 1.2,
    hours: 3,
  },
  325: {
    amount: 1.2,
    hours: 3,
  },
  350: {
    amount: 1.2,
    hours: 3,
  },
  375: {
    amount: 1.2,
    hours: 3,
  },
  400: {
    amount: 1.2,
    hours: 3,
  },
  425: {
    amount: 1.2,
    hours: 3,
  },
  450: {
    amount: 1.2,
    hours: 3,
  },
  475: {
    amount: 1.2,
    hours: 3,
  },
  500: {
    amount: 1.2,
    hours: 3,
  },
  525: {
    amount: 1.25,
    hours: 3,
  },
  550: {
    amount: 1.2,
    hours: 3,
  },
  575: {
    amount: 1.2,
    hours: 3,
  },
  600: {
    amount: 1.2,
    hours: 3,
  },
  625: {
    amount: 1.2,
    hours: 3,
  },
  650: {
    amount: 1.2,
    hours: 3,
  },
  675: {
    amount: 1.2,
    hours: 3,
  },
  700: {
    amount: 1.2,
    hours: 3,
  },
  725: {
    amount: 1.2,
    hours: 3,
  },
  750: {
    amount: 1.2,
    hours: 3,
  },
  775: {
    amount: 1.2,
    hours: 3,
  },
  800: {
    amount: 1.2,
    hours: 3,
  },
  825: {
    amount: 1.2,
    hours: 3,
  },
  850: {
    amount: 1.2,
    hours: 3,
  },
  875: {
    amount: 1.2,
    hours: 3,
  },
  900: {
    amount: 1.2,
    hours: 3,
  },
  925: {
    amount: 1.2,
    hours: 3,
  },
  950: {
    amount: 1.2,
    hours: 3,
  },
  975: {
    amount: 1.2,
    hours: 3,
  },
  1000: {
    amount: 1.2,
    hours: 3,
  },
  1025: {
    amount: 1.2,
    hours: 3,
  },
  1050: {
    amount: 1.2,
    hours: 3,
  },
  1075: {
    amount: 1.2,
    hours: 3,
  },
  1100: {
    amount: 1.2,
    hours: 3,
  },
  1125: {
    amount: 1.2,
    hours: 3,
  },
  1150: {
    amount: 1.2,
    hours: 3,
  },
  1175: {
    amount: 1.2,
    hours: 3,
  },
  1200: {
    amount: 1.2,
    hours: 3,
  },
  1225: {
    amount: 1.2,
    hours: 3,
  },
  1250: {
    amount: 1.2,
    hours: 3,
  },
  1275: {
    amount: 1.2,
    hours: 3,
  },
  1300: {
    amount: 1.2,
    hours: 3,
  },
  1325: {
    amount: 1.2,
    hours: 3,
  },
  1350: {
    amount: 1.2,
    hours: 3,
  },
  1375: {
    amount: 1.2,
    hours: 3,
  },
  1400: {
    amount: 1.2,
    hours: 3,
  },
  1425: {
    amount: 1.2,
    hours: 3,
  },
  1450: {
    amount: 1.2,
    hours: 3,
  },
  1475: {
    amount: 1.2,
    hours: 3,
  },
  1500: {
    amount: 1.2,
    hours: 3,
  },
  1525: {
    amount: 1.2,
    hours: 3,
  },
  1550: {
    amount: 1.2,
    hours: 3,
  },
  1575: {
    amount: 1.2,
    hours: 3,
  },
  1600: {
    amount: 1.2,
    hours: 3,
  },
  1625: {
    amount: 1.2,
    hours: 3,
  },
  1650: {
    amount: 1.2,
    hours: 3,
  },
  1675: {
    amount: 1.2,
    hours: 3,
  },
  1700: {
    amount: 1.2,
    hours: 3,
  },
  1725: {
    amount: 1.2,
    hours: 3,
  },
  1750: {
    amount: 1.2,
    hours: 3,
  },
  1775: {
    amount: 1.2,
    hours: 3,
  },
  1800: {
    amount: 1.2,
    hours: 3,
  },
  1825: {
    amount: 1.2,
    hours: 3,
  },
  1850: {
    amount: 1.2,
    hours: 3,
  },
  1875: {
    amount: 1.2,
    hours: 3,
  },
  1900: {
    amount: 1.2,
    hours: 3,
  },
  1925: {
    amount: 1.2,
    hours: 3,
  },
  1950: {
    amount: 1.2,
    hours: 3,
  },
  1975: {
    amount: 1.2,
    hours: 3,
  },
  2000: {
    amount: 1.2,
    hours: 3,
  },
  2025: {
    amount: 1.5,
    hours: 3,
  },
  2050: {
    amount: 1.5,
    hours: 3,
  },
  2075: {
    amount: 1.5,
    hours: 3,
  },
  2100: {
    amount: 1.5,
    hours: 3,
  },
  2125: {
    amount: 1.5,
    hours: 3,
  },
  2150: {
    amount: 1.5,
    hours: 3,
  },
  2175: {
    amount: 1.5,
    hours: 3,
  },
  2200: {
    amount: 1.5,
    hours: 3,
  },
  2225: {
    amount: 1.5,
    hours: 3,
  },
  2250: {
    amount: 1.5,
    hours: 3,
  },
  2275: {
    amount: 1.5,
    hours: 3,
  },
  2300: {
    amount: 1.5,
    hours: 3,
  },
  2325: {
    amount: 1.5,
    hours: 3,
  },
  2350: {
    amount: 1.5,
    hours: 3,
  },
  2375: {
    amount: 1.5,
    hours: 3,
  },
  2400: {
    amount: 1.5,
    hours: 3,
  },
  2425: {
    amount: 1.5,
    hours: 3,
  },
  2450: {
    amount: 1.5,
    hours: 3,
  },
  2475: {
    amount: 1.5,
    hours: 3,
  },
  2500: {
    amount: 1.5,
    hours: 3,
  },
  2525: {
    amount: 1.5,
    hours: 3,
  },
  2550: {
    amount: 1.5,
    hours: 3,
  },
  2575: {
    amount: 1.5,
    hours: 3,
  },
  2600: {
    amount: 1.5,
    hours: 3,
  },
  2625: {
    amount: 1.5,
    hours: 3,
  },
  2650: {
    amount: 1.5,
    hours: 3,
  },
  2675: {
    amount: 1.5,
    hours: 3,
  },
  2700: {
    amount: 1.5,
    hours: 3,
  },
  2725: {
    amount: 1.5,
    hours: 3,
  },
  2750: {
    amount: 1.5,
    hours: 3,
  },
  2775: {
    amount: 1.5,
    hours: 3,
  },
  2800: {
    amount: 1.5,
    hours: 3,
  },
  2825: {
    amount: 1.5,
    hours: 3,
  },
  2850: {
    amount: 1.5,
    hours: 3,
  },
  2875: {
    amount: 1.5,
    hours: 3,
  },
  2900: {
    amount: 1.5,
    hours: 3,
  },
  2925: {
    amount: 1.5,
    hours: 3,
  },
  2950: {
    amount: 1.5,
    hours: 3,
  },
  2975: {
    amount: 1.5,
    hours: 3,
  },
  3000: {
    amount: 1.5,
    hours: 3,
  },
  3025: {
    amount: 1.9,
    hours: 3,
  },
  3050: {
    amount: 1.9,
    hours: 3,
  },
  3075: {
    amount: 1.9,
    hours: 3,
  },
  3100: {
    amount: 1.9,
    hours: 3,
  },
  3125: {
    amount: 1.9,
    hours: 3,
  },
  3150: {
    amount: 1.9,
    hours: 3,
  },
  3175: {
    amount: 1.9,
    hours: 3,
  },
  3200: {
    amount: 1.9,
    hours: 3,
  },
  3225: {
    amount: 1.9,
    hours: 3,
  },
  3250: {
    amount: 1.9,
    hours: 3,
  },
  3275: {
    amount: 1.9,
    hours: 3,
  },
  3300: {
    amount: 1.9,
    hours: 3,
  },
  3325: {
    amount: 1.9,
    hours: 3,
  },
  3350: {
    amount: 1.9,
    hours: 3,
  },
  3375: {
    amount: 1.9,
    hours: 3,
  },
  3400: {
    amount: 1.9,
    hours: 3,
  },
  3425: {
    amount: 1.9,
    hours: 3,
  },
  3450: {
    amount: 1.9,
    hours: 3,
  },
  3475: {
    amount: 1.9,
    hours: 3,
  },
  3500: {
    amount: 1.9,
    hours: 3,
  },
  3525: {
    amount: 2.3,
    hours: 3,
  },
  3550: {
    amount: 2.3,
    hours: 3,
  },
  3575: {
    amount: 2.3,
    hours: 3,
  },
  3600: {
    amount: 2.3,
    hours: 3,
  },
  3625: {
    amount: 2.3,
    hours: 3,
  },
  3650: {
    amount: 2.3,
    hours: 3,
  },
  3675: {
    amount: 2.3,
    hours: 3,
  },
  3700: {
    amount: 2.3,
    hours: 3,
  },
  3725: {
    amount: 2.3,
    hours: 3,
  },
  3750: {
    amount: 2.3,
    hours: 3,
  },
  3775: {
    amount: 2.3,
    hours: 3,
  },
  3800: {
    amount: 2.3,
    hours: 3,
  },
  3825: {
    amount: 2.3,
    hours: 3,
  },
  3850: {
    amount: 2.3,
    hours: 3,
  },
  3875: {
    amount: 2.3,
    hours: 3,
  },
  3900: {
    amount: 2.3,
    hours: 3,
  },
  3925: {
    amount: 2.3,
    hours: 3,
  },
  3950: {
    amount: 2.3,
    hours: 3,
  },
  3975: {
    amount: 2.3,
    hours: 3,
  },
  4000: {
    amount: 2.3,
    hours: 3,
  },
  4025: {
    amount: 2.5,
    hours: 3,
  },
  4050: {
    amount: 2.5,
    hours: 3,
  },
  4075: {
    amount: 2.5,
    hours: 3,
  },
  4100: {
    amount: 2.5,
    hours: 3,
  },
  4125: {
    amount: 2.5,
    hours: 3,
  },
  4150: {
    amount: 2.5,
    hours: 3,
  },
  4175: {
    amount: 2.5,
    hours: 3,
  },
  4200: {
    amount: 2.5,
    hours: 3,
  },
  4225: {
    amount: 2.5,
    hours: 3,
  },
  4250: {
    amount: 2.5,
    hours: 3,
  },
  4275: {
    amount: 2.5,
    hours: 3,
  },
  4300: {
    amount: 2.5,
    hours: 3,
  },
  4325: {
    amount: 2.5,
    hours: 3,
  },
  4350: {
    amount: 2.5,
    hours: 3,
  },
  4375: {
    amount: 2.5,
    hours: 3,
  },
  4400: {
    amount: 2.5,
    hours: 3,
  },
  4425: {
    amount: 2.5,
    hours: 3,
  },
  4450: {
    amount: 2.5,
    hours: 3,
  },
  4475: {
    amount: 2.5,
    hours: 3,
  },
  4500: {
    amount: 2.5,
    hours: 3,
  },
  4525: {
    amount: 3,
    hours: 3,
  },
  4550: {
    amount: 3,
    hours: 3,
  },
  4575: {
    amount: 3,
    hours: 3,
  },
  4600: {
    amount: 3,
    hours: 3,
  },
  4625: {
    amount: 3,
    hours: 3,
  },
  4650: {
    amount: 3,
    hours: 3,
  },
  4675: {
    amount: 3,
    hours: 3,
  },
  4700: {
    amount: 3,
    hours: 3,
  },
  4725: {
    amount: 3,
    hours: 3,
  },
  4750: {
    amount: 3,
    hours: 3,
  },
  4775: {
    amount: 3,
    hours: 3,
  },
  4800: {
    amount: 3,
    hours: 3,
  },
  4825: {
    amount: 3,
    hours: 3,
  },
  4850: {
    amount: 3,
    hours: 3,
  },
  4875: {
    amount: 3,
    hours: 3,
  },
  4900: {
    amount: 3,
    hours: 3,
  },
  4925: {
    amount: 3,
    hours: 3,
  },
  4950: {
    amount: 3,
    hours: 3,
  },
  4975: {
    amount: 3,
    hours: 3,
  },
  5000: {
    amount: 3,
    hours: 3,
  },
  5025: {
    amount: 3.6,
    hours: 3,
  },
  5050: {
    amount: 3.6,
    hours: 3,
  },
  5075: {
    amount: 3.6,
    hours: 3,
  },
  5100: {
    amount: 3.6,
    hours: 3,
  },
  5125: {
    amount: 3.6,
    hours: 3,
  },
  5150: {
    amount: 3.6,
    hours: 3,
  },
  5175: {
    amount: 3.6,
    hours: 3,
  },
  5200: {
    amount: 3.6,
    hours: 3,
  },
  5225: {
    amount: 3.6,
    hours: 3,
  },
  5250: {
    amount: 3.6,
    hours: 3,
  },
  5275: {
    amount: 3.6,
    hours: 3,
  },
  5300: {
    amount: 3.6,
    hours: 3,
  },
  5325: {
    amount: 3.6,
    hours: 3,
  },
  5350: {
    amount: 3.6,
    hours: 3,
  },
  5375: {
    amount: 3.6,
    hours: 3,
  },
  5400: {
    amount: 3.6,
    hours: 3,
  },
  5425: {
    amount: 3.6,
    hours: 3,
  },
  5450: {
    amount: Infinity,
    hours: 3,
  },
  5475: {
    amount: Infinity,
    hours: 3,
  },
  5500: {
    amount: Infinity,
    hours: 3,
  },
  5525: {
    amount: Infinity,
    hours: 3,
  },
  5550: {
    amount: Infinity,
    hours: 3,
  },
  5575: {
    amount: Infinity,
    hours: 3,
  },
  5600: {
    amount: Infinity,
    hours: 3,
  },
  5625: {
    amount: Infinity,
    hours: 3,
  },
  5650: {
    amount: Infinity,
    hours: 3,
  },
  5675: {
    amount: Infinity,
    hours: 3,
  },
  5700: {
    amount: Infinity,
    hours: 3,
  },
  5725: {
    amount: Infinity,
    hours: 3,
  },
  5750: {
    amount: Infinity,
    hours: 3,
  },
  5775: {
    amount: Infinity,
    hours: 3,
  },
  5800: {
    amount: Infinity,
    hours: 3,
  },
  5825: {
    amount: Infinity,
    hours: 3,
  },
  5850: {
    amount: Infinity,
    hours: 3,
  },
  5875: {
    amount: Infinity,
    hours: 3,
  },
  5900: {
    amount: Infinity,
    hours: 3,
  },
  5925: {
    amount: Infinity,
    hours: 3,
  },
  5950: {
    amount: Infinity,
    hours: 3,
  },
  5975: {
    amount: Infinity,
    hours: 3,
  },
  6000: {
    amount: Infinity,
    hours: 3,
  },
  6025: {
    amount: Infinity,
    hours: 3,
  },
  6050: {
    amount: Infinity,
    hours: 3,
  },
  6075: {
    amount: Infinity,
    hours: 3,
  },
  6100: {
    amount: Infinity,
    hours: 3,
  },
  6125: {
    amount: Infinity,
    hours: 3,
  },
  6150: {
    amount: Infinity,
    hours: 3,
  },
  6175: {
    amount: Infinity,
    hours: 3,
  },
  6200: {
    amount: Infinity,
    hours: 3,
  },
  6225: {
    amount: Infinity,
    hours: 3,
  },
  6250: {
    amount: Infinity,
    hours: 3,
  },
  6275: {
    amount: Infinity,
    hours: 3,
  },
  6300: {
    amount: Infinity,
    hours: 3,
  },
  6325: {
    amount: Infinity,
    hours: 3,
  },
  6350: {
    amount: Infinity,
    hours: 3,
  },
  6375: {
    amount: Infinity,
    hours: 3,
  },
  6400: {
    amount: Infinity,
    hours: 3,
  },
  6425: {
    amount: Infinity,
    hours: 3,
  },
  6450: {
    amount: Infinity,
    hours: 3,
  },
  6475: {
    amount: Infinity,
    hours: 3,
  },
  6500: {
    amount: Infinity,
    hours: 3,
  },
  6525: {
    amount: Infinity,
    hours: 3,
  },
  6550: {
    amount: Infinity,
    hours: 3,
  },
  6575: {
    amount: Infinity,
    hours: 3,
  },
  6600: {
    amount: Infinity,
    hours: 3,
  },
  6625: {
    amount: Infinity,
    hours: 3,
  },
  6650: {
    amount: Infinity,
    hours: 3,
  },
  6675: {
    amount: Infinity,
    hours: 3,
  },
  6700: {
    amount: Infinity,
    hours: 3,
  },
  6725: {
    amount: Infinity,
    hours: 3,
  },
  6750: {
    amount: Infinity,
    hours: 3,
  },
  6775: {
    amount: Infinity,
    hours: 3,
  },
  6800: {
    amount: Infinity,
    hours: 3,
  },
  6825: {
    amount: Infinity,
    hours: 3,
  },
  6850: {
    amount: Infinity,
    hours: 3,
  },
  6875: {
    amount: Infinity,
    hours: 3,
  },
  6900: {
    amount: Infinity,
    hours: 3,
  },
  6925: {
    amount: Infinity,
    hours: 3,
  },
  6950: {
    amount: Infinity,
    hours: 3,
  },
  6975: {
    amount: Infinity,
    hours: 3,
  },
  7000: {
    amount: Infinity,
    hours: 3,
  },
};

export const getDota2BoostResult = (formValue: Dota2BoostFormValue): Result => {
  if (formValue.ranks[0] === formValue.ranks[1]) {
    return { amount: 0, hours: 0 };
  }

  const startRank =
    formValue.ranks[0] % 25 === 0
      ? formValue.ranks[0]
      : +(formValue.ranks[0] / 25).toFixed() * 25;
  const rankRange = range(startRank, formValue.ranks[1] + 1, 25);

  const result = rankRange.reduce(
    (acc, rank) => ({
      amount: acc.amount + MENU[rank].amount,
      hours: acc.hours + MENU[rank].hours,
    }),
    { amount: 0, hours: 0 }
  );

  if (formValue.heroes) {
    result.amount += result.amount * 0.15;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  if (formValue.booster) {
    result.amount += result.amount * 3;
    result.hours += result.hours * 0.2;
  }

  return {
    discountPercent: 15,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getDota2BoostResult;
