import { Result } from '@interfaces/result';
import { WowSoulAshFormValue } from '@interfaces/formValue';

export const getWowSoulAshResult = (formValue: WowSoulAshFormValue): Result => {
  let amount = 17;

  if (formValue.selfplay) {
    amount += amount * 0.1;
  }

  return { discountPercent: 10, amount, hours: 0 };
};

export default getWowSoulAshResult;
