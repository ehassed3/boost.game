import { Result } from '@interfaces/result';
import { Dota2CalibrationFormValue } from '@interfaces/formValue';

const getAmountByRank = (rank: number) => {
  if (rank < 2000) {
    return 2;
  }

  if (rank < 3000) {
    return 2.2;
  }

  if (rank < 4000) {
    return 2.5;
  }

  if (rank < 5000) {
    return 2.9;
  }

  if (rank < 6000) {
    return 3.6;
  }

  if (rank < 6500) {
    return 3.9;
  }

  return 5.9;
};

export const getDota2CalibrationResult = (
  formValue: Dota2CalibrationFormValue
): Result => {
  const result = {
    amount: getAmountByRank(formValue.rank),
    hours: +formValue.games,
  };

  result.amount *= +formValue.games;

  if (formValue.heroes) {
    result.amount += result.amount * 0.15;
  }

  if (formValue.stream) {
    result.amount += result.amount * 0.2;
  }

  if (formValue.booster) {
    result.amount += result.amount * 0.6;
  }

  return {
    discountPercent: 10,
    amount: result.amount,
    hours: result.hours,
  };
};

export default getDota2CalibrationResult;
