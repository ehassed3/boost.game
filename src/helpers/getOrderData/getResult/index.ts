import getPoePowerLevelingResult from '@helpers/getOrderData/getResult/getPoePowerLevelingResult';

import getWowPowerLevelingResult from '@helpers/getOrderData/getResult/getWowPowerLevelingResult';
import getWowCovenantResult from '@helpers/getOrderData/getResult/getWowCovenantResult';
import getWowArenaResult from '@helpers/getOrderData/getResult/getWowArenaResult';
import getWowHonorResult from '@helpers/getOrderData/getResult/getWowHonorResult';
import getWowConquestCapResult from '@helpers/getOrderData/getResult/getWowConquestCapResult';
import getWowRaidsResult from '@helpers/getOrderData/getResult/getWowRaidsResult';
import getWowMythicDungeonsResult from '@helpers/getOrderData/getResult/getWowMythicDungeonsResult';
import getWowMythic8DungeonsResult from '@helpers/getOrderData/getResult/getWowMythic8DungeonsResult';
import getWowTorghastTowerLayerResult from '@helpers/getOrderData/getResult/getWowTorghastTowerLayerResult';
import getWowSoulAshResult from '@helpers/getOrderData/getResult/getWowSoulAshResult';
import getWowHeroResult from '@helpers/getOrderData/getResult/getWowHeroResult';

import getWowClassicPowerLevelingResult from '@helpers/getOrderData/getResult/getWowClassicPowerLevelingResult';
import getWowClassicHonorResult from '@helpers/getOrderData/getResult/getWowClassicHonorResult';
import getWowClassicRankResult from '@helpers/getOrderData/getResult/getWowClassicRankResult';

import getDota2BoostResult from '@helpers/getOrderData/getResult/getDota2BoostResult';
import getDota2CalibrationResult from '@helpers/getOrderData/getResult/getDota2CalibrationResult';
import getDota2LowPriorityResult from '@helpers/getOrderData/getResult/getDota2LowPriorityResult';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import { DirtyOrder } from '@interfaces/order';
import { Result } from '@interfaces/result';
import {
  PoePowerLevelingFormValue,
  WowPowerLevelingFormValue,
  WowCovenantFormValue,
  WowArenaFormValue,
  WowHonorFormValue,
  WowConquestCapFormValue,
  WowRaidsFormValue,
  WowMythicDungeonsFormValue,
  WowMythic8DungeonsFormValue,
  WowTorghastTowerLayerFormValue,
  WowSoulAshFormValue,
  WowHeroFormValue,
  WowClassicPowerLevelingFormValue,
  WowClassicHonorFormValue,
  WowClassicRankFormValue,
  Dota2BoostFormValue,
  Dota2CalibrationFormValue,
  Dota2LowPriorityFormValue,
} from '@interfaces/formValue';

const getResult = (order: DirtyOrder): Result => {
  switch (order.game) {
    case GAME.POE:
      switch (order.position) {
        case POSITION.POWER_LEVELING:
          return getPoePowerLevelingResult(
            order.formValue as PoePowerLevelingFormValue
          );
        default:
          return { amount: 0, hours: 0 };
      }
    case GAME.WOW:
      switch (order.category) {
        case CATEGORY.CHARACTER_BOOST:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowPowerLevelingResult(
                order.formValue as WowPowerLevelingFormValue
              );
            case POSITION.COVENANT:
              return getWowCovenantResult(
                order.formValue as WowCovenantFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.ARENA:
              return getWowArenaResult(order.formValue as WowArenaFormValue);
            case POSITION.HONOR:
              return getWowHonorResult(order.formValue as WowHonorFormValue);
            case POSITION.CONQUEST_CAP:
              return getWowConquestCapResult(
                order.formValue as WowConquestCapFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        case CATEGORY.DUNGEONS_AND_RAIDS:
          switch (order.position) {
            case POSITION.RAIDS:
              return getWowRaidsResult(order.formValue as WowRaidsFormValue);
            case POSITION.MYTHIC_DUNGEONS:
              return getWowMythicDungeonsResult(
                order.formValue as WowMythicDungeonsFormValue
              );
            case POSITION.MYTHIC_8_DUNGEONS:
              return getWowMythic8DungeonsResult(
                order.formValue as WowMythic8DungeonsFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        case CATEGORY.TORGHAST_AND_SOUL_ASH:
          switch (order.position) {
            case POSITION.TORGHAST_TOWER_LAYER:
              return getWowTorghastTowerLayerResult(
                order.formValue as WowTorghastTowerLayerFormValue
              );
            case POSITION.SOUL_ASH:
              return getWowSoulAshResult(
                order.formValue as WowSoulAshFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        case CATEGORY.ACHIEVEMENTS:
          switch (order.position) {
            case POSITION.HERO:
              return getWowHeroResult(order.formValue as WowHeroFormValue);
            default:
              return { amount: 0, hours: 0 };
          }
        default:
          return { amount: 0, hours: 0 };
      }
    case GAME.WOW_CLASSIC:
      switch (order.category) {
        case CATEGORY.POWER_LEVELING:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowClassicPowerLevelingResult(
                order.formValue as WowClassicPowerLevelingFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.HONOR:
              return getWowClassicHonorResult(
                order.formValue as WowClassicHonorFormValue
              );
            case POSITION.RANK:
              return getWowClassicRankResult(
                order.formValue as WowClassicRankFormValue
              );
            default:
              return { amount: 0, hours: 0 };
          }
        default:
          return { amount: 0, hours: 0 };
      }
    case GAME.DOTA_2:
      switch (order.position) {
        case POSITION.BOOST:
          return getDota2BoostResult(order.formValue as Dota2BoostFormValue);
        case POSITION.CALIBRATION:
          return getDota2CalibrationResult(
            order.formValue as Dota2CalibrationFormValue
          );
        case POSITION.LOW_PRIORITY:
          return getDota2LowPriorityResult(
            order.formValue as Dota2LowPriorityFormValue
          );
        default:
          return { amount: 0, hours: 0 };
      }
    default:
      return { amount: 0, hours: 0 };
  }
};

export default getResult;
