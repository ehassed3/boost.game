import { IntlShape } from 'react-intl';

import { Result, FormattedResult } from '@interfaces/result';
import Currency from '@interfaces/currency';

const getFormattedResult = (
  intl: IntlShape,
  currency: Currency,
  result: Result,
  statusPercent = 0,
  promocodePercent = 0
): FormattedResult => {
  if (result.amount === Infinity) {
    return {
      discountPercent: 0,
      fullPrice: {
        amount: Infinity,
        currency: currency.code,
      },
      price: {
        amount: Infinity,
        currency: currency.code,
      },
      time: '',
    };
  }

  const discountPercent =
    (result.discountPercent || 0) + statusPercent + promocodePercent;

  const fullPrice = {
    amount: Math.round(result.amount * currency.multiplier),
    currency: currency.code,
  };
  const price = {
    amount: discountPercent
      ? Math.floor(
          fullPrice.amount - fullPrice.amount * (discountPercent / 100)
        )
      : fullPrice.amount,
    currency: currency.code,
  };

  if (!result.hours) {
    return {
      discountPercent,
      fullPrice,
      price,
      time: '',
    };
  }

  return {
    discountPercent,
    fullPrice,
    price,
    time:
      result.hours > 120
        ? intl.formatMessage(
            { id: 'result.days' },
            { days: Math.ceil(result.hours / 24) || 1 }
          )
        : intl.formatMessage(
            { id: 'result.hours' },
            { hours: Math.ceil(result.hours) || 1 }
          ),
  };
};

export default getFormattedResult;
