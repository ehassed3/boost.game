import { IntlShape } from 'react-intl';

import getOrderDetails from '@helpers/getOrderData/getDetails';
import getOrderResult from '@helpers/getOrderData/getResult';
import getFormattedResult from '@helpers/getOrderData/getResult/getFormattedResult';
import getOrderTitle from '@helpers/getOrderData/getTitle';

import { DirtyOrder, RawOrder } from '@interfaces/order';

const getRawOrder = (
  intl: IntlShape,
  order: DirtyOrder,
  user: string,
  statusPercent: number,
  promocodePercent: number
): RawOrder => ({
  category: order.category,
  details: getOrderDetails(intl, order),
  game: order.game,
  locale: order.locale,
  position: order.position,
  result: getFormattedResult(
    intl,
    order.currency,
    getOrderResult(order),
    statusPercent,
    promocodePercent
  ),
  stream: order.formValue.stream,
  title: getOrderTitle(intl, order),
  user,
  promocode: order.promocode,
});

export default getRawOrder;
