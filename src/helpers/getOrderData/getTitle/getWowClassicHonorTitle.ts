import { IntlShape } from 'react-intl';

import { WowClassicHonorFormValue } from '@interfaces/formValue';

export const getWowClassicHonorTitle = (
  intl: IntlShape,
  formValue: WowClassicHonorFormValue
): string =>
  formValue.isSwitched
    ? intl.formatMessage(
        { id: 'wowClassicHonor.resultHourlyTitle' },
        { hours: formValue.hours }
      )
    : intl.formatMessage(
        { id: 'wowClassicHonor.resultTitle' },
        { honor: formValue.honor }
      );

export default getWowClassicHonorTitle;
