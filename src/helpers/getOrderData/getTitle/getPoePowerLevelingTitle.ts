import { IntlShape } from 'react-intl';

import { PoePowerLevelingFormValue } from '@interfaces/formValue';

export const getPoePowerLevelingTitle = (
  intl: IntlShape,
  formValue: PoePowerLevelingFormValue
): string =>
  intl.formatMessage(
    { id: 'poePowerLeveling.resultTitle' },
    {
      startLevel: formValue.levels[0],
      endLevel: formValue.levels[1],
      difficulty: formValue.difficulty.label,
    }
  );

export default getPoePowerLevelingTitle;
