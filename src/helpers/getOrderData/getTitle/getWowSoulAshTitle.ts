import { IntlShape } from 'react-intl';

export const getWowSoulAshTitle = (intl: IntlShape): string =>
  intl.formatMessage({ id: 'wowSoulAsh.resultTitle' });

export default getWowSoulAshTitle;
