import { IntlShape } from 'react-intl';

import { Dota2BoostFormValue } from '@interfaces/formValue';

export const getDota2BoostTitle = (
  intl: IntlShape,
  formValue: Dota2BoostFormValue
): string =>
  intl.formatMessage(
    { id: 'dota2Boost.resultTitle' },
    { startRank: formValue.ranks[0], endRank: formValue.ranks[1] }
  );

export default getDota2BoostTitle;
