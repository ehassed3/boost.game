import { IntlShape } from 'react-intl';

import { WowPowerLevelingFormValue } from '@interfaces/formValue';

export const getWowPowerLevelingTitle = (
  intl: IntlShape,
  formValue: WowPowerLevelingFormValue
): string =>
  formValue.isGear
    ? intl.formatMessage(
        { id: 'wowPowerLeveling.resultGearTitle' },
        {
          startLevel: formValue.levels[0],
          endLevel: formValue.levels[1],
          gear: formValue.gear,
        }
      )
    : intl.formatMessage(
        { id: 'wowPowerLeveling.resultTitle' },
        { startLevel: formValue.levels[0], endLevel: formValue.levels[1] }
      );

export default getWowPowerLevelingTitle;
