import { IntlShape } from 'react-intl';

export const getWowConquestCapTitle = (intl: IntlShape): string =>
  intl.formatMessage({ id: 'wowConquestCap.resultTitle' });

export default getWowConquestCapTitle;
