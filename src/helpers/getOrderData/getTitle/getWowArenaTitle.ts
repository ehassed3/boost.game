import { IntlShape } from 'react-intl';

import { WowArenaFormValue } from '@interfaces/formValue';

export const getWowArenaTitle = (
  intl: IntlShape,
  formValue: WowArenaFormValue
): string =>
  intl.formatMessage(
    { id: 'wowArena.resultTitle' },
    { rating: formValue.rating, player: formValue.player.label }
  );

export default getWowArenaTitle;
