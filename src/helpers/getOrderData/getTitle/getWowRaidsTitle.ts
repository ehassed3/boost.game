import { IntlShape } from 'react-intl';

import { WowRaidsFormValue } from '@interfaces/formValue';

export const getWowRaidsTitle = (
  intl: IntlShape,
  formValue: WowRaidsFormValue
): string =>
  intl.formatMessage(
    { id: 'wowRaids.resultTitle' },
    { raids: formValue.raids, difficulty: formValue.difficulty.label }
  );

export default getWowRaidsTitle;
