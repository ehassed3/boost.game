import { IntlShape } from 'react-intl';

import getPoePowerLevelingTitle from '@helpers/getOrderData/getTitle/getPoePowerLevelingTitle';

import getWowPowerLevelingTitle from '@helpers/getOrderData/getTitle/getWowPowerLevelingTitle';
import getWowCovenantTitle from '@helpers/getOrderData/getTitle/getWowCovenantTitle';
import getWowArenaTitle from '@helpers/getOrderData/getTitle/getWowArenaTitle';
import getWowHonorTitle from '@helpers/getOrderData/getTitle/getWowHonorTitle';
import getWowConquestCapTitle from '@helpers/getOrderData/getTitle/getWowConquestCapTitle';
import getWowRaidsTitle from '@helpers/getOrderData/getTitle/getWowRaidsTitle';
import getWowMythicDungeonsTitle from '@helpers/getOrderData/getTitle/getWowMythicDungeonsTitle';
import getWowMythic8DungeonsTitle from '@helpers/getOrderData/getTitle/getWowMythic8DungeonsTitle';
import getWowTorghastTowerLayerTitle from '@helpers/getOrderData/getTitle/getWowTorghastTowerLayerTitle';
import getWowSoulAshTitle from '@helpers/getOrderData/getTitle/getWowSoulAshTitle';
import getWowHeroTitle from '@helpers/getOrderData/getTitle/getWowHeroTitle';

import getWowClassicPowerLevelingTitle from '@helpers/getOrderData/getTitle/getWowClassicPowerLevelingTitle';
import getWowClassicHonorTitle from '@helpers/getOrderData/getTitle/getWowClassicHonorTitle';
import getWowClassicRankTitle from '@helpers/getOrderData/getTitle/getWowClassicRankTitle';

import getDota2BoostTitle from '@helpers/getOrderData/getTitle/getDota2BoostTitle';
import getDota2CalibrationTitle from '@helpers/getOrderData/getTitle/getDota2CalibrationTitle';
import getDota2LowPriorityTitle from '@helpers/getOrderData/getTitle/getDota2LowPriorityTitle';

import GAME from '@constants/game';
import CATEGORY from '@constants/category';
import POSITION from '@constants/position';

import { DirtyOrder } from '@interfaces/order';
import {
  PoePowerLevelingFormValue,
  WowPowerLevelingFormValue,
  WowCovenantFormValue,
  WowArenaFormValue,
  WowHonorFormValue,
  WowRaidsFormValue,
  WowMythicDungeonsFormValue,
  WowTorghastTowerLayerFormValue,
  WowClassicPowerLevelingFormValue,
  WowClassicHonorFormValue,
  WowClassicRankFormValue,
  Dota2BoostFormValue,
  Dota2CalibrationFormValue,
  Dota2LowPriorityFormValue,
} from '@interfaces/formValue';

const getTitle = (intl: IntlShape, order: DirtyOrder): string => {
  switch (order.game) {
    case GAME.POE:
      switch (order.position) {
        case POSITION.POWER_LEVELING:
          return getPoePowerLevelingTitle(
            intl,
            order.formValue as PoePowerLevelingFormValue
          );
        default:
          return '';
      }
    case GAME.WOW:
      switch (order.category) {
        case CATEGORY.CHARACTER_BOOST:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowPowerLevelingTitle(
                intl,
                order.formValue as WowPowerLevelingFormValue
              );
            case POSITION.COVENANT:
              return getWowCovenantTitle(
                intl,
                order.formValue as WowCovenantFormValue
              );
            default:
              return '';
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.ARENA:
              return getWowArenaTitle(
                intl,
                order.formValue as WowArenaFormValue
              );
            case POSITION.HONOR:
              return getWowHonorTitle(
                intl,
                order.formValue as WowHonorFormValue
              );
            case POSITION.CONQUEST_CAP:
              return getWowConquestCapTitle(intl);
            default:
              return '';
          }
        case CATEGORY.DUNGEONS_AND_RAIDS:
          switch (order.position) {
            case POSITION.RAIDS:
              return getWowRaidsTitle(
                intl,
                order.formValue as WowRaidsFormValue
              );
            case POSITION.MYTHIC_DUNGEONS:
              return getWowMythicDungeonsTitle(
                intl,
                order.formValue as WowMythicDungeonsFormValue
              );
            case POSITION.MYTHIC_8_DUNGEONS:
              return getWowMythic8DungeonsTitle(intl);
            default:
              return '';
          }
        case CATEGORY.TORGHAST_AND_SOUL_ASH:
          switch (order.position) {
            case POSITION.TORGHAST_TOWER_LAYER:
              return getWowTorghastTowerLayerTitle(
                intl,
                order.formValue as WowTorghastTowerLayerFormValue
              );
            case POSITION.SOUL_ASH:
              return getWowSoulAshTitle(intl);
            default:
              return '';
          }
        case CATEGORY.ACHIEVEMENTS:
          switch (order.position) {
            case POSITION.HERO:
              return getWowHeroTitle(intl);
            default:
              return '';
          }
        default:
          return '';
      }
    case GAME.WOW_CLASSIC:
      switch (order.category) {
        case CATEGORY.POWER_LEVELING:
          switch (order.position) {
            case POSITION.POWER_LEVELING:
              return getWowClassicPowerLevelingTitle(
                intl,
                order.formValue as WowClassicPowerLevelingFormValue
              );
            default:
              return '';
          }
        case CATEGORY.PVP:
          switch (order.position) {
            case POSITION.HONOR:
              return getWowClassicHonorTitle(
                intl,
                order.formValue as WowClassicHonorFormValue
              );
            case POSITION.RANK:
              return getWowClassicRankTitle(
                intl,
                order.formValue as WowClassicRankFormValue
              );
            default:
              return '';
          }
        default:
          return '';
      }
    case GAME.DOTA_2:
      switch (order.position) {
        case POSITION.BOOST:
          return getDota2BoostTitle(
            intl,
            order.formValue as Dota2BoostFormValue
          );
        case POSITION.CALIBRATION:
          return getDota2CalibrationTitle(
            intl,
            order.formValue as Dota2CalibrationFormValue
          );
        case POSITION.LOW_PRIORITY:
          return getDota2LowPriorityTitle(
            intl,
            order.formValue as Dota2LowPriorityFormValue
          );
        default:
          return '';
      }
    default:
      return '';
  }
};

export default getTitle;
