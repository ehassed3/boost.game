import { IntlShape } from 'react-intl';

import { WowClassicRankFormValue } from '@interfaces/formValue';

export const getWowClassicRankTitle = (
  intl: IntlShape,
  formValue: WowClassicRankFormValue
): string =>
  intl.formatMessage(
    { id: 'wowClassicRank.resultTitle' },
    { startRank: formValue.ranks[0], endRank: formValue.ranks[1] }
  );

export default getWowClassicRankTitle;
