import { IntlShape } from 'react-intl';

export const getWowMythic8DungeonsTitle = (intl: IntlShape): string =>
  intl.formatMessage({ id: 'wowMythic8Dungeons.resultTitle' });

export default getWowMythic8DungeonsTitle;
