import { IntlShape } from 'react-intl';

export const getWowHeroTitle = (intl: IntlShape): string =>
  intl.formatMessage({ id: 'wowHero.resultTitle' });

export default getWowHeroTitle;
