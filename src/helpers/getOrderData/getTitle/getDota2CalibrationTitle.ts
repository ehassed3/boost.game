import { IntlShape } from 'react-intl';

import { Dota2CalibrationFormValue } from '@interfaces/formValue';

export const getDota2CalibrationTitle = (
  intl: IntlShape,
  formValue: Dota2CalibrationFormValue
): string =>
  intl.formatMessage(
    { id: 'dota2Calibration.resultTitle' },
    { games: formValue.games, rank: formValue.rank }
  );

export default getDota2CalibrationTitle;
