import { IntlShape } from 'react-intl';

import { WowMythicDungeonsFormValue } from '@interfaces/formValue';

export const getWowMythicDungeonsTitle = (
  intl: IntlShape,
  formValue: WowMythicDungeonsFormValue
): string =>
  intl.formatMessage(
    { id: 'wowMythicDungeons.resultTitle' },
    { mythicLevel: formValue.mythic.level, runs: formValue.runs }
  );

export default getWowMythicDungeonsTitle;
