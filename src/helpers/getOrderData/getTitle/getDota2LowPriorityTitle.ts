import { IntlShape } from 'react-intl';

import { Dota2LowPriorityFormValue } from '@interfaces/formValue';

export const getDota2LowPriorityTitle = (
  intl: IntlShape,
  formValue: Dota2LowPriorityFormValue
): string =>
  intl.formatMessage(
    { id: 'dota2LowPriority.resultTitle' },
    { games: formValue.games }
  );

export default getDota2LowPriorityTitle;
