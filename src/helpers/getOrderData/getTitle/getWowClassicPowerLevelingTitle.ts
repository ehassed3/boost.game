import { IntlShape } from 'react-intl';

import { WowClassicPowerLevelingFormValue } from '@interfaces/formValue';

export const getWowClassicPowerLevelingTitle = (
  intl: IntlShape,
  formValue: WowClassicPowerLevelingFormValue
): string =>
  intl.formatMessage(
    { id: 'wowClassicPowerLeveling.resultTitle' },
    { startLevel: formValue.levels[0], endLevel: formValue.levels[1] }
  );

export default getWowClassicPowerLevelingTitle;
