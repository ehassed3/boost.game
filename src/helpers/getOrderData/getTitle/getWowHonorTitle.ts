import { IntlShape } from 'react-intl';

import { WowHonorFormValue } from '@interfaces/formValue';

export const getWowHonorTitle = (
  intl: IntlShape,
  formValue: WowHonorFormValue
): string =>
  intl.formatMessage(
    { id: 'wowHonor.resultTitle' },
    { honor: formValue.honor }
  );

export default getWowHonorTitle;
