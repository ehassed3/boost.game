import { IntlShape } from 'react-intl';

import { WowCovenantFormValue } from '@interfaces/formValue';

export const getWowCovenantTitle = (
  intl: IntlShape,
  formValue: WowCovenantFormValue
): string =>
  formValue.isSwitched
    ? intl.formatMessage(
        { id: 'wowCovenant.resultTitle' },
        {
          startLevel: formValue.levels[0],
          endLevel: formValue.levels[1],
          covenant: formValue.covenant.label,
        }
      )
    : intl.formatMessage(
        { id: 'wowCovenant.resultCampaignTitle' },
        { covenant: formValue.covenant.label }
      );

export default getWowCovenantTitle;
