import { IntlShape } from 'react-intl';

import { WowTorghastTowerLayerFormValue } from '@interfaces/formValue';

export const getWowTorghastTowerLayerTitle = (
  intl: IntlShape,
  formValue: WowTorghastTowerLayerFormValue
): string =>
  intl.formatMessage(
    { id: 'wowTorghastTowerLayer.resultTitle' },
    { layer: formValue.layer, wings: formValue.wings.label }
  );

export default getWowTorghastTowerLayerTitle;
