import { Order, PublicOrder } from '@interfaces/order';
import { Client, Manager, Booster } from '@interfaces/user';

const getPublicOrder = (
  order: Order,
  boosters: Booster[] | Booster,
  managers: Manager[] | Manager,
  users: Client[] | Client
): PublicOrder => {
  const booster = Array.isArray(boosters)
    ? boosters.find((boosterItem) => boosterItem.email === order.booster)
    : boosters;
  const manager = Array.isArray(managers)
    ? managers.find((managerItem) => managerItem.email === order.manager)
    : managers;
  const user = Array.isArray(users)
    ? users.find((userItem) => userItem.email === order.user)
    : users;

  return {
    ...(booster
      ? {
          booster: {
            description: booster.description,
            image: booster.image,
            name: booster.name,
            rating: booster.rating,
            reviews: booster.reviews,
            streamChannel: booster.streamChannel,
          },
        }
      : {}),
    category: order.category,
    channelId: order.channelId,
    details: order.details,
    game: order.game,
    ...(manager
      ? {
          manager: {
            description: manager.description,
            image: manager.image,
            name: manager.name,
            rating: manager.rating,
            reviews: manager.reviews,
          },
        }
      : {}),
    position: order.position,
    result: order.result,
    review: order.review,
    screenshots: order.screenshots,
    shortId: order.shortId,
    status: order.status,
    stream: order.stream,
    title: order.title,
    user: {
      name: user?.name || 'Anonymous',
      image: user?.image || '',
    },
  };
};

export default getPublicOrder;
